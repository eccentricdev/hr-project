import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-node-modal',
  templateUrl: './add-node-modal.component.html',
  styleUrls: ['./add-node-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddNodeModalComponent implements OnInit {
  name: string = ''
  agenId: number = 0
  positionId: number =  0
  constructor(
    public dialogRef: MatDialogRef<AddNodeModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit(): void {
    console.log(this.data)
  }

  add(){
    this.dialogRef.close({
      close: false,
      name: this.name,
      agenId: this.agenId || null,
      agenName: this.name ? this.name : this.data.agency?.find((x: any) => x?.agency_uid == this.agenId)?.agency_name_th || '',
      positionId: this.positionId || null,
      positionName: this.data.position?.find(x => x?.position_uid == this.positionId)?.position_name_th,
      noManofPowers: this.data.agency?.find((x: any) => x?.agency_uid == this.agenId)?.no_of_manpowers || 0,
      agencyCode: this.data.agency?.find((x: any) => x?.agency_uid == this.agenId)?.agency_code || 0,
      positionCode: this.data.position?.find(x => x?.position_uid == this.positionId)?.position_code,
    });
  }

  close(){
    this.dialogRef.close({close: true})
  }

}
