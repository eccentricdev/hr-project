import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-layout-search',
  templateUrl: './layout-search.component.html',
  styleUrls: ['./layout-search.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutSearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
