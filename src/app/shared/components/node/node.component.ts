import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class NodeComponent implements OnInit {
  
  @Input() node 
  @Input() show = true
  @Output() onDropSuccess = new EventEmitter<{targetNode: any,dropNode: any}>()
  @Output() onGrabNode = new EventEmitter<any>()
  @Output() targetNode = new EventEmitter<any>()
  @Output() selectNode = new EventEmitter<any>()
  @Output() onAddNode = new EventEmitter<any>()
  @Output() onDeleteNode = new EventEmitter<any>()

  constructor() { }

  ngOnInit(): void {
   
  }
  allowDrop(ev) {
    ev.preventDefault();
  }

  selectItem(node){
    this.selectNode.emit(node)
  }

  onDrop(event,targetNode){
    event.preventDefault();    
    this.targetNode.emit(targetNode)
    let data = event.dataTransfer.getData("node");
    let parseJson = JSON.parse(data)
    this.onDropSuccess.emit({
      targetNode: {...targetNode},
      dropNode: {...parseJson}
    })
  }  
  
  dragOver(event){
    event.preventDefault()
  }

  dragStart(event,node){
    event.dataTransfer.setData('node', JSON.stringify(node))
    this.onGrabNode.emit(node)
  }


  addNode(){
    this.onAddNode.emit(this.node)
  }

  deleteNode(){
    this.onDeleteNode.emit(this.node)
  }

  color(level: number){
    switch(level){
      case 1 : return '#B3147D'
      case 2 : return '#FF00A8'
      case 3 : return '#EB4CB5'
      case 4 : return '#E894CB'
      case 5 : return '#F4DBEB'
    }
    
  }




  

}
