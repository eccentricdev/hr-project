import { Component, OnInit, ChangeDetectionStrategy, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view-upload-dialog',
  templateUrl: './view-upload-dialog.component.html',
  styleUrls: ['./view-upload-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewUploadDialogComponent implements OnInit {

  url : any
  host:string = `${environment.rsuArApiName}/`
  constructor(
    private sanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<ViewUploadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    console.log(this.host+this.data);
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.host+this.data)
  }

}
