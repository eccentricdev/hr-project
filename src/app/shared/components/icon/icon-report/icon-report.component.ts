import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-report',
  templateUrl: './icon-report.component.html',
  styleUrls: ['./icon-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconReportComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
