import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconReportComponent } from './icon-report.component';

describe('IconReportComponent', () => {
  let component: IconReportComponent;
  let fixture: ComponentFixture<IconReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
