import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-search',
  templateUrl: './icon-search.component.html',
  styleUrls: ['./icon-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconSearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
