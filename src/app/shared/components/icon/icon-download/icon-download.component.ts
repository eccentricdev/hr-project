import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-download',
  templateUrl: './icon-download.component.html',
  styleUrls: ['./icon-download.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconDownloadComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
