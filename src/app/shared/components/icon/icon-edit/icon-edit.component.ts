import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-edit',
  templateUrl: './icon-edit.component.html',
  styleUrls: ['./icon-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconEditComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
