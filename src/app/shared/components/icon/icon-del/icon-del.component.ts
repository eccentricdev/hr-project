import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-del',
  templateUrl: './icon-del.component.html',
  styleUrls: ['./icon-del.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconDelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
