import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconAddInFormComponent } from './icon-add-in-form.component';

describe('IconAddInFormComponent', () => {
  let component: IconAddInFormComponent;
  let fixture: ComponentFixture<IconAddInFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconAddInFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconAddInFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
