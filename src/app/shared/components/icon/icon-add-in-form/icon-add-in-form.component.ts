import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-add-in-form',
  templateUrl: './icon-add-in-form.component.html',
  styleUrls: ['./icon-add-in-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconAddInFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
