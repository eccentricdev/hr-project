import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconAddInTableComponent } from './icon-add-in-table.component';

describe('IconAddInTableComponent', () => {
  let component: IconAddInTableComponent;
  let fixture: ComponentFixture<IconAddInTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconAddInTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconAddInTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
