import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-add-in-table',
  templateUrl: './icon-add-in-table.component.html',
  styleUrls: ['./icon-add-in-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconAddInTableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
