import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconDelInFormComponent } from './icon-del-in-form.component';

describe('IconDelInFormComponent', () => {
  let component: IconDelInFormComponent;
  let fixture: ComponentFixture<IconDelInFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconDelInFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconDelInFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
