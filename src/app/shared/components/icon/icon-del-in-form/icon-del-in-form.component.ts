import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-del-in-form',
  templateUrl: './icon-del-in-form.component.html',
  styleUrls: ['./icon-del-in-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconDelInFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
