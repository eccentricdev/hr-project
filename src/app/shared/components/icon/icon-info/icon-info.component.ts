import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-icon-info',
  templateUrl: './icon-info.component.html',
  styleUrls: ['./icon-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconInfoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
