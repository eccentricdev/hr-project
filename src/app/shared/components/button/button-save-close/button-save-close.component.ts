import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-save-close',
  templateUrl: './button-save-close.component.html',
  styleUrls: ['./button-save-close.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonSaveCloseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
