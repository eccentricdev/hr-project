import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonSaveCloseComponent } from './button-save-close.component';

describe('ButtonSaveCloseComponent', () => {
  let component: ButtonSaveCloseComponent;
  let fixture: ComponentFixture<ButtonSaveCloseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonSaveCloseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonSaveCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
