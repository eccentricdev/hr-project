import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-signature',
  templateUrl: './button-signature.component.html',
  styleUrls: ['./button-signature.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonSignatureComponent implements OnInit {
  @Input() disabled: boolean = false
  @Input() name:string = 'ลงนาม'

  constructor() { }

  ngOnInit(): void {
  }

}
