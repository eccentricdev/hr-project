import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-file-export',
  templateUrl: './button-file-export.component.html',
  styleUrls: ['./button-file-export.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonFileExportComponent implements OnInit {

  @Input() name: any = ''
  
  constructor() { }

  ngOnInit(): void {
  }

}
