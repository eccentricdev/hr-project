import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-print',
  templateUrl: './button-print.component.html',
  styleUrls: ['./button-print.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonPrintComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
