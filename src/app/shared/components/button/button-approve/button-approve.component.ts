import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-approve',
  templateUrl: './button-approve.component.html',
  styleUrls: ['./button-approve.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonApproveComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
