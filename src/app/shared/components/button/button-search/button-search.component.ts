import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-search',
  templateUrl: './button-search.component.html',
  styleUrls: ['./button-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonSearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
