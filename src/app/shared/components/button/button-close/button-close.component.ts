import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-close',
  templateUrl: './button-close.component.html',
  styleUrls: ['./button-close.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonCloseComponent implements OnInit {
  @Input() title:string = 'ยกเลิก'
  constructor() { }

  ngOnInit(): void {
  }

}
