import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-save',
  templateUrl: './button-save.component.html',
  styleUrls: ['./button-save.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonSaveComponent implements OnInit {
@Input() title:string = 'บันทึก'
@Input() width:string = 'auto'
  constructor() { }

  ngOnInit(): void {
  }

}
