import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-process',
  templateUrl: './button-process.component.html',
  styleUrls: ['./button-process.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonProcessComponent implements OnInit {

  @Input() disabled: boolean = false
  @Input() name: any = 'ประมวลผล'
  
  constructor() { }

  ngOnInit(): void {
  }

}
