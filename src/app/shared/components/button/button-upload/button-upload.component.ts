import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { UploadFilesService } from 'src/app/core/service/share/upload-files.service';

@Component({
  selector: 'app-button-upload',
  templateUrl: './button-upload.component.html',
  styleUrls: ['./button-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonUploadComponent implements OnInit {
  fileAttr: any

  // data = {
  //   file_download: "https://hr-api.71dev.com/dev/uploads/files/20210617100605_hsd2qtie.jpg",
  //   file_name: "20210617100605_hsd2qtie.jpg",
  //   file_url: "uploads/files/20210617100605_hsd2qtie.jpg",
  //   mime_type_name: "jpg",
  //   original_fileName: "business-2846221_1920.jpg"
  // }
  
  @Output() resultFile = new EventEmitter()
  constructor(public uploadFilesService: UploadFilesService,
    public appService: AppService) { }

  ngOnInit(): void {
  }

  uploadFileEvt(event) {
    console.log(event)
    const formData: FormData = new FormData();
    console.log(formData);
    formData.append('file', event.item(0), event.item(0).name)
    console.log(formData);
    // this.resultFile.emit(this.data)
    this.uploadFilesService.uploadDocument(formData).pipe(
      tap((res: any) => {
        if (res.status == 'success') {
          console.log(res);
          this.resultFile.emit(res.message[0])
          this.appService.swaltAlert('อัพโหลดสำเร็จแล้ว')
        }
      })
    ).subscribe()
  }
}
