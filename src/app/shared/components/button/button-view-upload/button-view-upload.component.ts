import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-view-upload',
  templateUrl: './button-view-upload.component.html',
  styleUrls: ['./button-view-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonViewUploadComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
