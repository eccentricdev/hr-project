import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-reset',
  templateUrl: './button-reset.component.html',
  styleUrls: ['./button-reset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonResetComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
