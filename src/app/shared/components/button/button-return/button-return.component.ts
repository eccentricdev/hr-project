import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-return',
  templateUrl: './button-return.component.html',
  styleUrls: ['./button-return.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonReturnComponent implements OnInit {
  @Input() title:string = ''
  
  constructor() { }

  ngOnInit(): void {
  }

}
