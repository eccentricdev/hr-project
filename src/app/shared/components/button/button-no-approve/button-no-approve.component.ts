import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-button-no-approve',
  templateUrl: './button-no-approve.component.html',
  styleUrls: ['./button-no-approve.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonNoApproveComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
