import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-button-add',
  templateUrl: './button-add.component.html',
  styleUrls: ['./button-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonAddComponent implements OnInit {
  @Input() disabled: boolean = false
  @Input() name: any = 'เพิ่มรายการ'
  @Input() width: any = '90'
  
  constructor() { }

  ngOnInit(): void {
  }

}
