import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-file-dialog',
  templateUrl: './upload-file-dialog.component.html',
  styleUrls: ['./upload-file-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadFileDialogComponent implements OnInit {

  filesData:any
  remark:any
  file_name:any
  // files = {
  //   file_download: "https://hr-api.71dev.com/dev/uploads/files/20210617100605_hsd2qtie.jpg",
  //   file_name: "20210617100605_hsd2qtie.jpg",
  //   file_url: "uploads/files/20210617100605_hsd2qtie.jpg",
  //   mime_type_name: "jpg",
  //   original_fileName: "business-2846221_1920.jpg"
  // }

  constructor(
    public dialogRef: MatDialogRef<UploadFileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    
    if(this.data.editForm){
      this.file_name = this.data.editForm.file_name
      this.filesData = this.data.editForm.document_name
      this.remark = this.data.remark
    }
  }

  getResultFile(file){
    console.log(file);
    this.filesData = file
  }

  save(){
    this.dialogRef.close({file:this.filesData,remark:this.remark,file_name:this.file_name})
  }

  close(){
    this.dialogRef.close({file:this.data.editForm,remark:this.remark,file_name:this.file_name})
  }

}
