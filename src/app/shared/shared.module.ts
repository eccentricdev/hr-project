import { Injectable, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, DatePipe, registerLocaleData } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatStepperModule } from '@angular/material/stepper'
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu'
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import { MatSortModule } from '@angular/material/sort';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ButtonSearchComponent } from './components/button/button-search/button-search.component';
import { ButtonResetComponent } from './components/button/button-reset/button-reset.component';
import { ButtonAddComponent } from './components/button/button-add/button-add.component';
import { ButtonSaveComponent } from './components/button/button-save/button-save.component';
import { ButtonCloseComponent } from './components/button/button-close/button-close.component';
import { ButtonUploadComponent } from './components/button/button-upload/button-upload.component';
import { IconEditComponent } from './components/icon/icon-edit/icon-edit.component';
import { IconDelComponent } from './components/icon/icon-del/icon-del.component';
import { IconSearchComponent } from './components/icon/icon-search/icon-search.component';
import { IconReportComponent } from './components/icon/icon-report/icon-report.component';
import { IconAddInTableComponent } from './components/icon/icon-add-in-table/icon-add-in-table.component';
import { IconAddInFormComponent } from './components/icon/icon-add-in-form/icon-add-in-form.component';
import { IconDelInFormComponent } from './components/icon/icon-del-in-form/icon-del-in-form.component';
import localeTh from '@angular/common/locales/th';
import { UploadFileDialogComponent } from './components/upload-file-dialog/upload-file-dialog.component';
import { ButtonViewUploadComponent } from './components/button/button-view-upload/button-view-upload.component';
import { DialogPopupComponent } from './components/dialog-popup/dialog-popup.component';
import { ButtonSaveCloseComponent } from './components/button/button-save-close/button-save-close.component';
import { NgxOrgChartModule } from 'ngx-org-chart';
import { GetDataIdPipe } from './pipes/get-data-id.pipe';
import { NumberCharacterDirective } from './directives/number-character.directive';
import { NumberDirective } from './directives/number.directive';
import { NumberCharacterEngDirective } from './directives/number-character-eng.directive';
import { CurrencyInputMaskDirective } from './directives/currency-input-mask.directive';
import { NodeComponent } from './components/node/node.component';
import { AddNodeModalComponent } from './components/add-node-modal/add-node-modal.component';
import { ZoomDirective } from './directives/zoom.directive';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { ButtonPrintComponent } from './components/button/button-print/button-print.component';
import { DateBirthdayPipe } from './pipes/date-birthday.pipe';
import { GetArrayDatasPipe } from './pipes/get-array-datas.pipe';
import { ButtonPreviewComponent } from './components/button/button-preview/button-preview.component';
import { ButtonFileExportComponent } from './components/button/button-file-export/button-file-export.component';
import { ButtonApproveComponent } from './components/button/button-approve/button-approve.component';
import { ButtonNoApproveComponent } from './components/button/button-no-approve/button-no-approve.component';
import { SerachAutoPipe } from './pipes/serach-auto.pipe';
import { ButtonReturnComponent } from './components/button/button-return/button-return.component';
import { IconInfoComponent } from './components/icon/icon-info/icon-info.component';
import { IconDownloadComponent } from './components/icon/icon-download/icon-download.component';
import { ViewUploadDialogComponent } from './components/view-upload-dialog/view-upload-dialog.component';
import { ButtonProcessComponent } from './components/button/button-process/button-process.component';
import { ButtonSignatureComponent } from './components/button/button-signature/button-signature.component';
import { ThaiDatePipe } from './pipes/thai-date.pipe';
import { LayoutSearchComponent } from './components/layout-search/layout-search.component';



registerLocaleData(localeTh)
@Injectable()
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    let monthNamesThai = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.",
        "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค"];    
    // if (displayFormat === 'input') {
      date.setHours(7)
      let day: string = date.getDate().toLocaleString()
      day = +day < 10 ? '0' + day : day;
      // let month: string = (date.getMonth() + 1).toLocaleString()
      // month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${day}/${monthNamesThai[date.getMonth()]}/${year + 543}`;
    // }
    // return date.toDateString();
  }
}


export const PICK_FORMATS = {
  parse: {
    dateInput: {
      month: 'short',
      year: 'numeric',
      day: 'numeric'
    }
  },
  display: {
      dateInput: 'input',
      monthYearLabel: {day: 'numeric', year: 'numeric', month: 'long'},
      dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
      monthYearA11yLabel: {year: 'numeric', month: 'long'}
  }
};
//  format for Moment
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY',
  },
  display: {  
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'DD-MMM-YYYY',
    dateA11yLabel: 'DD-MMM-YYYY',
    monthYearA11yLabel: 'DD-MMM-YYYY',
  },
};

const mat = [
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  MatButtonModule,
  MatToolbarModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatGridListModule,
  MatTableModule,
  MatIconModule,
  MatChipsModule,
  MatCheckboxModule,
  MatStepperModule,
  MatRadioModule,
  MatSelectModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatSnackBarModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatExpansionModule,
  MatSortModule,
  MatPaginatorModule,
  MatInputModule,
  MatTabsModule,
  MatDialogModule,
  MatSidenavModule,
  MatDividerModule,
  MatListModule,
  MatProgressSpinnerModule,
  NgxOrgChartModule,
  MaterialFileInputModule
]

@NgModule({
  declarations: [
  ButtonSearchComponent,
  ButtonResetComponent,
  ButtonAddComponent,
  ButtonSaveComponent,
  ButtonCloseComponent,
  ButtonUploadComponent,
  IconEditComponent,
  IconDelComponent,
  IconSearchComponent,
  IconReportComponent,
  IconAddInTableComponent,
  IconAddInFormComponent,
  IconDelInFormComponent,
  UploadFileDialogComponent,
  ButtonViewUploadComponent,
  DialogPopupComponent,
  ButtonSaveCloseComponent,
  GetDataIdPipe,
  NumberCharacterDirective,
  NumberDirective,
  NumberCharacterEngDirective,
  CurrencyInputMaskDirective,
  NodeComponent,
  AddNodeModalComponent,
  ZoomDirective,
  ButtonPrintComponent,
  DateBirthdayPipe,
  GetArrayDatasPipe,
  ButtonPreviewComponent,
  ButtonFileExportComponent,
  ButtonApproveComponent,
  ButtonNoApproveComponent,
  SerachAutoPipe,
  ButtonReturnComponent,
  IconInfoComponent,
  IconDownloadComponent,
  ViewUploadDialogComponent,
  ButtonProcessComponent,
  ButtonSignatureComponent,
  ThaiDatePipe,
  LayoutSearchComponent
],
  imports: [
    ...mat
  ], 
  exports: [
    ...mat,
    ButtonSearchComponent,
    ButtonResetComponent,
    ButtonAddComponent,
    ButtonSaveComponent,
    ButtonCloseComponent,
    ButtonSaveCloseComponent,
    ButtonUploadComponent,
    IconEditComponent,
    IconDelComponent,
    IconSearchComponent,
    IconReportComponent,
    IconAddInTableComponent,
    IconAddInFormComponent,
    IconDelInFormComponent,
    ButtonViewUploadComponent,
    ButtonSaveCloseComponent,
    UploadFileDialogComponent,
    GetDataIdPipe,
    GetArrayDatasPipe,
    NumberCharacterDirective,
    NumberDirective,
    NumberCharacterEngDirective,
    CurrencyInputMaskDirective,
    NodeComponent,
    AddNodeModalComponent,
    ZoomDirective,
    ButtonPrintComponent,
    DateBirthdayPipe,
    ButtonPreviewComponent,
    ButtonFileExportComponent,
    ButtonApproveComponent,
    ButtonNoApproveComponent,
    SerachAutoPipe,
    ButtonReturnComponent,
    IconInfoComponent,
    IconDownloadComponent,
    ButtonProcessComponent,
    ButtonSignatureComponent,
    ThaiDatePipe,
    LayoutSearchComponent
  ],
  entryComponents: [UploadFileDialogComponent,ViewUploadDialogComponent],
  providers:[
    DatePipe,
    GetDataIdPipe,
    { provide: LOCALE_ID, useValue: 'th-TH'},
    { provide: MAT_DATE_FORMATS, useValue: PICK_FORMATS},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}},
    { provide: DateAdapter, useClass: AppDateAdapter}
  ]
})
export class SharedModule { }
