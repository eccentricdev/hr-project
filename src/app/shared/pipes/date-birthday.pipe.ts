import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateBirthday',
  pure: true
})
export class DateBirthdayPipe implements PipeTransform {

  transform(date: any): any {
    console.log(date);
    let day: any
    if(date){
      const convertAge = new Date(date);
      const timeDiff = Math.abs(Date.now() - convertAge.getTime());
      day = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
      console.log(day);
      return day;
    }
    
  }

}
