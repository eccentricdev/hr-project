import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thaiDate',
  pure: true
})
export class ThaiDatePipe implements PipeTransform {

  transform(value: Date) {
    if(!value) return "-"

    let date = new Date(value).toLocaleDateString('th-TH', {
      year: 'numeric',
      // month: 'long',
      month: '2-digit',
      day: '2-digit',
      // day: 'numeric',
  })
  return date
  }
}
