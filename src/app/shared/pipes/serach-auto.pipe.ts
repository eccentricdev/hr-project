import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'serachAuto',
  pure: true
})
export class SerachAutoPipe implements PipeTransform {

  transform(items: any[], searchText: string , text: any): any[] {
    console.log(items);
    console.log(searchText);
    console.log(text);
    if (!items) return [];
    if (!searchText) return items;
    if (!text) return text;
    text = text.toLowerCase()
    searchText = searchText.toLowerCase();
    console.log(searchText);
    var menu = items.filter(it => {
      // console.log(it);
      return it[text].toLowerCase().includes(searchText);
    });
    if (menu.length != 0) {
      return menu
    } 
  }

}
