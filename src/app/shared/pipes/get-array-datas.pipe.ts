import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getArrayDatas',
  pure: false
})
export class GetArrayDatasPipe implements PipeTransform {

  defaultUID = null
  transform(id: any, items: Array<any>, keyArray: string, key: string, defaultDatas?: any): any{
    if (id == this.defaultUID && defaultDatas != undefined) {      
      return []
    } else if (id != undefined) {
      if(items != undefined){
        let index = items.findIndex(x => x[keyArray] == id)            
        if(index == -1){
          return defaultDatas ? defaultDatas : [];
        } else {
          return index != -1 ? items[index][key] : defaultDatas ? defaultDatas : []
        }
      }  else{
        return defaultDatas ? defaultDatas : [];
      }    
    } else {
      return defaultDatas ? defaultDatas : [];
    }
  }

}
