export const configs: {
    formatDate: string
} = {
    formatDate: 'yyyy-MM-dd'
}