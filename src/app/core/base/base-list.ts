import { Directive, Inject, Injectable, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { environment } from "src/environments/environment";

@Directive()
export class BaseList {
    isProduction = environment.production
    isHideForm: boolean = environment.hideForm
    @ViewChild(MatSort,{ static: true }) sort: MatSort;
    @ViewChild(MatPaginator,{ static: true }) paginator: MatPaginator;

    protected updateMatTable(res: any){
        // console.log(this.paginator)
        // console.log(this.sort)
        res = new MatTableDataSource(res)
        res.paginator = this.paginator
        res.sort = this.sort        
        return res
    
    }
}