import { HttpClient, HttpEventType } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface IapiResponse<T>{
    apiResponse:{
        id:number,
        desc:string,
    },
    data:Array<T>
}

export class BaseService {

    host:string = `${environment.rsuArApiName}`
    hostCommon:string = `${environment.rsuCommonApiName}`
    protected prefix:string = `${this.host}/api`
    protected prefixCommon:string = `${this.hostCommon}`
    protected fullUrl:string = ''
    protected fullUrlCommon:string = ''

    constructor(
        endpoint:string,
        protected http: HttpClient
    ) {
        this.fullUrl = this.prefix + endpoint
        this.fullUrlCommon = this.prefixCommon + endpoint
    }

    // callService<T>(action: string, data?: any[]): Observable<IapiResponse<T>> {
    //     const body = {
    //         apiRequest: {
    //             action: action
    //         }
    //         , data: data || []
    //     };
    //     return this.http.post<IapiResponse<T>>(this.fullUrl, body)
    // }
    uploadDocument<HttpHeaderResponse>(target: FormData){
        return this.http.post(this.fullUrl,target,{
            reportProgress: true,
            observe:'events'
        }).pipe(
            map((event) => {
                switch(event.type){
                    case HttpEventType.UploadProgress:
                        const progress = Math.round(100 * event.loaded / event.total);
                        return { status: 'progress', message: `${progress}` };
                    case HttpEventType.Response:
                        return { status: 'success', message:  event.body }; 
                    default:
                        return `Unhandled event: ${event.type}`;
                    }
                }
            ), 
            catchError(err => {               
                return of({ status: 'error', message:  `${err.message}` })
            })
        )
    }

    download_file(url){
        let file = `${this.host}/${url}`
        console.log(file);
        window.open(file)
    }

    getAll<T>(): Observable<T[]>{
        return this.http.get<T[]>(this.fullUrl)
    }
    getAllCommon<T>(): Observable<T[]>{
        return this.http.get<T[]>(this.fullUrlCommon)
    }

    add<T>(data: T): Observable<T> {
        return this.http.post<T>(this.fullUrl,data)
    }
    addCommon<T>(data: T): Observable<T> {
        return this.http.post<T>(this.fullUrlCommon,data)
    }
    addSubEndpoint<T>(endpoint,data): Observable<T> {
        console.log(data)
        return this.http.post<T>(this.fullUrl+endpoint,data)
    }

    getCommon<T>(id: number): Observable<T> {
        return this.http.get<T>(`${this.fullUrlCommon}/${id}`)
    }
    getSubEndpoint<T>(endpoint,id): Observable<T> {
        console.log(id)
        return this.http.get<T>(`${this.fullUrl+endpoint}/${id}`)
    }
    get<T>(id: number): Observable<T> {
        return this.http.get<T>(`${this.fullUrl}/${id}`)
    }

    update<T>(data: T): Observable<Response>{
        return this.http.put<Response>(`${this.fullUrl}`,data)
    }
    updateCommon<T>(data: T): Observable<Response>{
        return this.http.put<Response>(`${this.fullUrlCommon}`,data)
    }

    deleteData(id: number): Observable<Response> {
        return this.http.delete<Response>(`${this.fullUrl}/${id}`)
    }

    query<T>(query: string): Observable<T> {
        return this.http.get<T>(`${this.fullUrl}${query}`)
    }
    queryCommon<T>(query: string): Observable<T> {
        return this.http.get<T>(`${this.fullUrlCommon}/${query}`)
    }
}

