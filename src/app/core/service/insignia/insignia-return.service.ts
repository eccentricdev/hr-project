import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InsigniaReturnService extends BaseService {

  host:string = `${environment.rsuArApiName}/api`

  constructor(public http : HttpClient) {
    super('/insignia/insignia_returns',http);
  }

  getList(){
    return this.http.get(`${this.host}/insignia/insignia_returns/list`)
  }
}
