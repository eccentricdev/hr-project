import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class MedalRequestDocumentService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/medal_request_documents',http);
  }
}
