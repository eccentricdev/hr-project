import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ReturnReasonService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/return_reasons',http);
  }
}
