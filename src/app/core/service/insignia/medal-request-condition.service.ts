import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class MedalRequestConditionService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/medal_request_conditions',http);
  }
}
