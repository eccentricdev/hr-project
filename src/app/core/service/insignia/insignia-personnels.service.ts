import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaPersonnelsService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_personnels',http);
  }

  get_Person(data){
    return this.http.get(`${this.host}/api/insignia/insignia_personnels/in/${data}`)
  }

  get_Citizen(data){
    return this.http.get(`${this.host}/api/insignia/insignia_personnels/out/${data}`)
  }


  // //สร้างคำขอรับเครื่องราช
  // add_request(data){
  //   console.log(data);
  //   return this.http.post(`${this.host}/insignia/insignia_eligible_processings/request`,data)
  // }


}
