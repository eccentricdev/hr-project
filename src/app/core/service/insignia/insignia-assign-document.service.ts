import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaAssignDocumentService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_assign_documents',http);
  }
}
