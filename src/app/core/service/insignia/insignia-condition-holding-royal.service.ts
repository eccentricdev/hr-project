import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaConditionHoldingRoyalService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_condition_holding_royals',http);
  }
}
