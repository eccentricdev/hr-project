import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class OperatorCompareService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/operator_compares',http);
  }
}
