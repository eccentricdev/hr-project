import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaReturnDetailService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_return_details',http);
  }
}
