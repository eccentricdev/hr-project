import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InsigniaEligibleProcessingService extends BaseService {

  host:string = `${environment.rsuArApiName}/api`

  constructor(public http : HttpClient) {
    super('/insignia/insignia_eligible_processings',http);
  }

  //ดึงข้อมูลปีที่ประมวลผล
  getProcess_date(){
    return this.http.get(`${this.host}/insignia/insignia_eligible_processings/processing_date`)
  }

  //แสดงข้อมูลหน้าหลัก
  getMain_list(){
    return this.http.get(`${this.host}/insignia/insignia_eligible_processings/list`)
  }

  //ประมวลผู้มีสิทธิ์รับเครื่องราช
  add_insignia(data){
    return this.http.post(`${this.host}/insignia/insignia_eligible_processings/insignia`,data)
  }

  //ประมวลผู้มีสิทธิ์รับเหรียญตรา
  add_medal(data){
    return this.http.post(`${this.host}/insignia/insignia_eligible_processings/medal`,data)
  }

  //สร้างคำขอรับเครื่องราช
  add_request(data){
    console.log(data);
    return this.http.post(`${this.host}/insignia/insignia_eligible_processings/request`,data)
  }


  
}
