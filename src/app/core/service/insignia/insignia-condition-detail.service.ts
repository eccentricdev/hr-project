import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaConditionDetailService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_condition_details',http);
  }
}
