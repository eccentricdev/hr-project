import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InsigniaAssignService extends BaseService {

  host:string = `${environment.rsuArApiName}/api`
  
  constructor(public http : HttpClient) {
    super('/insignia/insignia_assigns',http);
  }

  getList(){
    return this.http.get(`${this.host}/insignia/insignia_assigns/list`)
  }

  getProcess(data:any){
    return this.http.get(`${this.host}/insignia/insignia_assigns/group/${data}`)
  }

  addGroup(data){
    return this.http.get(`${this.host}/insignia/insignia_assigns/save/group`,data)
  }

  updateGroup(data){
    return this.http.get(`${this.host}/insignia/insignia_assigns/save/group`,data)
  }

  get_person(data){
    return this.http.get(`${this.host}/insignia/insignia_assigns/personnel/${data}`)
    
  }
}
