import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaCategoryService extends BaseService {

  constructor(public http : HttpClient) {
    super('/insignia/insignia_categories',http);
  }
}
