import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class InsigniaRequestConditionService extends BaseService {

  constructor(public http : HttpClient,
    public formBuilder: FormBuilder,) {
    super('/insignia/insignia_request_conditions',http);
  }

  // get_insignia_condition_position(el) {
  //   return this.formBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     insignia_condition_position_uid: el.insignia_condition_position_uid,
  //     insignia_request_condition_uid: el.insignia_request_condition_uid,
  //     position_uid: el.position_uid,
  //   })
  // }
  // create_insignia_condition_position() {
  //   return this.formBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     insignia_condition_position_uid: null,
  //     insignia_request_condition_uid:  null,
  //     position_uid:  null,
    
  //   })
  // }
  
  // get_insignia_condition_position_level(el) {
  //   return this.formBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     insignia_condition_position_level_uid: el.insignia_condition_position_level_uid,
  //     insignia_request_condition_uid: el.insignia_request_condition_uid,
  //     position_level_id: el.position_level_id,
  //   })
  // }
  // create_insignia_condition_position_level() {
  //   return this.formBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     insignia_condition_position_level_uid: null,
  //     insignia_request_condition_uid:  null,
  //     position_level_id:  null,
    
  //   })
  // }

  // get_insignia_condition_position_type(el) {
  //   return this.formBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     insignia_condition_position_type_uid: el.insignia_condition_position_type_uid,
  //     insignia_request_condition_uid: el.insignia_request_condition_uid,
  //     position_type_id: el.position_type_id,
  //   })
  // }
  // create_insignia_condition_position_type() {
  //   return this.formBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     insignia_condition_position_type_uid: null,
  //     insignia_request_condition_uid:  null,
  //     position_type_id:  null,
    
  //   })
  // }
  
  // get_insignia_condition_work_group(el) {
  //   return this.formBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     insignia_condition_work_group_uid: el.insignia_condition_work_group_uid,
  //     insignia_request_condition_uid: el.insignia_request_condition_uid,
  //     work_group_id: el.work_group_id,
  //   })
  // }
  // create_insignia_condition_work_group() {
  //   return this.formBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     insignia_condition_work_group_uid: null,
  //     insignia_request_condition_uid:  null,
  //     work_group_id:  null,
      
    
  //   })
  // }
  
  // get_insignia_condition_holding_royal(el) {
  //   return this.formBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     insignia_condition_holding_royal_uid: el.insignia_condition_holding_royal_uid,
  //     insignia_request_condition_uid: el.insignia_request_condition_uid,
  //     holding_insignia_uid: el.holding_insignia_uid,
  //     holding_duration_year: el.holding_duration_year,
  //     operator_compare_id: el.operator_compare_id,
  //     salary_start: el.salary_start,
  //     salary_end: el.salary_end,
  //     can_request_insignia_uid: el.can_request_insignia_uid,
  //   })
  // }
  // create_insignia_condition_holding_royal() {
  //   return this.formBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     insignia_condition_holding_royal_uid: null,
  //     insignia_request_condition_uid:  null,
  //     holding_insignia_uid:  null,
  //     holding_duration_year:  '',
  //     operator_compare_id:  null,
  //     salary_start:  0,
  //     salary_end:  0,
  //     can_request_insignia_uid:  null,
    
  //   })
  // }
  get_insignia_condition_detail(el) {
    console.log(el)
    return this.formBuilder.group({
      status_id:el.status_id,
      created_by:el.created_by,
      created_datetime:el.created_datetime,
      updated_by:el.updated_by,
      updated_datetime:el.updated_datetime,
      search:el.search,
      insignia_condition_detail_uid:el.insignia_condition_detail_uid,
      insignia_request_condition_uid:el.insignia_request_condition_uid,
      work_group_id:el.work_group_id,
      position_type_id:el.position_type_id,
      position_level_id:el.position_level_id,
      position_uid:el.position_uid,
      work_at_position_year:el.work_at_position_year,
      operator_compare_work_id:el.operator_compare_work_id,
      holding_insignia_uid:el.holding_insignia_uid,
      can_request_insignia_uid:el.can_request_insignia_uid,
      operator_compare_holder_id:el.operator_compare_holder_id,
      holding_year:el.holding_year,
      salary_min_amount:el.salary_min_amount,
      salary_max_amount:el.salary_max_amount,
      request_at_retire_year:el.request_at_retire_year,
    })
  }
  create_insignia_condition_detail() {
    return this.formBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      search: [''],
      insignia_condition_detail_uid: [null],
      insignia_request_condition_uid: [null],
      work_group_id: [null],
      position_type_id: [null],
      position_level_id: [null],
      position_uid: [null],
      work_at_position_year: [null],
      operator_compare_work_id: [null],
      holding_insignia_uid: [null],
      can_request_insignia_uid: [null],
      operator_compare_holder_id: [null],
      holding_year: [null],
      salary_min_amount: [null],
      salary_max_amount: [null],
      request_at_retire_year: true,
    
    })
  }
}
