import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InsigniaRequestService extends BaseService {

  host:string = `${environment.rsuArApiName}/api`

  constructor(public http : HttpClient) {
    super('/insignia/insignia_requests',http);
  }

  getListRequest(){
    return this.http.get(`${this.host}/insignia/insignia_requests/list`)
  }

  get_assing(data){
    console.log(data);
    return this.http.post(`${this.host}/insignia/insignia_requests/assign`,data)
  }

  add_group(data){
    console.log(data);
    return this.http.post(`${this.host}/insignia/insignia_requests/assign/group`,data)
  }

  //ดึงข้อมูลปีที่ประมวลผล
  get_date(data){
    return this.http.get(`${this.host}/insignia/insignia_requests/group//${data}`)
  }





}
