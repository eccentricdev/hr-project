import { BaseService } from './../../base/base-service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface Organization {
  organization_charts: Organization[];
  search?: any;
  organization_chart_uid: string;
  parent_organization_chart_uid: string;
  organization_chart_type_id: number;
  level: number;
  organization_chart_code: string;
  organization_chart_name: string;
  version_uid?: any;
  childs?:[]
}
@Injectable({
  providedIn: 'root'
})
export class OrganizationService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/organization/organization_charts',http)
  }
}
