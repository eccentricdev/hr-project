import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class WorkChartHeaderService extends BaseService {

  constructor(public http: HttpClient) {
    super('/v1/organization/work_chart_headers',http);
  }
}
