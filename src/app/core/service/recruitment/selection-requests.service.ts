import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SelectionRequestsService extends BaseService {
  
  public resultId$ = new BehaviorSubject<any>(null);

  constructor(public http : HttpClient) {
    super('/v1/recruitment/selection_requests',http);
  }

  nextState(value){
    console.log(value);
    this.resultId$.next(value)
  }

  getResultId(){
    return  this.resultId$.asObservable()
  }
}
