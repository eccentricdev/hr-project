import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SelectionResultsService extends BaseService {

  public resultType$ = new BehaviorSubject<any>(null);

  constructor(public http : HttpClient) {
    super('/v1/recruitment/selection_results',http);
  }

  nextState(value){
    console.log(value);
    this.resultType$.next(value)
  }

  getResultType(){
    return  this.resultType$.asObservable()
  }

  
}
