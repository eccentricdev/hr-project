import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class EmploymentContractsService extends BaseService {
  
  public resultId$ = new BehaviorSubject<any>(null);

  constructor(public http : HttpClient) {
    super('/v1/recruitment/employment_contracts',http);
  }

  nextState(value){
    console.log(value);
    this.resultId$.next(value)
  }

  getResultId(){
    return  this.resultId$.asObservable()
  }
  
}
