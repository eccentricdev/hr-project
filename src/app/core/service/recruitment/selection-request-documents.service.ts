import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SelectionRequestDocumentsService extends BaseService {

  constructor(public http : HttpClient) {
    super('/recruitment/selection_request_documents',http);
  }
}
