import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ApplicantService extends BaseService {

  public page$ = new BehaviorSubject<any>(null);

  constructor(public  http : HttpClient,
    public fb: FormBuilder, ) {
    super('/v1/recruitment/applicants',http);
  }

  nextState(value){
    console.log(value);
    this.page$.next(value)
  }

  getResultPage(){
    return  this.page$.asObservable()
  }

  arrayDoc(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.fb.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        applicant_document_uid: null,
        applicant_uid: null,
        document_id: null,
        file_name: '',
        document_url: '',
        mime_type_name: '',
      })
    } else {
      return this.fb.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        applicant_document_uid: el.applicant_document_uid,
        applicant_uid: el.applicant_uid,
        document_id: el.document_id,
        file_name: el.file_name,
        document_url: el.document_url,
        mime_type_name: el.mime_type_name,
      })
    }
  }

  addresses_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        applicant_address_uid: [null],
        applicant_uid: [null],
        address_uid:[null],
        address_type_id: [null],
          address : this.fb.group({
            address_uid:[null],
            address_name:[''],
            house_no:[''],
            village_no:[''],
            building_name:[''],
            building:[''],
            room_no:[''],
            floor_no:[''],
            village_name:[''],
            alley_name:[''],
            street_name:[''],
            sub_district_id:[null],
            other_sub_district_name_th:[''],
            other_sub_district_name_en:[''],
            other_district_name_th:[''],
            other_district_name_en:[''],
            other_province_name_th:[''],
            other_province_name_en:[''],
            postal_code:[''],
            country_id:[null],
            telephone_no:[''],
          })
      })
    } else {
      return this.fb.group({
        applicant_address_uid: element.applicant_address_uid,
        applicant_uid: element.applicant_uid,
        address_uid: element.address_uid,
        address_type_id: element.address_type_id,
          address : this.fb.group({
  
            address_uid:[element.address.address_uid],
            address_name:[element.address.address_name],
            house_no:[element.address.house_no],
            village_no:[element.address.village_no],
            building_name:[element.address.building_name],
            room_no:[element.address.room_no],
            floor_no:[element.address.floor_no],
            village_name:[element.address.village_name],
            alley_name:[element.address.alley_name],
            street_name:[element.address.street_name],
            sub_district_id:[element.address.sub_district_id],
            other_sub_district_name_th:[element.address.other_sub_district_name_th],
            other_sub_district_name_en:[element.address.other_sub_district_name_en],
            other_district_name_th:[element.address.other_district_name_th],
            other_district_name_en:[element.address.other_district_name_en],
            other_province_name_th:[element.address.other_province_name_th],
            other_province_name_en:[element.address.other_province_name_en],
            postal_code:[element.address.postal_code],
            country_id:[element.address.country_id],
            telephone_no:[element.address.telephone_no],
          })
        })
    }
  }
}
