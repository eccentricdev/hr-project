import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SelectionAssignApplicantsService extends BaseService {

  
  
  constructor(public http : HttpClient) {
    super('/v1/recruitment/selection_assign_applicants',http);
  }

  
  
}
