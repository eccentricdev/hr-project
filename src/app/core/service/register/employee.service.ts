import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseService } from '../../base/base-service';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends BaseService {

  constructor(
  public fb: FormBuilder,  
  public http:HttpClient
  ) { 
    super('/v1/profile/personnels',http)
  }


  
  personnel_contacts_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        personnel_contact_uid: [null],
        personnel_uid: [null],
        contact_uid: [null],
        contact_type_id: [null],
        contact : this.fb.group({
          contact_uid:[null],
          contact_name:[null],
          address_uid:[null],
          address : this.fb.group({
            address_uid:[null],
            address_name:[''],
            house_no:[''],
            village_no:[''],
            building_name:[''],
            room_no:[''],
            floor_no:[''],
            village_name:[''],
            alley_name:[''],
            street_name:[''],
            sub_district_id:[null],
            other_sub_district_name_th:[''],
            other_sub_district_name_en:[''],
            other_district_name_th:[''],
            other_district_name_en:[''],
            other_province_name_th:[''],
            other_province_name_en:[''],
            postal_code:[''],
            country_id:[null],
            telephone_no:[''],
          })
        })
      })
    } else {
      return this.fb.group({
        personnel_contact_uid: element.personnel_contact_uid,
        personnel_uid: element.personnel_uid,
        contact_uid: element.contact_uid,
        contact_type_id: element.contact_type_id,
        contact : this.fb.group({
          contact_uid:[element.contact.contact_uid],
          contact_name:[element.contact.contact_name],
          address_uid:[element.contact.address_uid],
          address : this.fb.group({
            address_uid:[element.contact.address.address_uid],
            address_name:[element.contact.address.address_name],
            house_no:[element.contact.address.house_no],
            village_no:[element.contact.address.village_no],
            building_name:[element.contact.address.building_name],
            room_no:[element.contact.address.room_no],
            floor_no:[element.contact.address.floor_no],
            village_name:[element.contact.address.village_name],
            alley_name:[element.contact.address.alley_name],
            street_name:[element.contact.address.street_name],
            sub_district_id:[element.contact.address.sub_district_id],
            other_sub_district_name_th:[element.contact.address.other_sub_district_name_th],
            other_sub_district_name_en:[element.contact.address.other_sub_district_name_en],
            other_district_name_th:[element.contact.address.other_district_name_th],
            other_district_name_en:[element.contact.address.other_district_name_en],
            other_province_name_th:[element.contact.address.other_province_name_th],
            other_province_name_en:[element.contact.address.other_province_name_en],
            postal_code:[element.contact.address.postal_code],
            country_id:[element.contact.address.country_id],
            telephone_no:[element.contact.address.telephone_no],
          })
        })
      })
    }
  }
  personnel_addresses_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        personnel_address_uid: [null],
        personnel_uid: [null],
        address_uid: [null],
        address_type_id: [null],
          address : this.fb.group({
            address_uid:[null],
            address_name:[''],
            house_no:[''],
            village_no:[''],
            building_name:[''],
            building:[''],
            room_no:[''],
            floor_no:[''],
            village_name:[''],
            alley_name:[''],
            street_name:[''],
            sub_district_id:[null],
            other_sub_district_name_th:[''],
            other_sub_district_name_en:[''],
            other_district_name_th:[''],
            other_district_name_en:[''],
            other_province_name_th:[''],
            other_province_name_en:[''],
            postal_code:[''],
            country_id:[null],
            telephone_no:[''],
          })
      })
    } else {
      return this.fb.group({
        personnel_address_uid: element.personnel_address_uid,
        personnel_uid: element.personnel_uid,
        address_uid: element.address_uid,
        address_type_id: element.address_type_id,
          address : this.fb.group({
            address_uid:[element.address.address_uid],
            address_name:[element.address.address_name],
            house_no:[element.address.house_no],
            village_no:[element.address.village_no],
            building_name:[element.address.building_name],
            room_no:[element.address.room_no],
            floor_no:[element.address.floor_no],
            village_name:[element.address.village_name],
            alley_name:[element.address.alley_name],
            street_name:[element.address.street_name],
            sub_district_id:[element.address.sub_district_id],
            other_sub_district_name_th:[element.address.other_sub_district_name_th],
            other_sub_district_name_en:[element.address.other_sub_district_name_en],
            other_district_name_th:[element.address.other_district_name_th],
            other_district_name_en:[element.address.other_district_name_en],
            other_province_name_th:[element.address.other_province_name_th],
            other_province_name_en:[element.address.other_province_name_en],
            postal_code:[element.address.postal_code],
            country_id:[element.address.country_id],
            telephone_no:[element.address.telephone_no],
          })
        })
    }
  }

  personnel_families_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        personnel_family_uid: [null],
        personnel_uid: [null],
        family_id: [null],
        relationship_id: [null],
          family : this.fb.group({
            created_by:[null],
            created_datetime:[null],
            update_by:[null],
            update_datetime:[null],
            updated_by:[null],
            updated_datetime:[null],
            family_id:[null],
            personnel_uid:[null],
            card_no:[''],
            prefix_id:[null],
            other_prefix_name:[''],
            first_name_th:[''],
            middle_name_th:[''],
            last_name_th:[''],
            first_name_en:[''],
            middle_name_en:[''],
            last_name_en:[''],
            nationality_id:[null],
            religion_id:[null],
            occupation_id:[null],
            living_status_id:[null],
            marital_status_id:[null],
            remark:[''],
            maiden_name:[''],
            row_order:[null],
            child_number:[0],
            is_child_study:[true],
           
          })
      })
    } else {
      return this.fb.group({
        personnel_family_uid: element.personnel_family_uid,
        personnel_uid: element.personnel_uid,
        family_id: element.family_id,
        relationship_id: element.relationship_id,
          family : this.fb.group({
            created_by:[element.family.created_by],
            created_datetime:[element.family.created_datetime],
            update_by:[element.family.update_by],
            update_datetime:[element.family.update_datetime],
            updated_by:[element.family.updated_by],
            updated_datetime:[element.family.updated_datetime],
            family_id:[element.family.family_id],
            personnel_uid:[element.family.personnel_uid],
            card_no:[element.family.card_no],
            prefix_id:[element.family.prefix_id],
            other_prefix_name:[element.family.other_prefix_name],
            first_name_th:[element.family.first_name_th],
            middle_name_th:[element.family.middle_name_th],
            last_name_th:[element.family.last_name_th],
            first_name_en:[element.family.first_name_en],
            middle_name_en:[element.family.middle_name_en],
            last_name_en:[element.family.last_name_en],
            nationality_id:[element.family.nationality_id],
            religion_id:[element.family.religion_id],
            occupation_id:[element.family.occupation_id],
            living_status_id:[element.family.living_status_id],
            marital_status_id:[element.family.marital_status_id],
            remark:[element.family.remark],
            maiden_name:[element.family.maiden_name],
            row_order:[element.family.row_order],
            child_number:[element.family.child_number],
            is_child_study:[element.family.is_child_study],
           
          })
        })
    }
  }
  educations_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [null],
        created_datetime: [null],
        update_by: [null],
        update_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        personnel_education_uid: [null],
        personnel_uid: [null],
        education_level_id: [null],
        quali_id: [null],
        field_name: [''],
        other_institute_name: [''],
        country_id: [null],
        country_name: [null],
        start_year: [null],
        finish_year: [null],
        grade: [null],
        edu_highest: [true],
        edu_employ: [true],
        start_date: [null],
        finish_date: [null],
        row_order: [null],
      })
    } else {
      return this.fb.group({
        status_id: element.is_deleted,
        created_by: element.created_by,
        created_datetime: element.created_datetime,
        update_by: element.update_by,
        updated_by: element.updated_by,
        updated_datetime: element.updated_datetime,
        personnel_education_uid: element.personnel_education_uid,
        personnel_uid: element.personnel_uid,
        education_level_id: element.education_level_id,
        quali_id: element.quali_id,
        field_name: element.field_name,
        other_institute_name: element.other_institute_name,
        country_id: element.country_id,
        country_name: element.country_name,
        start_year: element.start_year,
        finish_year: element.finish_year,
        grade: element.grade,
        edu_highest: element.edu_highest,
        edu_employ: element.edu_employ,
        start_date: element.start_date,
        finish_date: element.finish_date,
        row_order: element.row_order,
        })
    }
  }

  working_histories_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        created_by: [null],
        created_datetime: [null],
        update_by: [null],
        update_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        working_history_uid: [null],
        personnel_uid: [null],
        work_place_name: [''],
        job_position_name: [''],
        job_description: [''],
        final_salary_amount: [0],
        start_date: [null],
        end_date: [null],
        experience_income_amount: [null],
        profession_income_amount: [null],
        other_income_amount: [null],
        remark: [''],

      })
    } else {
      return this.fb.group({
        created_by: element.created_by,
        created_datetime: element.created_datetime,
        update_by: element.update_by,
        updated_by: element.updated_by,
        updated_datetime: element.updated_datetime,
        working_history_uid: element.working_history_uid,
        personnel_uid: element.personnel_uid,
        work_place_name: element.work_place_name,
        job_position_name: element.job_position_name,
        job_description: element.job_description,
        final_salary_amount: element.final_salary_amount,
        start_date: element.start_date,
        end_date: element.end_date,
        experience_income_amount: element.experience_income_amount,
        profession_income_amount: element.profession_income_amount,
        other_income_amount: element.other_income_amount,
        remark: element.remark,
        
        })
    }
  }

  working_records_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        created_by: [null],
        created_datetime: [null],
        update_by: [null],
        update_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        working_record_uid: [null],
        personnel_uid: [null],
        working_type_id: [null],
        level: [null],
        subject_name: [''],
        place_name: [''],
        position_name: [''],
        detail: [''],
        start_date: [null],
        end_date: [null],
        remark: [''],
      })
    } else {
      return this.fb.group({
        created_by: element.created_by,
        created_datetime: element.created_datetime,
        update_by: element.update_by,
        updated_by: element.updated_by,
        updated_datetime: element.updated_datetime,
        working_record_uid: element.working_record_uid,
        personnel_uid: element.personnel_uid,
        working_type_id: element.working_type_id,
        level: element.level,
        subject_name: element.subject_name,
        place_name: element.place_name,
        position_name: element.position_name,
        detail: element.detail,
        start_date: element.start_date,
        end_date: element.end_date,
        remark: element.remark,
        })
    }
  }
  name_change_histories_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        name_change_history_uid: [null],
        personnel_uid: [null],
        prefix_id: [null],
        other_prefix_name: [''],
        first_name_th: [''],
        middle_name_th: [''],
        last_name_th: [''],
        first_name_en: [''],
        middle_name_en: [''],
        last_name_en: [''],
        change_date: [null]
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        name_change_history_uid:element.name_change_history_uid,
        personnel_uid:element.personnel_uid,
        prefix_id:element.prefix_id,
        other_prefix_name:element.other_prefix_name,
        first_name_th:element.first_name_th,
        middle_name_th:element.middle_name_th,
        last_name_th:element.last_name_th,
        first_name_en:element.first_name_en,
        middle_name_en:element.middle_name_en,
        last_name_en:element.last_name_en,
        change_date:element.change_date
        })
    }
  }

  asset_holdings_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        asset_holding_uid: [null],
        personnel_uid: [null],
        asset_id: [null],
        asset_code: [''],
        asset_name: [''],
        receive_date: [null],
        return_date: [null],
        remark: ['']
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        asset_holding_uid:element.asset_holding_uid,
        personnel_uid:element.personnel_uid,
        asset_id:element.asset_id,
        asset_code:element.asset_code,
        asset_name:element.asset_name,
        receive_date:element.receive_date,
        return_date:element.return_date,
        remark:element.remark
        })
    }
  }

  personnel_awards_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_award_uid: [null],
        personnel_uid: [null],
        award_name: [''],
        award_by: [''],
        award_type_id: [null],
        receive_date: [null],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_award_uid:element.personnel_award_uid,
        personnel_uid:element.personnel_uid,
        award_name:element.award_name,
        award_by:element.award_by,
        award_type_id:element.award_type_id,
        receive_date:element.receive_date,
        })
    }
  }
 
  personnel_skills_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_skill_uid: [null],
        personnel_uid: [null],
        skill_name: [''],
        remark: [''],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_skill_uid:element.personnel_skill_uid,
        personnel_uid:element.personnel_uid,
        skill_name:element.skill_name,
        remark:element.remark,
        })
    }
  }
 
  personnel_trainings_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_training_uid: [null],
        personnel_uid: [null],
        from_date: [null],
        to_date: [null],
        course_name: [''],
        training_location: [''],
        training_type_id: [null],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_training_uid:element.personnel_training_uid,
        personnel_uid:element.personnel_uid,
        from_date:element.from_date,
        to_date:element.to_date,
        course_name:element.course_name,
        training_location:element.training_location,
        training_type_id:element.training_type_id,
        })
    }
  }

  personnel_licenses_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_license_uid: [null],
        personnel_uid: [null],
        license_no: [''],
        license_name: [''],
        license_by: [''],
        issue_date: [null],
        expiry_date: [null],
        is_never_expire: [true],
        personnel_license_documents: this.fb.array([]),
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_license_uid:element.personnel_license_uid,
        personnel_uid:element.personnel_uid,
        license_no:element.license_no,
        license_name:element.license_name,
        license_by:element.license_by,
        issue_date:element.issue_date,
        expiry_date:element.expiry_date,
        is_never_expire:element.is_never_expire,
        personnel_license_documents: this.fb.array([]),
        })
    }
  }
  personnel_license_documents_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_license_document_uid: [null],
        personnel_license_uid: [null],
        document_name: [''],
        document_url: [''],
        row_no: [''],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_license_document_uid:element.personnel_license_document_uid,
        personnel_license_uid:element.personnel_license_uid,
        document_name:element.document_name,
        document_url:element.document_url,
        row_no:element.row_no,
        })
    }
  }
  personnel_hires_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_hire_uid: [null],
        personnel_uid: [null],
        salary_amount: [null],
        personnel_group_id: [null],
        personnel_type_id: [null],
        personnel_sub_type_id: [null],
        work_type_id: [null],
        work_field_id: [null],
        work_group_id: [null],
        expertise_id: [null],
        subject_group_type_id: [null],
        subject_category_type_id: [null],
        teaching_subject_name: [''],
        start_date: [null],
        position_movement_type_id: [null],
        position_movement_date: [null],
        hire_date: [null],
        hire_no: [''],
        hire_order_date: [null],
        transfer_date: [null],
        work_shift_id: [null],
        work_status_id: [0],
        organization_uid: [null],
        organization_chart_header_uid: [null],
        agency_uid: [null],
        position_uid: [null],
        position_level_id: [null],
        academic_position_id: [null],
        teach_level_id: [null],
        academic_position_subject: [null],
        academic_position_subject_name: [''],
        work_expertise_name: [''],
        contract_type_id: [null],
        contract_time_id: [null],
        manpower_uid: [null],
        position_type_id: [null],
        // organization : this.fb.group({
        //   status_id: [null],
        //   created_by: [''],
        //   created_datetime: [null],
        //   updated_by: [''],
        //   updated_datetime: [null],
        //   organization_uid: [null],
        //   organization_code: [''],
        //   organization_name_en: [''],
        //   organization_name_th: [''],
        //   organization_short_name_en: [''],
        //   organization_short_name_th: [''],
        //   branch_name: [''],
        //   establish_date: [null],
        //   address: [''],
        //   address_no: [''],
        //   moo: [''],
        //   building_name: [''],
        //   room_no: [''],
        //   floor_no: [''],
        //   village_name: [''],
        //   soi_name: [''],
        //   road_name: [''],
        //   sub_district_id: [null],
        //   postal_code: [''],
        //   phone_no: [''],
        //   fax_no: ['']
        // }),
        // position : this.fb.group({
        //   status_id: [null],
        //   created_by: [''],
        //   created_datetime: [null],
        //   updated_by: [''],
        //   updated_datetime: [null],
        //   position_uid: [null],
        //   position_code: [''],
        //   position_name_en: [''],
        //   position_name_th: [''],
        //   position_short_name_en: [''],
        //   position_short_name_th: [''],
        //   work_detail: [''],
        //   remark: [''],
        //   effective_date: [null],
        //   end_date: [null]
        // }),
        // work_status : this.fb.group({
        //   status_id: [null],
        //   created_by: [''],
        //   created_datetime: [null],
        //   updated_by: [''],
        //   updated_datetime: [null],
        //   work_status_id: [null],
        //   work_status_code: [''],
        //   work_status_name_th: [''],
        //   work_status_name_en: ['']
        // }),
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_hire_uid:element.personnel_hire_uid,
        personnel_uid:element.personnel_uid,
        salary_amount:element.salary_amount,
        personnel_group_id:element.personnel_group_id,
        personnel_type_id:element.personnel_type_id,
        personnel_sub_type_id:element.personnel_sub_type_id,
        work_type_id:element.work_type_id,
        work_field_id:element.work_field_id,
        work_group_id:element.work_group_id,
        expertise_id:element.expertise_id,
        subject_group_type_id:element.subject_group_type_id,
        subject_category_type_id:element.subject_category_type_id,
        teaching_subject_name:element.teaching_subject_name,
        start_date:element.start_date,
        position_movement_type_id:element.position_movement_type_id,
        position_movement_date:element.position_movement_date,
        hire_date:element.hire_date,
        hire_no:element.hire_no,
        hire_order_date:element.hire_order_date,
        transfer_date:element.transfer_date,
        work_shift_id:element.work_shift_id,
        work_status_id:element.work_status_id,
        organization_chart_header_uid:element.organization_chart_header_uid,
        agency_uid:element.agency_uid,
        position_uid:element.position_uid,
        position_level_id:element.position_level_id,
        academic_position_id:element.academic_position_id,
        teach_level_id:element.teach_level_id,
        academic_position_subject:element.academic_position_subject,
        academic_position_subject_name:element.academic_position_subject_name,
        work_expertise_name:element.work_expertise_name,
        contract_type_id:element.contract_type_id,
        contract_time_id:element.contract_time_id,
        manpower_uid:element.manpower_uid,
        position_type_id:element.position_type_id,
        // organization : this.fb.group({
        //   status_id:element.organization?.status_id,
        //   created_by:element.organization?.created_by,
        //   created_datetime:element.organization?.created_datetime,
        //   updated_by:element.organization?.updated_by,
        //   updated_datetime:element.organization?.updated_datetime,
        //   organization_uid:element.organization?.organization_uid,
        //   organization_code:element.organization?.organization_code,
        //   organization_name_en:element.organization?.organization_name_en,
        //   organization_name_th:element.organization?.organization_name_th,
        //   organization_short_name_en:element.organization?.organization_short_name_en,
        //   organization_short_name_th:element.organization?.organization_short_name_th,
        //   branch_name:element.organization?.branch_name,
        //   establish_date:element.organization?.establish_date,
        //   address:element.organization?.address,
        //   address_no:element.organization?.address_no,
        //   moo:element.organization?.moo,
        //   building_name:element.organization?.building_name,
        //   room_no:element.organization?.room_no,
        //   floor_no:element.organization?.floor_no,
        //   village_name:element.organization?.village_name,
        //   soi_name:element.organization?.soi_name,
        //   road_name:element.organization?.road_name,
        //   sub_district_id:element.organization?.sub_district_id,
        //   postal_code:element.organization?.postal_code,
        //   phone_no:element.organization?.phone_no,
        //   fax_no:element.organization?.fax_no
         
        // }),
        // position : this.fb.group({
        //   status_id: element.position?.status_id,
        //   created_by: element.position?.created_by,
        //   created_datetime: element.position?.created_datetime,
        //   updated_by: element.position?.updated_by,
        //   updated_datetime: element.position?.updated_datetime,
        //   position_uid: element.position?.position_uid,
        //   position_code: element.position?.position_code,
        //   position_name_en: element.position?.position_name_en,
        //   position_name_th: element.position?.position_name_th,
        //   position_short_name_en: element.position?.position_short_name_en,
        //   position_short_name_th: element.position?.position_short_name_th,
        //   work_detail: element.position?.work_detail,
        //   remark: element.position?.remark,
        //   effective_date: element.position?.effective_date,
        //   end_date: element.position?.end_date
        // }),
        // work_status : this.fb.group({
        //   status_id:element.work_status?.status_id,
        //   created_by:element.work_status?.created_by,
        //   created_datetime:element.work_status?.created_datetime,
        //   updated_by:element.work_status?.updated_by,
        //   updated_datetime:element.work_status?.updated_datetime,
        //   work_status_id:element.work_status?.work_status_id,
        //   work_status_code:element.work_status?.work_status_code,
        //   work_status_name_th:element.work_status?.work_status_name_th,
        //   work_status_name_en:element.work_status?.work_status_name_en,
        // }),
        })
    }
  }

  personnel_academic_works_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_academic_work_uid: [null],
        personnel_uid: [null],
        academic_work_type_id: [null],
        academic_work_title_name: [''],
        journal_name: [''],
        reference_url: [null],
        academic_work_description: [''],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_academic_work_uid:element.personnel_academic_work_uid,
        personnel_uid:element.personnel_uid,
        academic_work_type_id:element.academic_work_type_id,
        academic_work_title_name:element.academic_work_title_name,
        journal_name:element.journal_name,
        reference_url:element.reference_url,
        academic_work_description:element.academic_work_description,
       
        })
    }
  }

  personnel_executive_position_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_executive_position_uid: [null],
        personnel_uid: [null],
        position_uid: [null],
        is_deputy: [true],
        salary_amount: [0],
        start_date: [null],
        end_date: [null],
        remark: [null],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_executive_position_uid:element.personnel_executive_position_uid,
        personnel_uid:element.personnel_uid,
        position_uid:element.position_uid,
        is_deputy:element.is_deputy,
        salary_amount:element.salary_amount,
        start_date:element.start_date,
        end_date:element.end_date,
        remark:element.remark,
       
        })
    }
  }

  personnel_position_history_form(element?) {
    console.log('array', element)
    if (element == undefined) {
      return this.fb.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_position_history_uid: [null],
        personnel_uid: [null],
        position_uid: [null],
        old_salary_amount: [0],
        effective_date: [null],
        position_movement_type_id: [null],
        remark: [null],
      })
    } else {
      return this.fb.group({
        status_id:element.status_id,
        created_by:element.created_by,
        created_datetime:element.created_datetime,
        updated_by:element.updated_by,
        updated_datetime:element.updated_datetime,
        personnel_position_history_uid:element.personnel_position_history_uid,
        personnel_uid:element.personnel_uid,
        position_uid:element.position_uid,
        old_salary_amount:element.old_salary_amount,
        effective_date:element.effective_date,
        position_movement_type_id:element.position_movement_type_id,
        remark:element.remark,
       
        })
    }
  }

}
