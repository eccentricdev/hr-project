import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { profile } from 'console';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class PersonnelSalaryAdjustmentsService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/profile/position_salary_adjustments',http)
  }
}
