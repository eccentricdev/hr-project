import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public user$  = new BehaviorSubject(null)
  public pass$  = new BehaviorSubject(null)
  constructor() { }

  swaltAlert(text:string = 'บันทึกสำเร็จ') {
    swal.fire({
      text: text,
      icon: 'success',
    })
  }
  
  swaltAlertError(text:string) {
    swal.fire({
      text: text,
      icon: 'error',
    })
  }

  delete() {
    return new Promise((resolve, reject) => {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        resolve(result.isConfirmed)
      })
    })
  }
}
