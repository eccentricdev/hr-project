import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class DisciplineApproveStatussService extends BaseService {

  constructor(public http : HttpClient) {
    super('/v1/discipline/discipline_approve_statuss',http);
  }
}
