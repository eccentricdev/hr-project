import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DisciplineApproveResultService extends BaseService {

  constructor(public http : HttpClient) {
    super('/discipline/discipline_approve_results',http);
  }

  // getDA_resultAll(){
  //   return this.http.get(`${environment.rsuArApiName}/api/discipline/discipline_approve_results`)
  // }

}
