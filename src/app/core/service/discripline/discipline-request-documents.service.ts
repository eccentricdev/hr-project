import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class DisciplineRequestDocumentsService extends BaseService {

  constructor(public http : HttpClient) {
    super('/v1/discipline​/discipline_request_documents',http);
  }
}
