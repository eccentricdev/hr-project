import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class AppealRequestCommitteesService extends BaseService {

  constructor(public http : HttpClient) {
    super('/v1/discipline/appeal_request_committees',http);
  }
}

