import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ReligionService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/common/religions',http)
  }
}
