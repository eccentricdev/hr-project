import { TestBed } from '@angular/core/testing';

import { PositionTypesService } from './position-types.service';

describe('PositionTypesService', () => {
  let service: PositionTypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PositionTypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
