import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SubjectGroupTypesService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/common/subject_group_types',http)
  }
}