import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

export interface prefix {
  search?: any;
  prefix_id: 0,
  prefix_code: string,
  prefix_name_th: string,
  prefix_name_en: string,
  prefix_short_name_th: string,
  prefix_short_name_en: string,
  status_id: 0,
  display_priority: 0,
  is_common: true,
  create_by: string,
  create_datetime: string,
  update_by: string,
  update_datetime: string,
}

@Injectable({
  providedIn: 'root'
})
export class PrefixService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/common/prefixs',http)
  }
}
