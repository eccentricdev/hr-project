import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class SocialSecurityService extends BaseService {

  constructor(
  public http:HttpClient
  ) { 
    super('/v1/salary/social_securitys',http)
  }
}
