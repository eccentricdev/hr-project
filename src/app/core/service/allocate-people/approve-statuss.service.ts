import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseForm } from '../../base/base-form';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ApproveStatussService extends BaseService {

  constructor(public http : HttpClient) {
    super('/v1/common/approve_statuss',http);
  }
}
