import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ManpowerAssignmentsService extends BaseService {

  public resultType$ = new BehaviorSubject<any>(null);
  
  constructor(public http : HttpClient) {
    super('/v1/manpower/manpower_assignments',http);
  }

  nextState(value){
    console.log(value);
    this.resultType$.next(value)
  }

  getResultType(){
    return  this.resultType$.asObservable()
  }

  
}
