import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ManpowerAssignmentDocumentsService extends BaseService {

  constructor(public http : HttpClient) {
    super('/manpower/manpower_assignment_documents',http);
  }
}
