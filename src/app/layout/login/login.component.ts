import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  user : any 
  password : any
  constructor(
    private router: Router,
    public dialog: MatDialog,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }

  authen(){
    
    if(this.user == 'admin' && this.password == '1234'){
      this.router.navigate(['/app/home_menu'])
      this.AppService.user$.next(this.user)
      this.AppService.pass$.next(this.password)
    }else{
      // this.AppService.swaltAlertError('รหัสผิดพลาด')
      this.router.navigate(['/app/home_menu'])
      this.AppService.user$.next(null)
      this.AppService.pass$.next(null)
    }
    
  }
}
