import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { timer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit {
  @Output() onToggleMenu = new EventEmitter<boolean>()
  isExpandMenu: boolean = true
  user : any = null
  constructor(
    // public odic: OidsAuthService,
    public AppService: AppService,
    
  ) { }

  ngOnInit(): void {
    // console.log(this.odic.user)
    timer(800, 800).pipe(
      ).subscribe(t => {
        this.AppService.user$.pipe(
          tap(x=>this.user = x),
          // tap(x=>console.log(x)),
        ).subscribe()
      });
    }
  windowsSlid(){
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }
  toggleMenu(){
    this.isExpandMenu = !this.isExpandMenu
    this.onToggleMenu.emit(this.isExpandMenu)
  }
}
