import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {
  defaultFontSize = 16
  themeClass: string;
  isDarkTheme: boolean = true
  constructor(
    public overlayContainer: OverlayContainer,
    private cdRef: ChangeDetectorRef,
    private router: Router
  ) {    
  }

  ngOnInit(): void {
    // this.toogleTheme()
  }

  toogleTheme(){
      
      let newThemeClass = this.isDarkTheme ? 'dark-theme' : 'default-theme'
      this.isDarkTheme = !this.isDarkTheme
      
      const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
      const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('-theme'));
      if (themeClassesToRemove.length) {
         overlayContainerClasses.remove(...themeClassesToRemove);
         document.body.classList.remove(...themeClassesToRemove)
      }
      overlayContainerClasses.add(newThemeClass);
      document.body.classList.add(newThemeClass)
    }
  


  logout(){
    // window.location.href = 'https://rsu-iam.71dev.com/Account/Logout'
    this.router.navigate(['/login'])
  }

  IncreaseFontSize(event: Event){    
    this.defaultFontSize ++
    document.body.style.fontSize = this.defaultFontSize + 'px';
    event.stopPropagation()
  }

  DecreaseFontSize(event: Event){
    this.defaultFontSize --
    document.body.style.fontSize = this.defaultFontSize + 'px';
    event.stopPropagation()
  }
}
