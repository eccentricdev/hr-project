import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './layout/login/login.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';

const routes: Routes = [
  // {
  //   path:'',
  //   component: MainLayoutComponent,
  //   pathMatch:'full'
  // },
  {
    path:'login',
    component: LoginComponent,
    pathMatch:'full'
  },
  // {
  //   path: '**',
  //   component: LoginComponent,
  //   pathMatch:'full'
  // },
  {
    path:'',
    component: MainLayoutComponent,
    loadChildren: () => import('./feature/home-menu/home-menu.module')
    .then(x => x.HomeMenuModule),
    pathMatch:'full'
  },
  {
    path:'app',
    component:MainLayoutComponent,
    children:[
      {
        path:'',
        redirectTo:'home_menu',
        pathMatch:'full'
      },
      {
        path:'home_menu',
        loadChildren: () => import('./feature/home-menu/home-menu.module')
          .then(x => x.HomeMenuModule)
      },
      // โครงสร้างองค์กร >> เริ่ม
      {
        path:'position_chart',
        loadChildren: () => import('./feature/organizational-structure/position-chart/position-chart.module')
          .then(x => x.PositionChartModule)
      },
      {
        path:'organization_chart',
        loadChildren: () => import('./feature/organizational-structure/organization-chart/organization-chart.module')
          .then(x => x.OrganizationChartModule)
      },

      {
        path:'organization_data',
        loadChildren: () => import('./feature/organizational-structure/set-organization/set-organization.module')
          .then(x => x.SetOrganizationModule)
      },
      {
        path:'department_type',
        loadChildren: () => import('./feature/organizational-structure/set-department-type/set-department-type.module')
          .then(x => x.SetDepartmentTypeModule)
      },
      {
        path:'department_data',
        loadChildren: () => import('./feature/organizational-structure/set-department-data/set-department-data.module')
          .then(x => x.SetDepartmentDataModule)
      },
      {
        path:'detail_job_description',
        loadChildren: () => import('./feature/organizational-structure/set-detail-job-description/set-detail-job-description.module')
          .then(x => x.SetDetailJobDescriptionModule)
      },
      {
        path:'position_data',
        loadChildren: () => import('./feature/organizational-structure/set-position-data/set-position-data.module')
          .then(x => x.SetPositionDataModule)
      },
      // โครงสร้างองค์กร >> จบ

      // ทะเบียนประวัติ >> เริ่ม
      {
        path:'pr_save_profile_registration',
        loadChildren: () => import('./feature/profile-registration/save-profile-registration/save-profile-registration.module')
          .then(x => x.SaveProfileRegistrationModule)
      },
      {
        path:'pr_edit_profile_registration',
        loadChildren: () => import('./feature/profile-registration/save-request-to-edit-history/save-request-to-edit-history.module')
          .then(x => x.SaveRequestToEditHistoryModule)
      },
      {
        path:'pr_edit_position_salary',
        loadChildren: () => import('./feature/profile-registration/save-position-salary/save-position-salary.module')
          .then(x => x.SavePositionSalaryModule)
      },
      {
        path:'pr_academic_position',
        loadChildren: () => import('./feature/profile-registration/save-academic-position/save-academic-position.module')
          .then(x => x.SaveAcademicPositionModule)
      },
      {
        path:'pr_educational_background',
        loadChildren: () => import('./feature/profile-registration/save-educational-background/save-educational-background.module')
          .then(x => x.SaveEducationalBackgroundModule)
      },
      {
        path:'pr_transfer',
        loadChildren: () => import('./feature/profile-registration/save-transfer/save-transfer.module')
          .then(x => x.SaveTransferModule)
      },
      {
        path:'pr_resgin',
        loadChildren: () => import('./feature/profile-registration/save-resgin/save-resgin.module')
          .then(x => x.SaveResginModule)
      },
      
      {
        path:'pr_set_title',
        loadChildren: () => import('./feature/profile-registration/set-title/set-title.module')
          .then(x => x.SetTitleModule)
      },
      {
        path:'pr_set_relationship',
        loadChildren: () => import('./feature/profile-registration/set-relationship/set-relationship.module')
          .then(x => x.SetRelationshipModule)
      },
      {
        path:'pr_set_education',
        loadChildren: () => import('./feature/profile-registration/set-educational-background/set-educational-background.module')
          .then(x => x.SetEducationalBackgroundModule)
      },
      {
        path:'pr_set_qualification',
        loadChildren: () => import('./feature/profile-registration/set-qualification/set-qualification.module')
          .then(x => x.SetQualificationModule)
      },
      {
        path:'pr_set_personnel_group',
        loadChildren: () => import('./feature/profile-registration/set-personnel-group/set-personnel-group.module')
          .then(x => x.SetPersonnelGroupModule)
      },
      {
        path:'pr_set_personnel_type',
        loadChildren: () => import('./feature/profile-registration/set-personnel-type/set-personnel-type.module')
          .then(x => x.SetPersonnelTypeModule)
      },
      {
        path:'pr_set_position_level',
        loadChildren: () => import('./feature/profile-registration/set-position-level/set-position-level.module')
          .then(x => x.SetPositionLevelModule)
      },
      {
        path:'pr_set_academic_position',
        loadChildren: () => import('./feature/profile-registration/set-academic-position/set-academic-position.module')
          .then(x => x.SetAcademicPositionModule)
      },
      {
        path:'pr_set_line_work',
        loadChildren: () => import('./feature/profile-registration/set-line-of-work/set-line-of-work.module')
          .then(x => x.SetLineOfWorkModule)
      },
      {
        path:'pr_set_work_group',
        loadChildren: () => import('./feature/profile-registration/set-work-group/set-work-group.module')
          .then(x => x.SetWorkGroupModule)
      },
      {
        path:'pr_set_type_termination',
        loadChildren: () => import('./feature/profile-registration/set-type-of-termination/set-type-of-termination.module')
          .then(x => x.SetTypeOfTerminationModule)
      },
      // ทะเบียนประวัติ >> สุด

      //จัดสรรอัตรา >> เริ่ม
      {
        path:'save_allocate_people',
        loadChildren: () => import('./feature/allocate-people/save-allocate-people/save-allocate-people.module')
          .then(x => x.SaveAllocatePeopleModule)
      },
      {
        path:'save_request_people',
        loadChildren: () => import('./feature/allocate-people/save-request-people/save-request-people.module')
          .then(x => x.SaveRequestPeopleModule)
      },
      {
        path:'set_number_people',
        loadChildren: () => import('./feature/allocate-people/set-number-people/set-number-people.module')
          .then(x => x.SetNumberPeopleModule)
      },
      {
        path:'search_position_people',
        loadChildren: () => import('./feature/allocate-people/search-position/search-position.module')
          .then(x => x.SearchPositionModule)
      },      
      //จัดสรรอัตรา >> สิ้นสุด
      
      //สรรหาอัตรา >> เริ่มต้น
      {
        path:'save_job_application',
        loadChildren: () => import('./feature/recruitment-and-selection/save-job-application/save-job-application.module')
          .then(x => x.SaveJobApplicationModule)
      },  
      {
        path:'save_employment_contract',
        loadChildren: () => import('./feature/recruitment-and-selection/save-employment-contract/save-employment-contract.module')
          .then(x => x.SaveEmploymentContractModule)
      },  
      {
        path:'recruitment_employ',
        loadChildren: () => import('./feature/recruitment-and-selection/search-job-application/search-job-application.module')
          .then(x => x.SearchJobApplicationModule)
      }, 
      {
        path:'save-selection-results',
        loadChildren: () => import('./feature/recruitment-and-selection/save-selection-results/save-selection-results.module')
          .then(x => x.SaveSelectionResultsModule)
      }, 
      
      
      
      //สรรหาอัตรา >> สิ้นสุด
      

      //ปฏิบัติงานประจำวัน >> เริ่ม
      {
        path:'general_leave',
        loadChildren: () => import('./feature/time/general-leave/general-leave.module')
          .then(x => x.GeneralLeaveModule)
      },
      {
        path:'leave_education',
        loadChildren: () => import('./feature/time/leave-education/leave-education.module')
          .then(x => x.LeaveEducationModule)
      },
      {
        path:'service',
        loadChildren: () => import('./feature/time/service/service.module')
          .then(x => x.ServiceModule)
      },
      {
        path:'ot',
        loadChildren: () => import('./feature/time/ot/ot.module')
          .then(x => x.OtModule)
      },
      {
        path:'leave_type',
        loadChildren: () => import('./feature/time/leave-type/leave-type.module')
          .then(x => x.LeaveTypeModule)
      },
      {
        path:'holiday',
        loadChildren: () => import('./feature/time/holiday/holiday.module')
          .then(x => x.HolidayModule)
      },
      {
        path:'workshift',
        loadChildren: () => import('./feature/time/workshift/workshift.module')
          .then(x => x.WorkshiftModule)
      },
      //ปฏิบัติงานประจำวัน >> สิ้นสุด

      //เงินเดือน >> เริ่ม
      {
        path:'personal-income',
        loadChildren: () => import('./feature/payroll/personal-income/personal-income.module')
          .then(x => x.PersonalIncomeModule)
      },
      {
        path:'personal-deduct',
        loadChildren: () => import('./feature/payroll/personal-deduct/personal-deduct.module')
          .then(x => x.PersonalDeductModule)
      },
      {
        path:'fund-member',
        loadChildren: () => import('./feature/payroll/fund-member/fund-member.module')
          .then(x => x.FundMemberModule)
      },
      {
        path:'employee-other-payment',
        loadChildren: () => import('./feature/payroll/employee-other-payment/employee-other-payment.module')
          .then(x => x.EmployeeOtherPaymentModule)
      },
      {
        path:'payment-process',
        loadChildren: () => import('./feature/payroll/payment-process/payment-process.module')
          .then(x => x.PaymentProcessModule)
      },
      {
        path:'employee-account',
        loadChildren: () => import('./feature/payroll/employee-account/employee-account.module')
          .then(x => x.EmployeeAccountModule)
      },
      {
        path:'employee-deductible',
        loadChildren: () => import('./feature/payroll/employee-deductible/employee-deductible.module')
          .then(x => x.EmployeeDeductibleModule)
      },
      {
        path:'salary-adjust',
        loadChildren: () => import('./feature/payroll/salary-adjust/salary-adjust.module')
          .then(x => x.SalaryAdjustModule)
      },
      {
        path:'sso-contribution',
        loadChildren: () => import('./feature/payroll/sso-contribution/sso-contribution.module')
          .then(x => x.SsoContributionModule)
      },
      {
        path:'payment-period',
        loadChildren: () => import('./feature/payroll/payment-period/payment-period.module')
          .then(x => x.PaymentPeriodModule)
      },
      {
        path:'bank',
        loadChildren: () => import('./feature/payroll/bank/bank.module')
          .then(x => x.BankModule)
      },
      {
        path:'salary-group',
        loadChildren: () => import('./feature/payroll/salary-group/salary-group.module')
          .then(x => x.SalaryGroupModule)
      },
      {
        path:'provident-fund',
        loadChildren: () => import('./feature/payroll/provident-fund/provident-fund.module')
          .then(x => x.ProvidentFundModule)
      },
      {
        path:'pay-tax',
        loadChildren: () => import('./feature/payroll/pay-tax/pay-tax.module')
          .then(x => x.PayTaxModule)
      },
      {
        path:'income-deduct',
        loadChildren: () => import('./feature/payroll/income-deduct/income-deduct.module')
          .then(x => x.IncomeDeductModule)
      },
      {
        path:'cooperative',
        loadChildren: () => import('./feature/payroll/cooperative/cooperative.module')
          .then(x => x.CooperativeModule)
      },
      {
        path:'salary-range',
        loadChildren: () => import('./feature/payroll/salary-range/salary-range.module')
          .then(x => x.SalaryRangeModule)
      },
      //เงินเดือน >> สิ้นสุด


      //วินัยบุคคล >> เริ่ม
      {
        path:'save_complain',
        loadChildren: () => import('./feature/personnel-discipline/main/save-complain/save-complain.module')
          .then(x => x.SaveComplainModule)
      },
      {
        path:'save_discipline_information',
        loadChildren: () => import('./feature/personnel-discipline/main/save-discipline-information/save-discipline-information.module')
          .then(x => x.SaveDisciplineInformationModule)
      },
      
      {
        path:'save_sentence',
        loadChildren: () => import('./feature/personnel-discipline/main/save-sentence/save-sentence.module')
          .then(x => x.SaveSentenceModule)
      },
      
      {
        path:'set-court',
        loadChildren: () => import('./feature/personnel-discipline/setting/set-court/set-court.module')
          .then(x => x.SetCourtModule)
      },

      {
        path:'set-schedule',
        loadChildren: () => import('./feature/personnel-discipline/setting/set-schedule/set-schedule.module')
          .then(x => x.SetScheduleModule)
      },

      {
        path:'set-step-consider',
        loadChildren: () => import('./feature/personnel-discipline/setting/set-step-consider/set-step-consider.module')
          .then(x => x.SetStepConsiderModule)
      },
      
      {
        path:'set-type-penalty',
        loadChildren: () => import('./feature/personnel-discipline/setting/set-type-penalty/set-type-penalty.module')
          .then(x => x.SetTypePenaltyModule)
      },
      
      {
        path:'set-type-penalty-personnel',
        loadChildren: () => import('./feature/personnel-discipline/setting/set-type-penalty-personnel/set-type-penalty-personnel.module')
          .then(x => x.SetTypePenaltyPersonnelModule)
      },
      

      //วินัยบุคคล >> สิ้นสุด

      //เครื่องราชอิสริยาภรณ์ >> เริ่ม
      {
        path:'follow_royal',
        loadChildren: () => import('./feature/insignia/main/follow-royal/follow-royal.module')
          .then(x => x.FollowRoyalModule)
      },
      
      {
        path:'present_royal',
        loadChildren: () => import('./feature/insignia/main/present-royal/present-royal.module')
          .then(x => x.PresentRoyalModule)
      },
      
      {
        path:'request_royal_medal',
        loadChildren: () => import('./feature/insignia/main/request-royal-medal/request-royal-medal.module')
          .then(x => x.RequestRoyalMedalModule)
      },
      
      {
        path:'request_royal_right',
        loadChildren: () => import('./feature/insignia/main/request-royal-right/request-royal-right.module')
          .then(x => x.RequestRoyalRightModule)
      },
      
      {
        path:'return_royal',
        loadChildren: () => import('./feature/insignia/main/return-royal/return-royal.module')
          .then(x => x.ReturnRoyalModule)
      },
      
      {
        path:'save_recive_royal',
        loadChildren: () => import('./feature/insignia/main/save-recive-royal/save-recive-royal.module')
          .then(x => x.SaveReciveRoyalModule)
      },
      
      {
        path:'set_royal_date',
        loadChildren: () => import('./feature/insignia/setting/set-royal-date/set-royal-date.module')
          .then(x => x.SetRoyalDateModule)
      },
      
      {
        path:'set_royal_details',
        loadChildren: () => import('./feature/insignia/setting/set-royal-details/set-royal-details.module')
          .then(x => x.SetRoyalDetailsModule)
      },
      
      {
        path:'set_royal_medal',
        loadChildren: () => import('./feature/insignia/setting/set-royal-medal/set-royal-medal.module')
          .then(x => x.SetRoyalMedalModule)
      },
      
      {
        path:'set_royal_right',
        loadChildren: () => import('./feature/insignia/setting/set-royal-right/set-royal-right.module')
          .then(x => x.SetRoyalRightModule)
      },
      
      {
        path:'set_royal_type',
        loadChildren: () => import('./feature/insignia/setting/set-royal-type/set-royal-type.module')
          .then(x => x.SetRoyalTypeModule)
      },
      
      //เครื่องราชอิสริยาภรณ์ >> สิ้นสุด
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
