import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';
import { OverdueMoneyService } from 'src/app/core/service/salary/overdue-money.service';

@Component({
  selector: 'app-employee-other-payment-list',
  templateUrl: './employee-other-payment-list.component.html',
  styleUrls: ['./employee-other-payment-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeOtherPaymentListComponent extends BaseList implements OnInit {
  rows: any = []
  personnelDatas: any = []
  moneyTypeDatas: any = []
  overdueMoney$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public PersonnelService:PersonnelService,
    public OverdueMoneyService:OverdueMoneyService,
    public MoneyTypeService:MoneyTypeService
  ) {
    super();
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.overdueMoney$ = this.OverdueMoneyService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/employee-other-payment/add'])
  }
  editPage(id) {
    this.router.navigate(['app/employee-other-payment/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.OverdueMoneyService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.overdueMoney$ = this.OverdueMoneyService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}