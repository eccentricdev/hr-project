import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmployeeOtherPaymentListComponent } from './employee-other-payment-list/employee-other-payment-list.component';
import { EmployeeOtherPaymentFormComponent } from './employee-other-payment-form/employee-other-payment-form.component';

const routes:Routes = [
  {
    path:'',
    component:EmployeeOtherPaymentListComponent
  },
  {
    path:'add',
    component:EmployeeOtherPaymentFormComponent
  },
  {
    path:'edit/:id',
    component:EmployeeOtherPaymentFormComponent
  },
  
]


@NgModule({
  declarations: [EmployeeOtherPaymentListComponent, EmployeeOtherPaymentFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class EmployeeOtherPaymentModule { }
