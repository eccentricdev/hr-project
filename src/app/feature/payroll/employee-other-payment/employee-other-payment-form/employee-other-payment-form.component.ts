import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { OverdueMoneyService } from 'src/app/core/service/salary/overdue-money.service';
import { throwError } from 'rxjs';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';
import { PaymentPeriodService } from 'src/app/core/service/salary/payment-period.service';

@Component({
  selector: 'app-employee-other-payment-form',
  templateUrl: './employee-other-payment-form.component.html',
  styleUrls: ['./employee-other-payment-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeOtherPaymentFormComponent extends BaseForm implements OnInit {

  personnelDatas: any = []
  moneyTypeDatas: any = []
  paymentPeriodDatas: any = []

  paymentPeriodMonthDatas: any = []

  periodYear: any = null
  monthName = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public PersonnelService: PersonnelService,
    public OverdueMoneyService: OverdueMoneyService,
    public MoneyTypeService: MoneyTypeService,
    public PaymentPeriodService: PaymentPeriodService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(formBuilder, activeRoute);
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = [...x]),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.PaymentPeriodService.getAll().pipe(
      tap(x => this.paymentPeriodDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.OverdueMoneyService.get(this.id).pipe(
          tap((res: any) => this.periodYear = res.payment_period_detail.payment_period_uid),
          tap(res => this.setForm(res)),
          concatMap((res: any) => {
            return this.PaymentPeriodService.get(res.payment_period_detail.payment_period_uid).pipe(
              tap(x => console.log(x)),
              tap((x: any) => {
                var dataSort = []
                 dataSort = x.payment_period_details.sort((a, b) => a.row_order - b.row_order)
                this.paymentPeriodMonthDatas = [...dataSort]
                this.cdRef.detectChanges()
              }),
            )
          }),
          
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      overdue_money_uid: res.overdue_money_uid,
      payment_period_detail_uid: res.payment_period_detail_uid,
      personnel_uid: res.personnel_uid,
      order_no: res.order_no,
      order_date: res.order_date,
      from_date: res.from_date,
      to_date: res.to_date,
      order_amount: res.order_amount,
      paid_amount: res.paid_amount,
      overdue_amount: res.overdue_amount,
      money_type_uid: res.money_type_uid,
    })
  }

  selectPeriodYear(payment_period_uid) {
    console.log(payment_period_uid)
    this.form.get('payment_period_detail_uid').setValue(null)
    this.PaymentPeriodService.get(payment_period_uid).pipe(
      tap((x: any) => {
        var dataSort = x.payment_period_details.sort((a, b) => a.row_order - b.row_order)
        this.paymentPeriodMonthDatas = dataSort
      }),
      tap(x => console.log(x)),
    ).subscribe()
  }

  close() {
    this.router.navigate(['app/employee-other-payment'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.OverdueMoneyService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.OverdueMoneyService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      overdue_money_uid: [null],
      payment_period_detail_uid: [null],
      personnel_uid: [null],
      order_no: [''],
      order_date: [null],
      from_date: [null],
      to_date: [null],
      order_amount: 0,
      paid_amount: 0,
      overdue_amount: 0,
      money_type_uid: [null],
    })
  }

}
