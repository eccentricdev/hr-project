import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-cooperative-list',
  templateUrl: './cooperative-list.component.html',
  styleUrls: ['./cooperative-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CooperativeListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/cooperative/add'])
  }
  editPage(id) {
    this.router.navigate(['app/cooperative/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}