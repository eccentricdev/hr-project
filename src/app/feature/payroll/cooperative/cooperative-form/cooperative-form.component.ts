import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-cooperative-form',
  templateUrl: './cooperative-form.component.html',
  styleUrls: ['./cooperative-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CooperativeFormComponent extends BaseForm implements OnInit {

  rows:any = [{1:1}]

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  close(){
    this.router.navigate(['app/cooperative'])
  }

  save() {
    this.router.navigate(['app/cooperative'])
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
