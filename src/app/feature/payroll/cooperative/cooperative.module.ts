import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { CooperativeListComponent } from './cooperative-list/cooperative-list.component';
import { CooperativeFormComponent } from './cooperative-form/cooperative-form.component';

const routes:Routes = [
  {
    path:'',
    component:CooperativeListComponent
  },
  {
    path:'add',
    component:CooperativeFormComponent
  },
  {
    path:'edit/:id',
    component:CooperativeFormComponent
  },
  
]


@NgModule({
  declarations: [CooperativeListComponent, CooperativeFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class CooperativeModule { }
