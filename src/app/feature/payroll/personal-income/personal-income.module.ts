import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PersonalIncomeListComponent } from './personal-income-list/personal-income-list.component';
import { PersonalIncomeFormComponent } from './personal-income-form/personal-income-form.component';
import { PersonalIncomeSubCreateComponent } from './personal-income-sub-create/personal-income-sub-create.component';
import { PersonalIncomeSubUpdateComponent } from './personal-income-sub-update/personal-income-sub-update.component';

const routes:Routes = [
  {
    path:'',
    component:PersonalIncomeListComponent
  },
  {
    path:'add',
    component:PersonalIncomeFormComponent
  },
  {
    path:'edit/:id',
    component:PersonalIncomeFormComponent
  },
  {
    path:'form-add/:id',
    component:PersonalIncomeSubCreateComponent
  },
  {
    path:'form-update/:id',
    component:PersonalIncomeSubUpdateComponent
  },
  
]


@NgModule({
  declarations: [PersonalIncomeListComponent, PersonalIncomeFormComponent, PersonalIncomeSubCreateComponent, PersonalIncomeSubUpdateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PersonalIncomeModule { }
