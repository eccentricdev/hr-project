import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { ExtraMoneyService } from 'src/app/core/service/salary/extra-money.service';

@Component({
  selector: 'app-personal-income-list',
  templateUrl: './personal-income-list.component.html',
  styleUrls: ['./personal-income-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalIncomeListComponent extends BaseList implements OnInit {
  rows: any = []
  personnelDatas: any = []
  extraMoney$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public ExtraMoneyService: ExtraMoneyService,
    public PersonnelService: PersonnelService
  ) {
    super();
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.extraMoney$ = this.ExtraMoneyService.getAll().pipe(
      map((datas: any) => {
        datas.forEach((item, index) => {
          if (this.rows.findIndex(i => i.personnel_uid == item.personnel_uid) === -1) {
            var filterPersonal = datas.filter((personalData) => personalData.personnel_uid == item.personnel_uid)
            const result = filterPersonal.reduce((sum, amount) => {
              if (amount.status_id == 1) {
                return sum + amount.extra_amount
              } else {
                return sum + 0
              }
            }, 0)
            this.rows.push({ ...item, totalAmount: result })
          }
        });
        return this.rows;
      }),
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/personal-income/add'])
  }
  editPage(id) {
    this.router.navigate(['app/personal-income/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}