import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { ExtraMoneyService } from 'src/app/core/service/salary/extra-money.service';

import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { StatusService } from 'src/app/core/service/common/status.service';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';
@Component({
  selector: 'app-personal-income-form',
  templateUrl: './personal-income-form.component.html',
  styleUrls: ['./personal-income-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalIncomeFormComponent extends BaseForm implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  academicYear = []
  semesterData = []
  statusDatas = []
  rows: any = [{ 1: 1 }]
  personnelDatas = []
  moneyTypeDatas = []
  extraMoneyDatas = []
  personnelSelected = null
  // extraMoney$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public PersonnelService: PersonnelService,
    public ExtraMoneyService: ExtraMoneyService,
    public MoneyTypeService: MoneyTypeService,
    public StatusService:StatusService
  ) {
    super(formBuilder, activeRoute);
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.StatusService.getAll().pipe(
      tap(x => this.statusDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    // this.extraMoney$ = this.ExtraMoneyService.getAll().pipe(
    //   map(x => this.updateMatTable(x))
    // )
  }

  updateMatTable(res: any) {
    res = new MatTableDataSource(res)
    res.paginator = this.paginator
    res.sort = this.sort
    return res
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.personnelSelected = this.id;
        this.selectPersonel()
        break;
      case 'add':

        break;
    }
  }

  addForm() {
    if (this.personnelSelected) {
      this.router.navigate(['app/personal-income/form-add/' + this.personnelSelected])
    } else {
      this.AppService.swaltAlertError('กรุณาเลือกบุคลากร')
    }
  }

  updateForm(id) {
    this.router.navigate(['app/personal-income/form-update/' + id])
  }

  selectPersonel(){
    this.ExtraMoneyService.getAll().pipe(
      map((res:any) => res.filter(x => x.personnel_uid == this.personnelSelected)),
      tap(x => this.extraMoneyDatas = this.updateMatTable(x)),
      tap(x => console.log(x))
    ).subscribe()
  }

  close() {
    this.router.navigate(['app/personal-income'])
  }

  save() {
    this.router.navigate(['app/personal-income'])
  }

  createForm() {
    return this.baseFormBuilder.group({

    })
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.ExtraMoneyService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.selectPersonel()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }

}
