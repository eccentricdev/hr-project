import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FundMemberListComponent } from './fund-member-list/fund-member-list.component';
import { FundMemberFormComponent } from './fund-member-form/fund-member-form.component';

const routes:Routes = [
  {
    path:'',
    component:FundMemberListComponent
  },
  {
    path:'add',
    component:FundMemberFormComponent
  },
  {
    path:'edit/:id',
    component:FundMemberFormComponent
  },
  
]


@NgModule({
  declarations: [FundMemberListComponent, FundMemberFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class FundMemberModule { }
