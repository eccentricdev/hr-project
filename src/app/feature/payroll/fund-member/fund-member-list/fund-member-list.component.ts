import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { PersonnelProvidentFundService } from 'src/app/core/service/salary/personnel-provident-fund.service';
import { ProvidentFundService } from 'src/app/core/service/salary/provident-fund.service';

@Component({
  selector: 'app-fund-member-list',
  templateUrl: './fund-member-list.component.html',
  styleUrls: ['./fund-member-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FundMemberListComponent extends BaseList implements OnInit {
  rows: any = []
  personnelProvidentFund$ = new Observable()
  personnelDatas:any = []
  providentFundDatas:any = []
  constructor(
    public router: Router,
    public AppService: AppService,
    public PersonnelProvidentFundService:PersonnelProvidentFundService,
    public PersonnelService:PersonnelService,
    public ProvidentFundService:ProvidentFundService
  ) {
    super();
    this.personnelProvidentFund$ = this.PersonnelProvidentFundService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.ProvidentFundService.getAll().pipe(
      tap(x => this.providentFundDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/fund-member/add'])
  }
  editPage(id) {
    this.router.navigate(['app/fund-member/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.PersonnelProvidentFundService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.personnelProvidentFund$ = this.PersonnelProvidentFundService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}