import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { PersonnelProvidentFundService } from 'src/app/core/service/salary/personnel-provident-fund.service';
import { ProvidentFundService } from 'src/app/core/service/salary/provident-fund.service';

@Component({
  selector: 'app-fund-member-form',
  templateUrl: './fund-member-form.component.html',
  styleUrls: ['./fund-member-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FundMemberFormComponent extends BaseForm implements OnInit {

  rows: any = [{ 1: 1 }]
  personnelDatas: any = []
  providentFundDatas: any = []

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public PersonnelProvidentFundService: PersonnelProvidentFundService,
    public PersonnelService: PersonnelService,
    public ProvidentFundService: ProvidentFundService
  ) {
    super(formBuilder, activeRoute);
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.ProvidentFundService.getAll().pipe(
      tap(x => this.providentFundDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PersonnelProvidentFundService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      personnel_provident_fund_uid: res.personnel_provident_fund_uid,
      personnel_uid: res.personnel_uid,
      provident_fund_uid: res.provident_fund_uid,
      membership_no: res.membership_no,
      start_date: res.start_date,
      end_date: res.end_date,
    })
  }

  close() {
    this.router.navigate(['app/fund-member'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.PersonnelProvidentFundService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.PersonnelProvidentFundService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      personnel_provident_fund_uid: [null],
      personnel_uid: [null],
      provident_fund_uid: [null],
      membership_no: [''],
      start_date: [null],
      end_date: [null],
    })
  }

}
