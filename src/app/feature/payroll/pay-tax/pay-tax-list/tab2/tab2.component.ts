import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.component.html',
  styleUrls: ['./tab2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tab2Component extends BaseForm implements OnInit {

  rows:any = [{1:1}]

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  save() {
    
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
