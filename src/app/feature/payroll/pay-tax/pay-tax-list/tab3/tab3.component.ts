import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.component.html',
  styleUrls: ['./tab3.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tab3Component implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/pay-tax/add'])
  }
  editPage(id) {
    this.router.navigate(['app/pay-tax/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}