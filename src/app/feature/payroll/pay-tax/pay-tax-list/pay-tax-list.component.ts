import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-pay-tax-list',
  templateUrl: './pay-tax-list.component.html',
  styleUrls: ['./pay-tax-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PayTaxListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
