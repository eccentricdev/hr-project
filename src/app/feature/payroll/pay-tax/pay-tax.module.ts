import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PayTaxListComponent } from './pay-tax-list/pay-tax-list.component';
import { PayTaxFormComponent } from './pay-tax-form/pay-tax-form.component';
import { Tab1Component } from './pay-tax-list/tab1/tab1.component';
import { Tab2Component } from './pay-tax-list/tab2/tab2.component';
import { Tab3Component } from './pay-tax-list/tab3/tab3.component';

const routes:Routes = [
  {
    path:'',
    component:PayTaxListComponent
  },
  {
    path:'add',
    component:PayTaxFormComponent
  },
  {
    path:'edit/:id',
    component:PayTaxFormComponent
  },
  
]


@NgModule({
  declarations: [PayTaxListComponent, PayTaxFormComponent, Tab1Component, Tab2Component, Tab3Component],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PayTaxModule { }
