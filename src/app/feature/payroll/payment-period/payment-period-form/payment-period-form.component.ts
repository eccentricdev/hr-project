import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { PaymentPeriodService } from 'src/app/core/service/salary/payment-period.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { catchError, tap } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-payment-period-form',
  templateUrl: './payment-period-form.component.html',
  styleUrls: ['./payment-period-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentPeriodFormComponent extends BaseForm implements OnInit {

  statusDatas: any = []

  dataSource: MatTableDataSource<any>;
  displayedColumns = ['no', 'name', 'start_date', 'end_date', 'pay_date', 'ot_start_date', 'ot_end_date', 'is_closed']
  monthName = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public PaymentPeriodService: PaymentPeriodService,
    public StatusService: StatusService
  ) {
    super(formBuilder, activeRoute);
    this.StatusService.getAll().pipe(
      tap(x => this.statusDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PaymentPeriodService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
          tap(x => {
            this.form.get('payment_period_code').disable()
            this.form.get('payment_period_name_th').disable()
            this.form.get('payment_period_name_en').disable()
            this.form.get('payment_year_id').disable()
            this.form.get('effective_date').disable()
            this.form.get('status_id').disable()
          }),
          tap((x: any) => {
            if (x.payment_period_details.length == 0) {
              var i = 1;
              for (; i <= 12;) {
                this.addMonth(i)
                i++;
              }
            } else {
              var dataSort = x.payment_period_details.sort((a, b) => a.row_order - b.row_order)
              console.log(dataSort)
              dataSort.forEach(element => {
                this.addEditMonth(element)
              });
            }
          }),
          tap(x => this.dataSource = new MatTableDataSource((this.form.get('payment_period_details') as FormArray).controls))
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      payment_period_uid: res.payment_period_uid,
      payment_period_code: res.payment_period_code,
      payment_period_name_th: res.payment_period_name_th,
      payment_period_name_en: res.payment_period_name_en,
      payment_year_id: res.payment_year_id,
      effective_date: res.effective_date
    })
  }

  close() {
    this.router.navigate(['app/payment-period'])
  }

  closeAfterSave(id) {
    this.router.navigate(['app/payment-period/edit/', id])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.PaymentPeriodService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.PaymentPeriodService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.closeAfterSave(x.payment_period_uid)
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      payment_period_uid: [null],
      payment_period_code: [''],
      payment_period_name_th: [''],
      payment_period_name_en: [''],
      payment_year_id: [null],
      effective_date: [null],
      payment_period_details: this.baseFormBuilder.array([])
    })
  }

  get albums(): FormArray {
    return this.form.get('payment_period_details') as FormArray;
  }

  addMonth(i) {
    (<FormArray>this.form.get('payment_period_details')).push(this.formAddMonth(i))
  }

  addEditMonth(res) {
    (<FormArray>this.form.get('payment_period_details')).push(this.formEditAddMonth(res))
  }

  formAddMonth(i) {
    return this.formBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      payment_period_detail_uid: null,
      payment_period_uid: null,
      start_date: new Date(),
      end_date: new Date(),
      pay_date: new Date(),
      ot_start_date: new Date(),
      ot_end_date: new Date(),
      is_closed: false,
      row_order: i
    })
  }

  formEditAddMonth(res) {
    return this.formBuilder.group({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      payment_period_detail_uid: res.payment_period_detail_uid,
      payment_period_uid: res.payment_period_uid,
      start_date: res.start_date,
      end_date: res.end_date,
      pay_date: res.pay_date,
      ot_start_date: res.ot_start_date,
      ot_end_date: res.ot_end_date,
      is_closed: res.is_closed,
      row_order: res.row_order
    })
  }
}
