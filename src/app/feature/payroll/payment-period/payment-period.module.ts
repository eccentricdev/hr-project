import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PaymentPeriodListComponent } from './payment-period-list/payment-period-list.component';
import { PaymentPeriodFormComponent } from './payment-period-form/payment-period-form.component';

const routes:Routes = [
  {
    path:'',
    component:PaymentPeriodListComponent
  },
  {
    path:'add',
    component:PaymentPeriodFormComponent
  },
  {
    path:'edit/:id',
    component:PaymentPeriodFormComponent
  },
  
]


@NgModule({
  declarations: [PaymentPeriodListComponent, PaymentPeriodFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PaymentPeriodModule { }
