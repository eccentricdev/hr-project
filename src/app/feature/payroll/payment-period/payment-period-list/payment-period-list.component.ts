import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PaymentPeriodService } from 'src/app/core/service/salary/payment-period.service';

@Component({
  selector: 'app-payment-period-list',
  templateUrl: './payment-period-list.component.html',
  styleUrls: ['./payment-period-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentPeriodListComponent extends BaseList implements OnInit {
  rows: any = []
  paymentPeriody$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public PaymentPeriodService:PaymentPeriodService
  ) {
    super();
    this.paymentPeriody$ = this.PaymentPeriodService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/payment-period/add'])
  }
  editPage(id) {
    this.router.navigate(['app/payment-period/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.PaymentPeriodService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.paymentPeriody$ = this.PaymentPeriodService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}