import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalaryRangeListComponent } from './salary-range-list/salary-range-list.component';
import { SalaryRangeFormComponent } from './salary-range-form/salary-range-form.component';

const routes:Routes = [
  {
    path:'',
    component:SalaryRangeListComponent
  },
  {
    path:'add',
    component:SalaryRangeFormComponent
  },
  {
    path:'edit/:id',
    component:SalaryRangeFormComponent
  },
  
]


@NgModule({
  declarations: [SalaryRangeListComponent, SalaryRangeFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SalaryRangeModule { }
