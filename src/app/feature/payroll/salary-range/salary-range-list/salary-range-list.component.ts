import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-salary-range-list',
  templateUrl: './salary-range-list.component.html',
  styleUrls: ['./salary-range-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SalaryRangeListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/salary-range/add'])
  }
  editPage(id) {
    this.router.navigate(['app/salary-range/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}