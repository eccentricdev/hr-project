import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-employee-deductible-form',
  templateUrl: './employee-deductible-form.component.html',
  styleUrls: ['./employee-deductible-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeDeductibleFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]

  checkbox1:boolean = false
  checkbox2:boolean = false
  
  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  close(){
    this.router.navigate(['app/employee-deductible'])
  }

  save() {
    this.router.navigate(['app/employee-deductible'])
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
