import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-employee-deductible-list',
  templateUrl: './employee-deductible-list.component.html',
  styleUrls: ['./employee-deductible-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeDeductibleListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/employee-deductible/add'])
  }
  editPage(id) {
    this.router.navigate(['app/employee-deductible/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}