import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmployeeDeductibleListComponent } from './employee-deductible-list/employee-deductible-list.component';
import { EmployeeDeductibleFormComponent } from './employee-deductible-form/employee-deductible-form.component';

const routes:Routes = [
  {
    path:'',
    component:EmployeeDeductibleListComponent
  },
  {
    path:'add',
    component:EmployeeDeductibleFormComponent
  },
  {
    path:'edit/:id',
    component:EmployeeDeductibleFormComponent
  },
  
]


@NgModule({
  declarations: [EmployeeDeductibleListComponent, EmployeeDeductibleFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class EmployeeDeductibleModule { }
