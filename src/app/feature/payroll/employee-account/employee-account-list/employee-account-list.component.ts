import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-employee-account-list',
  templateUrl: './employee-account-list.component.html',
  styleUrls: ['./employee-account-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeAccountListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/employee-account/add'])
  }
  editPage(id) {
    this.router.navigate(['app/employee-account/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}