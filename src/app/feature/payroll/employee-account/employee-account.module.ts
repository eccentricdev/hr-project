import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmployeeAccountListComponent } from './employee-account-list/employee-account-list.component';
import { EmployeeAccountFormComponent } from './employee-account-form/employee-account-form.component';

const routes:Routes = [
  {
    path:'',
    component:EmployeeAccountListComponent
  },
  {
    path:'add',
    component:EmployeeAccountFormComponent
  },
  {
    path:'edit/:id',
    component:EmployeeAccountFormComponent
  },
  
]


@NgModule({
  declarations: [EmployeeAccountListComponent, EmployeeAccountFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class EmployeeAccountModule { }
