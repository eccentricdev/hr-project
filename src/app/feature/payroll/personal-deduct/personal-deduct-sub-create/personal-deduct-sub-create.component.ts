import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { DeductionMoneyService } from 'src/app/core/service/salary/deduction-money.service';
import { ExtraMoneyService } from 'src/app/core/service/salary/extra-money.service';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';

@Component({
  selector: 'app-personal-deduct-sub-create',
  templateUrl: './personal-deduct-sub-create.component.html',
  styleUrls: ['./personal-deduct-sub-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalDeductSubCreateComponent extends BaseForm implements OnInit {

  personnelDatas = []
  moneyTypeDatas = []
  statusDatas = []

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public PersonnelService: PersonnelService,
    public MoneyTypeService: MoneyTypeService,
    // public ExtraMoneyService: ExtraMoneyService,
    public DeductionMoneyService: DeductionMoneyService,
    public StatusService:StatusService
  ) {
    super(formBuilder, activeRoute);
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.StatusService.getAll().pipe(
      tap(x => this.statusDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    this.form.get('personnel_uid').setValue(this.id);
  }

  close() {
    this.router.navigate(['app/personal-deduct/add'])
  }

  save() {
    this.DeductionMoneyService.add(this.form.getRawValue()).pipe(
      catchError(err => {
        this.AppService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.AppService.swaltAlert()
        this.close()
      })
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],

      deduction_money_uid: [null],
      personnel_uid: [null],
      money_type_uid: [null],
      deduction_amount: 0,
      start_date: [null],
      end_date: [null]
    })
  }

}
