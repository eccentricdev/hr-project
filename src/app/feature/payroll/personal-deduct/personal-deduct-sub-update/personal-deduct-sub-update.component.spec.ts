import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDeductSubUpdateComponent } from './personal-deduct-sub-update.component';

describe('PersonalDeductSubUpdateComponent', () => {
  let component: PersonalDeductSubUpdateComponent;
  let fixture: ComponentFixture<PersonalDeductSubUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalDeductSubUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDeductSubUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
