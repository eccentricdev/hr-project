import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PersonalDeductListComponent } from './personal-deduct-list/personal-deduct-list.component';
import { PersonalDeductFormComponent } from './personal-deduct-form/personal-deduct-form.component';
import { PersonalDeductSubCreateComponent } from './personal-deduct-sub-create/personal-deduct-sub-create.component';
import { PersonalDeductSubUpdateComponent } from './personal-deduct-sub-update/personal-deduct-sub-update.component';

const routes:Routes = [
  {
    path:'',
    component:PersonalDeductListComponent
  },
  {
    path:'add',
    component:PersonalDeductFormComponent
  },
  {
    path:'edit/:id',
    component:PersonalDeductFormComponent
  },
  {
    path:'form-add/:id',
    component:PersonalDeductSubCreateComponent
  },
  {
    path:'form-update/:id',
    component:PersonalDeductSubUpdateComponent
  },
]


@NgModule({
  declarations: [PersonalDeductListComponent, PersonalDeductFormComponent, PersonalDeductSubCreateComponent, PersonalDeductSubUpdateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PersonalDeductModule { }
