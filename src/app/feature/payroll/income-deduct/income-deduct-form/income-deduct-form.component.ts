import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { IncomeTypeService } from 'src/app/core/service/salary/income-type.service';
import { MoneyCalculationTypeService } from 'src/app/core/service/salary/money-calculation-type.service';
import { MoneyCategoryService } from 'src/app/core/service/salary/money-category.service';
import { StatusService } from 'src/app/core/service/common/status.service';

@Component({
  selector: 'app-income-deduct-form',
  templateUrl: './income-deduct-form.component.html',
  styleUrls: ['./income-deduct-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncomeDeductFormComponent extends BaseForm implements OnInit {

  rows:any = [{1:1}]

  checkbox1:boolean = false
  checkbox2:boolean = false
  checkbox3:boolean = false

  moneyCategoryDatas = []
  moneyCalculationTypeDatas = []
  incomeTypeDatas = []
  statusDatas = []
  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public MoneyTypeService:MoneyTypeService,
    public IncomeTypeService:IncomeTypeService,
    public MoneyCalculationTypeService:MoneyCalculationTypeService,
    public MoneyCategoryService:MoneyCategoryService,
    public StatusService:StatusService,
    ) {
    super(formBuilder, activeRoute);
    this.IncomeTypeService.getAll().pipe(
      tap(x => this.incomeTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyCalculationTypeService.getAll().pipe(
      tap(x => this.moneyCalculationTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyCategoryService.getAll().pipe(
      tap(x => this.moneyCategoryDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.StatusService.getAll().pipe(
      tap(x => this.statusDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.MoneyTypeService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      money_type_uid:res.money_type_uid,
      money_type_code:res.money_type_code,
      money_type_name_th:res.money_type_name_th,
      money_type_name_en:res.money_type_name_en,
      money_category_id:res.money_category_id,
      money_calculation_type_id:res.money_calculation_type_id,
      amount:res.amount,
      income_type_id:res.income_type_id,
      effective_date:res.effective_date
    })
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.MoneyTypeService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
      
        this.MoneyTypeService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  close(){
    this.router.navigate(['app/income-deduct'])
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],

      money_type_uid:[null],
      money_type_code:[''],
      money_type_name_th:[''],
      money_type_name_en:[''],
      money_category_id:[null],
      money_calculation_type_id:[null],
      amount:0,
      income_type_id:[null],
      effective_date:[null]
    })
  }

}
