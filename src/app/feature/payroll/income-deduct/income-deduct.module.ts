import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { IncomeDeductListComponent } from './income-deduct-list/income-deduct-list.component';
import { IncomeDeductFormComponent } from './income-deduct-form/income-deduct-form.component';
const routes:Routes = [
  {
    path:'',
    component:IncomeDeductListComponent
  },
  {
    path:'add',
    component:IncomeDeductFormComponent
  },
  {
    path:'edit/:id',
    component:IncomeDeductFormComponent
  },
  
]


@NgModule({
  declarations: [IncomeDeductListComponent, IncomeDeductFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class IncomeDeductModule { }
