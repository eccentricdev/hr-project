import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { IncomeTypeService } from 'src/app/core/service/salary/income-type.service';
import { MoneyCalculationTypeService } from 'src/app/core/service/salary/money-calculation-type.service';
import { MoneyCategoryService } from 'src/app/core/service/salary/money-category.service';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';

@Component({
  selector: 'app-income-deduct-list',
  templateUrl: './income-deduct-list.component.html',
  styleUrls: ['./income-deduct-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncomeDeductListComponent extends BaseList implements OnInit {
  rows: any = []
  moneyCalculationTypeDatas: any = []
  moneyCategoryDatas: any = []
  moneyType$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public MoneyTypeService:MoneyTypeService,
    public MoneyCalculationTypeService:MoneyCalculationTypeService,
    public MoneyCategoryService:MoneyCategoryService,
  ) {
    super();
    this.moneyType$ = this.MoneyTypeService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
    this.MoneyCalculationTypeService.getAll().pipe(
      tap(x => this.moneyCalculationTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.MoneyCategoryService.getAll().pipe(
      tap(x => this.moneyCategoryDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/income-deduct/add'])
  }
  editPage(id) {
    this.router.navigate(['app/income-deduct/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.MoneyTypeService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.moneyType$ = this.MoneyTypeService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}