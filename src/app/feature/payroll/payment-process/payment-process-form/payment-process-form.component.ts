import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { SalaryProcessService } from 'src/app/core/service/salary/salary-process.service';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import { PaymentPeriodService } from 'src/app/core/service/salary/payment-period.service';

@Component({
  selector: 'app-payment-process-form',
  templateUrl: './payment-process-form.component.html',
  styleUrls: ['./payment-process-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentProcessFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows: any = [{ 1: 1 }]
  periodYear: any = null
  processDate = new Date()
  organizationDatas:any = []
  paymentPeriodDatas:any = []
  paymentPeriodMonthDatas:any = []

  monthName = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public SalaryProcessService:SalaryProcessService,
    public OrganizationDataService:OrganizationDataService,
    public PaymentPeriodService:PaymentPeriodService
  ) {
    super(formBuilder, activeRoute);
    this.OrganizationDataService.getAll().pipe(
      tap(x => this.organizationDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.PaymentPeriodService.getAll().pipe(
      tap(x => this.paymentPeriodDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.SalaryProcessService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      organization_uid: res.organization_uid,
      payment_period_detail_uid: res.payment_period_detail_uid
    })
  }

  selectPeriodYear(payment_period_uid) {
    console.log(payment_period_uid)
    this.form.get('payment_period_detail_uid').setValue(null)
    this.PaymentPeriodService.get(payment_period_uid).pipe(
      tap((x: any) => {
        var dataSort = x.payment_period_details.sort((a, b) => a.row_order - b.row_order)
        this.paymentPeriodMonthDatas = dataSort
      }),
      tap(x => console.log(x)),
    ).subscribe()
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.SalaryProcessService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
console.log(this.form.getRawValue())
        this.SalaryProcessService.addSubEndpoint('/processing',this.form.getRawValue()).pipe(
          catchError(err => {
            console.log(err)
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  close() {
    this.router.navigate(['app/payment-process'])
  }

  createForm() {
    return this.baseFormBuilder.group({
      organization_uid: [null],
      payment_period_detail_uid: [null],
    })
  }

}
