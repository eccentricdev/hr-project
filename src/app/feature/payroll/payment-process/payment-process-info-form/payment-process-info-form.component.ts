import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { SalaryProcessService } from 'src/app/core/service/salary/salary-process.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SalaryProcessDetailService } from 'src/app/core/service/salary/salary-process-detail.service';

@Component({
  selector: 'app-payment-process-info-form',
  templateUrl: './payment-process-info-form.component.html',
  styleUrls: ['./payment-process-info-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentProcessInfoFormComponent extends BaseForm implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild('sorter1') sorter1: MatSort;
  @ViewChild('sorter2') sorter2: MatSort;
  @ViewChild('paginator1') paginator1: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;

  salaryProcessData: any = null
  salaryProcessDatasList$ = new Observable()

  salaryProcessDeduction: any = []
  salaryProcessIncome: any = []
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public SalaryProcessService: SalaryProcessService,
    public SalaryProcessDetailService: SalaryProcessDetailService,
    // private cdRef: ChangeDetectorRef,
  ) {
    super(formBuilder, activeRoute);
    // this.OrganizationDataService.getAll().pipe(
    //   tap(x => this.organizationDatas = x),
    //   tap(x => console.log(x))
    // ).subscribe()
    // this.PaymentPeriodService.getAll().pipe(
    //   tap(x => this.paymentPeriodDatas = x),
    //   tap(x => console.log(x))
    // ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.SalaryProcessDetailService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap((x: any) => {
            this.salaryProcessData = x
            this.salaryProcessIncome = this.updateMatTableSort1(x.salary_process_income)
            this.salaryProcessDeduction = this.updateMatTableSort2(x.salary_process_deduction)
          }),
          // tap(() => this.cdRef.detectChanges())
        ).subscribe()
        // this.SalaryProcessService.getSubEndpoint('/personnels', this.id).pipe(
        //   tap(x => console.log(x)),
        //   tap((x: any) => this.salaryProcessDatasList = this.updateMatTable(x.salary_group_detail)),
        // ).subscribe()
        // this.salaryProcessDatasList$ = this.SalaryProcessService.getSubEndpoint('/personnels', this.id).pipe(
        //   tap(x => console.log(x)),
        //   map(x => this.updateMatTable(x))
        // )
        break;
      case 'add':

        break;
    }
  }

  // updateMatTable(res: any) {
  //   console.log(this.paginator)
  //   console.log(this.sort)
  //   res = new MatTableDataSource(res)
  //   res.paginator = this.paginator
  //   res.sort = this.sort
  //   return res
  // }

  updateMatTableSort1(res: any) {
    console.log(this.paginator)
    console.log(this.sort)
    res = new MatTableDataSource(res)
    res.paginator = this.paginator1
    res.sort = this.sorter1
    return res
  }

  updateMatTableSort2(res: any) {
    console.log(this.paginator)
    console.log(this.sort)
    res = new MatTableDataSource(res)
    res.paginator = this.paginator2
    res.sort = this.sorter2
    return res
  }

  addIncome() {
    this.router.navigate(['app/payment-process/add-income', this.salaryProcessData.salary_process_detail_uid,{ state: 'add' }])
  }
  editIncome(id) {
    this.router.navigate(['app/payment-process/edit-income', id,{ state: 'edit' }])
  }

  addDeduction() {
    this.router.navigate(['app/payment-process/add-deduction', this.salaryProcessData.salary_process_detail_uid,{ state: 'add' }])
  }
  editDeduction(id) {
    this.router.navigate(['app/payment-process/edit-deduction', id,{ state: 'edit' }])
  }

  save() {
    switch (this.state) {
      case 'edit':
        // this.SalaryProcessService.update(this.form.getRawValue()).pipe(
        //   catchError(err => {
        //     this.AppService.swaltAlertError('Error')
        //     return throwError(err)
        //   })).subscribe((x: any) => {
        //     console.log(x)
        //     this.AppService.swaltAlert()
        //     this.close()
        //   })
        break;
      case 'add':
        console.log(this.form.getRawValue())
        // this.SalaryProcessService.addSubEndpoint('/processing', this.form.getRawValue()).pipe(
        //   catchError(err => {
        //     console.log(err)
        //     this.AppService.swaltAlertError('Error')
        //     return throwError(err)
        //   })).subscribe((x: any) => {
        //     console.log(x)
        //     this.AppService.swaltAlert()
        //     this.close()
        //   })
        break;
    }

  }

  close() {
    this.router.navigate(['app/payment-process/edit',this.salaryProcessData.salary_process_uid])
  }

}
