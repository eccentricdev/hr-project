import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PaymentProcessListComponent } from './payment-process-list/payment-process-list.component';
import { PaymentProcessFormComponent } from './payment-process-form/payment-process-form.component';
import { PaymentProcessInfoComponent } from './payment-process-info/payment-process-info.component';
import { PaymentProcessInfoFormComponent } from './payment-process-info-form/payment-process-info-form.component';
import { PaymentProcessInfoDeductionFormComponent } from './payment-process-info-deduction-form/payment-process-info-deduction-form.component';
import { PaymentProcessInfoIncomeFormComponent } from './payment-process-info-income-form/payment-process-info-income-form.component';

const routes:Routes = [
  {
    path:'',
    component:PaymentProcessListComponent
  },
  {
    path:'add',
    component:PaymentProcessFormComponent
  },
  {
    path:'edit/:id',
    component:PaymentProcessInfoComponent
  },
  {
    path:'edit-info/:id',
    component:PaymentProcessInfoFormComponent
  },
  {
    path:'add-income/:id',
    component:PaymentProcessInfoIncomeFormComponent
  },
  {
    path:'edit-income/:id',
    component:PaymentProcessInfoIncomeFormComponent
  },
  {
    path:'add-deduction/:id',
    component:PaymentProcessInfoDeductionFormComponent
  },
  {
    path:'edit-deduction/:id',
    component:PaymentProcessInfoDeductionFormComponent
  },
]


@NgModule({
  declarations: [PaymentProcessListComponent, PaymentProcessFormComponent, PaymentProcessInfoComponent, PaymentProcessInfoFormComponent, PaymentProcessInfoDeductionFormComponent, PaymentProcessInfoIncomeFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PaymentProcessModule { }
