import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { SalaryProcessService } from 'src/app/core/service/salary/salary-process.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-payment-process-info',
  templateUrl: './payment-process-info.component.html',
  styleUrls: ['./payment-process-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentProcessInfoComponent extends BaseForm implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  salaryProcessData: any = null
  salaryProcessDatasList$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public SalaryProcessService: SalaryProcessService,
  ) {
    super(formBuilder, activeRoute);
    // this.OrganizationDataService.getAll().pipe(
    //   tap(x => this.organizationDatas = x),
    //   tap(x => console.log(x))
    // ).subscribe()
    // this.PaymentPeriodService.getAll().pipe(
    //   tap(x => this.paymentPeriodDatas = x),
    //   tap(x => console.log(x))
    // ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.SalaryProcessService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.salaryProcessData = x),
        ).subscribe()
        // this.SalaryProcessService.getSubEndpoint('/personnels', this.id).pipe(
        //   tap(x => console.log(x)),
        //   tap((x: any) => this.salaryProcessDatasList = this.updateMatTable(x.salary_group_detail)),
        // ).subscribe()
        this.salaryProcessDatasList$ = this.SalaryProcessService.getSubEndpoint('/personnels', this.id).pipe(
          tap(x => console.log(x)),
          map(x => this.updateMatTable(x))
        )
        break;
      case 'add':

        break;
    }
  }

  updateMatTable(res: any) {
    console.log(this.paginator)
    console.log(this.sort)
    res = new MatTableDataSource(res)
    res.paginator = this.paginator
    res.sort = this.sort
    return res
  }

  edit(id){
    this.router.navigate(['app/payment-process/edit-info',id])
  }

  save() {
    switch (this.state) {
      case 'edit':
        // this.SalaryProcessService.update(this.form.getRawValue()).pipe(
        //   catchError(err => {
        //     this.AppService.swaltAlertError('Error')
        //     return throwError(err)
        //   })).subscribe((x: any) => {
        //     console.log(x)
        //     this.AppService.swaltAlert()
        //     this.close()
        //   })
        break;
      case 'add':
        console.log(this.form.getRawValue())
        // this.SalaryProcessService.addSubEndpoint('/processing', this.form.getRawValue()).pipe(
        //   catchError(err => {
        //     console.log(err)
        //     this.AppService.swaltAlertError('Error')
        //     return throwError(err)
        //   })).subscribe((x: any) => {
        //     console.log(x)
        //     this.AppService.swaltAlert()
        //     this.close()
        //   })
        break;
    }

  }

  close() {
    this.router.navigate(['app/payment-process'])
  }

}
