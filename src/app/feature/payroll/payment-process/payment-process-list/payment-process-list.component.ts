import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PaymentPeriodService } from 'src/app/core/service/salary/payment-period.service';
import { SalaryProcessService } from 'src/app/core/service/salary/salary-process.service';
// import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';

@Component({
  selector: 'app-payment-process-list',
  templateUrl: './payment-process-list.component.html',
  styleUrls: ['./payment-process-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentProcessListComponent extends BaseList implements OnInit {
  rows: any = []
  salaryProcess$ = new Observable()
  // organizations: any = []
  paymentPeriods: any = []
  monthName = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
  constructor(
    public router: Router,
    public AppService: AppService,
    public SalaryProcessService: SalaryProcessService,
    public PaymentPeriodService :PaymentPeriodService
    // public OrganizationDataService:OrganizationDataService
  ) {
    super();
    this.PaymentPeriodService.getAll().pipe(
      tap(x => this.paymentPeriods = x),
      tap(x => console.log(x))
    ).subscribe()
    this.salaryProcess$ = this.SalaryProcessService.getAll().pipe(
      tap(x => console.log(x)),
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/payment-process/add'])
  }
  editPage(id) {
    this.router.navigate(['app/payment-process/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.SalaryProcessService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.salaryProcess$ = this.SalaryProcessService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}