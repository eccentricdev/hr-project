import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { IncomeTypeService } from 'src/app/core/service/salary/income-type.service';
import { SalaryProcessDeductionService } from 'src/app/core/service/salary/salary-process-deduction.service';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';

@Component({
  selector: 'app-payment-process-info-deduction-form',
  templateUrl: './payment-process-info-deduction-form.component.html',
  styleUrls: ['./payment-process-info-deduction-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentProcessInfoDeductionFormComponent extends BaseForm implements OnInit {

  moneyTypesDatas: any = []

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public MoneyTypeService: MoneyTypeService,
    public SalaryProcessDeductionService: SalaryProcessDeductionService
  ) {
    super(formBuilder, activeRoute);
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypesDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)

    this.state = this.activeRoute.snapshot.paramMap.get('state')

    switch (this.state) {
      case 'edit':
        this.SalaryProcessDeductionService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':
        this.form.patchValue({
          salary_process_detail_uid: this.id
        })
        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,

      salary_process_deduction_uid: res.salary_process_deduction_uid,
      salary_process_detail_uid: res.salary_process_detail_uid,
      money_type_uid: res.money_type_uid,
      deduction_amount: res.deduction_amount,
      suspend_payment_status: res.suspend_payment_status
    })
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.SalaryProcessDeductionService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        console.log(this.form.getRawValue())
        this.SalaryProcessDeductionService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            console.log(err)
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  close() {
    switch (this.state) {
      case 'edit':
        this.router.navigate(['app/payment-process/edit-info', this.form.value.salary_process_detail_uid])
        break;
      case 'add':
        this.router.navigate(['app/payment-process/edit-info', this.id])
        break;
    }
    
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],

      salary_process_deduction_uid: [null],
      salary_process_detail_uid: [null],
      money_type_uid: [null],
      deduction_amount: 0,
      suspend_payment_status: false
    })
  }

}
