import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { BankTypeService } from 'src/app/core/service/salary/bank-type.service';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Component({
  selector: 'app-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BankFormComponent extends BaseForm implements OnInit {

  rows:any = [{1:1}]

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public BankTypeService:BankTypeService
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.BankTypeService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      bank_type_id:res.bank_type_id,
      bank_type_code:res.bank_type_code,
      bank_type_name_th:res.bank_type_name_th,
      bank_type_name_en:res.bank_type_name_en
    })
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.BankTypeService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
      
        this.BankTypeService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  close(){
    this.router.navigate(['app/bank'])
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],

      bank_type_id:[null],
      bank_type_code:[''],
      bank_type_name_th:[''],
      bank_type_name_en:['']
    })
  }

}
