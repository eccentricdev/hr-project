import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { BankTypeService } from 'src/app/core/service/salary/bank-type.service';

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BankListComponent extends BaseList implements OnInit {
  rows: any = []
  bankType$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public BankTypeService: BankTypeService
  ) {
    super();
    this.bankType$ = this.BankTypeService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/bank/add'])
  }
  editPage(id) {
    this.router.navigate(['app/bank/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.BankTypeService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.bankType$ = this.BankTypeService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}