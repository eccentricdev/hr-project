import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { BankListComponent } from './bank-list/bank-list.component';
import { BankFormComponent } from './bank-form/bank-form.component';

const routes:Routes = [
  {
    path:'',
    component:BankListComponent
  },
  {
    path:'add',
    component:BankFormComponent
  },
  {
    path:'edit/:id',
    component:BankFormComponent
  },
  
]


@NgModule({
  declarations: [BankListComponent, BankFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class BankModule { }
