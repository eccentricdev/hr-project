import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ProvidentFundService } from 'src/app/core/service/salary/provident-fund.service';

@Component({
  selector: 'app-provident-fund-list',
  templateUrl: './provident-fund-list.component.html',
  styleUrls: ['./provident-fund-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProvidentFundListComponent extends BaseList implements OnInit {
  rows: any = []
  providentFund$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public ProvidentFundService:ProvidentFundService
  ) {
    super();
    this.providentFund$ = this.ProvidentFundService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/provident-fund/add'])
  }
  editPage(id) {
    this.router.navigate(['app/provident-fund/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.ProvidentFundService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.providentFund$ = this.ProvidentFundService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}