import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProvidentFundListComponent } from './provident-fund-list/provident-fund-list.component';
import { ProvidentFundFormComponent } from './provident-fund-form/provident-fund-form.component';

const routes:Routes = [
  {
    path:'',
    component:ProvidentFundListComponent
  },
  {
    path:'add',
    component:ProvidentFundFormComponent
  },
  {
    path:'edit/:id',
    component:ProvidentFundFormComponent
  },
  
]


@NgModule({
  declarations: [ProvidentFundListComponent, ProvidentFundFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ProvidentFundModule { }
