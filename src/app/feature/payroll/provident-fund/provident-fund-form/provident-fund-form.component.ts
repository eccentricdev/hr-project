import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { catchError, tap } from 'rxjs/operators';
import { ProvidentFundService } from 'src/app/core/service/salary/provident-fund.service';
import { throwError } from 'rxjs';
import { MoneyTypeService } from 'src/app/core/service/salary/money-type.service';

@Component({
  selector: 'app-provident-fund-form',
  templateUrl: './provident-fund-form.component.html',
  styleUrls: ['./provident-fund-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProvidentFundFormComponent extends BaseForm implements OnInit {

  rows: any = [{ 1: 1 }]
  moneyTypeDatas:any = []
  
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public ProvidentFundService:ProvidentFundService,
    public MoneyTypeService:MoneyTypeService
  ) {
    super(formBuilder, activeRoute);
    this.MoneyTypeService.getAll().pipe(
      tap(x => this.moneyTypeDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.ProvidentFundService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      provident_fund_uid: res.provident_fund_uid,
      provident_fund_code: res.provident_fund_code,
      provident_fund_name_th: res.provident_fund_name_th,
      provident_fund_name_en: res.provident_fund_name_en,
      provident_fund_short_name: res.provident_fund_short_name,
      provident_fund_no: res.provident_fund_no,
      employee_money_type_uid: res.employee_money_type_uid,
      employer_money_type_uid: res.employer_money_type_uid,
      employee_contribute_money_type_uid: res.employee_contribute_money_type_uid,
      employee_contribute_amount: res.employee_contribute_amount,
      employer_contribute_amount: res.employer_contribute_amount,
      remark: res.remark
    })
  }

  close() {
    this.router.navigate(['app/provident-fund'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.ProvidentFundService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.ProvidentFundService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      provident_fund_uid: [null],
      provident_fund_code: [''],
      provident_fund_name_th: [''],
      provident_fund_name_en: [''],
      provident_fund_short_name: [''],
      provident_fund_no: [''],
      employee_money_type_uid: [null],
      employer_money_type_uid: [null],
      employee_contribute_money_type_uid: [null],
      employee_contribute_amount: 0,
      employer_contribute_amount: 0,
      remark: ['']
    })
  }

}
