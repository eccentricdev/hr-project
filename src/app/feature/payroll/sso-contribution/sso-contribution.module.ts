import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SsoContributionListComponent } from './sso-contribution-list/sso-contribution-list.component';
import { SsoContributionFormComponent } from './sso-contribution-form/sso-contribution-form.component';

const routes:Routes = [
  {
    path:'',
    component:SsoContributionListComponent
  },
  {
    path:'add',
    component:SsoContributionFormComponent
  },
  {
    path:'edit/:id',
    component:SsoContributionFormComponent
  },
  
]


@NgModule({
  declarations: [SsoContributionListComponent, SsoContributionFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SsoContributionModule { }
