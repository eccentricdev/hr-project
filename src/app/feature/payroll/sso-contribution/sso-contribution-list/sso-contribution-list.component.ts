import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { BankTypeService } from 'src/app/core/service/salary/bank-type.service';
import { SocialSecurityService } from 'src/app/core/service/salary/social-security.service';

@Component({
  selector: 'app-sso-contribution-list',
  templateUrl: './sso-contribution-list.component.html',
  styleUrls: ['./sso-contribution-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SsoContributionListComponent extends BaseList implements OnInit {
  rows: any = []
  personnelDatas: any = []
  socialSecurity$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public PersonnelService: PersonnelService,
    public SocialSecurityService:SocialSecurityService
  ) {
    super();
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
    this.socialSecurity$ = this.SocialSecurityService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/sso-contribution/add'])
  }
  editPage(id) {
    this.router.navigate(['app/sso-contribution/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.SocialSecurityService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.socialSecurity$ = this.SocialSecurityService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}