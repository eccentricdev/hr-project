import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { catchError, tap } from 'rxjs/operators';
import { SocialSecurityService } from 'src/app/core/service/salary/social-security.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-sso-contribution-form',
  templateUrl: './sso-contribution-form.component.html',
  styleUrls: ['./sso-contribution-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SsoContributionFormComponent extends BaseForm implements OnInit {

  rows: any = [{ 1: 1 }]
  personnelDatas: any = []
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public PersonnelService: PersonnelService,
    public SocialSecurityService: SocialSecurityService
  ) {
    super(formBuilder, activeRoute);
    this.PersonnelService.getAll().pipe(
      tap(x => this.personnelDatas = x),
      tap(x => console.log(x))
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.SocialSecurityService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      social_security_uid: res.social_security_uid,
      personnel_uid: res.personnel_uid,
      is_deduct: res.is_deduct,
      social_security_no: res.social_security_no,
      has_previous_social_security: res.has_previous_social_security,
      previous_company_name: res.previous_company_name,
      hospital_name: res.hospital_name,
      hospital1_name: res.hospital1_name,
      hospital2_name: res.hospital2_name,
    })
  }

  close() {
    this.router.navigate(['app/sso-contribution'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.SocialSecurityService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.SocialSecurityService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      social_security_uid: [null],
      personnel_uid: [null],
      is_deduct: [null],
      social_security_no: [''],
      has_previous_social_security: [null],
      previous_company_name: [''],
      hospital_name: [''],
      hospital1_name: [''],
      hospital2_name: ['']
    })
  }

}
