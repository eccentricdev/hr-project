import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { SalaryGroupService } from 'src/app/core/service/salary/salary-group.service';

@Component({
  selector: 'app-salary-group-list',
  templateUrl: './salary-group-list.component.html',
  styleUrls: ['./salary-group-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SalaryGroupListComponent extends BaseList implements OnInit {
  rows: any = []
  salaryGroup$ = new Observable()
  constructor(
    public router: Router,
    public AppService: AppService,
    public SalaryGroupService:SalaryGroupService
  ) {
    super();
    this.salaryGroup$ = this.SalaryGroupService.getAll().pipe(
      map(x => this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/salary-group/add'])
  }
  editPage(id) {
    this.router.navigate(['app/salary-group/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
      if (action) {
        this.SalaryGroupService.deleteData(id).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.salaryGroup$ = this.SalaryGroupService.getAll()
            this.AppService.swaltAlert('ลบข้อมูลสำเสร็จ')
          })
      }
    })
  }
}