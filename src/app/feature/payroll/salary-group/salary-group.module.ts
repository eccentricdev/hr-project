import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalaryGroupListComponent } from './salary-group-list/salary-group-list.component';
import { SalaryGroupFormComponent } from './salary-group-form/salary-group-form.component';
import { CreateSubSalaryGroupComponent } from './salary-group-form/create-sub-salary-group/create-sub-salary-group.component';

const routes:Routes = [
  {
    path:'',
    component:SalaryGroupListComponent
  },
  {
    path:'add',
    component:SalaryGroupFormComponent
  },
  {
    path:'edit/:id',
    component:SalaryGroupFormComponent
  },
  
]


@NgModule({
  declarations: [SalaryGroupListComponent, SalaryGroupFormComponent, CreateSubSalaryGroupComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SalaryGroupModule { }
