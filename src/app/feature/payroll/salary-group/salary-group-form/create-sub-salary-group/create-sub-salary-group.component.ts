import { Component, OnInit, ChangeDetectionStrategy, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-sub-salary-group',
  templateUrl: './create-sub-salary-group.component.html',
  styleUrls: ['./create-sub-salary-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateSubSalaryGroupComponent implements OnInit {
  state: any
  dataReturn = {
    running_wage: 0,
    salary_wage_rate: 0,
    daily_wage_rate: 0,
    hour_wage_rate: 0
  }
  @ViewChild('running_wage') running_wage: ElementRef;
  @ViewChild('salary_wage_rate') salary_wage_rate: ElementRef;
  @ViewChild('daily_wage_rate') daily_wage_rate: ElementRef;
  @ViewChild('hour_wage_rate') hour_wage_rate: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<CreateSubSalaryGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  checkNull(event) {
    switch (event) {
      case 'running_wage':
        if (!this.running_wage.nativeElement.value) {
          this.running_wage.nativeElement.value = 0
        }
        break;
      case 'salary_wage_rate':
        if (!this.salary_wage_rate.nativeElement.value) {
          this.salary_wage_rate.nativeElement.value = 0
        }
        break;
      case 'daily_wage_rate':
        if (!this.daily_wage_rate.nativeElement.value) {
          this.daily_wage_rate.nativeElement.value = 0
        }
        break;
      case 'hour_wage_rate':
        if (!this.hour_wage_rate.nativeElement.value) {
          this.hour_wage_rate.nativeElement.value = 0
        }
        break;
    }
  }

  ngOnInit(): void {
    this.state = this.data[0].state
    switch (this.state) {
      case 'add':
        break;
      case 'edit':
        var data = this.data[0].data
        console.log(this.data[0].data)
        this.dataReturn = {
          running_wage: data.running_wage,
          salary_wage_rate: data.salary_wage_rate,
          daily_wage_rate: data.daily_wage_rate,
          hour_wage_rate: data.hour_wage_rate,
        }
        break;
    }
  }

  beforeSave() {
    this.save()
  }

  save() {
    var res = {
      state: this.state,
      id: null,
      data: this.dataReturn,
      oldData: this.data[0].data,
      index: this.data[0].index
    }
    this.close(res)
  }

  close(res) {
    this.dialogRef.close(res)
  }

}
