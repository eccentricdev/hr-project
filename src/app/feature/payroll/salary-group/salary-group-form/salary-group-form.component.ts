import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { SalaryGroupService } from 'src/app/core/service/salary/salary-group.service';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CreateSubSalaryGroupComponent } from './create-sub-salary-group/create-sub-salary-group.component';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: 'app-salary-group-form',
  templateUrl: './salary-group-form.component.html',
  styleUrls: ['./salary-group-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SalaryGroupFormComponent extends BaseForm implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  rows: any = [{ 1: 1 }]

  salaryGroups: any = []

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog,
    public SalaryGroupService: SalaryGroupService,
    private cdRef: ChangeDetectorRef
  ) {
    super(formBuilder, activeRoute);
  }

  updateMatTable(res: any) {
    console.log(this.paginator)
    console.log(this.sort)
    res = new MatTableDataSource(res)
    res.paginator = this.paginator
    res.sort = this.sort
    return res
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.SalaryGroupService.get(this.id).pipe(
          tap(res => this.setForm(res)),
          tap((x: any) => this.salaryGroups = this.updateMatTable(x.salary_group_detail)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  manageForm(state, data = null, index = 0) {
    const dialogRef = this.dialog.open(
      CreateSubSalaryGroupComponent, {
      width: '70%',
      disableClose: true,
      data: [{
        state: state,
        data: data,
        index: index
      }]
    }
    )

    dialogRef.afterClosed().subscribe((callback: any) => {
      console.log(callback)
      let salary_group_detail = this.form.get('salary_group_detail') as FormArray
      switch (callback.state) {
        case 'add':
          console.log('add')
          salary_group_detail.push(this.createFormSalaryGroupDetail(callback.data, callback.id))
          this.salaryGroups = this.updateMatTable(this.form.getRawValue().salary_group_detail)
          this.cdRef.detectChanges()
          break;
        case 'edit':
          console.log('edit')
          salary_group_detail.controls[callback.index].patchValue({
            status_id: callback.oldData.status_id,
            created_by: callback.oldData.created_by,
            created_datetime: callback.oldData.created_datetime,
            updated_by: callback.oldData.updated_by,
            updated_datetime: callback.oldData.updated_datetime,
            salary_group_detail_uid: callback.oldData.salary_group_detail_uid,
            salary_group_uid: callback.oldData.salary_group_uid,
            running_wage: callback.data.running_wage,
            salary_wage_rate: callback.data.salary_wage_rate,
            daily_wage_rate: callback.data.daily_wage_rate,
            hour_wage_rate: callback.data.hour_wage_rate
          })
          this.salaryGroups = this.updateMatTable(this.form.getRawValue().salary_group_detail)
          this.cdRef.detectChanges()
          break;
        default:
          break;
      }
    })
  }

  createFormSalaryGroupDetail(res, id) {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      salary_group_detail_uid: [null],
      salary_group_uid: id,
      running_wage: res.running_wage,
      salary_wage_rate: res.salary_wage_rate,
      daily_wage_rate: res.daily_wage_rate,
      hour_wage_rate: res.hour_wage_rate,
    })
  }

  setForm(res) {
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      salary_group_uid: res.salary_group_uid,
      salary_group_code: res.salary_group_code,
      salary_group_name_th: res.salary_group_name_th
    })
    res.salary_group_detail.forEach(element => {
      this.setSalaryGroupDetail(element)
    });
  }

  setSalaryGroupDetail(res) {
    (<FormArray>this.form.get('salary_group_detail')).push(this.createFormSalaryGroupDetailEdit(res))
  }

  createFormSalaryGroupDetailEdit(res) {
    return this.baseFormBuilder.group({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      salary_group_detail_uid: res.salary_group_detail_uid,
      salary_group_uid: res.salary_group_uid,
      running_wage: res.running_wage,
      salary_wage_rate: res.salary_wage_rate,
      daily_wage_rate: res.daily_wage_rate,
      hour_wage_rate: res.hour_wage_rate,
    })
  }

  deleteItem(index) {
    (<FormArray>this.form.get('salary_group_detail')).removeAt(index)
    this.salaryGroups = this.updateMatTable(this.form.getRawValue().salary_group_detail)
  }

  close() {
    this.router.navigate(['app/salary-group'])
  }

  closeAfterSave(id) {
    this.router.navigate(['app/salary-group/edit/', id])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.SalaryGroupService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.close()
          })
        break;
      case 'add':
        this.SalaryGroupService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.AppService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.AppService.swaltAlert()
            this.closeAfterSave(x.salary_group_uid)
          })
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      salary_group_uid: [null],
      salary_group_code: [''],
      salary_group_name_th: [''],
      salary_group_detail: this.baseFormBuilder.array([])
    })
  }

}
