import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalaryAdjustListComponent } from './salary-adjust-list/salary-adjust-list.component';
import { SalaryAdjustFormComponent } from './salary-adjust-form/salary-adjust-form.component';

const routes:Routes = [
  {
    path:'',
    component:SalaryAdjustListComponent
  },
  {
    path:'add',
    component:SalaryAdjustFormComponent
  },
  {
    path:'edit/:id',
    component:SalaryAdjustFormComponent
  },
  
]


@NgModule({
  declarations: [SalaryAdjustListComponent, SalaryAdjustFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SalaryAdjustModule { }
