import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-salary-adjust-list',
  templateUrl: './salary-adjust-list.component.html',
  styleUrls: ['./salary-adjust-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SalaryAdjustListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/salary-adjust/add'])
  }
  editPage(id) {
    this.router.navigate(['app/salary-adjust/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}