import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralLeaveListComponent } from './general-leave-list/general-leave-list.component';
import { GeneralLeaveFormComponent } from './general-leave-form/general-leave-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:GeneralLeaveListComponent
  },
  {
    path:'add',
    component:GeneralLeaveFormComponent
  },
  {
    path:'edit/:id',
    component:GeneralLeaveFormComponent
  },
  
]

@NgModule({
  declarations: [GeneralLeaveListComponent, GeneralLeaveFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class GeneralLeaveModule { }
