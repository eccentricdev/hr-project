import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralLeaveFormComponent } from './general-leave-form.component';

describe('GeneralLeaveFormComponent', () => {
  let component: GeneralLeaveFormComponent;
  let fixture: ComponentFixture<GeneralLeaveFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralLeaveFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralLeaveFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
