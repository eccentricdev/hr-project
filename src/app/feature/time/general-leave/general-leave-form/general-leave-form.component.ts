import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
@Component({
  selector: 'app-general-leave-form',
  templateUrl: './general-leave-form.component.html',
  styleUrls: ['./general-leave-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralLeaveFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  addfile(){
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: []  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  close(){
    this.router.navigate(['app/general_leave'])
  }

  save() {
    this.router.navigate(['app/general_leave'])
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
