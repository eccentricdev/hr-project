import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';
@Component({
  selector: 'app-general-leave-list',
  templateUrl: './general-leave-list.component.html',
  styleUrls: ['./general-leave-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralLeaveListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/general_leave/add'])
  }
  editPage(id) {
    this.router.navigate(['app/general_leave/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}