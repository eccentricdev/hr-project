import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralLeaveListComponent } from './general-leave-list.component';

describe('GeneralLeaveListComponent', () => {
  let component: GeneralLeaveListComponent;
  let fixture: ComponentFixture<GeneralLeaveListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralLeaveListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralLeaveListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
