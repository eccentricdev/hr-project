import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtDialogPersonOtComponent } from './ot-dialog-person-ot.component';

describe('OtDialogPersonOtComponent', () => {
  let component: OtDialogPersonOtComponent;
  let fixture: ComponentFixture<OtDialogPersonOtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtDialogPersonOtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtDialogPersonOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
