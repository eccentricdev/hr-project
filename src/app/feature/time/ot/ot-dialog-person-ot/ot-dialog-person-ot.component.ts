import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-ot-dialog-person-ot',
  templateUrl: './ot-dialog-person-ot.component.html',
  styleUrls: ['./ot-dialog-person-ot.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OtDialogPersonOtComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<OtDialogPersonOtComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
  }

  save(){
    this.close()
  }

  close(){
    this.dialogRef.close('close')
  }

}
