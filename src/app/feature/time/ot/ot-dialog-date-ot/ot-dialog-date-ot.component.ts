import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-ot-dialog-date-ot',
  templateUrl: './ot-dialog-date-ot.component.html',
  styleUrls: ['./ot-dialog-date-ot.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OtDialogDateOtComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
