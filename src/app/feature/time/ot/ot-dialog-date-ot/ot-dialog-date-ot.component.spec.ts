import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtDialogDateOtComponent } from './ot-dialog-date-ot.component';

describe('OtDialogDateOtComponent', () => {
  let component: OtDialogDateOtComponent;
  let fixture: ComponentFixture<OtDialogDateOtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtDialogDateOtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtDialogDateOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
