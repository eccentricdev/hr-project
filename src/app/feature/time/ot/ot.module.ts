import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OtFormComponent } from './ot-form/ot-form.component';
import { OtListComponent } from './ot-list/ot-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { OtDialogPersonOtComponent } from './ot-dialog-person-ot/ot-dialog-person-ot.component';
import { OtDialogDateOtComponent } from './ot-dialog-date-ot/ot-dialog-date-ot.component';

const routes:Routes = [
  {
    path:'',
    component:OtListComponent
  },
  {
    path:'add',
    component:OtFormComponent
  },
  {
    path:'edit/:id',
    component:OtFormComponent
  },
  
]


@NgModule({
  declarations: [OtFormComponent, OtListComponent, OtDialogPersonOtComponent, OtDialogDateOtComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class OtModule { }
