import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveEducationFormComponent } from './leave-education-form.component';

describe('LeaveEducationFormComponent', () => {
  let component: LeaveEducationFormComponent;
  let fixture: ComponentFixture<LeaveEducationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaveEducationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveEducationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
