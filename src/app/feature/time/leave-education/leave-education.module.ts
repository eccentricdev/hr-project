import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveEducationFormComponent } from './leave-education-form/leave-education-form.component';
import { LeaveEducationListComponent } from './leave-education-list/leave-education-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:LeaveEducationListComponent
  },
  {
    path:'add',
    component:LeaveEducationFormComponent
  },
  {
    path:'edit/:id',
    component:LeaveEducationFormComponent
  },
  
]


@NgModule({
  declarations: [LeaveEducationFormComponent, LeaveEducationListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class LeaveEducationModule { }
