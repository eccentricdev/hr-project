import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveEducationListComponent } from './leave-education-list.component';

describe('LeaveEducationListComponent', () => {
  let component: LeaveEducationListComponent;
  let fixture: ComponentFixture<LeaveEducationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaveEducationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveEducationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
