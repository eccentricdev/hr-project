import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-leave-education-list',
  templateUrl: './leave-education-list.component.html',
  styleUrls: ['./leave-education-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeaveEducationListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/leave_education/add'])
  }
  editPage(id) {
    this.router.navigate(['app/leave_education/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}