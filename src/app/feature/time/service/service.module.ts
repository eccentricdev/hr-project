import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceFormComponent } from './service-form/service-form.component';
import { ServiceListComponent } from './service-list/service-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ServiceListComponent
  },
  {
    path:'add',
    component:ServiceFormComponent
  },
  {
    path:'edit/:id',
    component:ServiceFormComponent
  },
  
]


@NgModule({
  declarations: [ServiceFormComponent, ServiceListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ServiceModule { }
