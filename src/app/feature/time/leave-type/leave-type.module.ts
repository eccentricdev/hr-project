import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveTypeFormComponent } from './leave-type-form/leave-type-form.component';
import { LeaveTypeListComponent } from './leave-type-list/leave-type-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:LeaveTypeListComponent
  },
  {
    path:'add',
    component:LeaveTypeFormComponent
  },
  {
    path:'edit/:id',
    component:LeaveTypeFormComponent
  },
  
]


@NgModule({
  declarations: [LeaveTypeFormComponent, LeaveTypeListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class LeaveTypeModule { }
