import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-leave-type-list',
  templateUrl: './leave-type-list.component.html',
  styleUrls: ['./leave-type-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeaveTypeListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/leave_type/add'])
  }
  editPage(id) {
    this.router.navigate(['app/leave_type/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}