import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-leave-type-form',
  templateUrl: './leave-type-form.component.html',
  styleUrls: ['./leave-type-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeaveTypeFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]

  checkboxTime:boolean = false
  checkboxPerson:boolean = false
  checkboxRemark:boolean = false

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  close(){
    this.router.navigate(['app/leave_type'])
  }

  save() {
    this.router.navigate(['app/leave_type'])
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
