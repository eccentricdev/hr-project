import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-workshift-form',
  templateUrl: './workshift-form.component.html',
  styleUrls: ['./workshift-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkshiftFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]

  checkboxDay1:boolean = false
  checkboxDay2:boolean = false
  checkboxDay3:boolean = false
  checkboxDay4:boolean = false
  checkboxDay5:boolean = false
  checkboxDay6:boolean = false
  checkboxDay7:boolean = false

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public AppService: AppService,
    public dialog: MatDialog
    ) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  close(){
    this.router.navigate(['app/workshift'])
  }

  save() {
    this.router.navigate(['app/workshift'])
  }

  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }

}
