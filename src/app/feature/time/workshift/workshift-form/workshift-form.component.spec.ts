import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshiftFormComponent } from './workshift-form.component';

describe('WorkshiftFormComponent', () => {
  let component: WorkshiftFormComponent;
  let fixture: ComponentFixture<WorkshiftFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkshiftFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshiftFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
