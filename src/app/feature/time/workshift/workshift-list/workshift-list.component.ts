import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-workshift-list',
  templateUrl: './workshift-list.component.html',
  styleUrls: ['./workshift-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkshiftListComponent implements OnInit {
  rows: any = []
  constructor(
    public router: Router,
    public AppService: AppService
  ) { }

  ngOnInit(): void {
  }
  addPage() {
    this.router.navigate(['app/workshift/add'])
  }
  editPage(id) {
    this.router.navigate(['app/workshift/edit', id])
  }

  deleteItem(id) {
    this.AppService.delete().then(action => {
      console.log(action)
    })
  }
}