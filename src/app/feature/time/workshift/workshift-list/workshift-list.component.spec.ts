import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshiftListComponent } from './workshift-list.component';

describe('WorkshiftListComponent', () => {
  let component: WorkshiftListComponent;
  let fixture: ComponentFixture<WorkshiftListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkshiftListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshiftListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
