import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkshiftFormComponent } from './workshift-form/workshift-form.component';
import { WorkshiftListComponent } from './workshift-list/workshift-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:WorkshiftListComponent
  },
  {
    path:'add',
    component:WorkshiftFormComponent
  },
  {
    path:'edit/:id',
    component:WorkshiftFormComponent
  },
  
]


@NgModule({
  declarations: [WorkshiftFormComponent, WorkshiftListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class WorkshiftModule { }
