import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HolidayListComponent } from './holiday-list/holiday-list.component';
import { HolidayFormComponent } from './holiday-form/holiday-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:HolidayListComponent
  },
  {
    path:'add',
    component:HolidayFormComponent
  },
  {
    path:'edit/:id',
    component:HolidayFormComponent
  },
  
]


@NgModule({
  declarations: [HolidayListComponent, HolidayFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class HolidayModule { }
