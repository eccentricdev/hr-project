import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveRequestPeopleListComponent } from './save-request-people-list/save-request-people-list.component';
import { SaveRequestPeopleFormComponent } from './save-request-people-form/save-request-people-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupDetailManpowersComponent } from './popup-detail-manpowers/popup-detail-manpowers.component';

const routes:Routes = [
  {
    path:'',
    component:SaveRequestPeopleListComponent
  },
  {
    path:'add',
    component:SaveRequestPeopleFormComponent
  },
  {
    path:'edit/:id',
    component:SaveRequestPeopleFormComponent
  },
  
]

@NgModule({
  declarations: [
    SaveRequestPeopleListComponent, 
    SaveRequestPeopleFormComponent, 
    PopupDetailManpowersComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveRequestPeopleModule { }
