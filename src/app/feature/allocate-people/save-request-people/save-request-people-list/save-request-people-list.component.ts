import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, map, tap } from 'rxjs/operators';
import { configs } from 'src/app/core/configs/config';
import { ManpowerAssignmentsService } from 'src/app/core/service/allocate-people/manpower-assignments.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { AppService } from 'src/app/core/service/app.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-request-people-list',
  templateUrl: './save-request-people-list.component.html',
  styleUrls: ['./save-request-people-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveRequestPeopleListComponent implements OnInit {

  @ViewChild("t1Sort") sort1: MatSort;
  @ViewChild("t2Sort") sort2: MatSort;
  @ViewChild("t1Paginator") pagt1: MatPaginator;
  @ViewChild("t2Paginator") pagt2: MatPaginator

  dataSearch = {
    agency_uid: null,
    position_uid: null,
    request_start_date:null,
    request_end_date:null
  }

  rows1: any = []
  rows2: any = []
  dataSource
  RequestsDatas: any = []
  agencysDatas: any = []
  RequestsDatas$ = new Observable()
  datasDisplay1$ = new Observable()
  datasDisplay2$ = new Observable()
  positionDatas: any = []
  startDate:any
  endDate:any
  constructor(public router: Router,
    private cdRef: ChangeDetectorRef,
    public appSV: AppService,
    public datePipe : DatePipe,
    public manpowerAssignmentsService: ManpowerAssignmentsService,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public manpowerRequestsService: ManpowerRequestsService) {
    this.RequestsDatas$ = this.manpowerRequestsService.getAll()
    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()

    this.agencysService.getAll().pipe(
      tap(x => this.agencysDatas = x)
    ).subscribe()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService() {
    this.manpowerRequestsService.getAll().pipe(
      tap(x => console.log(x)),
      map((x:any) => x.filter(x => x.approve_status_id == 2)),
      tap(x => {
        console.log(x);
        this.rows1 = new MatTableDataSource(x)
        this.rows1.paginator = this.pagt1
        this.rows1.sort = this.sort1
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

    this.manpowerAssignmentsService.getAll().pipe(
      tap(x => x),
      tap(x => console.log(x)),
      tap(x => {
        this.rows2 = new MatTableDataSource(x)
        this.rows2.paginator = this.pagt2
        this.rows2.sort = this.sort2
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

  }

  search() {
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.request_start_date = start
    this.dataSearch.request_end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay1$ = this.manpowerRequestsService.query(`?${queryStr}`)
      this.datasDisplay2$ = this.manpowerAssignmentsService.query(`?${queryStr}`)
      this.datasDisplay1$.pipe(
        tap((x: any) => {
          this.rows1 = new MatTableDataSource(x)
          this.rows1.paginator = this.pagt1
          this.rows1.sort = this.sort1
        })
      ).subscribe()
      this.datasDisplay2$.pipe(
        tap((x: any) => {
          console.log(x);
          this.rows2 = new MatTableDataSource(x)
          this.rows2.paginator = this.pagt2
          this.rows2.sort = this.sort2
        })
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      agency_uid: null,
      position_uid: null,
      request_start_date:null,
    request_end_date:null
    }
    this.getService()
  }


  // addPage(){
  //   this.router.navigate(['app/save_request_people/add']) 
  // }
  editPage1(id) {
    this.router.navigate(['app/save_request_people/edit', id])
    this.manpowerAssignmentsService.nextState('no')
  }

  editPage2(id) {
    this.router.navigate(['app/save_request_people/edit', id])
    this.manpowerAssignmentsService.nextState('yes')
  }

}
