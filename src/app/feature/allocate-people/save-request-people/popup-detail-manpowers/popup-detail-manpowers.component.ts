import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { PositionService } from 'src/app/core/service/organization/position.service';

@Component({
  selector: 'app-popup-detail-manpowers',
  templateUrl: './popup-detail-manpowers.component.html',
  styleUrls: ['./popup-detail-manpowers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupDetailManpowersComponent  implements OnInit {
  

  formDetails: any
  manpowers$ = new Observable()
  position$ = new Observable()
  file:any = {
    status_id: null,
    manpower_status_id:null,
    manpower_using_status_id:null,
    created_by: "",
    created_datetime: null,
    updated_by: "",
    updated_datetime: null,
    manpower_assignment_detail_uid: null,
    manpower_assignment_uid: null,
    manpower_uid: null,
    salary_amount:0,
    row_order: 0
  }
  constructor( public dialogRef: MatDialogRef<PopupDetailManpowersComponent>,
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public positionService : PositionService,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    public manpowersService : ManpowersService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.manpowers$ = this.manpowersService.getAll().pipe(
        tap(x => console.log(x)),
        map((res:any) => res.filter(x => x.position_uid == this.data.dataAss.position_uid && x.manpower_status_id == 1)),
        tap(x => console.log(x))
      )
      this.position$ = this.positionService.getAll()
     }

  ngOnInit(): void {
    console.log(this.data);
  
    
    if(this.data.editForm != null){
      this.getArrayDetails(this.data.editForm)
    }
  }


  getStatus(item){
    console.log(item);
    this.manpowersService.get(item).pipe(
      tap(x => console.log(x)),
      tap((x:any) => {
        this.getArrayDetails(x)
        // this.file.manpower_status_id = x.manpower_status_id
        // this.file.
      })
    ).subscribe()
  }

  
  



  

  getArrayDetails(el) {
    console.log(el);
    this.file = {
      status_id: el.status_id,
      manpower_status_id:el.manpower_status_id,
      manpower_using_status_id:el.manpower_using_status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      manpower_assignment_detail_uid: el.manpower_assignment_detail_uid,
      manpower_assignment_uid: el.manpower_assignment_uid,
      manpower_uid: el.manpower_uid,
      salary_amount:el.salary_amount,
      row_order: el.row_order
    }
  }

  createArrayDetails(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        manpower_status_id:null,
        manpower_using_status_id:null,
        created_by: '',
        created_datetime: null,
        updated_by: '',
        updated_datetime: null,
        manpower_assignment_detail_uid: null,
        manpower_assignment_uid: null,
        manpower_uid: el.manpower_uid,
        salary_amount:el.salary_amount,
        row_order: 0
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        manpower_status_id:el.manpower_status_id,
        manpower_using_status_id:el.manpower_using_status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        manpower_assignment_detail_uid: el.manpower_assignment_detail_uid,
        manpower_assignment_uid: el.manpower_assignment_uid,
        manpower_uid: el.manpower_uid,
        salary_amount:el.salary_amount,
        row_order: el.row_order
      })
    }
  }

  

  save(){
    console.log(this.data.dataAss.no_of_manpower);
    console.log(this.data.form.value.manpower_assignment_details.length);
    if(this.data.form.value.manpower_assignment_details.length < this.data.dataAss.no_of_manpower){
      if(this.data.editForm == null){
        (<FormArray>this.data.form.get('manpower_assignment_details')).push(this.createArrayDetails(this.file))  
      }else{
        (<FormArray>this.data.form.get('manpower_assignment_details')).controls[this.data.ind].patchValue({
          manpower_uid:this.file.manpower_uid,
          salary_amount:this.file.salary_amount
        })
      }
    }else{
      this.appService.swaltAlertError('อัตรากำลังที่ขอเท่ากับรายละเอียดการให้อัตรา ไม่สามารถเพิ่มข้อมูลได้')
    }
   
  
  }

  saveClose(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('manpower_assignment_details')).push(this.createArrayDetails(this.file))  
      this.close()
    }else{
      (<FormArray>this.data.form.get('manpower_assignment_details')).controls[this.data.ind].patchValue({
        manpower_uid:this.file.manpower_uid,
        salary_amount:this.file.salary_amount
      })
      this.close()
    }
  }
  close(){
  this.dialogRef.close('close')
  }

}
