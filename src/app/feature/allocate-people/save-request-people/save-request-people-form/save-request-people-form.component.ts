import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { element } from 'protractor';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap, throttleTime } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowerAssignmentsService } from 'src/app/core/service/allocate-people/manpower-assignments.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupDetailManpowersComponent } from '../popup-detail-manpowers/popup-detail-manpowers.component';
import swal from 'sweetalert2'
import { ManpowerAssignmentDocumentsService } from 'src/app/core/service/allocate-people/manpower-assignment-documents.service';


@Component({
  selector: 'app-save-request-people-form',
  templateUrl: './save-request-people-form.component.html',
  styleUrls: ['./save-request-people-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveRequestPeopleFormComponent extends BaseForm implements OnInit {

  rows: any = []
  manpowerRequests$ = new Observable()
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  qualificationsDats: any[]
  educationLevelsDatas: any = []
  positionDatas: any = []
  manpower: any = []
  agencysDatas: any = []
  dataSource
  dataEdit: any
  dataEditAss: any
  dataType: any


  constructor(public formBuilder: FormBuilder,
    public manpowerAssignmentDocumentsService: ManpowerAssignmentDocumentsService,
    public manpowerAssignmentsService: ManpowerAssignmentsService,
    public manpowerRequestsService: ManpowerRequestsService,
    public manpowersService: ManpowersService,
    public educationLevelsService: EducationLevelsService,
    public qualificationsService: QualificationsService,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public router: Router,
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public appService: AppService,) {
    super(formBuilder, activeRoute);
    this.manpowersService.getAll().pipe(
      tap(x => this.manpower = x)
    ).subscribe()

    this.manpowerRequests$ = this.manpowerRequestsService.getAll().pipe(
      tap(x => console.log(x)),
    )
    this.qualificationsService.getAll().pipe(
      tap(x => this.qualificationsDats = x)
    ).subscribe()

    this.educationLevelsService.getAll().pipe(
      tap(x => this.educationLevelsDatas = x)
    ).subscribe()

    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()

    this.agencysService.getAll().pipe(
      tap(x => this.agencysDatas = x)
    ).subscribe()
  }

  ngOnInit(): void {

    console.log(this.id)
    console.log(this.state)




    // let setName = 'MR' + num + 1
    // console.log(setName);
    // this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':

        this.manpowerAssignmentsService.getResultType().pipe(
          tap(x => console.log(x)),
          tap(x => {
            this.dataType = x
            if (x == 'no') {
              this.manpowerRequestsService.get(this.id).pipe(
                tap(x => console.log(x)),
                tap(x => this.dataEditAss = x),
                tap((x: any) => {
                  this.form.get('manpower_request_uid').setValue(x.manpower_request_uid)
                  this.form.get('agency_uid').setValue(x.agency_uid)

                })
              ).subscribe()
            } else if (x == 'yes') {
              this.manpowerAssignmentsService.get(this.id).pipe(
                tap(x => console.log(x)),
                tap(x => this.dataEdit = x),
                concatMap(x => {
                  return this.manpowerRequestsService.get(this.dataEdit.manpower_request_uid).pipe(
                    tap(x => console.log(x)),
                    tap(x => this.dataEditAss = x),
                  )
                }),
                tap(() => {
                  this.form.patchValue({
                    status_id: this.dataEdit.status_id,
                    created_by: this.dataEdit.created_by,
                    created_datetime: this.dataEdit.created_datetime,
                    updated_by: this.dataEdit.updated_by,
                    updated_datetime: this.dataEdit.updated_datetime,
                    manpower_assignment_uid: this.dataEdit.manpower_assignment_uid,
                    manpower_request_uid: this.dataEdit.manpower_request_uid,
                    agency_uid: this.dataEdit.agency_uid,
                    remark: this.dataEdit.remark
                  })
                  let formDetails = this.form.get('manpower_assignment_details') as FormArray
                  let formDocs = this.form.get('manpower_assignment_documents') as FormArray
                  while (formDetails.length != 0) {
                    formDetails.removeAt(0)
                  }
                  while (formDocs.length != 0) {
                    formDocs.removeAt(0)
                  }
                  this.dataEdit.manpower_assignment_details.forEach(element => {
                    formDetails.push(this.getArrayDetails(element))
                  });
                  this.dataEdit.manpower_assignment_documents.forEach(element => {
                    formDocs.push(this.getArraymanpowerDoc(element))
                  });
                }),
              ).subscribe()
            }
          })
        ).subscribe()
        break;
      case 'add':

        break;
    }

  }


  save() {
    console.log('form', this.form.getRawValue());
    switch (this.dataType) {
      case 'yes':
        this.manpowerAssignmentsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appService.swaltAlert()
            this.router.navigate(['app/save_request_people'])
          })
        break;
      case 'no':
        this.manpowerAssignmentsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appService.swaltAlert()
            this.router.navigate(['app/save_request_people'])
          })
        break;
    }
  }

  close() {
    this.router.navigate(['app/save_request_people'])
  }



  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('manpower_assignment_documents') as FormArray
        formarray.push(this.createArrayDocManpower(callback))
      }
    })
  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('manpower_assignment_documents') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })
  }

  popDetailAdd() {
    if (this.form.value.manpower_assignment_details.length < this.dataEditAss.no_of_manpower) {
      const dialogRef = this.dialog.open(
        PopupDetailManpowersComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: null, dataAss: this.dataEditAss }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
      })
    } else {
      this.appService.swaltAlertError('อัตรากำลังที่ขอเท่ากับรายละเอียดการให้อัตรา ไม่สามารถเพิ่มข้อมูลได้')
    }

  }

  popDetailEdit(array, index) {
    console.log(array);
    console.log(status);
    const dialogRef = this.dialog.open(
      PopupDetailManpowersComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index, dataAss: this.dataEditAss }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })
  }

  downloadDoc(url) {
    this.manpowerAssignmentsService.download_file(url)
  }

  createArrayDetails() {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      manpower_assignment_detail_uid: null,
      manpower_assignment_uid: null,
      manpower_uid: null,
      salary_amount: 0,
      row_order: 0
    })
  }

  getArrayDetails(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      manpower_assignment_detail_uid: el.manpower_assignment_detail_uid,
      manpower_assignment_uid: el.manpower_assignment_uid,
      manpower_uid: el.manpower_uid,
      salary_amount: el.salary_amount,
      row_order: el.row_order
    })
  }

  deleteDetail(i) {
    let form = this.form.get('manpower_assignment_details') as FormArray
    form.removeAt(i)
  }



  createArrayDocManpower(el) {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      manpower_assignment_document_uid: null,
      manpower_assignment_uid: this.dataType == 'yes' ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  getArraymanpowerDoc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      manpower_assignment_document_uid: el.manpower_assignment_document_uid,
      manpower_assignment_uid: el.manpower_assignment_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.manpowerAssignmentDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appService.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('manpower_assignment_documents') as FormArray
                formarray.removeAt(i)
                this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('manpower_assignment_documents') as FormArray
      formarray.removeAt(i)
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      manpower_assignment_uid: null,
      manpower_request_uid: null,
      agency_uid: null,
      remark: '',
      manpower_assignment_details: this.baseFormBuilder.array([]),
      manpower_assignment_documents: this.baseFormBuilder.array([])
    })
  }
}
