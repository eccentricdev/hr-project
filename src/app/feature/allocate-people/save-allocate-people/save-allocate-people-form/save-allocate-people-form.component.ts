import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { log } from 'console';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowerRequestDocumentsService } from 'src/app/core/service/allocate-people/manpower-request-documents.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-allocate-people-form',
  templateUrl: './save-allocate-people-form.component.html',
  styleUrls: ['./save-allocate-people-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveAllocatePeopleFormComponent extends BaseForm implements OnInit {


  academicYear = []
  semesterData = []
  rows: any = []
  getToday = new Date()
  dataSource
  dataEdit
  educationLevels$ = new Observable()
  position$ = new Observable()
  qualifications$ = new Observable()
  agencysData$ = new Observable()
  randomNum = 0



  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public manpowerRequestDocumentsService: ManpowerRequestDocumentsService,
    public manpowerRequestsService: ManpowerRequestsService,
    public educationLevelsService: EducationLevelsService,
    public qualificationsService: QualificationsService,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public appService: AppService,) {
    super(formBuilder, activeRoute);
    this.agencysData$ = this.agencysService.getAll()

  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('request_by').disable()
    // this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.manpowerRequestsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(x => {
            this.qualifications$ = this.qualificationsService.getAll()
            this.position$ = this.positionService.getAll()
            this.educationLevels$ = this.educationLevelsService.getAll()
          }),
          tap(x => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              manpower_request_uid: this.dataEdit.manpower_request_uid,
              document_no: this.dataEdit.document_no,
              request_date: this.dataEdit.request_date,
              request_by: this.dataEdit.request_by,
              position_uid: this.dataEdit.position_uid,
              education_level_id: this.dataEdit.education_level_id,
              qualification_id: this.dataEdit.qualification_id,
              no_of_manpower: this.dataEdit.no_of_manpower,
              agency_uid: this.dataEdit.agency_uid,
              approve_status_id: this.dataEdit.approve_status_id,
              remark: this.dataEdit.remark,
            })
            let form = this.form.get('manpower_request_documents') as FormArray
            while (form.length != 0) {
              form.removeAt(0)
            }
            if (this.dataEdit.manpower_request_documents) {
              this.dataEdit.manpower_request_documents.forEach(element => {
                form.push(this.get_Doc(element))
              });
            }
          })
        ).subscribe()
        break;
      case 'add':
        // this.randomNumber()
        // this.form.get('document_no').setValue('TM1001')
        this.form.get('request_date').setValue(this.getToday)
        this.qualifications$ = this.qualificationsService.getAll()
        this.position$ = this.positionService.getAll()
        this.educationLevels$ = this.educationLevelsService.getAll()
        break;
    }
  }


  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('manpower_request_documents') as FormArray
        formarray.push(this.create_Doc(callback))
      }
    })
  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }// ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('manpower_request_documents') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })
  }

  downloadDoc(url) {
    this.manpowerRequestsService.download_file(url)
  }


  close() {
    this.router.navigate(['app/save_allocate_people'])
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.manpowerRequestsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appService.swaltAlert()
            this.router.navigate(['app/save_allocate_people'])
          })
        break;
      case 'add':
        this.manpowerRequestsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appService.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appService.swaltAlert()
            this.router.navigate(['app/save_allocate_people'])
          })
        break;
    }
  }

  approve() {
    this.form.get('approve_status_id').setValue(2)
    console.log(this.form.getRawValue());
    this.manpowerRequestsService.update(this.form.getRawValue()).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.router.navigate(['app/save_allocate_people'])
      })
  }

  noApprove() {
    this.form.get('approve_status_id').setValue(3)
    console.log(this.form.getRawValue());
    this.manpowerRequestsService.update(this.form.getRawValue()).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.router.navigate(['app/save_allocate_people'])
      })

  }


  create_Doc(el) {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      manpower_request_document_uid: null,
      manpower_request_uid: this.id ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  get_Doc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      manpower_request_document_uid: el.manpower_request_document_uid,
      manpower_request_uid: el.manpower_request_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.manpowerRequestDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appService.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('manpower_request_documents') as FormArray
                formarray.removeAt(i)
                this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('manpower_request_documents') as FormArray
      formarray.removeAt(i)
    }
  }



  createForm() {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      manpower_request_uid: null,
      document_no: '',
      request_date: '',
      request_by: 'นาง สมจิต จันทร์กระจ่าง',
      position_uid: null,
      education_level_id: null,
      qualification_id: null,
      no_of_manpower: 0,
      remark: '',
      agency_uid: null,
      approve_status_id: 1,
      manpower_request_documents: this.baseFormBuilder.array([])
    })
  }

}
