import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveAllocatePeopleListComponent } from './save-allocate-people-list/save-allocate-people-list.component';
import { SaveAllocatePeopleFormComponent } from './save-allocate-people-form/save-allocate-people-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SaveAllocatePeopleListComponent
  },
  {
    path:'add',
    component:SaveAllocatePeopleFormComponent
  },
  {
    path:'edit/:id',
    component:SaveAllocatePeopleFormComponent
  },
  
]


@NgModule({
  declarations: [
    SaveAllocatePeopleListComponent, 
    SaveAllocatePeopleFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveAllocatePeopleModule { }
