import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { AppService } from 'src/app/core/service/app.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-allocate-people-list',
  templateUrl: './save-allocate-people-list.component.html',
  styleUrls: ['./save-allocate-people-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveAllocatePeopleListComponent extends BaseList implements OnInit {
  rows: any = []
  dataSource
  positionDatas: any = []
  agencysDatas: any = []
  datasDisplay$ = new Observable()

  dataSearch = {
    agency_uid: null,
    position_uid: null
  }

  constructor(public router: Router,
    private cdRef: ChangeDetectorRef,
    public positionService: PositionService,
    public appSV: AppService,
    public agencysService: AgencysService,
    public manpowerRequestsService: ManpowerRequestsService) {
    super();
    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()
    this.agencysService.getAll().pipe(
      tap(x => this.agencysDatas = x)
    ).subscribe()
  }

  ngOnInit(): void {
    this.getService()
  }

  

  getService() {
     this.manpowerRequestsService.getAll().pipe(
      tap(x =>  this.rows = this.updateMatTable(x)),
      tap(x => console.log(this.rows)),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

    
    
  }

  search() {
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.manpowerRequestsService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x))
      ).subscribe()
    }
  }

  clear() {

    this.dataSearch = {
      agency_uid: null,
      position_uid: null
    }
    this.getService()
  }

  addPage() {
    this.router.navigate(['app/save_allocate_people/add'])
  }
  editPage(id) {
    this.router.navigate(['app/save_allocate_people/edit', id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.manpowerRequestsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
