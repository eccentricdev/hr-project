import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { configs } from 'src/app/core/configs/config';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersPersonnelService } from 'src/app/core/service/allocate-people/manpowers-personnel.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';

@Component({
  selector: 'app-search-position-list',
  templateUrl: './search-position-list.component.html',
  styleUrls: ['./search-position-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPositionListComponent extends BaseList implements OnInit {


  datasDisplay$ = new Observable()
  
  rows:any = []
  positionDatas: any = []
  agencyDatas:any = []
  usingStatussDatas:any = []

  startDate:any = null
  endDate:any = null

  dataSearch = {
    manpower_no:'',
    position_uid:null,
    agency_uid:null,
    manpower_using_status_id:null,
    available_start_date:null,
    available_end_date:null,
  }

  constructor(public router : Router,
    public cdRef : ChangeDetectorRef,
    public datePipe : DatePipe,
    public manpowersPersonnelService : ManpowersPersonnelService,
    public positionService: PositionService,
    public manpowerUsingStatussService : ManpowerUsingStatussService,
    public agencysService: AgencysService) { 
    super();
      this.positionService.getAll().pipe(
        tap(x => this.positionDatas = x)
      ).subscribe()
      this.agencysService.getAll().pipe(
        tap(x => this.agencyDatas = x)
      ).subscribe()
      this.manpowerUsingStatussService.getAll().pipe(
        tap(x => this.usingStatussDatas = x)
      ).subscribe()
    }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.manpowersPersonnelService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x)),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search() {
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.available_start_date = start
    this.dataSearch.available_end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.manpowersPersonnelService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x)),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      manpower_no:'',
      position_uid:null,
      agency_uid:null,
      manpower_using_status_id:null,
      available_start_date:null,
    available_end_date:null,
    }
    this.startDate = null
    this.endDate = null
    this.getService()
  }
  

  addPage(){
    this.router.navigate(['app/search_position_people/add']) 
  }
  editPage(id){
    this.router.navigate(['app/search_position_people/edit',id])
  }

}
