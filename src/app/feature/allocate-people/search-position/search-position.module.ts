import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPositionFormComponent } from './search-position-form/search-position-form.component';
import { SearchPositionListComponent } from './search-position-list/search-position-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes:Routes = [
  {
    path:'',
    component:SearchPositionListComponent
  },
  {
    path:'add',
    component:SearchPositionFormComponent
  },
  {
    path:'edit/:id',
    component:SearchPositionFormComponent
  },
  
]


@NgModule({
  declarations: [
    SearchPositionFormComponent, 
    SearchPositionListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class SearchPositionModule { }
