import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-search-position-form',
  templateUrl: './search-position-form.component.html',
  styleUrls: ['./search-position-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPositionFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
