import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetNumberPeopleFormComponent } from './set-number-people-form/set-number-people-form.component';
import { SetNumberPeopleListComponent } from './set-number-people-list/set-number-people-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetNumberPeopleListComponent
  },
  {
    path:'add',
    component:SetNumberPeopleFormComponent
  },
  {
    path:'edit/:id',
    component:SetNumberPeopleFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetNumberPeopleFormComponent, 
    SetNumberPeopleListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetNumberPeopleModule { }
