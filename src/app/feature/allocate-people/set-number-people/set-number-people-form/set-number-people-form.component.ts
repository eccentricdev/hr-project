import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowerStatussService } from 'src/app/core/service/allocate-people/manpower-statuss.service';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { PositionService } from 'src/app/core/service/organization/position.service';

@Component({
  selector: 'app-set-number-people-form',
  templateUrl: './set-number-people-form.component.html',
  styleUrls: ['./set-number-people-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetNumberPeopleFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  dataEdit: any
  position$ = new Observable()
  status$ = new Observable()
  usingStatus$ = new Observable()
  
  constructor(public formBuilder: FormBuilder,
    public manpowersService: ManpowersService,
    public positionService:PositionService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public manpowerStatussService : ManpowerStatussService,
    public manpowerUsingStatussService : ManpowerUsingStatussService,
    public statusService: StatusService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.status$ = this.manpowerStatussService.getAll()
    this.usingStatus$ = this.manpowerUsingStatussService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    // this.form.get('manpower_no').disable()
    // this.form.get('manpower_no').disable()
    switch (this.state) {
      case 'edit':
        this.manpowersService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(x => {
            this.position$ = this.positionService.getAll().pipe(
              tap(x => console.log(x))
            )
          }),
          tap(x => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              manpower_status_id:this.dataEdit.manpower_status_id,
              manpower_using_status_id:this.dataEdit.manpower_using_status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              manpower_uid: this.dataEdit.manpower_uid,
              manpower_no: this.dataEdit.manpower_no,
              position_uid: this.dataEdit.position_uid,
              effective_date: this.dataEdit.effective_date,
              available_date: this.dataEdit.available_date,
              remark: this.dataEdit.remark
            })
          })
        ).subscribe()
        break;
      case 'add':
        // this.form.get('manpower_no').setValue('T1001')
          this.position$ = this.positionService.getAll().pipe(
            tap(x => console.log(x))
          )
        break;
    }
  }

  close() {
    this.router.navigate(['app/set_number_people'])
  }

  save() {
      switch (this.state) {
        case 'edit':
          this.manpowersService.update(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/set_number_people'])
            })
          break;
        case 'add':
          this.manpowersService.add(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/set_number_people'])
            })
          break;
      }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: null,
      manpower_status_id:null,
      manpower_using_status_id:null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      manpower_uid: null,
      manpower_no: '',
      position_uid: null,
      effective_date: null,
      available_date: null,
      remark: ''
    })
  }

}
