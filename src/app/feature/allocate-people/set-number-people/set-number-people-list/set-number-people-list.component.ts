import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { ManpowerStatussService } from 'src/app/core/service/allocate-people/manpower-statuss.service';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';

import swal from 'sweetalert2'

@Component({
  selector: 'app-set-number-people-list',
  templateUrl: './set-number-people-list.component.html',
  styleUrls: ['./set-number-people-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetNumberPeopleListComponent extends BaseList implements OnInit {
  datasDisplay$ = new Observable()
  status$ = new Observable()
  rows:any = []
  dataSource
  positionDatas:any = []
  statusDatas: any = []
  usingStatusDatas: any = []
  dataSearch = {
    manpower_no: null,
    position_uid: null,
    status_id:null
  }
  // @ViewChild(MatSort, {static: true}) sort: MatSort;
  // @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(public router : Router,
    private cdRef: ChangeDetectorRef,
    public appSV: AppService,
    public manpowerUsingStatussService : ManpowerUsingStatussService,
    public manpowerStatussService : ManpowerStatussService,
    public statusService : StatusService,
    public positionService : PositionService,
    public manpowersService: ManpowersService) { 
    super();
      // this.positionService.getAll().pipe(
      //   tap(x => this.positionDatas = x)
      // ).subscribe()

     this.status$ = this.statusService.getAll().pipe(
        tap(x => this.statusDatas = x)
      )
    }

  ngOnInit(): void {
    this.getService()
  }

  getService(){

    this.manpowersService.getAll().pipe(
      tap(x => x),
      tap(x => this.rows = this.updateMatTable(x)),
      concatMap(() => {
        return this.manpowerStatussService.getAll().pipe(
          tap(x => this.statusDatas = x)
        )
      }),
      concatMap(() => {
        return this.manpowerUsingStatussService.getAll().pipe(
          tap(x => this.usingStatusDatas = x)
        )
      }),
      concatMap(() => {
        return this.positionService.getAll().pipe(
          tap(x => this.positionDatas = x)
        )
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search() {
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.manpowersService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x))
      ).subscribe()
    }
  }

  clear() {

    this.dataSearch = {
      manpower_no: null,
      position_uid: null,
      status_id:null
    }
    this.getService()
  }


  

  addPage(){
    this.router.navigate(['app/set_number_people/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set_number_people/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.manpowersService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }


}
