import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { AppealRequestsService } from 'src/app/core/service/discripline/appeal-requests.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { DisciplineRequestOffendersService } from 'src/app/core/service/discripline/discipline-request-offenders.service';
import { DisciplineRequestsService } from 'src/app/core/service/discripline/discipline-requests.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-complain-list',
  templateUrl: './save-complain-list.component.html',
  styleUrls: ['./save-complain-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveComplainListComponent extends BaseList implements OnInit {

  personnels$ = new Observable()
  punishmentTypes$ = new Observable()
  status$ = new Observable()
  datasDisplay$ = new Observable()
  disciplineRequests$ = new Observable()
  offenders$ = new Observable()
  
  rows: any = []
  status: any = []
  punishmentTypes: any = []
  personnels: any = []
  name: any = ''

  searchData:any = ''

  dataSearch = {
    // is_appeal_request: false,
    // is_complain: false,
    discipline_approve_status_id: null,
    responsible_personnel_guid: null,
    punishment_type_id: null
  }

  constructor(public router: Router,
    public appService: AppService,
    public personnelService: PersonnelService,
    public disciplineRequestsService: DisciplineRequestsService,
    public disciplineRequestOffendersService: DisciplineRequestOffendersService,
    public disciplineApproveStatussService: DisciplineApproveStatussService,
    public punishmentTypesService: PunishmentTypesService,
    public appealRequestsService: AppealRequestsService) {
    super();
    this.disciplineRequests$ = this.disciplineRequestsService.getAll()
    this.personnels$ = this.personnelService.getAll().pipe(
      tap(x => this.personnels = x)
    )
    this.punishmentTypes$ = this.punishmentTypesService.getAll()
    this.status$ = this.disciplineApproveStatussService.getAll()
    this.offenders$ = this.disciplineRequestOffendersService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService() {
    this.appealRequestsService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x)),
    ).subscribe()
  }

  addPage() {
    this.router.navigate(['app/save_complain/add'])
  }
  editPage(id) {
    this.router.navigate(['app/save_complain/edit', id])
  }

  getName(value) {
    console.log(value);

  }

  
  // search() {
  //   console.log(this.dataSearch);
  //   let queryStr = createQueryStringFromObject(this.dataSearch)
  //   if (queryStr) {
  //     this.datasDisplay$ = this.appealRequestsService.query(`?${queryStr}`)
  //     this.datasDisplay$.pipe(
  //       tap(x => this.rows = this.updateMatTable(x))
  //     ).subscribe()
  //   }
  // }

  search(){
    console.log(this.searchData);
      setTimeout(() => {
        this.appealRequestsService.query(`?search=${this.searchData}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  clear() {
    this.searchData = ''
    this.getService()
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.appealRequestsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
