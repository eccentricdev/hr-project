import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveComplainListComponent } from './save-complain-list/save-complain-list.component';
import { SaveComplainFormComponent } from './save-complain-form/save-complain-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SetNumberPeopleFormComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-form/set-number-people-form.component';
import { SetNumberPeopleListComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-list/set-number-people-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


const routes:Routes = [
  {
    path:'',
    component:SaveComplainListComponent
  },
  {
    path:'add',
    component:SaveComplainFormComponent
  },
  {
    path:'edit/:id',
    component:SaveComplainFormComponent
  },
  
]


@NgModule({
  declarations: [
    SaveComplainListComponent, 
    SaveComplainFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveComplainModule { }
