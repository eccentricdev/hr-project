import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { AppealRequestCommitteesService } from 'src/app/core/service/discripline/appeal-request-committees.service';
import { AppealRequestDocumentsService } from 'src/app/core/service/discripline/appeal-request-documents.service';
import { AppealRequestsService } from 'src/app/core/service/discripline/appeal-requests.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { DisciplineRequestOffendersService } from 'src/app/core/service/discripline/discipline-request-offenders.service';
import { DisciplineRequestsService } from 'src/app/core/service/discripline/discipline-requests.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-save-complain-form',
  templateUrl: './save-complain-form.component.html',
  styleUrls: ['./save-complain-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveComplainFormComponent extends BaseForm implements OnInit {

  personnels$ = new Observable()
  approveStatuss$ = new Observable()
  drOffenders$ = new Observable()
  punishmentTypes$ = new Observable()

  personnelsDatas: any = []
  disciplineReques: any = []
  punishmentTypes: any = []

  dataEdit: any
  offender_uid: any = null
  dataRequest: any

  dateToday = new Date()


  constructor(public formBuilder: FormBuilder,
    public personnelService: PersonnelService,
    public cdRef: ChangeDetectorRef,
    public appealRequestsService: AppealRequestsService,
    public appealRequestCommitteesService: AppealRequestCommitteesService,
    public appealRequestDocumentsService: AppealRequestDocumentsService,
    public punishmentTypesService: PunishmentTypesService,
    public disciplineRequestsService: DisciplineRequestsService,
    public disciplineRequestOffendersService: DisciplineRequestOffendersService,
    public disciplineApproveStatussService: DisciplineApproveStatussService,
    public dialog: MatDialog,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.personnels$ = this.personnelService.getAll().pipe(
      tap(x => this.personnelsDatas = x)
    )
    this.disciplineRequestsService.getAll().pipe(
      tap(x => this.disciplineReques = x)
    ).subscribe()

    this.punishmentTypes$ = this.punishmentTypesService.getAll().pipe(
      tap(x => this.punishmentTypes = x)
    )
    this.drOffenders$ = this.disciplineRequestOffendersService.getAll()
    this.approveStatuss$ = this.disciplineApproveStatussService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('document_date').setValue(this.dateToday)
    this.form.get('document_no').disable()
    this.form.get('informed_date').setValue(this.dateToday)
    switch (this.state) {
      case 'edit':
        this.appealRequestsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(x => {
            this.getRequest(this.dataEdit.discipline_request_offender_uid)
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              appeal_request_uid: this.dataEdit.appeal_request_uid,
              discipline_request_offender_uid: this.dataEdit.discipline_request_offender_uid,
              is_appeal_request: this.dataEdit.is_appeal_request,
              is_complain: this.dataEdit.is_complain,
              document_no: this.dataEdit.document_no,
              document_date: this.dataEdit.document_date,
              informed_personnel_uid: this.dataEdit.informed_personnel_uid,
              informed_date: this.dataEdit.informed_date,
              responsible_personnel_guid: this.dataEdit.responsible_personnel_guid,
              acknowledge_date: this.dataEdit.acknowledge_date,
              complainant: this.dataEdit.complainant,
              discipline_approve_status_id: this.dataEdit.discipline_approve_status_id,
              appeal_request_subject_name: this.dataEdit.appeal_request_subject_name,
              appeal_request_description: this.dataEdit.appeal_request_description,
              appeal_request_result_detail: this.dataEdit.appeal_request_result_detail,
              punishment_type_id: this.dataEdit.punishment_type_id,
            })
            let form1 = this.form.get('appeal_request_committee') as FormArray
            let form2 = this.form.get('appeal_request_document') as FormArray
            while (form1.length != 0) {
              form1.removeAt(0)
            }
            while (form2.length != 0) {
              form2.removeAt(0)
            }
            if (this.dataEdit.appeal_request_committee.length != 0) {
              this.dataEdit.appeal_request_committee.forEach(element => {
                form1.push(this.getAR_DOC_committee(element))
              });
            }
            if (this.dataEdit.appeal_request_document.length != 0) {
              this.dataEdit.appeal_request_document.forEach(element => {
                form2.push(this.getAR_DOC(element))
              });
            }


          })
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getRequest(id) {
    console.log(id);
    this.disciplineRequestOffendersService.get(id).pipe(
      tap(x => console.log(x)),
      tap(x => this.dataRequest = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  addfile1() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    }
    )
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray1 = this.form.get('appeal_request_document') as FormArray
        formarray1.push(this.createAR_DOC(callback))
      }
    })
  }
  addfile2() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray2 = this.form.get('appeal_request_committee') as FormArray
        formarray2.push(this.createAR_DOC_committee(callback))
      }
    })

  }

  editfileDoc1(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.appeal_request_committee_remark }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ             
    }
    )
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('appeal_request_committee') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          appeal_request_committee_remark: callback.remark
        })
      }
    })

  }

  editfileDoc2(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ             
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('appeal_request_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark
        })
      }
    })

  }

  downloadDoc(url) {
    this.appealRequestsService.download_file(url)
  }



  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.appealRequestsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_complain'])
          })
        break;
      case 'add':
        this.appealRequestsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_complain'])
          })
        break;
    }
  }

  close() {
    this.router.navigate(['app/save_complain'])
  }

  getAR_DOC_committee(el) {
    return this.baseFormBuilder.group({
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      appeal_request_committee_uid: el.appeal_request_committee_uid,
      appeal_request_uid: el.appeal_request_uid,
      appeal_request_committee_remark: el.appeal_request_committee_remark,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order
    })
  }

  createAR_DOC_committee(el) {
    return this.baseFormBuilder.group({
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      appeal_request_committee_uid: null,
      appeal_request_uid: this.id ? this.id : null,
      appeal_request_committee_remark: el.remark,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null
    })
  }

  getAR_DOC(el) {
    return this.baseFormBuilder.group({
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      appeal_request_document_uid: el.appeal_request_document_uid,
      appeal_request_uid: el.appeal_request_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }

  createAR_DOC(el) {
    return this.baseFormBuilder.group({
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      appeal_request_document_uid: null,
      appeal_request_uid: this.id ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  deleteItemCom(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.appealRequestCommitteesService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('appeal_request_committee') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('appeal_request_committee') as FormArray
      formarray.removeAt(i)
    }
  }


  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.appealRequestDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('appeal_request_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('appeal_request_document') as FormArray
      formarray.removeAt(i)
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      appeal_request_uid: [null],
      discipline_request_offender_uid: [null],
      is_appeal_request: false,
      is_complain: false,
      document_no: [''],
      document_date: [null],
      informed_personnel_uid: [null],
      informed_date: [null],
      responsible_personnel_guid: [null],
      acknowledge_date: [null],
      complainant: [''],
      discipline_approve_status_id: [1],
      appeal_request_subject_name: [''],
      appeal_request_description: [''],
      appeal_request_result_detail: [''],
      fault_type_id: [null],
      punishment_type_id: [null],
      appeal_request_committee: this.baseFormBuilder.array([]),
      appeal_request_document: this.baseFormBuilder.array([])
    })
  }

}
