import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DisciplineApproveResultService } from 'src/app/core/service/discripline/discipline-approve-result.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';

@Component({
  selector: 'app-popup-list-complain',
  templateUrl: './popup-list-complain.component.html',
  styleUrls: ['./popup-list-complain.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupListComplainComponent implements OnInit {

  personnel$ = new Observable()
  punishmentTypes$ = new Observable()
  approveResult$ = new Observable()

  positionId: any
  agencyId: any
  agenDatas = []

  request = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    discipline_request_offender_uid: null,
    discipline_request_uid: null,
    personnel_uid: null,
    punishment_type_id:null
  }

  constructor(
    public dialogRef: MatDialogRef<PopupListComplainComponent>,
    public formBuilder: FormBuilder,
    public agencysService: AgencysService,
    public punishmentTypesService : PunishmentTypesService,
    public disciplineApproveResultService : DisciplineApproveResultService,
    public activeRoute: ActivatedRoute,
    public cdRef: ChangeDetectorRef,
    public personnelService: PersonnelService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.approveResult$ = this.disciplineApproveResultService.getAll()
    this.personnel$ = this.personnelService.getAll()
    this.punishmentTypes$ = this.punishmentTypesService.getAll()
    this.agencysService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.agenDatas = x)
    ).subscribe()
  }

  ngOnInit(): void {
    if (this.data.editForm != null) {
      console.log(this.data.editForm);
      console.log(this.data);
      this.getArray(this.data.editForm)
      this.cdRef.detectChanges()
    }else{
      this.request.discipline_request_uid = this.data.id
    }
  }

  getArray(el) {
    console.log(el);
    this.request = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_request_offender_uid: el.discipline_request_offender_uid,
      discipline_request_uid: el.discipline_request_uid,
      personnel_uid: el.personnel_uid,
      punishment_type_id: el.punishment_type_id,
    }
    this.getData(el.personnel_uid)
  }

  getData(id?) {
    console.log(id);
    this.personnelService.get(id ? id : this.request.personnel_uid).pipe(
      tap(x => console.log(x)),
      tap((x: any) => {
        this.positionId = x.personnel_hires[0]?.position?.position_name_th
        this.agencyId = x.personnel_hires[0]?.agency_uid
        console.log(this.positionId);
        console.log(this.agencyId);
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        discipline_request_offender_uid: null,
        discipline_request_uid: null,
        personnel_uid: null,
        punishment_type_id: null
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        discipline_request_offender_uid: el.discipline_request_offender_uid,
        discipline_request_uid: el.discipline_request_uid,
        personnel_uid: el.personnel_uid,
        punishment_type_id: el.punishment_type_id
      })
    }
  }

  save() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('discipline_request_offender')).push(this.createArray(this.request))  
    }else{
      (<FormArray>this.data.form.get('discipline_request_offender')).controls[this.data.ind].patchValue({
        personnel_uid:this.request.personnel_uid,
        punishment_type_id:this.request.punishment_type_id
      })
    }
  }


  saveClose() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('discipline_request_offender')).push(this.createArray(this.request)) 
      this.close() 
    }else{
      (<FormArray>this.data.form.get('discipline_request_offender')).controls[this.data.ind].patchValue({
        personnel_uid:this.request.personnel_uid,
        punishment_type_id:this.request.punishment_type_id
      })
      this.close()
    }
  }

  close() {
    this.dialogRef.close('close')
  }


}
