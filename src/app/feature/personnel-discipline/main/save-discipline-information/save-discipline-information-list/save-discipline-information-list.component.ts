import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { DisciplineRequestOffendersService } from 'src/app/core/service/discripline/discipline-request-offenders.service';
import { DisciplineRequestsService } from 'src/app/core/service/discripline/discipline-requests.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-discipline-information-list',
  templateUrl: './save-discipline-information-list.component.html',
  styleUrls: ['./save-discipline-information-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveDisciplineInformationListComponent extends BaseList implements OnInit {

  disciplineRequests$ = new Observable()
  disciplineRequests:any = []
  personnels:any = []
  punishmentTypes:any = []
  approveStatuss:any = []
  rows:any = []
  approveStatuss$ = new Observable()
  datasDisplay$ = new Observable()

  searchData:any = ''

  dataSearch = {
    personnel_uid: null,
    discipline_approve_result_id:null,
    punishment_type_id:null
  }

  constructor(public router : Router,
    public personnelService : PersonnelService,
    public disciplineApproveStatussService : DisciplineApproveStatussService,
    public punishmentTypesService : PunishmentTypesService,
    public disciplineRequestOffendersService : DisciplineRequestOffendersService,
    public disciplineRequestsService : DisciplineRequestsService,
    public appService : AppService) {
    super();

    this.approveStatuss$ = this.disciplineApproveStatussService.getAll()

    this.personnelService.getAll().pipe(
      tap(x => this.personnels = x)
    ).subscribe()
    this.disciplineApproveStatussService.getAll().pipe(
      tap(x => this.approveStatuss = x)
    ).subscribe()
    this.punishmentTypesService.getAll().pipe(
      tap(x => this.punishmentTypes = x)
    ).subscribe()
    
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.disciplineRequestOffendersService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x)),
      concatMap(() => {
        return this.disciplineRequests$ = this.disciplineRequestsService.getAll()
      })
    ).subscribe()
  }

  addPage(){
    this.router.navigate(['app/save_discipline_information/add']) 
  }
  editPage(id){
    this.router.navigate(['app/save_discipline_information/edit',id])
  }

  search(){
    console.log(this.searchData);
      setTimeout(() => {
        this.disciplineRequestOffendersService.query(`?search=${this.searchData}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  // search() {
  //   console.log(this.dataSearch);
  //   let queryStr = createQueryStringFromObject(this.dataSearch)
  //   if (queryStr) {
  //     this.datasDisplay$ = this.disciplineRequestOffendersService.query(`?${queryStr}`)
  //     this.datasDisplay$.pipe(
  //       tap(x => this.rows = this.updateMatTable(x))
  //     ).subscribe()
  //   }
  // }

  clear(){
    this.dataSearch = {
      personnel_uid: null,
      discipline_approve_result_id:null,
      punishment_type_id:null
    }
    this.getService()
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.disciplineRequestOffendersService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
