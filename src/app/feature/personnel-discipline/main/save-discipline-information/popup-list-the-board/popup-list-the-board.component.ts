import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { CommitteeTypesService } from 'src/app/core/service/discripline/committee-types.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';

@Component({
  selector: 'app-popup-list-the-board',
  templateUrl: './popup-list-the-board.component.html',
  styleUrls: ['./popup-list-the-board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupListTheBoardComponent implements OnInit {


  committeeTypes$ = new Observable()
  personnel$ = new Observable()

  request = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    discipline_request_committee_uid: null,
    discipline_request_uid: null,
    personnel_uid: null,
    committee_type_id: null,
    position_name: '',
    person_name:''
  }
  constructor(public dialogRef: MatDialogRef<PopupListTheBoardComponent>,
    public formBuilder: FormBuilder,
    public personnelService : PersonnelService,
    public committeeTypesService: CommitteeTypesService,
    public activeRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.committeeTypes$ = this.committeeTypesService.getAll()
    this.personnel$ = this.personnelService.getAll()
  }

  ngOnInit(): void {
    if (this.data.editForm != null) {
      console.log(this.data.editForm);
      console.log(this.data);
      this.getArray(this.data.editForm)
    }else{
      this.request.discipline_request_uid = this.data.id
    }
  }

  getArray(el) {
    console.log(el);
    this.request = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_request_committee_uid: el.discipline_request_committee_uid,
      discipline_request_uid: el.discipline_request_uid,
      personnel_uid: el.personnel_uid,
      committee_type_id: el.committee_type_id,
      position_name: el.position_name,
      person_name:el.person_name
    }
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        discipline_request_committee_uid: null,
        discipline_request_uid: null,
        personnel_uid: null,
        committee_type_id: null,
        position_name: '',
        person_name:''
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        discipline_request_committee_uid: el.discipline_request_committee_uid,
        discipline_request_uid: el.discipline_request_uid,
        personnel_uid: el.personnel_uid,
        committee_type_id: el.committee_type_id,
        position_name: el.position_name,
        person_name:el.person_name
      })
    }
  }

  save() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('discipline_request_committee')).push(this.createArray(this.request))  
    }else{
      (<FormArray>this.data.form.get('discipline_request_committee')).controls[this.data.ind].patchValue({
        personnel_uid: this.request.personnel_uid,
        committee_type_id: this.request.committee_type_id,
        position_name: this.request.position_name,
        person_name:this.request.person_name
      })
    }
  }


  saveClose() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('discipline_request_committee')).push(this.createArray(this.request)) 
      this.close() 
    }else{
      (<FormArray>this.data.form.get('discipline_request_committee')).controls[this.data.ind].patchValue({
        personnel_uid: this.request.personnel_uid,
        committee_type_id: this.request.committee_type_id,
        position_name: this.request.position_name,
        person_name:this.request.person_name
      })
      this.close()
    }
  }

  close() {
    this.dialogRef.close('close')
  }


}
