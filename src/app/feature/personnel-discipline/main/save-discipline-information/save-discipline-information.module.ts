import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveDisciplineInformationListComponent } from './save-discipline-information-list/save-discipline-information-list.component';
import { SaveDisciplineInformationFormComponent } from './save-discipline-information-form/save-discipline-information-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SetNumberPeopleFormComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-form/set-number-people-form.component';
import { SetNumberPeopleListComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-list/set-number-people-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupListComplainComponent } from './popup-list-complain/popup-list-complain.component';
import { PopupListTheBoardComponent } from './popup-list-the-board/popup-list-the-board.component';
import { PopupOffenderComponent } from './popup-offender/popup-offender.component';


const routes:Routes = [
  {
    path:'',
    component:SaveDisciplineInformationListComponent
  },
  {
    path:'add',
    component:SaveDisciplineInformationFormComponent
  },
  {
    path:'edit/:id',
    component:SaveDisciplineInformationFormComponent
  },
  
]


@NgModule({
  declarations: [
    SaveDisciplineInformationListComponent, 
    SaveDisciplineInformationFormComponent, PopupListComplainComponent, PopupListTheBoardComponent, PopupOffenderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveDisciplineInformationModule { }
