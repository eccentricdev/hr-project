import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { ActionTypesService } from 'src/app/core/service/discripline/action-types.service';
import { DisciplineApproveDocumentsService } from 'src/app/core/service/discripline/discipline-approve-documents.service';
import { DisciplineApproveResultService } from 'src/app/core/service/discripline/discipline-approve-result.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { DisciplineRequestCommitteesService } from 'src/app/core/service/discripline/discipline-request-committees.service';
import { DisciplineRequestDocumentsService } from 'src/app/core/service/discripline/discipline-request-documents.service';
import { DisciplineRequestOffendersService } from 'src/app/core/service/discripline/discipline-request-offenders.service';
import { DisciplineRequestsService } from 'src/app/core/service/discripline/discipline-requests.service';
import { FaultTypesService } from 'src/app/core/service/discripline/fault-types.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupListComplainComponent } from '../popup-list-complain/popup-list-complain.component';
import { PopupListTheBoardComponent } from '../popup-list-the-board/popup-list-the-board.component';
import { PopupOffenderComponent } from '../popup-offender/popup-offender.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-save-discipline-information-form',
  templateUrl: './save-discipline-information-form.component.html',
  styleUrls: ['./save-discipline-information-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveDisciplineInformationFormComponent extends BaseForm implements OnInit {
  actionTypes$ = new Observable()
  faultTypes$ = new Observable()
  personnel$ = new Observable()
  disciplineStatuss$ = new Observable()

  personnelDatas: any = []
  approverequests: any = []
  punishmentTypes: any = []
  agencys: any = []

  action_type_id: any
  dataEdit: any

  dateToday = new Date()


  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    public cdRef: ChangeDetectorRef,
    public activeRoute: ActivatedRoute,
    public actionTypesService: ActionTypesService,
    public agencysService: AgencysService,
    public faultTypesService: FaultTypesService,
    public personnelService: PersonnelService,
    public disciplineApproveDocumentsService: DisciplineApproveDocumentsService,
    public disciplineRequestDocumentsService: DisciplineRequestDocumentsService,
    public disciplineRequestCommitteesService: DisciplineRequestCommitteesService,
    public disciplineRequestsService: DisciplineRequestsService,
    public disciplineRequestOffendersService: DisciplineRequestOffendersService,
    public disciplineApproveResultService: DisciplineApproveResultService,
    public punishmentTypesService: PunishmentTypesService,
    public disciplineApproveStatussService: DisciplineApproveStatussService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.personnel$ = this.personnelService.getAll().pipe(
      tap(x => this.personnelDatas = x)
    )
    this.agencysService.getAll().pipe(
      tap(x => this.agencys = x)
    ).subscribe()

    this.actionTypes$ = this.actionTypesService.getAll()
    this.disciplineStatuss$ = this.disciplineApproveStatussService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('document_date').setValue(this.dateToday)
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.disciplineRequestsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          concatMap(() => {
            return this.personnelService.getAll().pipe(
              tap(x => this.personnelDatas = x)
            )
          }),
          concatMap(() => {
            return this.faultTypes$ = this.faultTypesService.getAll().pipe(
              tap((res: any) => {
                let data = res.find(x => x.fault_type_id == this.dataEdit.fault_type_id)
                console.log(data);
                this.action_type_id = data ? data.action_type_id : ''
              }),
            )
          }),
          tap(x => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              discipline_request_uid: this.dataEdit.discipline_request_uid,
              document_no: this.dataEdit.document_no,
              document_date: this.dataEdit.document_date,
              title_name: this.dataEdit.title_name,
              description: this.dataEdit.description,
              fault_type_id: this.dataEdit.fault_type_id,
              complainer: this.dataEdit.complainer,
              responsible_personnel_guid: this.dataEdit.responsible_personnel_guid,
              action_date: this.dataEdit.action_date,
              discipline_approve_status_id: this.dataEdit.discipline_approve_status_id,
            })
            let formoff = this.form.get('discipline_request_offender') as FormArray
            let formdoc = this.form.get('discipline_request_document') as FormArray
            let formcom = this.form.get('discipline_request_committee') as FormArray
            let formapp = this.form.get('discipline_approve_document') as FormArray

            while (formoff.length != 0) {
              formoff.removeAt(0)
            }
            while (formdoc.length != 0) {
              formdoc.removeAt(0)
            }
            while (formcom.length != 0) {
              formcom.removeAt(0)
            }
            while (formapp.length != 0) {
              formapp.removeAt(0)
            }

            this.dataEdit.discipline_request_offender = this.dataEdit.discipline_request_offender.map(x => {
              // console.log(x);
              let position = x.personnel?.personnel_hires[0]?.position?.position_name_th
              let agency = x.personnel?.personnel_hires[0]?.agency_uid
              // console.log(position);
              // console.log(agency);
              return { ...x, position: position, agency: agency }
            })
            this.dataEdit.discipline_request_offender.forEach(element => {
              // console.log(element);
              formoff.push(this.getDR_offender(element))
            });
            this.dataEdit.discipline_request_document.forEach(element => {
              formdoc.push(this.getDR_document(element))
            });
            this.dataEdit.discipline_request_committee.forEach(element => {
              formcom.push(this.getDR_committee(element))
            });
            this.dataEdit.discipline_approve_document.forEach(element => {
              formapp.push(this.getDA_document(element))
            });
          }),
          tap(() => this.cdRef.detectChanges())
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getFault() {
    this.faultTypes$ = this.faultTypesService.getAll().pipe(
      map((x: any) => x.filter(x => x.action_type_id == this.action_type_id))
    )
  }

  popUpListTheBoardAdd() {
    const dialogRef = this.dialog.open(
      PopupListTheBoardComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null, id: this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    }
    )
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

      // if (callback) {

      // }
    })
  }

  popUpListTheBoardEdit(array, index) {
    const dialogRef = this.dialog.open(
      PopupListTheBoardComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }


  popUpListComplainAdd() {
    const dialogRef = this.dialog.open(
      PopupListComplainComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null, id: this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      // if (callback) {
      this.form.value.discipline_request_offender = this.form.value.discipline_request_offender.map(el => {
        let data = this.personnelDatas.find(x => x.personnel_uid == el.personnel_uid)
        return { ...el, name: data.display_name_th, position: data.personnel_hires[0].position.position_name_th, agency: data.personnel_hires[0].agency_uid }
      })
      console.log(this.form.value.discipline_request_offender);
      // }
    })
  }

  popUpListComplainEdit(array, index) {
    const dialogRef = this.dialog.open(
      PopupListComplainComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpOffenderEdit(array, index) {
    const dialogRef = this.dialog.open(
      PopupOffenderComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('discipline_request_document') as FormArray
        formarray.push(this.createDR_document(callback))
        // this.ngOnInit()
      }
    })

  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ             
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('discipline_request_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark
        })
      }
    })

  }

  addfile2() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('discipline_approve_document') as FormArray
        formarray.push(this.createDA_document(callback))
        // this.ngOnInit()
      }
    })

  }

  editfile2(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ             
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('discipline_approve_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark
        })
      }
    })

  }

  close() {
    this.router.navigate(['app/save_discipline_information'])
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.disciplineRequestsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_discipline_information'])
          })
        break;
      case 'add':
        this.disciplineRequestsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_discipline_information'])
          })
        break;
    }
  }

  getDR_offender(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_request_offender_uid: el.discipline_request_offender_uid,
      discipline_request_uid: el.discipline_request_uid,
      personnel_uid: el.personnel_uid,
      position: el.position,
      agency: el.agency,
      discipline_approve_result_id: el.discipline_approve_result_id,
      punishment_type_id: el.punishment_type_id
    })
  }

  getDR_document(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_request_document_uid: el.discipline_request_document_uid,
      discipline_request_uid: el.discipline_request_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }

  createDR_document(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      discipline_request_document_uid: null,
      discipline_request_uid: this.id ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  getDA_document(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_approve_document_uid: el.discipline_approve_document_uid,
      discipline_request_uid: el.discipline_request_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }

  createDA_document(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      discipline_approve_document_uid: null,
      discipline_request_uid: this.id ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  getDR_committee(el) {
    return this.baseFormBuilder.group({
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      discipline_request_committee_uid: el.discipline_request_committee_uid,
      discipline_request_uid: el.discipline_request_uid,
      personnel_uid: el.personnel_uid,
      committee_type_id: el.committee_type_id,
      position_name: el.position_name,
      person_name: el.person_name
    })
  }

  deleteItemOff(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.disciplineRequestOffendersService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('discipline_request_offender') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('discipline_request_offender') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteItemBoard(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.disciplineRequestCommitteesService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('discipline_request_committee') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('discipline_request_committee') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.disciplineRequestDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('discipline_request_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('discipline_request_document') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteItemDocApprove(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.disciplineApproveDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('discipline_approve_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('discipline_approve_document') as FormArray
      formarray.removeAt(i)
    }
  }

  downloadDoc(url) {
    this.disciplineRequestsService.download_file(url)
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      discipline_request_uid: [null],
      document_no: [''],
      document_date: [null],
      title_name: [''],
      description: [''],
      fault_type_id: [null],
      complainer: [''],
      responsible_personnel_guid: [null],
      action_date: [null],
      discipline_approve_status_id: [1],
      discipline_request_offender: this.baseFormBuilder.array([]),
      discipline_request_document: this.baseFormBuilder.array([]),
      discipline_request_committee: this.baseFormBuilder.array([]),
      discipline_approve_document: this.baseFormBuilder.array([])
    })
  }

}
