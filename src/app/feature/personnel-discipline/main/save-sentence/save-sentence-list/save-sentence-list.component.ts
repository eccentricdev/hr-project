import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { AdjudicateRequestsService } from 'src/app/core/service/discripline/adjudicate-requests.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-sentence-list',
  templateUrl: './save-sentence-list.component.html',
  styleUrls: ['./save-sentence-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveSentenceListComponent extends BaseList implements OnInit {

  rows:any = []
  personnel$ = new Observable()
  approveStatuss$ = new Observable()
  datasDisplay$ = new Observable()

  searchData = ''

  dataSearch = {
    budget_year:'',
    discipline_approve_status_id:null,
    plaintiff_name:'',
    accused_name:''
  }
  constructor(public router : Router,
    public appService : AppService,
    public personnelService : PersonnelService,
    public disciplineApproveStatussService : DisciplineApproveStatussService,
    public adjudicateRequestsService : AdjudicateRequestsService,
    ) {
    super();
    this.personnel$ = this.personnelService.getAll()
    this.approveStatuss$ = this.disciplineApproveStatussService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.adjudicateRequestsService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  addPage(){
    this.router.navigate(['app/save_sentence/add']) 
  }
  editPage(id){
    this.router.navigate(['app/save_sentence/edit',id])
  }

  search(){
    console.log(this.searchData);
      setTimeout(() => {
        this.adjudicateRequestsService.query(`?search=${this.searchData}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  // search() {
  //   console.log(this.dataSearch);
  //   let queryStr = createQueryStringFromObject(this.dataSearch)
  //   if (queryStr) {
  //     this.datasDisplay$ = this.adjudicateRequestsService.query(`?${queryStr}`)
  //     this.datasDisplay$.pipe(
  //       tap(x => this.rows = this.updateMatTable(x))
  //     ).subscribe()
  //   }
  // }

  clear(){
    this.dataSearch = {
      budget_year:'',
      discipline_approve_status_id:null,
      plaintiff_name:'',
      accused_name:''
    }
    this.getService()
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.adjudicateRequestsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
