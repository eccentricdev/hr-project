import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { timeStamp } from 'console';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { AdjudicateRequestAccomplicesService } from 'src/app/core/service/discripline/adjudicate-request-accomplices.service';
import { AdjudicateRequestDocumentsService } from 'src/app/core/service/discripline/adjudicate-request-documents.service';
import { AdjudicateRequestSummonsesService } from 'src/app/core/service/discripline/adjudicate-request-summonses.service';
import { AdjudicateRequestsService } from 'src/app/core/service/discripline/adjudicate-requests.service';
import { CourtsService } from 'src/app/core/service/discripline/courts.service';
import { DisciplineApproveStatussService } from 'src/app/core/service/discripline/discipline-approve-statuss.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupRelatedPersonComponent } from '../popup-related-person/popup-related-person.component';
import { PopupSummonsComponent } from '../popup-summons/popup-summons.component';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-sentence-form',
  templateUrl: './save-sentence-form.component.html',
  styleUrls: ['./save-sentence-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveSentenceFormComponent extends BaseForm implements OnInit {

  approveStatuss$ = new Observable()
  personnel$ = new Observable()
  courts$ = new Observable()

  dateToday = new Date()

  dataEdit: any

  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public courtsService: CourtsService,
    public personnelService : PersonnelService,
    public disciplineApproveStatussService: DisciplineApproveStatussService,
    public adjudicateRequestAccomplicesService : AdjudicateRequestAccomplicesService,
    public adjudicateRequestDocumentsService : AdjudicateRequestDocumentsService,
    public adjudicateRequestSummonsesService : AdjudicateRequestSummonsesService,
    public adjudicateRequestsService: AdjudicateRequestsService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.personnel$ = this.personnelService.getAll()
    this.approveStatuss$ = this.disciplineApproveStatussService.getAll()
    this.courts$ = this.courtsService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('document_date').setValue(this.dateToday)
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.adjudicateRequestsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(() => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              adjudicate_request_uid: this.dataEdit.adjudicate_request_uid,
              document_no: this.dataEdit.document_no,
              document_date: this.dataEdit.document_date,
              informed_personnel_uid: this.dataEdit.informed_personnel_uid,
              informed_date: this.dataEdit.informed_date,
              discipline_approve_status_id: this.dataEdit.discipline_approve_status_id,
              adjudicate_request_subject_name: this.dataEdit.adjudicate_request_subject_name,
              budget_year: this.dataEdit.budget_year,
              decided_case_no: this.dataEdit.decided_case_no,
              decided_case_date: this.dataEdit.decided_case_date,
              undecided_case_no: this.dataEdit.undecided_case_no,
              undecided_case_date: this.dataEdit.undecided_case_date,
              litigation_department_name: this.dataEdit.litigation_department_name,
              indictment_date: this.dataEdit.indictment_date,
              plaintiff_name: this.dataEdit.plaintiff_name,
              accused_name: this.dataEdit.accused_name,
              court_id: this.dataEdit.court_id,
              trial_result_detail: this.dataEdit.trial_result_detail,
              capital_amount: this.dataEdit.capital_amount,
              adjudicate_request_description: this.dataEdit.adjudicate_request_description,
            })
            let formoff = this.form.get('adjudicate_request_accomplice') as FormArray
            let formdoc = this.form.get('adjudicate_request_document') as FormArray
            let formcom = this.form.get('adjudicate_request_summons') as FormArray
            while(formoff.length != 0){
              formoff.removeAt(0)
            }
            while(formdoc.length != 0){
              formdoc.removeAt(0)
            }
            while(formcom.length != 0){
              formcom.removeAt(0)
            }
            if(this.dataEdit.adjudicate_request_accomplice.length != 0){
              this.dataEdit.adjudicate_request_accomplice.forEach(element => {
                formoff.push(this.getAR_accomplice(element))
              });
            }
           
            if(this.dataEdit.adjudicate_request_document.length != 0){
              this.dataEdit.adjudicate_request_document.forEach(element => {
                formdoc.push(this.getAR_doc(element))
              });
            }
            
            if(this.dataEdit.adjudicate_request_summons.length != 0){
              this.dataEdit.adjudicate_request_summons.forEach(element => {
                formcom.push(this.getAR_summons(element))
              });
            }
          
         
          })
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }


  popUpRelatedPersonAdd() {
    const dialogRef = this.dialog.open(
      PopupRelatedPersonComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null , id:this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpRelatedPersonEdit(array,index) {
    const dialogRef = this.dialog.open(
      PopupRelatedPersonComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpSummonsAdd() {
    const dialogRef = this.dialog.open(
      PopupSummonsComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null ,id:this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpSummonsEdit(array,index) {
    const dialogRef = this.dialog.open(
      PopupSummonsComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: {editForm: null, state:'add'}         
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('adjudicate_request_document') as FormArray
        formarray.push(this.createAR_doc(callback))
      }
    })

  }

  editfile(array,index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array , state:'edit' ,remark: array.remark}  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ                     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('adjudicate_request_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark:callback.remark
        })
      }
    })

  }

  close() {
    this.router.navigate(['app/save_sentence'])
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.adjudicateRequestsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
           this.router.navigate(['app/save_sentence'])
          })
        break;
      case 'add':
        this.adjudicateRequestsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
           this.router.navigate(['app/save_sentence'])
          })
        break;
    }
    
  }

  downloadDoc(url){
    this.adjudicateRequestsService.download_file(url)
  }

  getAR_accomplice(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      adjudicate_request_accomplice_uid: el.adjudicate_request_accomplice_uid,
      adjudicate_request_uid: el.adjudicate_request_uid,
      accomplice_name: el.accomplice_name,
      tel_no: el.tel_no,
      tel_reserve_no: el.tel_reserve_no,
      email: el.email,
      relation_name: el.relation_name
    })
  }

  getAR_doc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      adjudicate_request_document_uid: el.adjudicate_request_document_uid,
      adjudicate_request_uid: el.adjudicate_request_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }


  createAR_doc(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      adjudicate_request_document_uid: null,
      adjudicate_request_uid: this.id? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }

  getAR_summons(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      adjudicate_request_summons_uid: el.adjudicate_request_summons_uid,
      adjudicate_request_uid: el.adjudicate_request_uid,
      seq: el.seq,
      apm_date: el.apm_date,
      adjudicate_date: el.adjudicate_date,
      adjudicate_detail: el.adjudicate_detail,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      result_description: el.result_description
    })
  }

  deleteItemAcc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.adjudicateRequestAccomplicesService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('adjudicate_request_accomplice') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('adjudicate_request_accomplice') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.adjudicateRequestDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('adjudicate_request_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('adjudicate_request_document') as FormArray
      formarray.removeAt(i)
    }
  }
  

  deleteItemSum(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.adjudicateRequestSummonsesService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('adjudicate_request_summons') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('adjudicate_request_summons') as FormArray
      formarray.removeAt(i)
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      adjudicate_request_uid: [null],
      document_no: [''],
      document_date: [null],
      informed_personnel_uid: [null],
      informed_date: [null],
      discipline_approve_status_id: [1],
      adjudicate_request_subject_name: [''],
      budget_year: 0,
      decided_case_no: [''],
      decided_case_date: [null],
      undecided_case_no: [''],
      undecided_case_date: [null],
      litigation_department_name: [''],
      indictment_date: [null],
      plaintiff_name: [''],
      accused_name: [''],
      court_id: [null],
      trial_result_detail: [''],
      capital_amount: 0,
      adjudicate_request_description: [''],
      adjudicate_request_accomplice: this.baseFormBuilder.array([]),
      adjudicate_request_document: this.baseFormBuilder.array([]),
      adjudicate_request_summons: this.baseFormBuilder.array([]),
    })
  }

}
