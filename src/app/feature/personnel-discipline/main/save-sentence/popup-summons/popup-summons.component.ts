import { Inject } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PopupDetailManpowersComponent } from 'src/app/feature/allocate-people/save-request-people/popup-detail-manpowers/popup-detail-manpowers.component';

@Component({
  selector: 'app-popup-summons',
  templateUrl: './popup-summons.component.html',
  styleUrls: ['./popup-summons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupSummonsComponent implements OnInit {

  request = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    adjudicate_request_summons_uid: null,
    adjudicate_request_uid: null,
    seq: 0,
    apm_date: null,
    adjudicate_date: null,
    adjudicate_detail: '',
    document_name: '',
    document_url: '',
    mime_type_name: '',
    result_description: ''
  }
  constructor(public dialogRef: MatDialogRef<PopupSummonsComponent>,
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    if (this.data.editForm != null) {
      console.log(this.data.editForm);
      this.getArray(this.data.editForm)
    }else{
      this.request.adjudicate_request_uid = this.data.id
    }
  }

  getArray(el){
    this.request = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      adjudicate_request_summons_uid: el.adjudicate_request_summons_uid,
      adjudicate_request_uid: el.adjudicate_request_uid,
      seq: el.seq,
      apm_date: el.apm_date,
      adjudicate_date: el.adjudicate_date,
      adjudicate_detail: el.adjudicate_detail,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      result_description: el.result_description
    }
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        adjudicate_request_summons_uid: null,
        adjudicate_request_uid: null,
        seq: 0,
        apm_date: null,
        adjudicate_date: null,
        adjudicate_detail: '',
        document_name: '',
        document_url: '',
        mime_type_name: '',
        result_description: ''
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        adjudicate_request_summons_uid: el.adjudicate_request_summons_uid,
        adjudicate_request_uid: el.adjudicate_request_uid,
        seq: el.seq,
        apm_date: el.apm_date,
        adjudicate_date: el.adjudicate_date,
        adjudicate_detail: el.adjudicate_detail,
        document_name: el.document_name,
        document_url: el.document_url,
        mime_type_name: el.mime_type_name,
        result_description: el.result_description
      })
    }
  }



  save(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('adjudicate_request_summons')).push(this.createArray(this.request))  
    }else{
      (<FormArray>this.data.form.get('adjudicate_request_summons')).controls[this.data.ind].patchValue({
        seq: this.request.seq,
        apm_date: this.request.apm_date,
        adjudicate_date: this.request.adjudicate_date,
        adjudicate_detail: this.request.adjudicate_detail,
        document_name: this.request.document_name,
        document_url: this.request.document_url,
        mime_type_name: this.request.mime_type_name,
        result_description: this.request.result_description
      })
    }
  }


  saveClose(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('adjudicate_request_summons')).push(this.createArray(this.request)) 
      this.close() 
    }else{
      (<FormArray>this.data.form.get('adjudicate_request_summons')).controls[this.data.ind].patchValue({
        seq: this.request.seq,
        apm_date: this.request.apm_date,
        adjudicate_date: this.request.adjudicate_date,
        adjudicate_detail: this.request.adjudicate_detail,
        document_name: this.request.document_name,
        document_url: this.request.document_url,
        mime_type_name: this.request.mime_type_name,
        result_description: this.request.result_description
      })
      this.close()
    }
  }

  close(){
    this.dialogRef.close('close')
    }
  

}
