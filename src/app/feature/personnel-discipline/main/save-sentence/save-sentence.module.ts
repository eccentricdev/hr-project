import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveSentenceListComponent } from './save-sentence-list/save-sentence-list.component';
import { SaveSentenceFormComponent } from './save-sentence-form/save-sentence-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SetNumberPeopleFormComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-form/set-number-people-form.component';
import { SetNumberPeopleListComponent } from 'src/app/feature/allocate-people/set-number-people/set-number-people-list/set-number-people-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupRelatedPersonComponent } from './popup-related-person/popup-related-person.component';
import { PopupSummonsComponent } from './popup-summons/popup-summons.component';

const routes:Routes = [
  {
    path:'',
    component:SaveSentenceListComponent
  },
  {
    path:'add',
    component:SaveSentenceFormComponent
  },
  {
    path:'edit/:id',
    component:SaveSentenceFormComponent
  },
  
]

@NgModule({
  declarations: [
    SaveSentenceListComponent, 
    SaveSentenceFormComponent, PopupRelatedPersonComponent, PopupSummonsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveSentenceModule { }
