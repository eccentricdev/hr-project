import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PopupDetailManpowersComponent } from 'src/app/feature/allocate-people/save-request-people/popup-detail-manpowers/popup-detail-manpowers.component';

@Component({
  selector: 'app-popup-related-person',
  templateUrl: './popup-related-person.component.html',
  styleUrls: ['./popup-related-person.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupRelatedPersonComponent implements OnInit {

  result = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    adjudicate_request_accomplice_uid: null,
    adjudicate_request_uid: null,
    accomplice_name: '',
    tel_no: '',
    tel_reserve_no: '',
    email: '',
    relation_name: ''
  }
  constructor(public dialogRef: MatDialogRef<PopupRelatedPersonComponent>,
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);
    if (this.data.editForm != null) {
      console.log(this.data.editForm);
      this.getArray(this.data.editForm)
    }else{
      this.result.adjudicate_request_uid = this.data.id
    }
  }

  getArray(el) {
    this.result = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      adjudicate_request_accomplice_uid: el.adjudicate_request_accomplice_uid,
      adjudicate_request_uid: el.adjudicate_request_uid,
      accomplice_name: el.accomplice_name,
      tel_no: el.tel_no,
      tel_reserve_no: el.tel_reserve_no,
      email: el.email,
      relation_name: el.relation_name
    }
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        adjudicate_request_accomplice_uid: null,
        adjudicate_request_uid: null,
        accomplice_name: '',
        tel_no: '',
        tel_reserve_no: '',
        email: '',
        relation_name: ''
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        adjudicate_request_accomplice_uid: el.adjudicate_request_accomplice_uid,
        adjudicate_request_uid: el.adjudicate_request_uid,
        accomplice_name: el.accomplice_name,
        tel_no: el.tel_no,
        tel_reserve_no: el.tel_reserve_no,
        email: el.email,
        relation_name: el.relation_name
      })
    }
  }



  save() {
    if (this.data.editForm == null) {
      (<FormArray>this.data.form.get('adjudicate_request_accomplice')).push(this.createArray(this.result))
    } else {
      (<FormArray>this.data.form.get('adjudicate_request_accomplice')).controls[this.data.ind].patchValue({
        accomplice_name: this.result.accomplice_name,
        tel_no: this.result.tel_no,
        tel_reserve_no: this.result.tel_reserve_no,
        email: this.result.email,
        relation_name: this.result.relation_name
      })
    }
  }


  saveClose() {
    if (this.data.editForm == null) {
      (<FormArray>this.data.form.get('adjudicate_request_accomplice')).push(this.createArray(this.result))
      this.close()
    } else {
      (<FormArray>this.data.form.get('adjudicate_request_accomplice')).controls[this.data.ind].patchValue({
        accomplice_name: this.result.accomplice_name,
        tel_no: this.result.tel_no,
        tel_reserve_no: this.result.tel_reserve_no,
        email: this.result.email,
        relation_name: this.result.relation_name
      })
      this.close()
    }
  }

  close() {
    this.dialogRef.close('close')
  }

}
