import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ConsiderationProcessesService } from 'src/app/core/service/discripline/consideration-processes.service';
import { ViewUploadDialogComponent } from 'src/app/shared/components/view-upload-dialog/view-upload-dialog.component';

@Component({
  selector: 'app-set-step-consider-form',
  templateUrl: './set-step-consider-form.component.html',
  styleUrls: ['./set-step-consider-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetStepConsiderFormComponent extends BaseForm implements OnInit {

  @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Choose File';
  dataEdit:any

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    public considerationProcessesService : ConsiderationProcessesService,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('file_name').disable()
    switch (this.state) {
      case 'edit':
      this.considerationProcessesService.get(this.id).pipe(
        tap(x => this.dataEdit = x),
        tap(() => {
          this.form.patchValue({
            status_id: this.dataEdit.status_id,
            created_by: this.dataEdit.created_by,
            created_datetime: this.dataEdit.created_datetime,
            updated_by: this.dataEdit.updated_by,
            updated_datetime: this.dataEdit.updated_datetime,
            consideration_process_id: this.dataEdit.consideration_process_id,
            consideration_process_remark: this.dataEdit.consideration_process_remark,
            file_name: this.dataEdit.file_name,
            document_name: this.dataEdit.document_name,
            document_url: this.dataEdit.document_url,
            mime_type_name: this.dataEdit.mime_type_name
          })
        })
      ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getResultFile(file){
    console.log(file);
    this.form.patchValue({
      file_name: file.original_fileName,
      document_name: file.file_name,
      document_url: file.file_url,
      mime_type_name: file.mime_type_name
    })
  }

  viewDoc(){
    this.considerationProcessesService.download_file(this.form.value.document_url)
  }



  close() {
    this.router.navigate(['app/set-step-consider'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.considerationProcessesService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
             this.router.navigate(['app/set-step-consider'])
          })
        break;
      case 'add':
        this.considerationProcessesService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
             this.router.navigate(['app/set-step-consider'])
          })
        break;
    }
    
  }

  viewfile(url) {
    const dialogRef = this.dialog.open(
      ViewUploadDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: url  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
      }
    })
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      consideration_process_id: [null],
      consideration_process_remark: [''],
      file_name: [''],
      document_name: [''],
      document_url: [''],
      mime_type_name: ['']
    })
  }

}
