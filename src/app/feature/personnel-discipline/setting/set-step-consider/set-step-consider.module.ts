import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetStepConsiderFormComponent } from './set-step-consider-form/set-step-consider-form.component';
import { SetStepConsiderListComponent } from './set-step-consider-list/set-step-consider-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetStepConsiderListComponent
  },
  {
    path:'add',
    component:SetStepConsiderFormComponent
  },
  {
    path:'edit/:id',
    component:SetStepConsiderFormComponent
  },
  
]


@NgModule({
  declarations: [
    SetStepConsiderFormComponent, 
    SetStepConsiderListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetStepConsiderModule { }
