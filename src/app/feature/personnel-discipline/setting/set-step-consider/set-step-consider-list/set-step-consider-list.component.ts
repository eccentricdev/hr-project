import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ConsiderationProcessesService } from 'src/app/core/service/discripline/consideration-processes.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-step-consider-list',
  templateUrl: './set-step-consider-list.component.html',
  styleUrls: ['./set-step-consider-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetStepConsiderListComponent extends BaseList implements OnInit {

  rows:any = []

  dataSearch = {
    consideration_process_remark : ''
  }

  constructor(public router : Router,
    public appService : AppService,
    public considerationProcessesService : ConsiderationProcessesService) {
    super();
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.considerationProcessesService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  search(){
    console.log(this.dataSearch.consideration_process_remark);
      setTimeout(() => {
        this.considerationProcessesService.query(`?consideration_process_remark=${this.dataSearch.consideration_process_remark}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/set-step-consider/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set-step-consider/edit',id])
  }

  downloadDoc(url){
    console.log(url);
    this.considerationProcessesService.download_file(url)
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.considerationProcessesService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
