import { state } from '@angular/animations';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { ImpositionsPersonnelTypeService } from 'src/app/core/service/discripline/impositions-personnel-type.service';
import { ImpositionsService } from 'src/app/core/service/discripline/impositions.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { PopupTypePenaltyComponent } from '../popup-type-penalty/popup-type-penalty.component';

@Component({
  selector: 'app-set-type-penalty-personnel-form',
  templateUrl: './set-type-penalty-personnel-form.component.html',
  styleUrls: ['./set-type-penalty-personnel-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypePenaltyPersonnelFormComponent extends BaseForm implements OnInit {


  personnelTypes$ = new Observable()
  dataEdit: any
  personnelType: any
  personnelTypeId: any
  punishmentTypes: any = []
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    public impositionsService: ImpositionsService,
    public punishmentTypesService: PunishmentTypesService,
    public impositionsPersonnelTypeService: ImpositionsPersonnelTypeService,
    public personnelTypesService: PersonnelTypesService,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.personnelTypes$ = this.personnelTypesService.getAll()
    this.punishmentTypesService.getAll().pipe(
      tap(x => this.punishmentTypes = x)
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.impositionsPersonnelTypeService.get(this.id).pipe(
          tap(x => this.dataEdit = x),
          tap(() => this.getPersonType(this.dataEdit)),
          tap(() => {
            let formarray = this.form.get('data') as FormArray
            while (formarray.length != 0) {
              formarray.removeAt(0)
            }
            this.dataEdit.forEach(element => {
              formarray.push(this.getArray(element))
            });
          }),

        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getPersonType(value) {
    console.log(value);
    this.personnelTypeId = value
    console.log(this.personnelTypeId);

  }

  popUpAdd() {
    const dialogRef = this.dialog.open(
      PopupTypePenaltyComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null, personId: this.state == 'add' ?  this.personnelTypeId : +this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpEdit(array, index) {
    console.log(array);
    const dialogRef = this.dialog.open(
      PopupTypePenaltyComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index, personId: this.id }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  deleteArray(data, i) {
    let formarray = this.form.get('data') as FormArray
    if (data.imposition_id) {
      this.impositionsService.deleteData(data.imposition_id).pipe(
        catchError(err => {
          // alert ตรงนี่
          this.appSV.swaltAlertError('Error')
          return throwError(err)
        })).subscribe((x: any) => {
          console.log(x)
          this.ngOnInit()
          this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
        })
    } else {
      formarray.removeAt(i)
    }

  }

  close() {
    this.router.navigate(['app/set-type-penalty-personnel'])
  }

  save() {
    console.log(this.form.getRawValue());
    let items = this.form.getRawValue()
    console.log(items.data);
    
    switch (this.state) {
      case 'edit':
        this.impositionsService.update(items.data).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set-type-penalty-personnel'])
          })
        break;
      case 'add':
        this.impositionsService.add(items.data).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set-type-penalty-personnel'])
          })
        break;
    }

  }

  getArray(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      imposition_id: el.imposition_id,
      personnel_type_id: el.personnel_type_id,
      punishment_type_uid: el.punishment_type_uid
    })
  }

  createForm() {
    return this.baseFormBuilder.group({
      data: this.baseFormBuilder.array([])
    })
  }



}
