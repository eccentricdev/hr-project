import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetTypePenaltyPersonnelFormComponent } from './set-type-penalty-personnel-form/set-type-penalty-personnel-form.component';
import { SetTypePenaltyPersonnelListComponent } from './set-type-penalty-personnel-list/set-type-penalty-personnel-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupTypePenaltyComponent } from './popup-type-penalty/popup-type-penalty.component';

const routes:Routes = [
  {
    path:'',
    component:SetTypePenaltyPersonnelListComponent
  },
  {
    path:'add',
    component:SetTypePenaltyPersonnelFormComponent
  },
  {
    path:'edit/:id',
    component:SetTypePenaltyPersonnelFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetTypePenaltyPersonnelFormComponent, 
    SetTypePenaltyPersonnelListComponent, PopupTypePenaltyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetTypePenaltyPersonnelModule { }
