import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/core/service/app.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import { PopupTypeDisciplineComponent } from '../../set-schedule/popup-type-discipline/popup-type-discipline.component';

@Component({
  selector: 'app-popup-type-penalty',
  templateUrl: './popup-type-penalty.component.html',
  styleUrls: ['./popup-type-penalty.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupTypePenaltyComponent implements OnInit {

  punishmentTypes$ = new Observable()

  file:any = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    imposition_id: null,
    personnel_type_id: null,
    punishment_type_uid: null
  }

  constructor(public dialogRef: MatDialogRef<PopupTypePenaltyComponent>,
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appService : AppService,
    public punishmentTypesService :PunishmentTypesService,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.punishmentTypes$ = this.punishmentTypesService.getAll()
    }

  ngOnInit(): void {
    console.log(this.data.personId);
    console.log(this.data);
    
    if(this.data.editForm != null){
      this.getArray(this.data.editForm)
    }else{
      this.file.personnel_type_id = this.data.personId
    }
  }

  getArray(el){
    this.file = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      imposition_id: el.imposition_id,
      personnel_type_id: el.personnel_type_id,
      punishment_type_uid: el.punishment_type_uid
    }
  }

  createArrayDetails(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        imposition_id: null,
        personnel_type_id: this.data.personId,
        punishment_type_uid: null
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        imposition_id: el.imposition_id,
        personnel_type_id: el.personnel_type_id,
        punishment_type_uid: el.punishment_type_uid
      })
    }
  }

  save(){
      if(this.data.editForm == null){
        (<FormArray>this.data.form.get('data')).push(this.createArrayDetails(this.file))  
      }else{
        (<FormArray>this.data.form.get('data')).controls[this.data.ind].patchValue({
          punishment_type_uid:this.file.punishment_type_uid,
        })
      }
  }


  saveClose(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('data')).push(this.createArrayDetails(this.file))  
      this.close()
    }else{
      (<FormArray>this.data.form.get('data')).controls[this.data.ind].patchValue({
        punishment_type_uid:this.file.punishment_type_uid,
      })
      this.close()
    }
  }

  close(){
    this.dialogRef.close('close')
    }

}
