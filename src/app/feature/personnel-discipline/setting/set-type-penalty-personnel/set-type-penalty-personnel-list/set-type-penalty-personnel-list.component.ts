import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { ImpositionsService } from 'src/app/core/service/discripline/impositions.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-type-penalty-personnel-list',
  templateUrl: './set-type-penalty-personnel-list.component.html',
  styleUrls: ['./set-type-penalty-personnel-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypePenaltyPersonnelListComponent extends BaseList  implements OnInit {

  rows:any = []
  personnelTypes:any = []

  dataSearch = {
    personnel_type_name_th : ''
  }

  constructor(public router : Router,
    public appService : AppService,
    public cdRef : ChangeDetectorRef,
    public personnelTypesService : PersonnelTypesService,
    public impositionsService : ImpositionsService) {
    super();
   
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.personnelTypesService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x)),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search(){
    console.log(this.dataSearch.personnel_type_name_th);
      setTimeout(() => {
        this.personnelTypesService.query(`?personnel_type_name_th=${this.dataSearch.personnel_type_name_th}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/set-type-penalty-personnel/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set-type-penalty-personnel/edit',id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.impositionsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
