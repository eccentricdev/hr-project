import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetCourtFormComponent } from './set-court-form/set-court-form.component';
import { SetCourtListComponent } from './set-court-list/set-court-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SaveEmploymentContractFormComponent } from 'src/app/feature/recruitment-and-selection/save-employment-contract/save-employment-contract-form/save-employment-contract-form.component';
import { SaveEmploymentContractListComponent } from 'src/app/feature/recruitment-and-selection/save-employment-contract/save-employment-contract-list/save-employment-contract-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


const routes:Routes = [
  {
    path:'',
    component:SetCourtListComponent
  },
  {
    path:'add',
    component:SetCourtFormComponent
  },
  {
    path:'edit/:id',
    component:SetCourtFormComponent
  },
  
]


@NgModule({
  declarations: [
    SetCourtFormComponent, 
    SetCourtListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetCourtModule { }
