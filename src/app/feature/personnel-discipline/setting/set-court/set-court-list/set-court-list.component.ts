import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, debounceTime, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { CourtsService } from 'src/app/core/service/discripline/courts.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-court-list',
  templateUrl: './set-court-list.component.html',
  styleUrls: ['./set-court-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetCourtListComponent extends BaseList implements OnInit {

  rows:any = []

  dataSearch = {
    courte_name_th : ''
  }

  constructor(public router : Router,
    public appService : AppService,
    public courtsService : CourtsService) {
    super();
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.courtsService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  } 

  search(){
    console.log(this.dataSearch.courte_name_th);
      setTimeout(() => {
        this.courtsService.query(`?search=${this.dataSearch.courte_name_th}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/set-court/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set-court/edit',id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.courtsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
