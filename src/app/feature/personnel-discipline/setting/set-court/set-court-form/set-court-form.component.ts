import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { CourtsService } from 'src/app/core/service/discripline/courts.service';

@Component({
  selector: 'app-set-court-form',
  templateUrl: './set-court-form.component.html',
  styleUrls: ['./set-court-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetCourtFormComponent extends BaseForm implements OnInit {

  statuss$ = new Observable()
  dataEdit: any

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public courtsService: CourtsService,
    public activeRoute: ActivatedRoute,
    public statusService: StatusService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.statuss$ = this.statusService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.courtsService.get(this.id).pipe(
          tap(x => this.dataEdit = x),
          tap(() => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              court_id: this.dataEdit.court_id,
              courte_name_th: this.dataEdit.courte_name_th,
              court_name_en: this.dataEdit.court_name_en
            })
          })
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  close() {
    this.router.navigate(['app/set-court'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.courtsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
             this.router.navigate(['app/set-court'])
          })
        break;
      case 'add':
        this.courtsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
             this.router.navigate(['app/set-court'])
          })
        break;
    }
   
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      court_id: [null],
      courte_name_th: [''],
      court_name_en: ['']
    })
  }

}
