import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ActionTypesService } from 'src/app/core/service/discripline/action-types.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';

@Component({
  selector: 'app-set-type-penalty-form',
  templateUrl: './set-type-penalty-form.component.html',
  styleUrls: ['./set-type-penalty-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypePenaltyFormComponent extends BaseForm implements OnInit {

  dataEdit:any
  actionTypes$ = new Observable()

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public actionTypesService : ActionTypesService,
    public punishmentTypesService : PunishmentTypesService,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.actionTypes$ = this.actionTypesService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.punishmentTypesService.get(this.id).pipe(
          tap(x => this.dataEdit = x),
          tap(() => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              punishment_type_id: this.dataEdit.punishment_type_id,
              action_type_id: this.dataEdit.action_type_id,
              punishment_type_code: this.dataEdit.punishment_type_code,
              punishment_type_name_th: this.dataEdit.punishment_type_name_th,
              punishment_type_name_en: this.dataEdit.punishment_type_name_en,
              punishment_type_remark: this.dataEdit.punishment_type_remark,
              is_seriously: this.dataEdit.is_seriously,
            })
          })
        ).subscribe()

        break;
      case 'add':

        break; 
    }
  }

  close(){
    this.router.navigate(['app/set-type-penalty'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.punishmentTypesService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
           this.router.navigate(['app/set-type-penalty'])
          })
        break;
      case 'add':
        this.punishmentTypesService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
           this.router.navigate(['app/set-type-penalty'])
          })
        break;
    }
    
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      punishment_type_id: [null],
      action_type_id: [null],
      punishment_type_code: [''],
      punishment_type_name_th: [''],
      punishment_type_name_en: [''],
      punishment_type_remark: [''],
      is_seriously: false,
    })
  }

}
