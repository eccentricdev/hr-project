import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PunishmentTypesService } from 'src/app/core/service/discripline/punishment-types.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-type-penalty-list',
  templateUrl: './set-type-penalty-list.component.html',
  styleUrls: ['./set-type-penalty-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypePenaltyListComponent extends BaseList implements OnInit {

  rows:any = []

  dataSearch = ''


  constructor(public router : Router,
    public appService : AppService,
    public punishmentTypesService : PunishmentTypesService) {
    super();
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.punishmentTypesService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  search(){
    console.log(this.dataSearch);
      setTimeout(() => {
        this.punishmentTypesService.query(`?search=${this.dataSearch}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/set-type-penalty/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set-type-penalty/edit',id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.punishmentTypesService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
