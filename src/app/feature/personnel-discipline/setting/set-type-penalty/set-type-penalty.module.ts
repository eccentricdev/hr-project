import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetTypePenaltyFormComponent } from './set-type-penalty-form/set-type-penalty-form.component';
import { SetTypePenaltyListComponent } from './set-type-penalty-list/set-type-penalty-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetTypePenaltyListComponent
  },
  {
    path:'add',
    component:SetTypePenaltyFormComponent
  },
  {
    path:'edit/:id',
    component:SetTypePenaltyFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetTypePenaltyFormComponent, 
    SetTypePenaltyListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetTypePenaltyModule { }
