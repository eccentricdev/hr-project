import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PopupListTheBoardComponent } from '../../../main/save-discipline-information/popup-list-the-board/popup-list-the-board.component';

@Component({
  selector: 'app-popup-type-discipline',
  templateUrl: './popup-type-discipline.component.html',
  styleUrls: ['./popup-type-discipline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupTypeDisciplineComponent implements OnInit {

  file: any = {
    status_id: [null],
    created_by: [null],
    created_datetime: [null],
    updated_by: [null],
    updated_datetime: [null],
    fault_type_id: [null],
    action_type_id: [null],
    fault_type_code: [''],
    fault_type_name_th: [''],
    fault_type_name_en: [''],
    fault_type_remark: ['']
  }

  constructor(public dialogRef: MatDialogRef<PopupTypeDisciplineComponent>,
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
console.log(this.data);
    if(this.data.editForm != null){
      console.log(this.data.editForm);
      this.getArray(this.data.editForm)
    }

  }


  getArray(el) {
    console.log(el);
    this.file = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      fault_type_id: el.fault_type_id,
      action_type_id: el.action_type_id,
      fault_type_code: el.fault_type_code,
      fault_type_name_th: el.fault_type_name_th,
      fault_type_name_en: el.fault_type_name_en,
      fault_type_remark: el.fault_type_remark
    }
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: [null],
        created_by: [null],
        created_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        fault_type_id: [null],
        action_type_id: [null],
        fault_type_code: [''],
        fault_type_name_th: [''],
        fault_type_name_en: [''],
        fault_type_remark: ['']
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        fault_type_id: el.fault_type_id,
        action_type_id: el.action_type_id,
        fault_type_code: el.fault_type_code,
        fault_type_name_th: el.fault_type_name_th,
        fault_type_name_en: el.fault_type_name_en,
        fault_type_remark: el.fault_type_remark
      })
    }
  }

  save() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('fault_type')).push(this.createArray(this.file))  
    }else{
      (<FormArray>this.data.form.get('fault_type')).controls[this.data.ind].patchValue({
        fault_type_code:this.file.fault_type_code,
        fault_type_name_th:this.file.fault_type_name_th,
      })
    }
  }




  saveClose() {
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('fault_type')).push(this.createArray(this.file)) 
      this.close() 
    }else{
      (<FormArray>this.data.form.get('fault_type')).controls[this.data.ind].patchValue({
        fault_type_code:this.file.fault_type_code,
        fault_type_name_th:this.file.fault_type_name_th,
      })
      this.close()
    }
  }

  close() {
    this.dialogRef.close('close')
  }

}
