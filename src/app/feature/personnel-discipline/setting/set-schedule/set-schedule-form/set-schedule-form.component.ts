import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ActionTypesService } from 'src/app/core/service/discripline/action-types.service';
import { FaultTypesService } from 'src/app/core/service/discripline/fault-types.service';
import { PopupTypeDisciplineComponent } from '../popup-type-discipline/popup-type-discipline.component';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-schedule-form',
  templateUrl: './set-schedule-form.component.html',
  styleUrls: ['./set-schedule-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetScheduleFormComponent extends BaseForm implements OnInit {

  
  dataEdit:any

  constructor(public formBuilder: FormBuilder,
    public router: Router,
    public actionTypesService: ActionTypesService,
    public dialog: MatDialog,
    public cdRef : ChangeDetectorRef,
    public faultTypesService: FaultTypesService,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.actionTypesService.get(this.id).pipe(
          tap(x => this.dataEdit = x),
          tap(() => {
            console.log(this.dataEdit);
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              action_type_id: this.dataEdit.action_type_id,
              action_type_code: this.dataEdit.action_type_code,
              action_type_name_th: this.dataEdit.action_type_name_th,
              action_type_name_en: this.dataEdit.action_type_name_en,
              action_type_remark: this.dataEdit.action_type_remark,
            })
            let formarray = this.form.get('fault_type') as FormArray
            while(formarray.length != 0){
              formarray.removeAt(0)
            }
            if(this.dataEdit.fault_type.length != 0){
              this.dataEdit.fault_type.forEach(element => {
                formarray.push(this.getArray(element))
              });
            }
          }),
          tap(() => this.cdRef.detectChanges())
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  popUpAdd() {
    const dialogRef = this.dialog.open(
      PopupTypeDisciplineComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popUpEdit(array, index) {
    const dialogRef = this.dialog.open(
      PopupTypeDisciplineComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  deleteItems(id,i){
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.faultTypesService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('fault_type') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('fault_type') as FormArray
      formarray.removeAt(i)
    }
    // let formarray = this.form.get('fault_type') as FormArray
    // formarray.removeAt(i)
  }

  close() {
    this.router.navigate(['app/set-schedule'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.actionTypesService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set-schedule'])
          })
        break;
      case 'add':
        this.actionTypesService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set-schedule'])
          })
        break;
    }

  }

  getArray(el){
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      fault_type_id: el.fault_type_id,
      action_type_id: el.action_type_id,
      fault_type_code: el.fault_type_code,
      fault_type_name_th: el.fault_type_name_th,
      fault_type_name_en: el.fault_type_name_en,
      fault_type_remark: el.fault_type_remark
    })
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      action_type_id: [null],
      action_type_code: [''],
      action_type_name_th: [''],
      action_type_name_en: [''],
      action_type_remark: [''],
      fault_type: this.baseFormBuilder.array([])
    })
  }

}
