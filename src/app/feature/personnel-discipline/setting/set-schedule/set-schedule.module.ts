import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetScheduleFormComponent } from './set-schedule-form/set-schedule-form.component';
import { SetScheduleListComponent } from './set-schedule-list/set-schedule-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupTypeDisciplineComponent } from './popup-type-discipline/popup-type-discipline.component';


const routes:Routes = [
  {
    path:'',
    component:SetScheduleListComponent
  },
  {
    path:'add',
    component:SetScheduleFormComponent
  },
  {
    path:'edit/:id',
    component:SetScheduleFormComponent
  },
  
]


@NgModule({
  declarations: [
    SetScheduleFormComponent, 
    SetScheduleListComponent, PopupTypeDisciplineComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetScheduleModule { }
