import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ActionTypesService } from 'src/app/core/service/discripline/action-types.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-schedule-list',
  templateUrl: './set-schedule-list.component.html',
  styleUrls: ['./set-schedule-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetScheduleListComponent extends BaseList implements OnInit {

  rows:any = []

  dataSearch = {
    action_type_name_th : ''
  }

  constructor(public router : Router,
    public appService : AppService,
    public actionTypesService : ActionTypesService) {
    super();
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.actionTypesService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  search(){
    console.log(this.dataSearch.action_type_name_th);
      setTimeout(() => {
        this.actionTypesService.query(`?action_type_name_th=${this.dataSearch.action_type_name_th}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/set-schedule/add']) 
  }
  editPage(id){
    this.router.navigate(['app/set-schedule/edit',id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.actionTypesService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
