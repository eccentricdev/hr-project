import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveSelectionResultsFormComponent } from './save-selection-results-form/save-selection-results-form.component';
import { SaveSelectionResultsListComponent } from './save-selection-results-list/save-selection-results-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { Tap1Component } from './tap/tap1/tap1.component';
import { Tap2Component } from './tap/tap2/tap2.component';
import { Tap3Component } from './tap/tap3/tap3.component';
import { HeardRecruitmentComponent } from './heard-recruitment/heard-recruitment.component';
import { PopupApplicationAddComponent } from './popup-application-add/popup-application-add.component';
import { PopupApplicationEditComponent } from './popup-application-edit/popup-application-edit.component';


const routes:Routes = [
  {
    path:'',
    component:SaveSelectionResultsListComponent
  },
  {
    path:'add',
    component:SaveSelectionResultsFormComponent
  },
  {
    path:'edit/:id',
    component:SaveSelectionResultsFormComponent
  },
  
]


@NgModule({
  declarations: [
    SaveSelectionResultsListComponent,
    SaveSelectionResultsFormComponent,
    HeardRecruitmentComponent,
    Tap1Component,
    Tap2Component,
    Tap3Component,
    PopupApplicationAddComponent,
    PopupApplicationEditComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveSelectionResultsModule { }
