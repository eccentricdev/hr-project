import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';


@Component({
  selector: 'app-heard-recruitment',
  templateUrl: './heard-recruitment.component.html',
  styleUrls: ['./heard-recruitment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeardRecruitmentComponent implements OnInit {

  @Input() dataEditRequest:any
  @Input() educationLevels:any
  @Input() qualifications:any
  @Input() personnelGroups:any
  @Input() personnelTypes:any
  display:any 
  
  
  constructor(public educationLevelsService : EducationLevelsService,
    public qualificationsService : QualificationsService,
    public personnelTypesService : PersonnelTypesService,
    public personnelGroupsService : PersonnelGroupsService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    this.display = this.dataEditRequest
  }

  ngOnInit(): void {
  }

}
