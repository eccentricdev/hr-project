import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';

@Component({
  selector: 'app-popup-application-edit',
  templateUrl: './popup-application-edit.component.html',
  styleUrls: ['./popup-application-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupApplicationEditComponent implements OnInit {


  request = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    selection_result_uid: null,
    application_uid: null,
    selection_request_uid: null,
    written_exam_date: null,
    written_exam_status_id: null,
    written_exam_1_score: 0,
    written_exam_2_score: 0,
    written_exam_remark: '',
    interview_date: null,
    interview_result_id: null,
    interview_score: 0,
    interview_remark: '',
    is_pass: false,
    manpower_uid: null,
    assign_date: null,
    assign_order: 0,
    sum_score: 0
  }

  requestsDatas:any = []
  applicantDatas:any = []
  totalExam: number = 0
  sumTotal: number = 0
  statusExam:any = [
    {
    id:1,
    name: 'ผ่าน'
    },
    {
    id:2,
    name: 'ไม่ผ่าน'
    }
]

  constructor(public dialogRef: MatDialogRef<PopupApplicationEditComponent>,
    public formBuilder: FormBuilder,
    public applicantService : ApplicantService,
    public selectionResultsService : SelectionResultsService,
    public SelectionRequestsService :SelectionRequestsService,
    public activeRoute: ActivatedRoute,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.SelectionRequestsService.getAll().pipe(
        tap(x => console.log(x)),
        tap(x => this.requestsDatas = x),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
      this.applicantService.getAll().pipe(
        tap(x => console.log(x)),
        tap((x:any) => {
          this.applicantDatas = x.map((el:any) => {
            return {...el,fullName:el.first_name_th + ' ' + el.last_name_th}
          })
          console.log(this.applicantDatas);
        }),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
     }

  ngOnInit(): void {
    console.log(this.data);
    if(this.data.editForm){
      this.getData(this.data.editForm)
      this.getTotalScore()
    }
  }


  getData(el) {
    this.request = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      selection_result_uid: el.selection_result_uid,
      application_uid: el.application_uid,
      selection_request_uid: el.selection_request_uid,
      written_exam_date: el.written_exam_date,
      written_exam_status_id: el.written_exam_status_id,
      written_exam_1_score: el.written_exam_1_score,
      written_exam_2_score: el.written_exam_2_score,
      written_exam_remark: el.written_exam_remark,
      interview_date: el.interview_date,
      interview_result_id: el.interview_result_id,
      interview_score: el.interview_score,
      interview_remark: el.interview_remark,
      is_pass: el.is_pass == null ? false : el.is_pass,
      manpower_uid: el.manpower_uid,
      assign_date: el.assign_date,
      assign_order: el.assign_order == null ? 0 : el.assign_order,
      sum_score: el.sum_score == null ? 0 : el.sum_score
    }
  }

  getTotalScore(){
    // console.log(this.request.written_exam_1_score);
    // console.log(this.request.written_exam_2_score);
    // console.log(this.request.interview_score);
    this.totalExam = this.request.written_exam_1_score + this.request.written_exam_2_score
    // console.log(this.totalExam);
    this.sumTotal = this.request.written_exam_1_score + this.request.written_exam_2_score + this.request.interview_score
    // console.log(this.sumTotal);
  }

  save(){
    console.log(this.request);
    let items = []
    items.push(this.request)
    this.selectionResultsService.update(items).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
      }) 
  }


  saveClose(){
    console.log(this.request);
    let items = []
    items.push(this.request)
    this.selectionResultsService.update(items).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.dialogRef.close(this.data.editForm.selection_request_uid)
      })
  }


  close(){
  this.dialogRef.close(this.data.editForm.selection_request_uid)
  }

}
