import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject, ViewChild } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { MaritalStatusService } from 'src/app/core/service/common/marital-status.service';
import { MilitaryStatusService } from 'src/app/core/service/common/military-status.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';

@Component({
  selector: 'app-popup-application-add',
  templateUrl: './popup-application-add.component.html',
  styleUrls: ['./popup-application-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupApplicationAddComponent extends BaseForm implements OnInit {


  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['select', '1', '2', '3'];
  selection = new SelectionModel<any>(true, []);
  rows: any = new MatTableDataSource<any>([]);
  agencys$ = new Observable()
  genders$ = new Observable()
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  position$ = new Observable()
  militaryStatus$ = new Observable()
  maritalStatus$ = new Observable()
  datasDisplay$ = new Observable()


  dataSearch = {
    position_uid: null,
    agency_uid: null,
    application_date: null,
    military_status_id: null,
    marital_status_id: null,
    gender_id: null,
  }

  constructor(
    public router: Router,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public gendersService: GendersService,
    public militaryStatusService: MilitaryStatusService,
    public maritalStatusService: MaritalStatusService,
    public applicantService: ApplicantService,
    public educationLevelsService: EducationLevelsService,
    public qualificationsService: QualificationsService,
    public dialogRef: MatDialogRef<PopupApplicationAddComponent>,
    public selectionResultsService: SelectionResultsService, // บันทึกอาเรย์การคัดเลือก
    public formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    super(formBuilder, activeRoute);

    this.agencys$ = this.agencysService.getAll()
    this.educationLevels$ = this.educationLevelsService.getAll()
    this.qualifications$ = this.qualificationsService.getAll()
    this.position$ = this.positionService.getAll()
    this.maritalStatus$ = this.maritalStatusService.getAll().pipe(
      tap(x => console.log(x)),
      map((x: any) => {
        let data = []
        return data = x.filter(x => x.marital_status_name_th == 'สมรส' || x.marital_status_name_th == 'หย่าร้าง' || x.marital_status_name_th == 'หม้าย' || x.marital_status_name_th == 'โสด')
      }),
      tap(x => console.log(x))
    )
    this.militaryStatus$ = this.militaryStatusService.getAll().pipe(
      tap(x => console.log(x)),
      map((x: any) => {
        let data = []
        return data = x.filter(x => x.military_status_name_th == 'ได้รับการยกเว้น' || x.military_status_name_th == 'เคยเกณฑ์ทหาร' || x.military_status_name_th == 'ยังไม่ได้เกณฑ์ทหาร' || x.military_status_name_th == 'ผ่อนผัน')
      }),
      tap(x => console.log(x))
    )
    this.genders$ = this.gendersService.getAll()

  }

  ngOnInit(): void {
    console.log(this.data);
    this.dataSearch.position_uid = this.data?.requestData?.position_uid
    this.dataSearch.agency_uid = this.data?.requestData?.agency_uid

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.rows?.data?.length;
    return numSelected === numRows;
  }

  masterToggle(checked) {
    console.log(checked);
    if (!checked) {
      this.selection.clear()
    } else {
      this.rows.data.forEach(row => this.selection.select(row));
    }
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  }


  search() {
    console.log(this.dataSearch);
    
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.applicantService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => console.log(x)),
        tap((x: any) => {
          this.rows = new MatTableDataSource(x)
          this.rows.paginator = this.paginator
          this.rows.sort = this.sort
        }),
        // tap(x => this.rows = this.updateMatTable(x)),
        tap(x => this.cdRef.detectChanges())
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      position_uid: null,
      agency_uid: null,
      application_date: null,
      military_status_id: null,
      marital_status_id: null,
      gender_id: null,
    }
    this.masterToggle(false)
    let formArray = this.form.get('data') as FormArray
    while (formArray.length != 0) {
      formArray.removeAt(0)
    }
    this.rows = []
  }





  selectList(check, item) {
    console.log(check);
    console.log(item);
    let formArray = this.form.get('data') as FormArray
    if (check) {
      while (formArray.length != 0) {
        formArray.removeAt(0)
      }
      formArray.push(this.createArray(item))
    } else {
      console.log(this.form.get('data').value);
      let items = this.form.get('data').value
      let index = items.findIndex(x => x.applicant_uid == item.applicant_uid)
      formArray.removeAt(index)
    }
  }

  selectListAll(check, item) {
    console.log(check);
    console.log(item);
    let formArray = this.form.get('data') as FormArray
    if (check) {
      while (formArray.length != 0) {
        formArray.removeAt(0)
      }
      console.log(item.filteredData);
      item.filteredData.forEach(element => {
        formArray.push(this.createArray(element))
      });
    } else {
      while (formArray.length != 0) {
        formArray.removeAt(0)
      }
    }
  }



  save() {
    console.log(this.form.getRawValue());
    let items = this.form.getRawValue()
    this.selectionResultsService.add(items.data).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        let formArray = this.form.get('data') as FormArray
        while (formArray.length != 0) {
          formArray.removeAt(0)
        }
        this.appService.swaltAlert()
      })
  }

  saveClose() {
    console.log(this.form.getRawValue());
    let items = this.form.getRawValue()
    this.selectionResultsService.add(items.data).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.dialogRef.close(x)
      })
  }

  getPreview(id) {
    console.log(this.data.request);
    this.router.navigate(['/app/save_job_application/edit', id])
    this.applicantService.nextState(this.data.request)
    this.dialogRef.close('close')
  }
  close() {
    this.dialogRef.close()
  }


  createArray(el) {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      selection_result_uid: null,
      application_uid: el.applicant_uid,
      selection_request_uid: this.data?.request,
      written_exam_date: null,
      written_exam_status_id: null,
      written_exam_1_score: 0,
      written_exam_2_score: 0,
      written_exam_remark: '',
      interview_date: null,
      interview_result_id: null,
      interview_score: 0,
      interview_remark: ''
    })
  }


  createForm() {
    return this.baseFormBuilder.group({
      data: this.baseFormBuilder.array([])
    })
  }



}
