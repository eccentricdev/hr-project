import { DatePipe } from '@angular/common';
import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { configs } from 'src/app/core/configs/config';
import { ManpowerAssignmentsService } from 'src/app/core/service/allocate-people/manpower-assignments.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { SelectionAssignApplicantsService } from 'src/app/core/service/recruitment/selection-assign-applicants.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-selection-results-list',
  templateUrl: './save-selection-results-list.component.html',
  styleUrls: ['./save-selection-results-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveSelectionResultsListComponent extends BaseList implements OnInit {


  @ViewChild("t1Sort") sort1: MatSort;
  @ViewChild("t2Sort") sort2: MatSort;
  @ViewChild("t1Paginator") pagt1: MatPaginator;
  @ViewChild("t2Paginator") pagt2: MatPaginator

  rows1: any = []
  rows2: any = []
  positionDatas: any = []
  personnelTypes: any = []
  agencyDatas: any = []
  requestDatas: any = []
  requestsDatas$ = new Observable()

  datasDisplay1$ = new Observable()
  datasDisplay2$ = new Observable()

  dataSearch = {
    agency_uid: null,
    position_uid: null,
    request_start_date: null,
    request_end_date: null
  }

  startDate: any = null
  endDate: any = null

  constructor(public router: Router,
    private cdRef: ChangeDetectorRef,
    public datePipe : DatePipe,
    public manpowerRequestsService: ManpowerRequestsService,
    public manpowerAssignmentsService: ManpowerAssignmentsService,
    public selectionAssignApplicantsService: SelectionAssignApplicantsService,
    public selectionResultsService: SelectionResultsService,
    public personnelTypesService: PersonnelTypesService,
    public SelectionRequestsService: SelectionRequestsService,
    public positionService: PositionService,
    public agencysService: AgencysService) {
    super();

    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()
    this.agencysService.getAll().pipe(
      tap(x => this.agencyDatas = x)
    ).subscribe()
    this.personnelTypesService.getAll().pipe(
      tap(x => this.personnelTypes = x)
    ).subscribe()
  }

  ngOnInit(): void {
    this.getService()

  }

  getService() {

    this.SelectionRequestsService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.requestDatas = x),
      tap((x: any) => {
        x = x.filter(x => x.selection_complete_status == false)
        this.rows1 = new MatTableDataSource(x);
        this.rows1.sort = this.sort1;
        this.rows1.paginator = this.pagt1;
      }),
      tap((x: any) => {
        x = x.filter(x => x.selection_complete_status == true && x.selection_result_count != 0)
        this.rows2 = new MatTableDataSource(x);
        this.rows2.sort = this.sort2;
        this.rows2.paginator = this.pagt2;
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

  }

  search() {
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.request_start_date = start
    this.dataSearch.request_end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay1$ = this.SelectionRequestsService.query(`?${queryStr}`)
      // this.datasDisplay2$ = this.selectionResultsService.query(`?${queryStr}`)
      this.datasDisplay1$.pipe(
        tap((x: any) => {
          x = x.filter(x => x.selection_complete_status == false)
          this.rows1 = new MatTableDataSource(x)
          this.rows1.paginator = this.pagt1
          this.rows1.sort = this.sort1
        }),
        tap((x: any) => {
          x = x.filter(x => x.selection_complete_status == true)
          this.rows2 = new MatTableDataSource(x)
          this.rows2.paginator = this.pagt2
          this.rows2.sort = this.sort2
        })
      ).subscribe()

    }
  }

  clear() {
    this.dataSearch = {
      agency_uid: null,
      position_uid: null,
      request_start_date: null,
      request_end_date: null
    }
    this.startDate = null
    this.endDate = null
    this.getService()
  }

  addPage() {
    this.router.navigate(['app/save-selection-results/add'])
  }
  editPage(id) {
    this.router.navigate(['app/save-selection-results/edit', id])
    this.selectionResultsService.nextState('รอสรรหา')
  }

  editPage2(id) {
    this.router.navigate(['app/save-selection-results/edit', id])
    this.selectionResultsService.nextState('สรรหาแล้ว')
  }


}
