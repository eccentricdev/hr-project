import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { concatMap, map, take, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowerAssignmentsService } from 'src/app/core/service/allocate-people/manpower-assignments.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionAssignApplicantsService } from 'src/app/core/service/recruitment/selection-assign-applicants.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { SelectionResultsRequestService } from 'src/app/core/service/recruitment/selection-results-request.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';

@Component({
  selector: 'app-save-selection-results-form',
  templateUrl: './save-selection-results-form.component.html',
  styleUrls: ['./save-selection-results-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveSelectionResultsFormComponent extends BaseForm implements OnInit {

  manpowerAssignments$ = new Observable()
  unSubAll$ = new Subject<void>();
  manpowerRequests$ = new Observable()
  personnelGroups$ = new Observable()
  personnelTypes$ = new Observable()
  usingStatuss$ = new Observable()
  applicant$ = new Observable()
  manpower$ = new Observable()
  taps = ['ข้อมูลในการสรรหา','คัดเลือกบุคลากร','สรุปผลคัดเลือกบุคลากร']
  dataEdit: any
  
  qualificationsDats: any[]
  educationLevelsDatas: any = []
  positionDatas: any = []
  manpower: any = []
  agencysDatas: any = []
  typeValue: any
  dataEditRequest: any
  requestId:any
  itemsResult:any = []
  
  constructor(
    public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    public educationLevelsService: EducationLevelsService,
    public qualificationsService: QualificationsService,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public manpowersService: ManpowersService,
    public manpowerRequestsService: ManpowerRequestsService,
    public manpowerAssignmentsService: ManpowerAssignmentsService,
    public selectionResultsService: SelectionResultsService,
    public selectionRequestsService: SelectionRequestsService,
    public personnelTypesService : PersonnelTypesService,
    public personnelGroupsService : PersonnelGroupsService,
    public manpowerUsingStatussService : ManpowerUsingStatussService,
    public selectionResultsRequestService : SelectionResultsRequestService,
    public applicantService : ApplicantService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.manpowerRequests$ = this.manpowerRequestsService.getAll()
    this.personnelGroups$ = this.personnelGroupsService.getAll()
    this.personnelTypes$ = this.personnelTypesService.getAll()
    this.usingStatuss$ = this.manpowerUsingStatussService.getAll()
    this.applicant$ = this.applicantService.getAll().pipe(
      tap(x => console.log(x))
    )

    this.manpower$ = this.manpowersService.getAll().pipe(
      takeUntil(this.unSubAll$),
      tap(x => this.manpower = x)
    )

    this.qualificationsService.getAll().pipe(
      takeUntil(this.unSubAll$),
      tap(x => this.qualificationsDats = x)
    ).subscribe()

    this.educationLevelsService.getAll().pipe(
      takeUntil(this.unSubAll$),
      tap(x => this.educationLevelsDatas = x)
    ).subscribe()

    this.positionService.getAll().pipe(
      takeUntil(this.unSubAll$),
      tap(x => this.positionDatas = x)
    ).subscribe()

    this.agencysService.getAll().pipe(
      takeUntil(this.unSubAll$),
      tap(x => this.agencysDatas = x)
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id);
    switch (this.state) {
      case 'edit':
        this.selectionResultsService.getResultType().pipe(
          takeUntil(this.unSubAll$),
          tap(x => console.log(x)),
          tap(x => this.typeValue = x),
          tap(() => {
            if (this.typeValue == 'รอสรรหา' || this.typeValue == 'สัญญาจ้าง' || this.typeValue == 'ใบสมัคร') {
              this.selectionRequestsService.get(this.id).pipe(
                tap(x => console.log(x)),
                tap(x => this.dataEditRequest = x),
                concatMap(() => {
                  return this.selectionResultsRequestService.get(this.dataEditRequest.selection_request_uid).pipe(
                    tap(x => console.log(x)),
                    map((x: any) => x.filter(x => x.is_pass == true)),
                    tap(x => this.itemsResult = x)
                  )
                }),
              ).subscribe()
            }else if(this.typeValue == 'สรรหาแล้ว'){
              this.selectionRequestsService.get(this.id).pipe(
                tap(x => console.log(x)),
                tap(x => this.dataEditRequest = x),
                concatMap(() => {
                  return this.selectionResultsRequestService.get(this.dataEditRequest.selection_request_uid).pipe(
                    tap(x => console.log(x)),
                    map((x: any) => x.filter(x => x.is_pass == true)),
                    tap(x => this.itemsResult = x)
                  )
                }),
              ).subscribe()
            }
          }),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  save() {

  }

  

  getRequestId(value){
    console.log(value);
    this.requestId = value
    this.selectionResultsRequestService.get(this.requestId).pipe(
      tap(x => console.log(x)),
      map((x: any) => x.filter(x => x.is_pass == true)),
      tap(x => this.itemsResult = x)
    ).subscribe()
  }


  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }

}
