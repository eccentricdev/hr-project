import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError, concatMap, filter, map, take, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { EmploymentContractsService } from 'src/app/core/service/recruitment/employment-contracts.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { SelectionResultsRequestService } from 'src/app/core/service/recruitment/selection-results-request.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';

@Component({
  selector: 'app-tap3',
  templateUrl: './tap3.component.html',
  styleUrls: ['./tap3.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap3Component extends BaseForm implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  rows: any = []
  @Input() dataEditRequest: any
  @Input() applicant: any
  @Input() manpower: any
  @Input() RequestId: any
  @Input() resultData: any = []
  display: any
  displayedColumns: string[] = ['2', '3', '4', '5', '6'];
  dataSource: any = new MatTableDataSource();
  requestId: any
  manpowerDatas: any = []
  constructor(public router: Router,
    public activeRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    public employmentContractsService : EmploymentContractsService,
    public selectionRequestsService: SelectionRequestsService,
    public selectionResultsService: SelectionResultsService,
    public appService: AppService,
    public selectionResultsRequestService: SelectionResultsRequestService,
    public formBuilder: FormBuilder,) {
    super(formBuilder, activeRoute);

  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    if (this.manpower || this.resultData) {
      
      this.manpower = this.manpower
      console.log(this.manpower);
      // this.requestId = this.RequestId
      // console.log(this.requestId);
      console.log(this.resultData);
      let formArray = this.form.get('datas') as FormArray
      while (formArray.length != 0) {
        formArray.removeAt(0)
      }
      this.resultData.forEach(element => {
        this.datasFormArray.push(this.createArray(element))
        this.dataSource.data = this.datasFormArray.controls;
        console.log(this.dataSource.data);

      });
      this.cdRef.detectChanges()


      // this.selectionResultsRequestService.get(this.requestId).pipe(
      //   tap(x => console.log(x)),
      //   map((x: any) => x.filter(x => x.is_pass == true)),
      //   tap((x: any) => {
      //     console.log(x);
      //     let formArray = this.form.get('datas') as FormArray
      //     while (formArray.length != 0) {
      //       formArray.removeAt(0)
      //     }
      //     x.forEach(element => {
      //       console.log(x);
      //       this.datasFormArray.push(this.createArray(element))
      //       this.dataSource.data =  this.datasFormArray.controls;
      //       console.log(this.dataSource.data);

      //     });
      //   }),
      //   tap(() => this.cdRef.detectChanges())
      // ).subscribe()

    }

  }



  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    // this.selectionRequestsService.getResultId().pipe(
    //   tap(x => console.log(x)),
    //   tap(x => {
    //     if(x){
    //       this.selectionResultsRequestService.get(x).pipe(
    //         tap(x => console.log(x)),
    //         map((x: any) => x.filter(x => x.is_pass == true)),
    //         tap((x: any) => {
    //           console.log(x);
    //           let formArray = this.form.get('datas') as FormArray
    //           while (formArray.length != 0) {
    //             formArray.removeAt(0)
    //           }
    //           x.forEach(element => {
    //             console.log(x);
    //             this.datasFormArray.push(this.createArray(element))
    //             this.dataSource.data =  this.datasFormArray.controls;
    //             console.log(this.dataSource.data);

    //           });
    //         }),
    //         tap(() => this.cdRef.detectChanges())
    //       ).subscribe()
    //     }
    //   })
    // )
  }

  get datasFormArray(): FormArray {
    return this.form.get('datas') as FormArray;
  }

  save() {
    let items = this.form.get('datas').value
    console.log(items);
    this.selectionResultsService.update(items).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.router.navigate(['/app/save-selection-results'])
      }) 

  }

  addEm(item) {
    console.log(item);
    this.employmentContractsService.nextState(item)
    this.router.navigate(['app/save_employment_contract/add'])
  }

  close() {
    this.router.navigate(['/app/save-selection-results'])
  }


  createArray(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      selection_result_uid: el.selection_result_uid,
      application_uid: el.application_uid,
      selection_request_uid: el.selection_request_uid,
      written_exam_date: el.written_exam_date,
      written_exam_status_id: el.written_exam_status_id,
      written_exam_1_score: el.written_exam_1_score,
      written_exam_2_score: el.written_exam_2_score,
      written_exam_remark: el.written_exam_remark,
      interview_date: el.interview_date,
      interview_result_id: el.interview_result_id,
      interview_score: el.interview_score,
      interview_remark: el.interview_remark,
      is_pass: el.is_pass,
      manpower_uid: el.manpower_uid,
      assign_date: new Date(),
      assign_order: el.assign_order,
      sum_score: el.written_exam_1_score + el.written_exam_2_score + el.interview_score
    })
  }


  createForm() {
    return this.baseFormBuilder.group({
      datas: this.baseFormBuilder.array([])
    })
  }

}
