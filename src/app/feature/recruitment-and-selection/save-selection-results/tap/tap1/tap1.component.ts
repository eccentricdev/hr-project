import { Input, SimpleChanges } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ManpowerAssignmentsService } from 'src/app/core/service/allocate-people/manpower-assignments.service';
import { ManpowerRequestsService } from 'src/app/core/service/allocate-people/manpower-requests.service';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';

@Component({
  selector: 'app-tap1',
  templateUrl: './tap1.component.html',
  styleUrls: ['./tap1.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap1Component implements OnInit {
  @Input() dataEditRequest: any
  @Input() manpowerRequestDatas: any
  @Input() qualificationsDats: any
  @Input() educationLevelsDatas: any
  @Input() positionDatas: any
  @Input() usingStatuss: any
  @Input() manpower: any
  @Input() agencysDatas: any
  display: any
  constructor(
    public router: Router,
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    this.display = this.dataEditRequest
  }

  ngOnInit(): void {

  }

  close() {
    this.router.navigate(['/app/save-selection-results'])

  }

}
