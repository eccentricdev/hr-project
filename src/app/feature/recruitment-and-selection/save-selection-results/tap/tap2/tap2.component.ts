import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, ChangeDetectorRef, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EventEmitter } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { SelectionResultsRequestService } from 'src/app/core/service/recruitment/selection-results-request.service';
import { PopupApplicationAddComponent } from '../../popup-application-add/popup-application-add.component';
import { PopupApplicationEditComponent } from '../../popup-application-edit/popup-application-edit.component';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';
import swal from 'sweetalert2'
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-tap2',
  templateUrl: './tap2.component.html',
  styleUrls: ['./tap2.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap2Component extends BaseList implements OnInit {

  @Input() form: FormGroup
  @Input() dataEditRequest: any
  @Input() applicant: any
  @Input() typePage: any
  @Output() onRequestId = new EventEmitter()
  
  rows: any = []
  select = []
  applicantData: any = []

  display: any
  requestId: any
  requestData:any
  page:any
  checkPass: boolean = false

  constructor(public dialog: MatDialog,
    public applicantService: ApplicantService,
    private cdRef: ChangeDetectorRef,
    public appService : AppService,
    public router : Router,
    public selectionResultsService : SelectionResultsService,
    public selectionRequestsService: SelectionRequestsService,
    public SelectionResultsRequestService: SelectionResultsRequestService) {
    super();
    this.applicantService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.applicantData = x)
    ).subscribe()
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    console.log(this.applicant);
    console.log(this.dataEditRequest);
    if (this.dataEditRequest ) {
      // this.page = this.typePage
      this.requestData = this.dataEditRequest
      this.requestId = this.dataEditRequest.selection_request_uid
      console.log(this.requestId);
      // this.cdRef.detectChanges()
      this.SelectionResultsRequestService.get(this.requestId).pipe(
        tap(x => console.log(x)),
        tap((x: any) => this.rows = this.updateMatTable(x)),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
    }
    
  }

  ngOnInit(): void {
    // if(this.page == 'ใบสมัคร'){
    //   this.popApplicationAdd()
    //   // setTimeout(() => {
        
    //   // }, 500);
    // }
  }

  // selectList(check,item){
  //   console.log(check);
  //   console.log(item);
  //   console.log(this.dataEditRequest.selection_request_manpower.length);
  //   console.log(this.select.length + 1);
  //   if(this.select.length + 1 <= this.dataEditRequest.selection_request_manpower.length  ){
  //     if(check){
  //       let setValue = item.is_pass = true
  //       console.log(setValue);
  //       this.select.push(item)
  //     }else{
  //       let setValue = item.is_pass = false
  //       console.log(setValue);
  //       let index = this.select.findIndex(x => x.selection_result_uid == item.selection_result_uid)
  //       this.select.splice(index,1)
  //     }
  //   }else{
  //     this.appService.swaltAlertError('รายชื่อผู้สมัครเกินเลขที่อัตราจ้าง ไม่สามารถเพิ่มข้อมูลได้')
  //   }
  //   this.cdRef.detectChanges()
   
  //   console.log(this.select);
    
    
  // }

  

  popApplicationAdd() {
    const dialogRef = this.dialog.open(
      PopupApplicationAddComponent, {
      width: '80%',
      // disableClose: true,
      data: { request: this.requestId ,requestData: this.requestData }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      this.selectionRequestsService.nextState({tap:2,id:this.requestId})
      this.SelectionResultsRequestService.get(this.requestId).pipe(
        tap(x => console.log(x)),
        tap((x: any) => this.rows = this.updateMatTable(x)),
        tap(() => this.onRequestId.emit(this.requestId)),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
    })
  }

  popApplicationEdit(array, index) {
    console.log(array);

    const dialogRef = this.dialog.open(
      PopupApplicationEditComponent, {
      width: '80%',
      // disableClose: true,
      data: { editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      this.selectionRequestsService.nextState({tap:2,id:callback})
      this.onRequestId.emit(callback)
      this.SelectionResultsRequestService.get(callback).pipe(
        tap((x: any) => this.rows = this.updateMatTable(x)),
        tap(() => this.cdRef.detectChanges())
      ).subscribe()
    })
  }

  deleteItems(item,i){
    console.log();
    if (item.selection_result_uid) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.selectionResultsService.deleteData(item.selection_result_uid).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appService.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                this.SelectionResultsRequestService.get(item.selection_request_uid).pipe(
                  tap(x => console.log(x)),
                  tap((x: any) => this.rows = this.updateMatTable(x)),
                  tap(() => this.cdRef.detectChanges())
                ).subscribe()
                this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    }
  }

  close() {
    this.router.navigate(['/app/save-selection-results'])

  }






}
