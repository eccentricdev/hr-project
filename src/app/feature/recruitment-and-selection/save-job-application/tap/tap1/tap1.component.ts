import { ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BloodGroupService } from 'src/app/core/service/common/blood-group.service';
import { Country, CountryService } from 'src/app/core/service/common/country.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { MaritalStatusService } from 'src/app/core/service/common/marital-status.service';
import { MilitaryStatusService } from 'src/app/core/service/common/military-status.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { ProvinceService } from 'src/app/core/service/common/province.service';
import { ReligionService } from 'src/app/core/service/common/religion.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { ViewUploadDialogComponent } from 'src/app/shared/components/view-upload-dialog/view-upload-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tap1',
  templateUrl: './tap1.component.html',
  styleUrls: ['./tap1.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap1Component implements OnInit {

  countryData = []
  Province = []
  host:string = `${environment.rsuArApiName}`

  agencys$ = new Observable()
  position$ = new Observable()
  religion$ = new Observable()
  bloodGroup$ = new Observable()
  deforms$ = new Observable()
  subDistrict$ = new Observable()
  prefix$ = new Observable()
  unSubAll$ = new Subject<void>();
  genders$ = new Observable();
  militaryStatus$ = new Observable()
  maritalStatus$ = new Observable()
  province$ = new Observable()
  country$ = new Observable()
  district: any = []
  districts: any = []


  countryName: any
  nationality_name: any
  ethnicity_name: any

  thai: boolean = true
  eng: boolean = false
  @Input() form: FormGroup
  @Input() nationality: any
  @Input() ethnicity: any

  ngOnChanges(changes: SimpleChanges): void {
    if (this.nationality || this.ethnicity) {
      // console.log(this.nationality);
      // console.log(this.ethnicity);
      this.nationality_name = this.nationality
      this.ethnicity_name = this.ethnicity
    }
  }

  constructor(
    public dialog: MatDialog,
    public agencysService: AgencysService,
    public positionService: PositionService,
    public prefixService: PrefixService,
    public countryService: CountryService,
    public provinceService: ProvinceService,
    public religionService: ReligionService,
    public bloodGroupService: BloodGroupService,
    public subDistrictService: SubDistrictService,
    public gendersService: GendersService,
    public militaryStatusSV: MilitaryStatusService,
    public maritalStatusSV: MaritalStatusService,
    public applicantService: ApplicantService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.agencys$ = this.agencysService.getAll()
    this.position$ = this.positionService.getAll()
    this.prefix$ = this.prefixService.getAll()
    this.religion$ = this.religionService.getAll()
    this.bloodGroup$ = this.bloodGroupService.getAll()
    this.genders$ = this.gendersService.getAll()
    this.militaryStatus$ = this.militaryStatusSV.getAll()
    this.maritalStatus$ = this.maritalStatusSV.getAll()
  }

  ngOnInit(): void {

    // this.form.get('nationality_name').valueChanges.pipe(
    //   debounceTime(500),
    //   switchMap(text => this.country$ = this.countryService.query(`?search=${text}`)),
    //   tap(() => this.cdRef.detectChanges()),
    //   takeUntil(this.unSubAll$)
    // ).subscribe()

    // this.form.get('ethnicity_name').valueChanges.pipe(
    //   debounceTime(500),
    //   switchMap(text => this.country$ = this.countryService.query(`?search=${text}`)),
    //   tap(() => this.cdRef.detectChanges()),
    //   takeUntil(this.unSubAll$)
    // ).subscribe()


  }

  viewfile(url) {
    const dialogRef = this.dialog.open(
      ViewUploadDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: url  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
      }
    })
  }


  select_ethnicity(coutnry: Country) {
    console.log(coutnry);
    this.form.get('ethnicity_id').setValue(coutnry.country_id)
  }
  select_nationality(coutnry: Country) {
    console.log(coutnry);
    this.form.get('nationality_id').setValue(coutnry.country_id)
  }



  getNationality(text) {
    if(text){
      setTimeout(() => {
        this.country$ = this.countryService.query(`?search=${text}`)
        this.cdRef.detectChanges()
      }, 500);
    }
  }

  getEthnicity(text) {
    if(text){
      setTimeout(() => {
        this.country$ = this.countryService.query(`?search=${text}`)
        this.cdRef.detectChanges()
      }, 500);
    }
  
  }

  getFile(file) {
    console.log(file);
    this.form.get('photo_url').setValue(file.file_url)
  }

  setPrefix(id) {
    this.form.get('prefix_idEng').setValue(id)
  }

}
