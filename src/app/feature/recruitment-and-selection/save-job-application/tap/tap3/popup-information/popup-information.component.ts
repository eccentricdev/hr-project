import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { element } from 'protractor';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { CountryService } from 'src/app/core/service/common/country.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';


@Component({
  selector: 'app-popup-information',
  templateUrl: './popup-information.component.html',
  styleUrls: ['./popup-information.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupInformationComponent implements OnInit {

  educations = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by:null,
    updated_datetime: null,
    applicant_education_uid: null,
    applicant_uid: null,
    education_level_id: null,
    quali_id: null,
    field_name: '',
    other_institute_name: '',
    country_id: null,
    start_year: null,
    finish_year: null,
    grade: '',
    edu_highest: true,
    edu_employ: true,
    start_date: null,
    finish_date: null,
    row_order: 0
  }
  
  // country = []
  // unSubAll$ = new Subject<void>();
  educationLevels$ = new Observable();
  qualifications$ = new Observable();
  name:any = 0
  constructor(public dialogRef: MatDialogRef<PopupInformationComponent>,
    // public countrySV:CountryService,
    public educationLevelsService:EducationLevelsService,
    public qualificationsService:QualificationsService,
    public formBuilder: FormBuilder,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.educationLevels$ = this.educationLevelsService.getAll().pipe(
        tap(x => console.log(x))
      )
      this.qualifications$ = this.qualificationsService.getAll().pipe(
        tap(x => console.log(x))
      )
    }

  ngOnInit(): void {
    console.log(this.data);
    if(this.data.editForm != null){
      this.getArray(this.data.editForm)
    }else{
      this.educations.applicant_uid = this.data.id
    }
  }

  getArray(el) {
    console.log(el);
    this.educations = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      applicant_education_uid: el.applicant_education_uid,
      applicant_uid: el.applicant_uid,
      education_level_id: el.education_level_id,
      quali_id: el.quali_id,
      field_name: el.field_name,
      other_institute_name: el.other_institute_name,
      country_id: el.country_id,
      start_year: el.start_year,
      finish_year: el.finish_year,
      grade: el.grade,
      edu_highest: el.edu_highest,
      edu_employ: el.edu_employ,
      start_date: el.start_date,
      finish_date: el.finish_date,
      row_order: el.row_order
    }
  }

  createArrayDetails(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        applicant_education_uid: null,
        applicant_uid: null,
        education_level_id: null,
        quali_id: null,
        field_name: '',
        other_institute_name: '',
        country_id: null,
        start_year: null,
        finish_year: null,
        grade: '',
        edu_highest: true,
        edu_employ: true,
        start_date: null,
        finish_date: null,
        row_order: 0
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        applicant_education_uid: el.applicant_education_uid,
        applicant_uid: el.applicant_uid,
        education_level_id: el.education_level_id,
        quali_id: el.quali_id,
        field_name: el.field_name,
        other_institute_name: el.other_institute_name,
        country_id: el.country_id,
        start_year: el.start_year,
        finish_year: el.finish_year,
        grade: el.grade,
        edu_highest: el.edu_highest,
        edu_employ: el.edu_employ,
        start_date: el.start_date,
        finish_date: el.finish_date,
        row_order: el.row_order
      })
    }
  }

  

  save(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('applicant_educations')).push(this.createArrayDetails(this.educations))  
    }else{
      (<FormArray>this.data.form.get('applicant_educations')).controls[this.data.ind].patchValue({
        status_id: this.educations.status_id,
        created_by: this.educations.created_by,
        created_datetime: this.educations.created_datetime,
        updated_by: this.educations.updated_by,
        updated_datetime: this.educations.updated_datetime,
        applicant_education_uid: this.educations.applicant_education_uid,
        applicant_uid: this.educations.applicant_uid,
        education_level_id: this.educations.education_level_id,
        quali_id: this.educations.quali_id,
        field_name: this.educations.field_name,
        other_institute_name: this.educations.other_institute_name,
        country_id: this.educations.country_id,
        start_year: this.educations.start_year,
        finish_year: this.educations.finish_year,
        grade: this.educations.grade,
        edu_highest: this.educations.edu_highest,
        edu_employ: this.educations.edu_employ,
        start_date: this.educations.start_date,
        finish_date: this.educations.finish_date,
        row_order: this.educations.row_order
      })
    }
}

  saveClose(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('applicant_educations')).push(this.createArrayDetails(this.educations))  
      this.close()
    }else{
      (<FormArray>this.data.form.get('applicant_educations')).controls[this.data.ind].patchValue({
        status_id: this.educations.status_id,
        created_by: this.educations.created_by,
        created_datetime: this.educations.created_datetime,
        updated_by: this.educations.updated_by,
        updated_datetime: this.educations.updated_datetime,
        applicant_education_uid: this.educations.applicant_education_uid,
        applicant_uid: this.educations.applicant_uid,
        education_level_id: this.educations.education_level_id,
        quali_id: this.educations.quali_id,
        field_name: this.educations.field_name,
        other_institute_name: this.educations.other_institute_name,
        country_id: this.educations.country_id,
        start_year: this.educations.start_year,
        finish_year: this.educations.finish_year,
        grade: this.educations.grade,
        edu_highest: this.educations.edu_highest,
        edu_employ: this.educations.edu_employ,
        start_date: this.educations.start_date,
        finish_date: this.educations.finish_date,
        row_order: this.educations.row_order
      })
      this.close()
    }
   
  }
  close(){
  this.dialogRef.close('close')
  }

}
