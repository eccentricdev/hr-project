import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { ApplicantEducationsService } from 'src/app/core/service/recruitment/applicant-educations.service';
import { PopupHistoryComponent } from './popup-history/popup-history.component';
import { PopupInformationComponent } from './popup-information/popup-information.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-tap3',
  templateUrl: './tap3.component.html',
  styleUrls: ['./tap3.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap3Component implements OnInit {

  @Input() form : FormGroup
  dataEditInfor: any
  @Input() dataEdit: any
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  educationLevelsData:any = []
  qualificationsData:any = []

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dataEdit) {
      // console.log(this.dataEdit);
       this.dataEditInfor = this.dataEdit.applicant_uid
       console.log(this.dataEditInfor);
    }
  }


  constructor(public dialog: MatDialog,
    public appService : AppService,
    public educationLevelsService:EducationLevelsService,
    public qualificationsService:QualificationsService,
    public applicantEducationsService :ApplicantEducationsService,
    ) {
      this.educationLevels$ = this.educationLevelsService.getAll()
      this.qualifications$ = this.qualificationsService.getAll()
      //  this.educationLevelsService.getAll().pipe(
      //   tap(x => this.educationLevelsData = x)
      //  ).subscribe()
      //  this.qualificationsService.getAll().pipe(
      //   tap(x => this.qualificationsData = x)
      //  ).subscribe()
     }

  ngOnInit(): void {
  }

  popUpInformation() {
      const dialogRef = this.dialog.open(
        PopupInformationComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: null , id: this.dataEditInfor? this.dataEditInfor : null  }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ        
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
      })
   
  }
  popUpInformationEdit(array,index) {
      const dialogRef = this.dialog.open(
        PopupInformationComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: array , ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ        
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
      })
   
  }

  deleteEducation(id,i){
    // console.log(data);
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.applicantEducationsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appService.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let form = this.form.get('applicant_educations') as FormArray
                form.removeAt(i)
                this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
    let form = this.form.get('applicant_educations') as FormArray
    form.removeAt(i)
    }
   
  }

  popUpHistory() {
      const dialogRef = this.dialog.open(
        PopupHistoryComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: null ,id: this.dataEditInfor? this.dataEditInfor : null  }   // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
      })
   
  }

  popUpHistoryEdit(array,index) {
      const dialogRef = this.dialog.open(
        PopupHistoryComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: array , ind: index }    // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
      })
   
  }

  deleteHistory(data,i){
    console.log(data);
    let form = this.form.get('applicant_working_histories') as FormArray
    form.removeAt(i)
  }
}


