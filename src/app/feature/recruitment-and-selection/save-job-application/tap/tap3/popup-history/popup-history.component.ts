import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';


@Component({
  selector: 'app-popup-history',
  templateUrl: './popup-history.component.html',
  styleUrls: ['./popup-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupHistoryComponent implements OnInit {
  

  working = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    applicant_working_history_uid: null,
    applicant_uid: null,
    work_place_name: '',
    job_position_name: '',
    job_description: '',
    final_salary_amount: 0,
    start_date: null,
    end_date: null,
    experience_income_amount: 0,
    profession_income_amount: 0,
    other_income_amount: 0,
    remark: ''
  }

  constructor(public dialogRef: MatDialogRef<PopupHistoryComponent>,
    public formBuilder: FormBuilder,
    // public activeRoute: ActivatedRoute,
    // public appService: AppService,
    private cdRef: ChangeDetectorRef,
    public manpowersService : ManpowersService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);

    if(this.data.editForm != null){
      this.getArray(this.data.editForm)
    }
    

  }

  getArray(el) {
    console.log(el);
    this.working = {
      status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        applicant_working_history_uid: el.applicant_working_history_uid,
        applicant_uid: el.applicant_uid,
        work_place_name: el.work_place_name,
        job_position_name: el.job_position_name,
        job_description: el.job_description,
        final_salary_amount: el.final_salary_amount,
        start_date: el.start_date,
        end_date: el.end_date,
        experience_income_amount: el.experience_income_amount,
        profession_income_amount: el.profession_income_amount,
        other_income_amount: el.other_income_amount,
        remark: el.remark
    }
  }

  createArrayDetails(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        applicant_working_history_uid: null,
        applicant_uid: null,
        work_place_name: '',
        job_position_name: '',
        job_description: '',
        final_salary_amount: 0,
        start_date: null,
        end_date: null,
        experience_income_amount: 0,
        profession_income_amount: 0,
        other_income_amount: 0,
        remark: ''
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        applicant_working_history_uid: el.applicant_working_history_uid,
        applicant_uid: el.applicant_uid,
        work_place_name: el.work_place_name,
        job_position_name: el.job_position_name,
        job_description: el.job_description,
        final_salary_amount: el.final_salary_amount,
        start_date: el.start_date,
        end_date: el.end_date,
        experience_income_amount: el.experience_income_amount,
        profession_income_amount: el.profession_income_amount,
        other_income_amount: el.other_income_amount,
        remark: el.remark
      })
    }
  }


  save(){
      if(this.data.editForm == null){
        (<FormArray>this.data.form.get('applicant_working_histories')).push(this.createArrayDetails(this.working))  
      }else{
        (<FormArray>this.data.form.get('applicant_working_histories')).controls[this.data.ind].patchValue({
        status_id: this.working.status_id,
        created_by: this.working.created_by,
        created_datetime: this.working.created_datetime,
        updated_by: this.working.updated_by,
        updated_datetime: this.working.updated_datetime,
        applicant_working_history_uid: this.working.applicant_working_history_uid,
        applicant_uid: this.working.applicant_uid,
        work_place_name: this.working.work_place_name,
        job_position_name: this.working.job_position_name,
        job_description: this.working.job_description,
        final_salary_amount: this.working.final_salary_amount,
        start_date: this.working.start_date,
        end_date: this.working.end_date,
        experience_income_amount: this.working.experience_income_amount,
        profession_income_amount: this.working.profession_income_amount,
        other_income_amount: this.working.other_income_amount,
        remark: this.working.remark
        })
      }
  }

  saveClose(){

    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('applicant_working_histories')).push(this.createArrayDetails(this.working))  
      this.close()
    }else{
      (<FormArray>this.data.form.get('applicant_working_histories')).controls[this.data.ind].patchValue({
        status_id: this.working.status_id,
        created_by: this.working.created_by,
        created_datetime: this.working.created_datetime,
        updated_by: this.working.updated_by,
        updated_datetime: this.working.updated_datetime,
        applicant_working_history_uid: this.working.applicant_working_history_uid,
        applicant_uid: this.working.applicant_uid,
        work_place_name: this.working.work_place_name,
        job_position_name: this.working.job_position_name,
        job_description: this.working.job_description,
        final_salary_amount: this.working.final_salary_amount,
        start_date: this.working.start_date,
        end_date: this.working.end_date,
        experience_income_amount: this.working.experience_income_amount,
        profession_income_amount: this.working.profession_income_amount,
        other_income_amount: this.working.other_income_amount,
        remark: this.working.remark
        })
      this.close()
    }
   
  }
  close(){
  this.dialogRef.close('close')
  }

}
