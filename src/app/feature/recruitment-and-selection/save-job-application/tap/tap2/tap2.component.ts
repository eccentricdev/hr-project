import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef, OnDestroy, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { concatMap, debounceTime, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { CountryService } from 'src/app/core/service/common/country.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { EmployeeAddressTypeService } from 'src/app/core/service/register/employee-address-type.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-tap2',
  templateUrl: './tap2.component.html',
  styleUrls: ['./tap2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Tap2Component implements OnInit, OnDestroy {

  @Input() form: FormGroup
  @Input() formAddress: FormGroup
  @Input() dataEdit: any
  subDistrict$ = new Observable()
  countryName: any = ''
  country$ = new Observable()
  subDistricts = []
  unSubAll$ = new Subject<void>();
  employeeAddressTypeData$ = new Observable()
  dataSub: any
  dataCountry: any

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dataEdit) {
      // console.log(this.dataEdit);
      this.dataEdit.forEach(element => {
        if(element.address.country_id){
          this.country$ = this.countryService.get(element.address.country_id).pipe(
            // tap(x => console.log(x)),
            map(x => {
              let data:any = []
              data.push(x)
              // console.log(data);
              return data
            }),
            // tap(x => console.log(x)),
            tap(x => this.dataCountry = x[0]),
            tap(x => this.countryName = this.dataCountry.country_name_en),
          )
        }
      });  
    }
  }


  constructor(public EmployeeAddressTypeSV: EmployeeAddressTypeService,
    public subDistrictSV: SubDistrictService,
    public countryService: CountryService,
    private cdRef: ChangeDetectorRef,) {
    this.employeeAddressTypeData$ = this.EmployeeAddressTypeSV.getAll()
  }

  ngOnInit(): void {

    this.formAddress.get('address.other_sub_district_name_th').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.subDistrictSV.query(`?search=${text}`)),
      tap((subDistricts: any) => this.subDistricts = [...subDistricts]),
      tap((subDistricts: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.formAddress.get('address.other_district_name_th').disable()
    this.formAddress.get('address.other_province_name_th').disable()
  }

  selectSubDistrict(data) {
    console.log(data)
    this.formAddress.get('address.sub_district_id').setValue(data.sub_district_id)
    this.formAddress.get('address.other_district_name_th').setValue(data.district_name_th)
    this.formAddress.get('address.other_province_name_th').setValue(data.province_name_th)
  }

  chengeAddressRegistered(value) {
    // console.log(value)
    if (value.checked) {
      let formAddresses = this.form.get('applicant_addresses').value[0]
      // console.log(formAddresses);
      this.subDistrictSV.query(`?search=${formAddresses.address.other_sub_district_name_th}`).pipe(
        // tap(x => console.log(x)),
        tap(x => this.dataSub = x[0]),
        // tap(x => console.log(this.dataSub)),
        tap(() => {
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address_type_id').setValue(22);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.address_name').setValue(formAddresses.address.address_name);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.house_no').setValue(formAddresses.address.house_no);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_no').setValue(formAddresses.address.village_no);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.building_name').setValue(formAddresses.address.building_name);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.room_no').setValue(formAddresses.address.room_no);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.floor_no').setValue(formAddresses.address.floor_no);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_name').setValue(formAddresses.address.village_name);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.alley_name').setValue(formAddresses.address.alley_name);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.street_name').setValue(formAddresses.address.street_name);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.sub_district_id').setValue(formAddresses.address.sub_district_id);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_th').setValue(formAddresses.address.other_sub_district_name_th);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_en').setValue(formAddresses.address.other_sub_district_name_en);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_th').setValue(this.dataSub.district_name_th);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_en').setValue(formAddresses.address.other_district_name_en);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_th').setValue(this.dataSub.province_name_th);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_en').setValue(formAddresses.address.other_province_name_en);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.postal_code').setValue(formAddresses.address.postal_code);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.country_id').setValue(formAddresses.address.country_id);
          (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.telephone_no').setValue(formAddresses.address.telephone_no);
        }),
        concatMap(() => {
          return this.countryService.get(formAddresses.address.country_id).pipe(
            tap(x => this.dataCountry = x),
            tap(x => this.countryName = this.dataCountry.country_name_en),
            // tap(x => console.log(this.dataCountry))
          )
        }),
      ).subscribe()
      // this.form.get('applicant_addresses').value.forEach((element, i) => {
      //   console.log(element);
      //   this.subDistrictSV.query(`?search=${element.address.other_sub_district_name_th}`).pipe(
      //     // tap(x => console.log(x)),
      //     tap(x => this.dataSub = x[0]),
      //     tap(x => console.log(this.dataSub)),
      //     tap(() => {
      //       if (i == 0) {
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address_type_id').setValue(22);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.address_name').setValue(element.address.address_name);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.house_no').setValue(element.address.house_no);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_no').setValue(element.address.village_no);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.building_name').setValue(element.address.building_name);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.room_no').setValue(element.address.room_no);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.floor_no').setValue(element.address.floor_no);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_name').setValue(element.address.village_name);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.alley_name').setValue(element.address.alley_name);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.street_name').setValue(element.address.street_name);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.sub_district_id').setValue(element.address.sub_district_id);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_th').setValue(element.address.other_sub_district_name_th);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_en').setValue(element.address.other_sub_district_name_en);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_th').setValue(this.dataSub.district_name_th);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_en').setValue(element.address.other_district_name_en);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_th').setValue(this.dataSub.province_name_th);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_en').setValue(element.address.other_province_name_en);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.postal_code').setValue(element.address.postal_code);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.country_id').setValue(element.address.country_id);
      //         (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.telephone_no').setValue(element.address.telephone_no);
      //       }
      //     }),
      //     tap(() => this.cdRef.detectChanges())
      //   ).subscribe()
      // });
    } else {
      if(this.countryName){
        this.countryName = ''
      }
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.address_name').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.house_no').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_no').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.building_name').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.room_no').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.floor_no').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.village_name').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.alley_name').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.street_name').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.sub_district_id').setValue(null);
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_th').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_sub_district_name_en').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_th').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_district_name_en').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_th').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.other_province_name_en').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.postal_code').setValue('');
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.country_id').setValue(null);
      (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address.telephone_no').setValue('');
    }
  }

  


  select_coutry(value) {
    // console.log(value);
    this.formAddress.get('address.country_id').setValue(value.country_id)
  }

  getCountry(text) {
    if (text) {
      setTimeout(() => {
        this.country$ = this.countryService.query(`?search=${text}`)
        this.cdRef.detectChanges()
      }, 300);
    }
  }

  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }

}
