import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { CountryService } from 'src/app/core/service/common/country.service';
import { DocumentTypesService } from 'src/app/core/service/common/document-types.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { ViewUploadDialogComponent } from 'src/app/shared/components/view-upload-dialog/view-upload-dialog.component';


@Component({
  selector: 'app-save-job-application-form',
  templateUrl: './save-job-application-form.component.html',
  styleUrls: ['./save-job-application-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveJobApplicationFormComponent extends BaseForm implements OnInit {


  dataEdit: any
  prefixEng: any
  dateToday: any = new Date()
  no: any
  dataResult: any
  ethnicity_name:any
  nationality_name:any

  documentTypes:any = []

  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    private cdRef: ChangeDetectorRef,
    public documentTypesService : DocumentTypesService,
    public selectionResultsService: SelectionResultsService,
    public countryService: CountryService,
    public applicantService: ApplicantService,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.documentTypesService.getAll().pipe(
      tap(x => this.documentTypes = x)
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('application_date').setValue(this.dateToday)
    this.form.get('application_no').disable()
    this.form.get('photo_url').disable()
    switch (this.state) {
      case 'edit':
        this.applicantService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          concatMap(() => {
            return this.applicantService.getResultPage().pipe(
              tap(x => console.log(x)),
              tap(x => this.dataResult = x)
            )
          }),
          tap((x) => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              applicant_uid: this.dataEdit.applicant_uid,
              application_no: this.dataEdit.application_no,
              application_date: this.dataEdit.application_date,
              prefix_id: this.dataEdit.prefix_id,
              prefix_idEng: this.dataEdit.prefix_id,
              other_prefix_name: this.dataEdit.other_prefix_name,
              first_name_th: this.dataEdit.first_name_th,
              middle_name_th: this.dataEdit.middle_name_th,
              last_name_th: this.dataEdit.last_name_th,
              first_name_en: this.dataEdit.first_name_en,
              middle_name_en: this.dataEdit.middle_name_en,
              last_name_en: this.dataEdit.last_name_en,
              birth_date: this.dataEdit.birth_date,
              gender_id: this.dataEdit.gender_id,
              country_of_birth_id: this.dataEdit.country_of_birth_id,
              country_of_birth_name: this.dataEdit.country_of_birth_name,
              province_of_birth_id: this.dataEdit.province_of_birth_id,
              province_of_birth_name: this.dataEdit.province_of_birth_name,
              district_id: this.dataEdit.district_id,
              district_name_th: this.dataEdit.district_name_th,
              nationality_id: this.dataEdit.nationality_id,
              nationality_name: this.dataEdit.nationality_name,
              ethnicity_id: this.dataEdit.ethnicity_id,
              ethnicity_name: this.dataEdit.ethnicity_name,
              religion_id: this.dataEdit.religion_id,
              blood_group_id: this.dataEdit.blood_group_id,
              deformation_id: this.dataEdit.deformation_id,
              citizen_id: this.dataEdit.citizen_id,
              citizen_card_issue_by: this.dataEdit.citizen_card_issue_by,
              citizen_card_issue_date: this.dataEdit.citizen_card_issue_date,
              citizen_card_expiry_date: this.dataEdit.citizen_card_expiry_date,
              tax_id: this.dataEdit.tax_id,
              social_security_no: this.dataEdit.social_security_no,
              passport_no: this.dataEdit.passport_no,
              passport_name: this.dataEdit.passport_name,
              passport_issue_date: this.dataEdit.passport_issue_date,
              passport_expiry_date: this.dataEdit.passport_expiry_date,
              visa_no: this.dataEdit.visa_no,
              visa_name: this.dataEdit.visa_name,
              visa_issue_date: this.dataEdit.visa_issue_date,
              visa_expiry_date: this.dataEdit.visa_expiry_date,
              work_permit_no: this.dataEdit.work_permit_no,
              work_permit_name: this.dataEdit.work_permit_name,
              work_permit_issue_date: this.dataEdit.work_permit_issue_date,
              work_permit_expiry_date: this.dataEdit.work_permit_expiry_date,
              applicant_status_id: this.dataEdit.applicant_status_id,
              email: this.dataEdit.email,
              marital_status_id: this.dataEdit.marital_status_id,
              military_status_id: this.dataEdit.military_status_id,
              agency_uid: this.dataEdit.agency_uid,
              position_uid: this.dataEdit.position_uid,
              photo_url:this.dataEdit.photo_url
            })
            this.dataEdit.applicant_educations.forEach(element => {
              let formarray = this.form.get('applicant_educations') as FormArray
              formarray.push(this.getApplicant_educations(element))
            });
            this.dataEdit.applicant_working_histories.forEach(element => {
              let formarray = this.form.get('applicant_working_histories') as FormArray
              formarray.push(this.getApplicant_working_histories(element))
            });
            this.dataEdit.applicant_addresses.forEach(element => {
              let formarray = this.form.get('applicant_addresses') as FormArray
              formarray.push(this.applicantService.addresses_form(element))
            });
            this.dataEdit.applicant_document.forEach(element => {
              let formarray = this.form.get('applicant_document') as FormArray
              formarray.push(this.applicantService.arrayDoc(element))
            });
          }),
          concatMap(() => {
            return this.countryService.get(this.dataEdit.ethnicity_id).pipe(
              tap(x => console.log(x)),
              tap((x: any) => {
                if(x){
                  this.ethnicity_name = x.country_name_th
                }
              } )
            )
          }),
          concatMap(() => {
            return this.countryService.get(this.dataEdit.nationality_id).pipe(
              tap(x => console.log(x)),
              tap((x: any) => {
                if(x){
                  this.nationality_name = x.country_name_th
                }
              })
            )
          }),
          tap(() => this.cdRef.detectChanges())
        ).subscribe()

        break;
      case 'add':

        for (let i = 0; i < 7; i++) {
          (<FormArray>this.form.get('applicant_document')).insert(i, this.applicantService.arrayDoc());
          (<FormArray>this.form.get('applicant_document')).controls[i].get('document_id').setValue(i+1);
        }


        (<FormArray>this.form.get('applicant_addresses')).insert(0, this.applicantService.addresses_form());
        (<FormArray>this.form.get('applicant_addresses')).controls[0].get('address_type_id').setValue(21);
        (<FormArray>this.form.get('applicant_addresses')).insert(1, this.applicantService.addresses_form());
        (<FormArray>this.form.get('applicant_addresses')).controls[1].get('address_type_id').setValue(22);

        break;
    }
  }

  setNo(value) {
    this.form.get('application_no').setValue(value)
  }

  viewfile(url) {
    const dialogRef = this.dialog.open(
      ViewUploadDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: url  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
      }
    })
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: []  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })

  }

  retrunRequest() {
    this.selectionResultsService.nextState('ใบสมัคร')
    this.router.navigate(['app/save-selection-results/edit', this.dataResult])
  }

  close() {
    this.router.navigate(['app/save_job_application'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.applicantService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_job_application'])
          })
        break;

      case 'add':
        this.applicantService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_job_application'])
          })
        break;
      default:
        break;
    }
  }

  getResuleFile(file, index) {
    (<FormArray>this.form.get('applicant_document')).controls[index].patchValue({
      file_name: file.original_fileName,
      document_url: file.file_url,
      mime_type_name: file.mime_type_name,
    })
  }



  getApplicant_educations(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      applicant_education_uid: el.applicant_education_uid,
      applicant_uid: el.applicant_uid,
      education_level_id: el.education_level_id,
      quali_id: el.quali_id,
      field_name: el.field_name,
      other_institute_name: el.other_institute_name,
      country_id: el.country_id,
      start_year: el.start_year,
      finish_year: el.finish_year,
      grade: el.grade,
      edu_highest: el.edu_highest,
      edu_employ: el.edu_employ,
      start_date: el.start_date,
      finish_date: el.finish_date,
      row_order: el.row_order
    })
  }

  getApplicant_working_histories(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      applicant_working_history_uid: el.applicant_working_history_uid,
      applicant_uid: el.applicant_uid,
      work_place_name: el.work_place_name,
      job_position_name: el.job_position_name,
      job_description: el.job_description,
      final_salary_amount: el.final_salary_amount,
      start_date: el.start_date,
      end_date: el.end_date,
      experience_income_amount: el.experience_income_amount,
      profession_income_amount: el.profession_income_amount,
      other_income_amount: el.other_income_amount,
      remark: el.remark
    })
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      applicant_uid: [null],
      application_no: [''],
      application_date: [null],
      prefix_id: [null],
      prefix_idEng: [null],
      other_prefix_name: [''],
      first_name_th: [''],
      middle_name_th: [''],
      last_name_th: [''],
      first_name_en: [''],
      middle_name_en: [''],
      last_name_en: [''],
      birth_date: [''],
      gender_id: [''],
      country_of_birth_id: [null],
      country_of_birth_name: [''],
      province_of_birth_id: [null],
      province_of_birth_name: [''],
      district_id: [null],
      district_name_th: [''],
      nationality_id: [null],
      nationality_name: [''],
      ethnicity_id: [null],
      ethnicity_name: [[null]],
      religion_id: [null],
      blood_group_id: [null],
      deformation_id: [null],
      citizen_id: [''],
      citizen_card_issue_by: [null],
      citizen_card_issue_date: [null],
      citizen_card_expiry_date: [null],
      tax_id: [''],
      social_security_no: [''],
      passport_no: [''],
      passport_name: [''],
      passport_issue_date: [null],
      passport_expiry_date: [null],
      visa_no: [''],
      visa_name: [''],
      visa_issue_date: [null],
      visa_expiry_date: [null],
      work_permit_no: [''],
      work_permit_name: [''],
      work_permit_issue_date: [null],
      work_permit_expiry_date: [null],
      applicant_status_id: [null],
      email: [null],
      marital_status_id: [null],
      military_status_id: [null],
      agency_uid: [null],
      position_uid: [null],
      photo_url:[''],
      applicant_addresses: this.baseFormBuilder.array([]),
      applicant_educations: this.baseFormBuilder.array([]),
      applicant_working_histories: this.baseFormBuilder.array([]),
      applicant_document: this.baseFormBuilder.array([])
    })
  }

}
