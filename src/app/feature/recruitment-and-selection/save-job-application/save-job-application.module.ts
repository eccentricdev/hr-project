import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveJobApplicationListComponent } from './save-job-application-list/save-job-application-list.component';
import { SaveJobApplicationFormComponent } from './save-job-application-form/save-job-application-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { Tap1Component } from './tap/tap1/tap1.component';
import { Tap2Component } from './tap/tap2/tap2.component';
import { Tap3Component } from './tap/tap3/tap3.component';
import { Tap4Component } from './tap/tap4/tap4.component';
import { PopupInformationComponent } from './tap/tap3/popup-information/popup-information.component';
import { PopupHistoryComponent } from './tap/tap3/popup-history/popup-history.component';

const routes:Routes = [
  {
    path:'',
    component:SaveJobApplicationListComponent
  },
  {
    path:'add',
    component:SaveJobApplicationFormComponent
  },
  {
    path:'edit/:id',
    component:SaveJobApplicationFormComponent
  },
  
]

@NgModule({
  declarations: [
    SaveJobApplicationListComponent, 
    SaveJobApplicationFormComponent,
    Tap1Component,
    Tap2Component,
    Tap3Component,
    Tap4Component,
    PopupInformationComponent,
    PopupHistoryComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveJobApplicationModule { }
