import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { configs } from 'src/app/core/configs/config';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';

@Component({
  selector: 'app-save-job-application-list',
  templateUrl: './save-job-application-list.component.html',
  styleUrls: ['./save-job-application-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveJobApplicationListComponent extends BaseList implements OnInit {

  datasDisplay$ = new Observable()

  rows: any = []
  agencysDatas: any = []
  positionDatas: any = []

  dataSearch = {
    first_name_th: '',
    last_name_th: '',
    agency_uid: null,
    position_uid: null,
    application_start_date: null,
    application_end_date: null
  }

  startDate: any = null
  endDate: any = null

  constructor(public router: Router,
    public datePipe : DatePipe,
    private cdRef: ChangeDetectorRef,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public applicantService: ApplicantService) {
    super();
    // this.positionService.getAll().pipe(
    //   tap(x => this.positionDatas = x)
    // ).subscribe()

    // this.agencysService.getAll().pipe(
    //   tap(x => this.agencysDatas = x)
    // ).subscribe()
  }

  ngOnInit(): void {
    this.getService()

  }

  getService() {
    this.applicantService.getAll().pipe(
      tap(x => x),
      tap(x => this.rows = this.updateMatTable(x)),
      concatMap(() => {
        return this.positionService.getAll().pipe(
          tap(x => this.positionDatas = x)
        )
      }),
      concatMap(() => {
        return this.agencysService.getAll().pipe(
          tap(x => this.agencysDatas = x)
        )
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search() {
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.application_start_date = start
    this.dataSearch.application_end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.applicantService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x))
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      first_name_th: '',
      last_name_th: '',
      agency_uid: null,
      position_uid: null,
      application_start_date: null,
      application_end_date: null
    }
    this.startDate = null
    this.endDate = null
    this.getService()
  }

  addPage() {
    this.router.navigate(['app/save_job_application/add'])
  }
  editPage(id) {
    this.router.navigate(['app/save_job_application/edit', id])
  }



}
