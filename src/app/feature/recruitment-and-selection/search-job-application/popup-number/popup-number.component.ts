import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, map , tap} from 'rxjs/operators';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { GetDataIdPipe } from 'src/app/shared/pipes/get-data-id.pipe';



@Component({
  selector: 'app-popup-number',
  templateUrl: './popup-number.component.html',
  styleUrls: ['./popup-number.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupNumberComponent implements OnInit {

  manpower$ = new Observable()
  position$ = new Observable()
  usingStatuss$ = new Observable()
  positionData:any = []
  manpowerDatas:any = []
  dataMan: any = []
  status:any
  name:any
  
  file:any = {
    status_id: [null],
    created_by: [null],
    created_datetime: [null],
    updated_by: [null],
    updated_datetime: [null],
    selection_request_manpower_uid: [null],
    selection_request_uid: [null],
    manpower_uid: [null],
    manpower_using_status_id:[null],
    row_order: 0,
  }
  constructor(public dialogRef: MatDialogRef<PopupNumberComponent>,
    public positionService :PositionService,
    // private getDataId: GetDataIdPipe,
    public formBuilder: FormBuilder,
    public manpowersService : ManpowersService,
    public activeRoute: ActivatedRoute,
    public manpowerUsingStatussService :ManpowerUsingStatussService,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      // this.manpower$ = this.manpowersService.getAll().pipe(
      //   map((res:any) => res.filter(x => x.status_id == 1)),
      // )
      this.usingStatuss$ = this.manpowerUsingStatussService.getAll()
     }

  ngOnInit(): void {
    console.log(this.data);
    console.log(this.data.position);
    
    if(this.data.editForm != null){
      console.log(this.data.editForm);
      this.getArray(this.data.editForm)
    }
  }

  getManPower(item){
    console.log(item);
    this.manpowersService.getAll().pipe(
      // tap(x => console.log(x)),
      tap((x:any) => this.manpowerDatas = x.filter(x => x.position_uid == this.data.position)),
      concatMap(x => {
        return this.positionService.getAll().pipe(
          tap(x => this.positionData = x)
        )
      }),
      map(() => this.manpowerDatas.filter(x => x.manpower_using_status_id == item)),
      map(x => {
        // console.log(x)
        return x = x.map(el => {
          let position = this.positionData.find(x => x.position_uid == el.position_uid).position_name_th
          return {...el,position_name_th: position}
        })
      }),
      tap(x => {
        // console.log(x);
       this.manpowerDatas =  x.map(el => {
          return {...el,name:el.manpower_no + '-' + el.position_name_th}
        })
        console.log(this.manpowerDatas);
        if(this.data.editForm != null){
          let editName = this.manpowerDatas.find(x => x.manpower_uid == this.data.editForm.manpower_uid)
          console.log(editName);
          if(editName){
            this.name = editName.name
          }
          
        }
        this.cdRef.detectChanges()
      }),
      // tap(() => )
    ).subscribe()
  }

  getManNumber(item){
    console.log(item);
    this.file.status_id = item.status_id
    this.file.manpower_uid = item.manpower_uid
    // this.manpowersService.get(item).pipe(
    //   tap(x => console.log(x)),
    //   tap((x:any) => this.file.status_id = x.status_id)
    // ).subscribe()
  }

  getArray(el) {
    console.log(el);
    this.file = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      manpower_uid: el.manpower_uid,
      selection_request_manpower_uid: el.selection_request_manpower_uid,
      selection_request_uid: el.selection_request_uid,
      manpower_using_status_id:el.manpower_using_status_id,
      row_order: el.row_order
    }
    this.getManPower(el.manpower_using_status_id)
  }

  createArray(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: [null],
    created_by: [null],
    created_datetime: [null],
    updated_by: [null],
    updated_datetime: [null],
    selection_request_manpower_uid: [null],
    selection_request_uid: [null],
    manpower_uid: [null],
    manpower_using_status_id:[null],
    row_order: 0,
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        updated_datetime: el.updated_datetime,
        manpower_uid: el.manpower_uid,
        manpower_using_status_id:el.manpower_using_status_id,
        selection_request_manpower_uid: el.selection_request_manpower_uid,
        selection_request_uid: el.selection_request_uid,
        row_order: el.row_order
      })
    }
  }

  save(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('selection_request_manpower')).push(this.createArray(this.file))  
    }else{
      (<FormArray>this.data.form.get('selection_request_manpower')).controls[this.data.ind].patchValue({
        manpower_uid:this.file.manpower_uid,
        manpower_using_status_id:this.file.manpower_using_status_id,
      })
    }
  }

  saveClose(){
    if(this.data.editForm == null){
      (<FormArray>this.data.form.get('selection_request_manpower')).push(this.createArray(this.file)) 
      this.close() 
    }else{
      (<FormArray>this.data.form.get('selection_request_manpower')).controls[this.data.ind].patchValue({
        manpower_uid:this.file.manpower_uid,
        manpower_using_status_id:this.file.manpower_using_status_id,
      })
      this.close()
    }
  }

  close(){
  this.dialogRef.close('close')
  }

}


