import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { MaritalStatusService } from 'src/app/core/service/common/marital-status.service';
import { MilitaryStatusService } from 'src/app/core/service/common/military-status.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-search-job-application-list',
  templateUrl: './search-job-application-list.component.html',
  styleUrls: ['./search-job-application-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchJobApplicationListComponent extends BaseList implements OnInit {

  datasDisplay$ = new Observable()

  rows:any = []
  positionDatas: any = []
  agencysDatas:any = []
  personnelTypes: any = []

  dataSearch = {
    position_uid:null,
    agency_uid:null,
    personnel_type_id:null
  }


  

  constructor(public router: Router,
    private cdRef: ChangeDetectorRef,
    public appSV: AppService,
    public positionService: PositionService,
    public selectionRequestsService : SelectionRequestsService,
    public personnelTypesService : PersonnelTypesService,
    public agencysService: AgencysService,
    ) {
    super();
    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()

    this.agencysService.getAll().pipe(
      tap(x => this.agencysDatas = x)
    ).subscribe()

    this.personnelTypesService.getAll().pipe(
      tap(x => this.personnelTypes = x)
    ).subscribe()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService() {
    this.selectionRequestsService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => {
       this.rows = this.updateMatTable(x)
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search() {
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.selectionRequestsService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x))
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      position_uid:null,
      agency_uid:null,
      personnel_type_id:null
    }
    this.getService()
  }

  addPage() {
    this.router.navigate(['app/recruitment_employ/add'])
  }
  editPage(id) {
    this.router.navigate(['app/recruitment_employ/edit', id])
  }

  deleteItem(id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.selectionRequestsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
