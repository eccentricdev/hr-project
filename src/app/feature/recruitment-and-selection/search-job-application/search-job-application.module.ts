import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchJobApplicationFormComponent } from './search-job-application-form/search-job-application-form.component';
import { SearchJobApplicationListComponent } from './search-job-application-list/search-job-application-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupNumberComponent } from './popup-number/popup-number.component';


const routes:Routes = [
  {
    path:'',
    component:SearchJobApplicationListComponent
  },
  {
    path:'add',
    component:SearchJobApplicationFormComponent
  },
  {
    path:'edit/:id',
    component:SearchJobApplicationFormComponent
  },
  
]

@NgModule({
  declarations: [
    SearchJobApplicationFormComponent, 
    SearchJobApplicationListComponent, PopupNumberComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SearchJobApplicationModule { }
