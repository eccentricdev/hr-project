import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ManpowerUsingStatussService } from 'src/app/core/service/allocate-people/manpower-using-statuss.service';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { SelectionRequestDocumentsService } from 'src/app/core/service/recruitment/selection-request-documents.service';
import { SelectionRequestsService } from 'src/app/core/service/recruitment/selection-requests.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupNumberComponent } from '../popup-number/popup-number.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-search-job-application-form',
  templateUrl: './search-job-application-form.component.html',
  styleUrls: ['./search-job-application-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchJobApplicationFormComponent extends BaseForm implements OnInit {

  personnelGroups$ = new Observable()
  personnelTypes$ = new Observable()
  agencys$ = new Observable()
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  position$ = new Observable()
  dateToday = new Date()
  dataEditMan: any
  dataEdit: any
  valuePosition: any
  manpowerDatas: any = []
  positionDatas: any = []
  usingStatuss: any = []
  constructor(public dialog: MatDialog,
    public manpowersService: ManpowersService,
    public AgencysService: AgencysService,
    public positionService: PositionService,
    public educationLevelsService: EducationLevelsService,
    public qualificationsService: QualificationsService,
    public personnelGroupsService: PersonnelGroupsService,
    public personnelTypesService: PersonnelTypesService,
    public selectionRequestsService: SelectionRequestsService,
    public manpowerUsingStatussService: ManpowerUsingStatussService,
    public selectionRequestDocumentsService: SelectionRequestDocumentsService,
    public formBuilder: FormBuilder,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.personnelGroups$ = this.personnelGroupsService.getAll()
    this.personnelTypes$ = this.personnelTypesService.getAll()
    this.agencys$ = this.AgencysService.getAll()
    this.educationLevels$ = this.educationLevelsService.getAll()
    this.qualifications$ = this.qualificationsService.getAll()
    this.position$ = this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    )
    this.manpowersService.getAll().pipe(
      tap(x => this.manpowerDatas = x)
    ).subscribe()
    this.manpowerUsingStatussService.getAll().pipe(
      tap(x => this.usingStatuss = x)
    ).subscribe()
  }

  ngOnInit(): void {
    this.form.get('request_date').setValue(this.dateToday)
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.selectionRequestsService.get(this.id).pipe(
          tap(x => this.dataEdit = x),
          tap(() => {
            this.getPosition(this.dataEdit.position_uid)
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              selection_request_uid: this.dataEdit.selection_request_uid,
              document_no: this.dataEdit.document_no,
              request_date: this.dataEdit.request_date,
              agency_uid: this.dataEdit.agency_uid,
              agency_type_id: this.dataEdit.agency_type_id,
              personnel_group_id: this.dataEdit.personnel_group_id,
              personnel_type_id: this.dataEdit.personnel_type_id,
              position_uid: this.dataEdit.position_uid,
              education_level_id: this.dataEdit.education_level_id,
              qualification_id: this.dataEdit.qualification_id,
              remark: this.dataEdit.remark,
              has_exam: this.dataEdit.has_exam,
              has_interview: this.dataEdit.has_interview,
              document_approve_status: this.dataEdit.document_approve_status,
            })
            let formMan = this.form.get('selection_request_manpower') as FormArray
            let formDoc = this.form.get('selection_request_document') as FormArray
            while (formMan.length != 0) {
              formMan.removeAt(0)
            }
            while (formDoc.length != 0) {
              formDoc.removeAt(0)
            }
            if (this.dataEdit.selection_request_manpower != 0) {
              this.dataEdit.selection_request_manpower.forEach(element => {
                formMan.push(this.getFormManpower(element))
              });
            }
            if (this.dataEdit.selection_request_document != 0) {
              this.dataEdit.selection_request_document.forEach(element => {
                formDoc.push(this.getFormDoc(element))
              });
            }
          })
        ).subscribe()
        break;
      case 'add':
        break;
    }
  }

  getPosition(value) {
    console.log(value);
    this.valuePosition = value
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }      // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('selection_request_document') as FormArray
        formarray.push(this.createFormDoc(callback))
      }
    })
  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }      // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('selection_request_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })
  }

  downloadDoc(url) {
    this.selectionRequestsService.download_file(url)
  }

  popupNumberAdd() {
    const dialogRef = this.dialog.open(
      PopupNumberComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null, position: this.valuePosition }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popupNumberEdit(array, index) {
    console.log(array);
    console.log(this.valuePosition);
    const dialogRef = this.dialog.open(
      PopupNumberComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index, position: this.valuePosition }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.selectionRequestsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/recruitment_employ'])
          })
        break;
      case 'add':
        this.selectionRequestsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/recruitment_employ'])
          })
        break;
    }
  }

  close() {
    this.router.navigate(['app/recruitment_employ'])
  }

  createFormManpower() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      selection_request_manpower_uid: [null],
      selection_request_uid: [null],
      manpower_uid: [null],
      manpower_using_status_id: [null],
      row_order: 0,
    })
  }

  getFormManpower(el) {
    return this.baseFormBuilder.group({
      status_id: [el.status_id],
      created_by: [el.created_by],
      created_datetime: [el.created_datetime],
      updated_by: [el.updated_by],
      updated_datetime: [el.updated_datetime],
      selection_request_manpower_uid: [el.selection_request_manpower_uid],
      selection_request_uid: [el.selection_request_uid],
      manpower_uid: [el.manpower_uid],
      manpower_using_status_id: [el.manpower_using_status_id],
      row_order: [el.row_order],
    })
  }

  createFormDoc(el) {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      selection_request_document_uid: null,
      selection_request_uid: this.id ? this.id : null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  getFormDoc(el) {
    return this.baseFormBuilder.group({
      status_id: [el.status_id],
      created_by: [el.created_by],
      created_datetime: [el.created_datetime],
      updated_by: [el.updated_by],
      updated_datetime: [el.updated_datetime],
      selection_request_document_uid: [el.selection_request_document_uid],
      selection_request_uid: [el.selection_request_uid],
      file_name: [el.file_name],
      document_name: [el.document_name],
      document_url: [el.document_url],
      mime_type_name: [el.mime_type_name],
      row_order: [el.row_order],
      remark: el.remark
    })
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.selectionRequestDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('selection_request_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('selection_request_document') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteDetail(item, i) {
    if (item.selection_request_uid) {
      // this.selectionRequestDocumentsService.deleteData(item.selection_request_uid).pipe(
      //   catchError(err => {
      //     this.appSV.swaltAlertError('Error')
      //     return throwError(err)
      //   })).subscribe((x: any) => {
      //     console.log(x)
      //     this.ngOnInit()
      //     this.appSV.swaltAlert()
      //   })
    } else {
      let formarray = this.form.get('selection_request_manpower') as FormArray
      formarray.removeAt(i)
    }
  }



  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      selection_request_uid: [null],
      document_no: [''],
      request_date: [null],
      agency_uid: [null],
      agency_type_id: [null],
      personnel_group_id: [null],
      personnel_type_id: [null],
      position_uid: [null],
      education_level_id: [null],
      qualification_id: [null],
      remark: [''],
      has_exam: false,
      has_interview: false,
      document_approve_status: [null],
      selection_request_manpower: this.baseFormBuilder.array([]),
      selection_request_document: this.baseFormBuilder.array([]),
    })
  }

}
