import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, concatMap, debounceTime, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ContractTypeService } from 'src/app/core/service/common/contract-type.service';
import { OccupationsService } from 'src/app/core/service/common/occupations.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { EmploymentContractDocumentsService } from 'src/app/core/service/recruitment/employment-contract-documents.service';
import { EmploymentContractsService } from 'src/app/core/service/recruitment/employment-contracts.service';
import { SelectionAssignApplicantsService } from 'src/app/core/service/recruitment/selection-assign-applicants.service';
import { SelectionResultsService } from 'src/app/core/service/recruitment/selection-results.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-employment-contract-form',
  templateUrl: './save-employment-contract-form.component.html',
  styleUrls: ['./save-employment-contract-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveEmploymentContractFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  dataEdit: any

  applicant$ = new Observable()
  prefix$ = new Observable()
  personnel$ = new Observable()
  contractType$ = new Observable()
  Occupations$ = new Observable()
  unSubAll$ = new Subject<void>();

  prefixDats: any = []
  personnelDats: any = []
  positionDatas: any = []
  agencyDatas: any = []
  dateToday = new Date()
  dataAdd: any
  dataAddress: any
  subDistricts: any = []



  constructor(public formBuilder: FormBuilder,
    public selectionResultsService: SelectionResultsService,
    public employmentContractDocumentsService: EmploymentContractDocumentsService,
    public employmentContractsService: EmploymentContractsService,
    public applicantService: ApplicantService,
    public prefixService: PrefixService,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public personnelService: PersonnelService,
    public contractTypeService: ContractTypeService,
    public OccupationsService: OccupationsService,
    public subDistrictService: SubDistrictService,
    public dialog: MatDialog,
    public router: Router,
    private cdRef: ChangeDetectorRef,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.Occupations$ = this.OccupationsService.getAll()
    this.applicant$ = this.applicantService.getAll().pipe(
      map((x: any) => {
        return x.map(el => {
          return { ...el, fullName: el.first_name_th + ' ' + el.last_name_th }
        })
      }),
      tap(x => console.log(x))
    )

    this.contractType$ = this.contractTypeService.getAll()

    this.personnel$ = this.personnelService.getAll().pipe(
      tap(x => console.log(x)),
      map((x: any) => {
        return x.map(el => {
          return { ...el, fullName: el.first_name_th + ' ' + el.last_name_th }
        })
      }),
      tap(x => console.log(x))
    )

    this.prefix$ = this.prefixService.getAll().pipe(
      tap(x => console.log(x)),
      tap(x => this.prefixDats = x)
    )

    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x)
    ).subscribe()

    this.agencysService.getAll().pipe(
      tap(x => this.agencyDatas = x)
    ).subscribe()

  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('year').disable()
    this.form.get('month').disable()
    this.form.get('document_date').disable()
    this.form.get('document_date').setValue(this.dateToday)
    this.form.get('address.other_district_name_th').disable()
    this.form.get('address.other_province_name_th').disable()

    this.form.get('address.other_sub_district_name_th').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.subDistrictService.query(`?search=${text}`)),
      tap((subDistricts: any) => this.subDistricts = [...subDistricts]),
      tap(() => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    switch (this.state) {
      case 'edit':
        this.employmentContractsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(x => {
            if(this.dataEdit.address){
              this.getAddess(this.dataEdit.address)
            }
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              employment_contract_uid: this.dataEdit.employment_contract_uid,
              work_start_date: this.dataEdit.work_start_date,
              document_no: this.dataEdit.document_no,
              document_date: this.dataEdit.document_date,
              application_uid: this.dataEdit.application_uid,
              personnel_uid: this.dataEdit.personnel_uid,
              contract_type: this.dataEdit.contract_type,
              contract_time_no: this.dataEdit.contract_time_no,
              attorney_personnel_uid: this.dataEdit.attorney_personnel_uid,
              start_date: this.dataEdit.start_date,
              end_date: this.dataEdit.end_date,
              year: this.dataEdit.year,
              month: this.dataEdit.month,
              salary_amount: this.dataEdit.salary_amount,
              beneficiary: this.dataEdit.beneficiary,
              beneficiary_phone_no: this.dataEdit.beneficiary_phone_no,
              has_surety: this.dataEdit.has_surety,
              guarantee_amount: this.dataEdit.guarantee_amount,
              surety_prefix_id: this.dataEdit.surety_prefix_id,
              surety_first_name: this.dataEdit.surety_first_name,
              surety_last_name: this.dataEdit.surety_last_name,
              surety_birth_date: this.dataEdit.surety_birth_date,
              surety_occupation_id: this.dataEdit.surety_occupation_id,
              surety_spouse_prefix_id: this.dataEdit.surety_spouse_prefix_id,
              surety_spouse_first_name: this.dataEdit.surety_spouse_first_name,
              surety_spouse_last_name: this.dataEdit.surety_spouse_last_name,
              address_uid: this.dataEdit.address_uid,
            })
            let formarray = this.form.get('employment_contract_document') as FormArray
            while(formarray.length != 0){
              formarray.removeAt(0)
            }
            if(this.dataEdit.employment_contract_document.length != 0){
              this.dataEdit.employment_contract_document.forEach(element => {
                formarray.push(this.get_Doc(element))
              });
            }
          })
        ).subscribe()

        break;
      case 'add':
        this.employmentContractsService.getResultId().pipe(
          tap(x => this.dataAdd = x),
          tap(() => {
            console.log(this.dataAdd);

            this.form.patchValue({
              application_uid: this.dataAdd.application_uid,
            })
          }),
          tap(() => this.cdRef.detectChanges())
        ).subscribe()
        break;
    }
  }

  getAddess(el) {
    this.form.get('address').patchValue({
      address_uid: el.address_uid,
      address_name: el.address_name,
      house_no: el.house_no,
      village_no: el.village_no,
      building_name: el.building_name,
      room_no: el.room_no,
      floor_no: el.floor_no,
      village_name: el.village_name,
      alley_name: el.alley_name,
      street_name: el.street_name,
      sub_district_id: el.sub_district_id,
      other_sub_district_name_th: el.other_sub_district_name_th,
      other_sub_district_name_en: el.other_sub_district_name_en,
      other_district_name_th: el.other_district_name_th,
      other_district_name_en: el.other_district_name_en,
      other_province_name_th: el.other_province_name_th,
      other_province_name_en: el.other_province_name_en,
      postal_code: el.postal_code,
      country_id: el.country_id,
      telephone_no: el.telephone_no
    })
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('employment_contract_document') as FormArray
        formarray.push(this.create_Doc(callback))
      }
    })

  }
  
  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit' ,remark: array.remark }
    }
    )
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('employment_contract_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })

  }

  downloadDoc(url) {
    this.employmentContractsService.download_file(url)
  }

  create_Doc(el) {
    return this.baseFormBuilder.group({
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      employment_contract_document_uid: null,
      employment_contract_uid: this.id ? this.id : null,
      remark: el.remark,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null
    })
  }


  get_Doc(el) {
    return this.baseFormBuilder.group({
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      employment_contract_document_uid: el.employment_contract_document_uid,
      employment_contract_uid: el.employment_contract_uid,
      remark: el.remark,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
    })
  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.employmentContractDocumentsService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('employment_contract_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('employment_contract_document') as FormArray
      formarray.removeAt(i)
    }
  }

  selectSubDistrict(data) {
    console.log(data)
    this.form.get('address.sub_district_id').setValue(data.sub_district_id)
    this.form.get('address.other_district_name_th').setValue(data.district_name_th)
    this.form.get('address.other_province_name_th').setValue(data.province_name_th)
  }


  setDate() {
    if (this.form.value.start_date && this.form.value.end_date) {
      let startDate = moment(this.form.value.start_date, 'dd/MM/yyyy');
      let endDate = moment(this.form.value.end_date, 'dd/MM/yyyy');
      let Month = endDate.diff(startDate, 'months');
      console.log('Month:' + Month);
      let Year = endDate.diff(startDate, 'years');
      console.log('Year:' + Year);
      console.log(Month);
      console.log(Year);
      if (Month || Year) {
        this.form.get('year').setValue(Year)
        this.form.get('month').setValue(Month)
      }
    }

  }

  retrunRequest() {
    this.selectionResultsService.nextState('สัญญาจ้าง')
    this.router.navigate(['app/save-selection-results/edit', this.dataAdd.selection_request_uid])
  }


  close() {
    this.router.navigate(['app/save_employment_contract'])
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.employmentContractsService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_employment_contract'])
          })
        break;

      case 'add':
        this.employmentContractsService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_employment_contract'])
          })
        break;
      default:
        break;
    }
  }

  getPosition(value) {
    console.log(value);
    this.positionService.get(value).pipe(
      tap(x => console.log(x))
    ).subscribe()

  }



  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [null],
      created_datetime: [null],
      updated_by: [null],
      updated_datetime: [null],
      employment_contract_uid: [null],
      work_start_date: [null],
      document_no: [''],
      document_date: [null],
      application_uid: [null],
      personnel_uid: [null],
      contract_type: [null],
      contract_time_no: [''],
      attorney_personnel_uid: [null],
      start_date: [null],
      end_date: [null],
      year: [null],
      month: [null],
      salary_amount: [0],
      beneficiary: [''],
      beneficiary_phone_no: [''],
      has_surety: true,
      guarantee_amount: [0],
      surety_prefix_id: [null],
      surety_first_name: [''],
      surety_last_name: [''],
      surety_birth_date: [null],
      surety_occupation_id: [null],
      surety_spouse_prefix_id: [null],
      surety_spouse_first_name: [''],
      surety_spouse_last_name: [''],
      address_uid: [null],
      employment_contract_document: this.baseFormBuilder.array([]),
      address: this.baseFormBuilder.group({
        address_uid: [null],
        address_name: [''],
        house_no: [''],
        village_no: [''],
        building_name: [''],
        room_no: [''],
        floor_no: [''],
        village_name: [''],
        alley_name: [''],
        street_name: [''],
        sub_district_id: [null],
        other_sub_district_name_th: [''],
        other_sub_district_name_en: [''],
        other_district_name_th: [''],
        other_district_name_en: [''],
        other_province_name_th: [''],
        other_province_name_en: [''],
        postal_code: [''],
        country_id: [null],
        telephone_no: ['']
      })
    })
  }

}
