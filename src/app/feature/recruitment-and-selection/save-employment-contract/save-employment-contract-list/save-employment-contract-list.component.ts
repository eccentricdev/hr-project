import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { BaseList } from 'src/app/core/base/base-list';
import { configs } from 'src/app/core/configs/config';
import { ContractTypeService } from 'src/app/core/service/common/contract-type.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { ApplicantService } from 'src/app/core/service/recruitment/applicant.service';
import { EmploymentContractsService } from 'src/app/core/service/recruitment/employment-contracts.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-employment-contract-list',
  templateUrl: './save-employment-contract-list.component.html',
  styleUrls: ['./save-employment-contract-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveEmploymentContractListComponent extends BaseList implements OnInit {

  datasDisplay$ = new Observable()

  rows: any = []
  positionDatas: any = []
  agencyDatas: any = []
  applicantData: any = []
  personnelData: any = []
  contractTypeData: any = []

  dataSearch:any = {
      application_uid: null,
      agency_uid: null,
      position_uid: null,
      document_start_date:null,
      document_end_date:null
  }

  startDate:any = null
  endDate:any = null

  constructor(public router: Router,
    public applicantService: ApplicantService,
    public employmentContractsService: EmploymentContractsService,
    public positionService: PositionService,
    private cdRef: ChangeDetectorRef,
    public datePipe : DatePipe,
    public contractTypeService: ContractTypeService,
    public personnelService: PersonnelService,
    public agencysService: AgencysService) {
    super();
    this.positionService.getAll().pipe(
      tap(x => this.positionDatas = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

    this.agencysService.getAll().pipe(
      tap(x => this.agencyDatas = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

    this.contractTypeService.getAll().pipe(
      tap(x => this.contractTypeData = x)
    ).subscribe()

    this.applicantService.getAll().pipe(
      map((x: any) => x),
      tap(x => {
        this.applicantData = x.map(el => {
          return { ...el, fullName: el.first_name_th + ' ' + el.last_name_th }
        })
        console.log(this.applicantData);

      }),
    ).subscribe()

    this.personnelService.getAll().pipe(
      map((x: any) => x),
      tap(x => {
        this.personnelData = x.map(el => {
          return { ...el, fullName: el.first_name_th + ' ' + el.last_name_th }
        })
        console.log(this.personnelData);

      }),
    ).subscribe()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService() {
    this.employmentContractsService.getAll().pipe(
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  search() {
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.document_start_date = start
    this.dataSearch.document_end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.datasDisplay$ = this.employmentContractsService.query(`?${queryStr}`)
      this.datasDisplay$.pipe(
        tap(x => this.rows = this.updateMatTable(x))
      ).subscribe()
    }
  }

  clear() {
    this.dataSearch = {
      application_uid: null,
      agency_uid: null,
      position_uid: null,
      document_start_date:null,
      document_end_date:null
    }
    this.startDate = null
    this.endDate = null
    this.getService()
  }

  addPage() {
    this.router.navigate(['app/save_employment_contract/add'])
  }
  editPage(id) {
    this.router.navigate(['app/save_employment_contract/edit', id])
  }


}
