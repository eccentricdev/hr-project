import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveEmploymentContractFormComponent } from './save-employment-contract-form/save-employment-contract-form.component';
import { SaveEmploymentContractListComponent } from './save-employment-contract-list/save-employment-contract-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SaveEmploymentContractListComponent
  },
  {
    path:'add',
    component:SaveEmploymentContractFormComponent
  },
  {
    path:'edit/:id',
    component:SaveEmploymentContractFormComponent
  },
  
]



@NgModule({
  declarations: [SaveEmploymentContractFormComponent, SaveEmploymentContractListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveEmploymentContractModule { }
