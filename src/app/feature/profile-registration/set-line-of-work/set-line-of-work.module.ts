import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetLineWorkListComponent } from './set-line-work-list/set-line-work-list.component';
import { SetLineWorkFormComponent } from './set-line-work-form/set-line-work-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetLineWorkListComponent
  },
  {
    path:'add',
    component:SetLineWorkFormComponent
  },
  {
    path:'edit/:id',
    component:SetLineWorkFormComponent
  },
  
]

@NgModule({
  declarations: [SetLineWorkListComponent, SetLineWorkFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetLineOfWorkModule { }
