import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetLineWorkListComponent } from './set-line-work-list.component';

describe('SetLineWorkListComponent', () => {
  let component: SetLineWorkListComponent;
  let fixture: ComponentFixture<SetLineWorkListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetLineWorkListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetLineWorkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
