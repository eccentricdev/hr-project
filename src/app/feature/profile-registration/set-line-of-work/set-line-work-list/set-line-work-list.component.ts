import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { WorkFieldsService } from 'src/app/core/service/common/work-fields.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-line-work-list',
  templateUrl: './set-line-work-list.component.html',
  styleUrls: ['./set-line-work-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetLineWorkListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  workFields$ = new Observable()
  req = {
    work_field_code : '',
    work_field_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public WorkFieldsSV : WorkFieldsService,
  ) { super()
    this.workFields$ = this.WorkFieldsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.workFields$  = this.WorkFieldsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      work_field_code : '',
      work_field_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_line_work/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_line_work/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.WorkFieldsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.workFields$ = this.WorkFieldsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })


        }
      })
    }
  }
}
