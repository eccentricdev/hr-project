import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { WorkFieldsService } from 'src/app/core/service/common/work-fields.service';

@Component({
  selector: 'app-set-line-work-form',
  templateUrl: './set-line-work-form.component.html',
  styleUrls: ['./set-line-work-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetLineWorkFormComponent extends BaseForm implements OnInit {
  constructor(
    public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public WorkFieldsSV: WorkFieldsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.WorkFieldsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      work_field_id:res.work_field_id,
      work_field_code:res.work_field_code,
      work_field_name_th:res.work_field_name_th,
      work_field_name_en:res.work_field_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_line_work'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.WorkFieldsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_line_work'])
          })
        break;
      case 'add':
      
        this.WorkFieldsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_line_work'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      work_field_id: [null],
      work_field_code: [''],
      work_field_name_th: [''],
      work_field_name_en: [''],
    })
  }
}
