import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetLineWorkFormComponent } from './set-line-work-form.component';

describe('SetLineWorkFormComponent', () => {
  let component: SetLineWorkFormComponent;
  let fixture: ComponentFixture<SetLineWorkFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetLineWorkFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetLineWorkFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
