import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { AcademicPositionsService } from 'src/app/core/service/common/academic-positions.service';

@Component({
  selector: 'app-academic-position-form',
  templateUrl: './academic-position-form.component.html',
  styleUrls: ['./academic-position-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcademicPositionFormComponent extends BaseForm implements OnInit {
  baseFormBuilder: any;
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public AcademicPositionsSV: AcademicPositionsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.AcademicPositionsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      academic_position_id:res.academic_position_id,
      academic_position_code:res.academic_position_code,
      academic_position_name_th:res.academic_position_name_th,
      academic_position_name_en:res.academic_position_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_academic_position'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.AcademicPositionsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_academic_position'])
          })
        break;
      case 'add':
      
        this.AcademicPositionsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_academic_position'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      academic_position_id: [null],
      academic_position_code: [''],
      academic_position_name_th: [''],
      academic_position_name_en: [''],
    })
  }
}
