import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicPositionListComponent } from './academic-position-list.component';

describe('AcademicPositionListComponent', () => {
  let component: AcademicPositionListComponent;
  let fixture: ComponentFixture<AcademicPositionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcademicPositionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicPositionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
