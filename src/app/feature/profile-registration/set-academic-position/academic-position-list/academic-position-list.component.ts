import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { AcademicPositionsService } from 'src/app/core/service/common/academic-positions.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-academic-position-list',
  templateUrl: './academic-position-list.component.html',
  styleUrls: ['./academic-position-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcademicPositionListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  academicPositions$ = new Observable()
  req = {
    education_level_code : '',
    education_level_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public AcademicPositionsSV : AcademicPositionsService,
  ) { super()
    this.academicPositions$ = this.AcademicPositionsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
   }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.academicPositions$  = this.AcademicPositionsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      education_level_code : '',
      education_level_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_academic_position/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_academic_position/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
          this.AcademicPositionsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.academicPositions$ = this.AcademicPositionsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })


        }
      })
    }
  }
}
