import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicPositionListComponent } from './academic-position-list/academic-position-list.component';
import { AcademicPositionFormComponent } from './academic-position-form/academic-position-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:AcademicPositionListComponent
  },
  {
    path:'add',
    component:AcademicPositionFormComponent
  },
  {
    path:'edit/:id',
    component:AcademicPositionFormComponent
  },
  
]

@NgModule({
  declarations: [AcademicPositionListComponent, AcademicPositionFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetAcademicPositionModule { }
