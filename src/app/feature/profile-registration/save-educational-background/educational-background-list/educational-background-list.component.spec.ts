import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationalBackgroundListComponent } from './educational-background-list.component';

describe('EducationalBackgroundListComponent', () => {
  let component: EducationalBackgroundListComponent;
  let fixture: ComponentFixture<EducationalBackgroundListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EducationalBackgroundListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationalBackgroundListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
