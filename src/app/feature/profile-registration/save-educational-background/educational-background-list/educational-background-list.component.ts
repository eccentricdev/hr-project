import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, takeUntil, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { CountryService } from 'src/app/core/service/common/country.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { SubjectGroupTypesService } from 'src/app/core/service/common/subject-group-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EducationLevelAdjustmentsService } from 'src/app/core/service/register/education-level-adjustments.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelHiresService } from 'src/app/core/service/register/personnel-hires.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-educational-background-list',
  templateUrl: './educational-background-list.component.html',
  styleUrls: ['./educational-background-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class EducationalBackgroundListComponent extends BaseList implements OnInit,OnDestroy {
  employeeData : any  = []
  req = {
    transfer_document_no : '',
    name : '',
    transfer_order_date : null,
    personnel_uid : null,
    approve_status : null,
  }
  unSubAll$ = new Subject<void>();
  educationLevelAdjustments$ = new Observable()
  employee$ = new Observable()
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  country$ = new Observable()
  subjectGroupType$ = new Observable()
  personnelHires$ = new Observable()
  position$ = new Observable()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  constructor(
    public router : Router,
    public appSV : AppService,
    public educationLevelAdjustmentsSV: EducationLevelAdjustmentsService,
    public employeeSV: EmployeeService,
    public educationLevelsSV: EducationLevelsService,
    public qualificationsSV: QualificationsService,
    public countrySV: CountryService,
    public SubjectGroupTypeSV: SubjectGroupTypesService,
    public PersonnelHiresSV : PersonnelHiresService,
    public PositionSV : PositionService,
    public AgencysSV : AgencysService,
    public ApproveStatussSV : ApproveStatussService,
  ) { super()
    this.educationLevelAdjustments$ = this.educationLevelAdjustmentsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
    this.educationLevels$ = this.educationLevelsSV.getAll()
    this.qualifications$ = this.qualificationsSV.getAll()
    this.country$ = this.countrySV.getAll()
    this.subjectGroupType$ = this.SubjectGroupTypeSV.getAll()
    this.employee$ = this.employeeSV.getAll()
    this.personnelHires$ = this.PersonnelHiresSV.getAll()
    this.position$ = this.PositionSV.getAll()
    this.agencys$ = this.AgencysSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.educationLevelAdjustments$  = this.educationLevelAdjustmentsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      transfer_document_no : '',
      name : '',
      transfer_order_date : null,
      personnel_uid : null,
      approve_status : null,
    }
    this.search()
  }
  inputSearch(){
    console.log()
    this.employeeSV.query(`?display_name_th=${this.req.name}`).pipe(
      tap((employee: any) => console.log(employee)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
  }
  select_personnels(value){
    console.log(value)
    this.req.personnel_uid = value
  }
  addPage(){
    this.router.navigate(['app/pr_educational_background/add'])
  }

  editPage(id){
    this.router.navigate(['app/pr_educational_background/edit',id])
  }

  dateChange(){

  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
          this.educationLevelAdjustmentsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.educationLevelAdjustments$ = this.educationLevelAdjustmentsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })
        }
      })
    }
  }
  ngOnDestroy(): void {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
