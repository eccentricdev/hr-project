import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationalBackgroundListComponent } from './educational-background-list/educational-background-list.component';
import { EducationalBackgroundFormComponent } from './educational-background-form/educational-background-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:EducationalBackgroundListComponent
  },
  {
    path:'add',
    component:EducationalBackgroundFormComponent
  },
  {
    path:'edit/:id',
    component:EducationalBackgroundFormComponent
  },
  
]

@NgModule({
  declarations: [EducationalBackgroundListComponent, EducationalBackgroundFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveEducationalBackgroundModule { }
