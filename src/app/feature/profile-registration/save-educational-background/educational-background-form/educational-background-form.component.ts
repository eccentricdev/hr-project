import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, delay, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { ApproveStatussService } from 'src/app/core/service/allocate-people/approve-statuss.service';
import { AppService } from 'src/app/core/service/app.service';
import { CountryService } from 'src/app/core/service/common/country.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { SubjectGroupTypesService } from 'src/app/core/service/common/subject-group-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { EducationLevelAdjustmentsService } from 'src/app/core/service/register/education-level-adjustments.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-educational-background-form',
  templateUrl: './educational-background-form.component.html',
  styleUrls: ['./educational-background-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class EducationalBackgroundFormComponent extends BaseForm implements OnInit,OnDestroy {

  unSubAll$ = new Subject<boolean>()
  employee$ = new Observable()
  educationLevels$ = new Observable()
  qualifications$ = new Observable()
  country$ = new Observable()
  subjectGroupType$ = new Observable()
  rows :any = []
  personnelHires :any = []
  personnelEducations :any = []
  employeeData : any = []

  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  agency_uid : any
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  @ViewChild('service_life') service_life: ElementRef<HTMLInputElement>
  
  constructor(
    public dialog :MatDialog,
    public router:Router,
    public fb: FormBuilder,  
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public educationLevelAdjustmentsSV: EducationLevelAdjustmentsService,
    public EmployeeSV: EmployeeService,
    public educationLevelsSV: EducationLevelsService,
    public qualificationsSV: QualificationsService,
    public countrySV: CountryService,
    public AgencysSV: AgencysService,
    public ApproveStatussSV: ApproveStatussService,
    public SubjectGroupTypeSV: SubjectGroupTypesService
    ) { 
      super(fb,activeRoute)
      this.agencys$ = this.AgencysSV.getAll()
      this.approveStatuss$ = this.ApproveStatussSV.getAll()
    }

  ngOnInit(): void {
    this.educationLevels$ = this.educationLevelsSV.getAll()
    this.qualifications$ = this.qualificationsSV.getAll()
    this.country$ = this.countrySV.getAll()
    this.subjectGroupType$ = this.SubjectGroupTypeSV.getAll()
  

    switch (this.state) { 
      case 'edit':
        this.educationLevelAdjustmentsSV.get(this.id).pipe(
          takeUntil(this.unSubAll$),
          tap(x => console.log(x)),
          tap(res => this.setUpform(res))
        ).subscribe()
        break;
      case 'add':

        break;
    }


    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
   
  }

    dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }

    select_personnels(data){
      console.log(data)
      this.EmployeeSV.get(data).pipe(
        tap(x => console.log(x)),
        tap((x:any) => {
          let todayDate = moment()
          if(x.birth_date == null){
            this.age.nativeElement.value = 'ไม่พบข้อมูล'
            
          }else{
            let calculateAge = todayDate.diff(x.birth_date, 'years')
            this.age.nativeElement.value = calculateAge.toString()
          }
          let startDate = moment()
          let calculateStartday = startDate.diff(x.personnel_hires[0].start_date, 'day')
          let calculateStartM = startDate.diff(x.personnel_hires[0].start_date, 'months')
          let calculateStartY = startDate.diff(x.personnel_hires[0].start_date, 'years')
          
          console.log(calculateStartday)
          console.log(calculateStartM)
          console.log(calculateStartY)
          if(x.personnel_hires[0].start_date == null){
            this.service_life.nativeElement.value = 'ไม่พบข้อมูล'
          }else{
            this.service_life.nativeElement.value = `${calculateStartday+`วัน`+calculateStartM+`เดือน`+calculateStartY+`ปี`}`
            this.form.get('start_date').setValue(x.personnel_hires[0].start_date)
          }
          this.position.nativeElement.value = x.personnel_hires[0]?.position?.position_name_th == undefined ? 'ไม่มีข้อมูล' : x.personnel_hires[0]?.position?.position_name_th
          this.form.get('personnel_uid').setValue(x.personnel_uid)
          this.form.get('personnel_no').setValue(x.personnel_no)
          this.form.get('citizen_id').setValue(x.citizen_id)
          this.form.get('display_name_th').setValue(x.display_name_th)
  
          this.agency_uid = x.personnel_hires[0].agency_uid
  
        }),
      ).subscribe()
  
    }

    openUpload() {
      const dialogRef = this.dialog.open(
        UploadFileDialogComponent, {
        width: '40%',
        disableClose: true,
        data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }

    createForm() {
      return this.baseFormBuilder.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [new Date()],
        updated_by: [''],
        updated_datetime: [new Date()],
        education_level_adjustment_uid: [null],
        personnel_uid: [null],
        education_level_id: [null],
        qualification_id: [null],
        field_name: [''],
        other_institute_name: [''],
        document_no: [''],
        country_id: [null],
        request_date: [null],
        start_year: [new Date()],
        finish_year: [new Date()],
        start_date: [null],
        grade: [''],
        approve_status: [null],
        personnel_no: [null],
        citizen_id: [''],
        display_name_th: [''],
      })
    }

    setUpform(res){
      this.form.patchValue(res)
      this.select_personnels(res.personnel_uid)
    }

    close(){
      this.router.navigate(['app/pr_educational_background'])
    }

    save() {
      switch (this.state) {
        case 'edit':
          this.educationLevelAdjustmentsSV.update(this.form.getRawValue()).pipe(
            takeUntil(this.unSubAll$),
            catchError(err => {
              console.log(err)
                this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/pr_educational_background'])
            })
        break;
  
        case 'add':
          this.educationLevelAdjustmentsSV.add(this.form.getRawValue()).pipe(
            takeUntil(this.unSubAll$),
            catchError(err => {
              console.log(err)
                this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/pr_educational_background'])
            })
        break;
        default:
          break;
      }
    }

    ngOnDestroy(): void {
      this.unSubAll$.next(true)
      this.unSubAll$.complete()
    }
    
}
