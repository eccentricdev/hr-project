import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';

@Component({
  selector: 'app-set-qualification-form',
  templateUrl: './set-qualification-form.component.html',
  styleUrls: ['./set-qualification-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetQualificationFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public QualificationsSV: QualificationsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.QualificationsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      qualification_id:res.qualification_id,
      qualification_code:res.qualification_code,
      qualification_name_th:res.qualification_name_th,
      qualification_name_en:res.qualification_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_qualification'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.QualificationsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_qualification'])
          })
        break;
      case 'add':
      
        this.QualificationsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_qualification'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      qualification_id: [null],
      qualification_code: [''],
      qualification_name_th: [''],
      qualification_name_en: [''],
    })
  }
}
