import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetQualificationFormComponent } from './set-qualification-form.component';

describe('SetQualificationFormComponent', () => {
  let component: SetQualificationFormComponent;
  let fixture: ComponentFixture<SetQualificationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetQualificationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetQualificationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
