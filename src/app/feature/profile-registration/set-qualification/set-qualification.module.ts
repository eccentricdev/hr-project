import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetQualificationComponent } from './set-qualification/set-qualification.component';
import { SetQualificationFormComponent } from './set-qualification-form/set-qualification-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetQualificationComponent
  },
  {
    path:'add',
    component:SetQualificationFormComponent
  },
  {
    path:'edit/:id',
    component:SetQualificationFormComponent
  },
  
]

@NgModule({
  declarations: [SetQualificationComponent, SetQualificationFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetQualificationModule { }
