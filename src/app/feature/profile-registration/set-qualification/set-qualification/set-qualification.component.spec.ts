import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetQualificationComponent } from './set-qualification.component';

describe('SetQualificationComponent', () => {
  let component: SetQualificationComponent;
  let fixture: ComponentFixture<SetQualificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetQualificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetQualificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
