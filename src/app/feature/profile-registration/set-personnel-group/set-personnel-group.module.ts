import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonnelGroupListComponent } from './personnel-group-list/personnel-group-list.component';
import { PersonnelGroupFormComponent } from './personnel-group-form/personnel-group-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PersonnelGroupListComponent
  },
  {
    path:'add',
    component:PersonnelGroupFormComponent
  },
  {
    path:'edit/:id',
    component:PersonnelGroupFormComponent
  },
  
]

@NgModule({
  declarations: [PersonnelGroupListComponent, PersonnelGroupFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetPersonnelGroupModule { }
