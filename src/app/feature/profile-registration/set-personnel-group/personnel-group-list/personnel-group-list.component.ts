import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-personnel-group-list',
  templateUrl: './personnel-group-list.component.html',
  styleUrls: ['./personnel-group-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonnelGroupListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  personnelGroup$ = new Observable()
  req = {
    personnel_group_code : '',
    personnel_group_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public PersonnelGroupsSV : PersonnelGroupsService,
  ) { super()
    this.personnelGroup$ = this.PersonnelGroupsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.personnelGroup$  = this.PersonnelGroupsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      personnel_group_code : '',
      personnel_group_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_personnel_group/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_personnel_group/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
          this.PersonnelGroupsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.personnelGroup$ = this.PersonnelGroupsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })


        }
      })
    }
  }
}
