import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelGroupListComponent } from './personnel-group-list.component';

describe('PersonnelGroupListComponent', () => {
  let component: PersonnelGroupListComponent;
  let fixture: ComponentFixture<PersonnelGroupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnelGroupListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
