import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';

@Component({
  selector: 'app-personnel-group-form',
  templateUrl: './personnel-group-form.component.html',
  styleUrls: ['./personnel-group-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonnelGroupFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PersonnelGroupsSV: PersonnelGroupsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PersonnelGroupsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      personnel_group_id:res.personnel_group_id,
      personnel_group_code:res.personnel_group_code,
      personnel_group_name_th:res.personnel_group_name_th,
      personnel_group_name_en:res.personnel_group_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_personnel_group'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.PersonnelGroupsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_personnel_group'])
          })
        break;
      case 'add':
      
        this.PersonnelGroupsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_personnel_group'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      personnel_group_id: [null],
      personnel_group_code: [''],
      personnel_group_name_th: [''],
      personnel_group_name_en: [''],
    })
  }
}
