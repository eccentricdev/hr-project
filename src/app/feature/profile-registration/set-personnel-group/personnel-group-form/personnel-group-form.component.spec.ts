import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelGroupFormComponent } from './personnel-group-form.component';

describe('PersonnelGroupFormComponent', () => {
  let component: PersonnelGroupFormComponent;
  let fixture: ComponentFixture<PersonnelGroupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnelGroupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
