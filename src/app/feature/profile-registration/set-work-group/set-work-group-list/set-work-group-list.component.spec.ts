import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetWorkGroupListComponent } from './set-work-group-list.component';

describe('SetWorkGroupListComponent', () => {
  let component: SetWorkGroupListComponent;
  let fixture: ComponentFixture<SetWorkGroupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetWorkGroupListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetWorkGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
