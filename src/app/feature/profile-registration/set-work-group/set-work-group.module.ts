import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetWorkGroupListComponent } from './set-work-group-list/set-work-group-list.component';
import { SetWorkGroupFormComponent } from './set-work-group-form/set-work-group-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetWorkGroupListComponent
  },
  {
    path:'add',
    component:SetWorkGroupFormComponent
  },
  {
    path:'edit/:id',
    component:SetWorkGroupFormComponent
  },
  
]

@NgModule({
  declarations: [SetWorkGroupListComponent, SetWorkGroupFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetWorkGroupModule { }
