import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { WorkGroupsService } from 'src/app/core/service/common/work-groups.service';

@Component({
  selector: 'app-set-work-group-form',
  templateUrl: './set-work-group-form.component.html',
  styleUrls: ['./set-work-group-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetWorkGroupFormComponent extends BaseForm implements OnInit {

  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public WorkGroupsSV: WorkGroupsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.WorkGroupsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      work_group_id:res.work_group_id,
      work_group_code:res.work_group_code,
      work_group_name_th:res.work_group_name_th,
      work_group_name_en:res.work_group_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_work_group'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.WorkGroupsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_work_group'])
          })
        break;
      case 'add':
      
        this.WorkGroupsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_work_group'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      work_group_id: [null],
      work_group_code: [''],
      work_group_name_th: [''],
      work_group_name_en: [''],
    })
  }
}
