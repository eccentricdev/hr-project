import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetWorkGroupFormComponent } from './set-work-group-form.component';

describe('SetWorkGroupFormComponent', () => {
  let component: SetWorkGroupFormComponent;
  let fixture: ComponentFixture<SetWorkGroupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetWorkGroupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetWorkGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
