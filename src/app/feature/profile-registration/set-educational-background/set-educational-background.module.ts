import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationalBackgroundComponent } from './educational-background/educational-background.component';
import { EducationalBackgroundFormComponent } from './educational-background-form/educational-background-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:EducationalBackgroundComponent
  },
  {
    path:'add',
    component:EducationalBackgroundFormComponent
  },
  {
    path:'edit/:id',
    component:EducationalBackgroundFormComponent
  },
  
]

@NgModule({
  declarations: [EducationalBackgroundComponent, EducationalBackgroundFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetEducationalBackgroundModule { }
