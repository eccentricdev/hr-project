import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';

@Component({
  selector: 'app-educational-background-form',
  templateUrl: './educational-background-form.component.html',
  styleUrls: ['./educational-background-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class EducationalBackgroundFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public EducationLevelsSV: EducationLevelsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.EducationLevelsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      education_level_id:res.education_level_id,
      education_level_code:res.education_level_code,
      education_level_name_th:res.education_level_name_th,
      education_level_name_en:res.education_level_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_education'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.EducationLevelsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_education'])
          })
        break;
      case 'add':
      
        this.EducationLevelsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_education'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      education_level_id: [null],
      education_level_code: [''],
      education_level_name_th: [''],
      education_level_name_en: [''],
    })
  }
}
