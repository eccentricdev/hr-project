import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormProfessionalLicenseComponent } from './save-profile-registration-form-professional-license.component';

describe('SaveProfileRegistrationFormProfessionalLicenseComponent', () => {
  let component: SaveProfileRegistrationFormProfessionalLicenseComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormProfessionalLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormProfessionalLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormProfessionalLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
