import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-professional-license-dialog',
  templateUrl: './professional-license-dialog.component.html',
  styleUrls: ['./professional-license-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfessionalLicenseDialogComponent implements OnInit {
  rows: any = []
  licenseForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_license_id: null,
    personnel_id: null,
    license_no: '',
    license_name: '',
    license_by: '',
    issue_date: null,
    expiry_date: null,
    is_never_expire: true,
    personnel_license_documents: [],
  }
  personnel_license_documentsForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_license_document_id: null,
    personnel_license_id: null,
    document_name: '',
    document_url: '',
    row_no: '',
  }
  personnel_license_documentsFormArray = []
  constructor(public dialogRef: MatDialogRef<ProfessionalLicenseDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public dialog: MatDialog,
    public EmployeeSV: EmployeeService,
  ) { }

  ngOnInit(): void {
  }

  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_licenses')).insert(0, this.EmployeeSV.personnel_licenses_form(this.licenseForm));
  }
  save() {
    (<FormArray>this.data.get('personnel_licenses')).insert(0, this.EmployeeSV.personnel_licenses_form(this.licenseForm));
    console.log(this.data)
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      this.personnel_license_documentsFormArray.push(this.personnel_license_documentsForm)
    })

  }
}
