import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalLicenseDialogComponent } from './professional-license-dialog.component';

describe('ProfessionalLicenseDialogComponent', () => {
  let component: ProfessionalLicenseDialogComponent;
  let fixture: ComponentFixture<ProfessionalLicenseDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfessionalLicenseDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalLicenseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
