import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProfessionalLicenseDialogComponent } from './professional-license-dialog/professional-license-dialog.component';
import swal from 'sweetalert2'
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelLicensesService } from 'src/app/core/service/register/personnel-licenses.service';

@Component({
  selector: 'app-save-profile-registration-form-professional-license',
  templateUrl: './save-profile-registration-form-professional-license.component.html',
  styleUrls: ['./save-profile-registration-form-professional-license.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormProfessionalLicenseComponent implements OnInit {
  rows : any  = []
  @Input() form : FormGroup
  constructor(public dialog :MatDialog,
              public appSV :AppService,
              public PersonnelLicensesSV :PersonnelLicensesService,
              ) { }

  ngOnInit(): void {
  }

  del_personnel_licenses_form(i,id){
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_licenses')).removeAt(i)
            this.PersonnelLicensesSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_licenses')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  openDialogLicense() { 
    const dialogRef = this.dialog.open(
      ProfessionalLicenseDialogComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
}
