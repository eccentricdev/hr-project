import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormCurrentPositionComponent } from './save-profile-registration-form-current-position.component';

describe('SaveProfileRegistrationFormCurrentPositionComponent', () => {
  let component: SaveProfileRegistrationFormCurrentPositionComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormCurrentPositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormCurrentPositionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormCurrentPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
