import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-save-profile-registration-form-current-position',
  templateUrl: './save-profile-registration-form-current-position.component.html',
  styleUrls: ['./save-profile-registration-form-current-position.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormCurrentPositionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
