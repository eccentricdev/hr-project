import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkChartComponent } from './work-chart.component';

describe('WorkChartComponent', () => {
  let component: WorkChartComponent;
  let fixture: ComponentFixture<WorkChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
