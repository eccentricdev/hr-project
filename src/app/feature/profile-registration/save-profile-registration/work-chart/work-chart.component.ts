import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';
import { WorkChartHeaderService } from 'src/app/core/service/organization/work-chart-header.service';

@Component({
  selector: 'app-work-chart',
  templateUrl: './work-chart.component.html',
  styleUrls: ['./work-chart.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkChartComponent implements OnInit,OnChanges,OnDestroy {
  @Input() form : FormGroup
  workChartHeader$ = new Observable()
  nodes = []
  unSubAll$ = new Subject<void>();
  work_chart : any = []
  workChartHeaderData : any = []
  constructor(
    public WorkChartHeaderSV: WorkChartHeaderService
  ) { 
    this.workChartHeader$ = this.WorkChartHeaderSV.getAll()
    this.WorkChartHeaderSV.getAll().pipe(
      tap((employee: any) => this.workChartHeaderData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.form.currentValue.value)
    
  }

  ngOnInit(): void {
    setTimeout(() => {
      console.log(this.form.value)  
      if(this.form.value.personnel_work_chart_header.work_chart_header_uid != null){
        this.selectWorkChart(this.form.value.personnel_work_chart_header.work_chart_header_uid)
      }
    }, 2000);
    
  }

  inputSearch(){
    console.log()
    this.WorkChartHeaderSV.query(`?work_chart_header_name_th=${this.form.get('work_chart_header_name')}`).pipe(
      tap((employee: any) => console.log(employee)),
      tap((employee: any) => this.workChartHeaderData = []),
      tap((employee: any) => this.workChartHeaderData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
  }
  selectWorkChart(id){
    console.log(id)
    this.form.get('personnel_work_chart_header.work_chart_header_uid').setValue(id)
    this.WorkChartHeaderSV.get(id).pipe(
      tap(x=>console.log(x)),
      tap((x:any)=>this.work_chart = x.work_chart),
      tap((x:any)=> console.log(this.work_chart) ),
      map(res => {
        let node = {
          ...res.work_chart,
          name: res.work_chart.position_name_th,
          childs: [  ...res.work_chart?.work_charts?.map(lv2 => {
            return {
              ...lv2,
              name: lv2.position_name_th || lv2.agency_name_th,
              childs: this.setupNode(lv2)
            }
          })]
        }
        console.log(node)
        return node
      }),
      tap(res => this.nodes = [ ...[res]]),

    ).subscribe()
  }

  setupNode(source: any){
    if(!source.work_charts) return []
    let node = source.work_charts.map(res => {
      return {
        ...res,
        name: res.position_name_th || res.agency_name_th,
        childs: this.setupNode(res)
      }
    })
    return node
  }
  ngOnDestroy(): void {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
