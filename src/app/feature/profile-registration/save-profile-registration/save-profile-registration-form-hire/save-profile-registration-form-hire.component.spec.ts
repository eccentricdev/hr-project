import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormHireComponent } from './save-profile-registration-form-hire.component';

describe('SaveProfileRegistrationFormHireComponent', () => {
  let component: SaveProfileRegistrationFormHireComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormHireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormHireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormHireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
