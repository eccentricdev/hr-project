import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ManpowersService } from 'src/app/core/service/allocate-people/manpowers.service';
import { AcademicPositionSubjectsService } from 'src/app/core/service/common/academic-position-subjects.service';
import { AcademicPositionsService } from 'src/app/core/service/common/academic-positions.service';
import { ContractTimesService } from 'src/app/core/service/common/contract-times.service';
import { ContractTypeService } from 'src/app/core/service/common/contract-type.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { ExpertisesService } from 'src/app/core/service/common/expertises.service';
import { PersonnelGroupsService } from 'src/app/core/service/common/personnel-groups.service';
import { PersonnelSubTypesService } from 'src/app/core/service/common/personnel-sub-types.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { PositionMovementTypesService } from 'src/app/core/service/common/position-movement-types.service';
import { SubjectCategoryTypesService } from 'src/app/core/service/common/subject-category-types.service';
import { SubjectGroupTypesService } from 'src/app/core/service/common/subject-group-types.service';
import { WorkFieldsService } from 'src/app/core/service/common/work-fields.service';
import { WorkGroupsService } from 'src/app/core/service/common/work-groups.service';
import { WorkShiftsService } from 'src/app/core/service/common/work-shifts.service';
import { WorkStatusService } from 'src/app/core/service/common/work-status.service';
import { WorkTypesService } from 'src/app/core/service/common/work-types.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';
import swal from 'sweetalert2'
import { PositionService } from 'src/app/core/service/organization/position.service';
import { PersonnelExecutivePositionsService } from 'src/app/core/service/register/personnel-executive-positions.service';
import { DialogExecutivePositionComponent } from '../dialog-executive-position/dialog-executive-position.component';
import { AppService } from 'src/app/core/service/app.service';
import { PositionTypesService } from 'src/app/core/service/common/position-types.service';

@Component({
  selector: 'app-save-profile-registration-form-hire',
  templateUrl: './save-profile-registration-form-hire.component.html',
  styleUrls: ['./save-profile-registration-form-hire.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormHireComponent implements OnInit {
  rows : any  = []
  @Input() form : FormGroup
  agencys$ = new Observable();
  position$ = new Observable();
  positionLevel$ = new Observable();
  organization$ = new Observable();
  organizationHeader$ = new Observable();
  academicPositions$ = new Observable();
  personnelGroups$ = new Observable();
  personnelTypes$ = new Observable();
  positionMovementTypes$ = new Observable();
  contractType$ = new Observable();
  contractTimes$ = new Observable();
  expertises$ = new Observable();
  subjectCategoryTypes$ = new Observable();
  subjectGroupTypes$ = new Observable();
  workShifts$ = new Observable();
  workTypes$ = new Observable();
  workStatus$ = new Observable();
  workFields$ = new Observable();
  workGroups$ = new Observable();
  educationLevels$ = new Observable();
  personnelSubTypes$ = new Observable();
  academicPositionSubjects$ = new Observable();
  positionTypes$ = new Observable();
  manpowers$ = new Observable();
  constructor(
    public AgencysSV : AgencysService,
    public appSV : AppService,
    public dialog : MatDialog,
    public PositionSV : PositionService,
    public PositionLevelsSV : PositionLevelsService,
    public OrganizationDataSV : OrganizationDataService,
    public OrganizationHeaderSV : OrganizationHeaderService,
    public AcademicPositionsSV : AcademicPositionsService,
    public PersonnelGroupsSV : PersonnelGroupsService,
    public PersonnelTypesSV : PersonnelTypesService,
    public PositionMovementTypesSV : PositionMovementTypesService,
    public ContractTypeSV : ContractTypeService,
    public ContractTimesSV : ContractTimesService,
    public ExpertisesSV : ExpertisesService,
    public SubjectCategoryTypesSV : SubjectCategoryTypesService,
    public SubjectGroupTypesSV : SubjectGroupTypesService,
    public WorkShiftsSV : WorkShiftsService,
    public WorkTypesSV : WorkTypesService,
    public WorkStatusSV : WorkStatusService,
    public WorkFieldsSV : WorkFieldsService,
    public WorkGroupsSV : WorkGroupsService,
    public EducationLevelsSV : EducationLevelsService,
    public PersonnelSubTypesSV : PersonnelSubTypesService,
    public AcademicPositionSubjectsSV : AcademicPositionSubjectsService,
    public PersonnelExecutivePositionsSV : PersonnelExecutivePositionsService,
    public PositionTypesSV : PositionTypesService,
    public manpowersSV: ManpowersService,
  ) { 
    this.agencys$ = this.AgencysSV.getAll()
    this.positionTypes$ = this.PositionTypesSV.getAll()
    this.position$ = this.PositionSV.getAll()
    this.positionLevel$ = this.PositionLevelsSV.getAll()
    this.organization$ = this.OrganizationDataSV.getAll().pipe(tap(x=>console.log(x)))
    this.organizationHeader$ = this.OrganizationHeaderSV.getAll().pipe(tap(x=>console.log(x)))
    this.academicPositions$ = this.AcademicPositionsSV.getAll()
    this.personnelGroups$ = this.PersonnelGroupsSV.getAll()
    this.personnelTypes$ = this.PersonnelTypesSV.getAll()
    this.positionMovementTypes$ = this.PositionMovementTypesSV.getAll()
    this.contractType$ = this.ContractTypeSV.getAll()
    this.contractTimes$ = this.ContractTimesSV.getAll()
    this.expertises$ = this.ExpertisesSV.getAll()
    this.subjectCategoryTypes$ = this.SubjectCategoryTypesSV.getAll()
    this.subjectGroupTypes$ = this.SubjectGroupTypesSV.getAll()
    this.workShifts$ = this.WorkShiftsSV.getAll()
    this.workTypes$ = this.WorkTypesSV.getAll()
    this.workStatus$ = this.WorkStatusSV.getAll()
    this.workFields$ = this.WorkFieldsSV.getAll()
    this.workGroups$ = this.WorkGroupsSV.getAll()
    this.educationLevels$ = this.EducationLevelsSV.getAll()
    this.personnelSubTypes$ = this.PersonnelSubTypesSV.getAll()
    this.academicPositionSubjects$ = this.AcademicPositionSubjectsSV.getAll()
    this.manpowers$ = this.manpowersSV.getAll()
  }

  ngOnInit(): void {
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }

    open_history_education() {
      const dialogRef = this.dialog.open(
        DialogExecutivePositionComponent, {
        width: '80%',
        disableClose: true,
        data: this.form 
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }


    delPersonnelExecutivePositions(i,id){
    
      if(id != null){
        {
          swal.getTitle()
          swal.fire({
            text: 'ยืนยันการลบรายการ',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก',
          }).then((result) => {
            console.log(result)
    
            if (result.value) {
              (<FormArray>this.form.get('personnel_executive_position')).removeAt(i)
              this.PersonnelExecutivePositionsSV.deleteData(id).pipe(
                catchError(err => {
                  // alert ตรงนี่
                  this.appSV.swaltAlertError('Error')
                  return throwError(err)
                })).subscribe((x: any) => {
                  console.log(x)
                
                  this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
                })
    
            }
          })
        }
      }else{
        {
          swal.getTitle()
          swal.fire({
            text: 'ยืนยันการลบรายการ',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก',
          }).then((result) => {
            console.log(result)
    
            if (result.value) {
              (<FormArray>this.form.get('personnel_executive_position')).removeAt(i)
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            }
          })
        }
      }
    }
}
