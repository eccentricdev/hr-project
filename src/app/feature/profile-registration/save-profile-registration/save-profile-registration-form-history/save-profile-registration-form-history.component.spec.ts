import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormHistoryComponent } from './save-profile-registration-form-history.component';

describe('SaveProfileRegistrationFormHistoryComponent', () => {
  let component: SaveProfileRegistrationFormHistoryComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
