import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogHistoryOutstandingWorksComponent } from './dialog-history-outstanding-works.component';

describe('DialogHistoryOutstandingWorksComponent', () => {
  let component: DialogHistoryOutstandingWorksComponent;
  let fixture: ComponentFixture<DialogHistoryOutstandingWorksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogHistoryOutstandingWorksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogHistoryOutstandingWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
