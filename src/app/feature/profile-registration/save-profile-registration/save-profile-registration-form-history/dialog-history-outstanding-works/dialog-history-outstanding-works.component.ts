import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AwardTypesService } from 'src/app/core/service/register/award-types.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-history-outstanding-works',
  templateUrl: './dialog-history-outstanding-works.component.html',
  styleUrls: ['./dialog-history-outstanding-works.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogHistoryOutstandingWorksComponent implements OnInit {
  awradForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_award_id: null,
    personnel_uid: null,
    award_name: '',
    award_by: '',
    award_type_id: null,
    receive_date: null,
  }
  awardTypes$ = new Observable()
  constructor(public dialogRef: MatDialogRef<DialogHistoryOutstandingWorksComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public EmployeeSV: EmployeeService,
    public AwardTypesSV: AwardTypesService,
  ) { 
    this.awardTypes$ = this.AwardTypesSV.getAll()
  }

  ngOnInit(): void {
  }
  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_awards')).insert(null, this.EmployeeSV.personnel_awards_form(this.awradForm));
  }
  save() {
    (<FormArray>this.data.get('personnel_awards')).insert(null, this.EmployeeSV.personnel_awards_form(this.awradForm));
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }

}
