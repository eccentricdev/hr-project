import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTalentComponent } from './dialog-talent.component';

describe('DialogTalentComponent', () => {
  let component: DialogTalentComponent;
  let fixture: ComponentFixture<DialogTalentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogTalentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTalentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
