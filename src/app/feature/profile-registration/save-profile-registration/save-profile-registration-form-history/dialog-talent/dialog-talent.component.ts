import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-talent',
  templateUrl: './dialog-talent.component.html',
  styleUrls: ['./dialog-talent.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogTalentComponent implements OnInit {

  skillForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_skill_id: null,
    personnel_uid: null,
    skill_name: '',
    remark: '',
  }
  constructor(public dialogRef: MatDialogRef<DialogTalentComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public EmployeeSV: EmployeeService,
  ) { }

  ngOnInit(): void {
  }
  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_skills')).insert(null, this.EmployeeSV.personnel_skills_form(this.skillForm));
  }
  save() {
    (<FormArray>this.data.get('personnel_skills')).insert(null, this.EmployeeSV.personnel_skills_form(this.skillForm));
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }

}
