import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { WorkTypesService } from 'src/app/core/service/register/work-types.service';

@Component({
  selector: 'app-dialog-work-history',
  templateUrl: './dialog-work-history.component.html',
  styleUrls: ['./dialog-work-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogWorkHistoryComponent implements OnInit {

  work_records = {
        is_deleted: true,
        create_by: null,
        create_datetime: null,
        update_by: null,
        update_datetime: null,
        delete_by: null,
        delete_datetime: null,
        working_record_id: null,
        personnel_uid: null,
        working_type_id: null,
        level: null,
        subject_name: '',
        place_name: '',
        position_name: '',
        detail: '',
        start_date: null,
        end_date: null,
        remark: '',
        row_order: null,
  }
  workTypes$ = new Observable()
  constructor(    public dialogRef: MatDialogRef<DialogWorkHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public EmployeeSV:EmployeeService,
    public WorkTypesSV:WorkTypesService,
    
    ) { 
      this.workTypes$ = this.WorkTypesSV.getAll()
    }

  ngOnInit(): void {
  }
  saveClose(){
    this.close();
    (<FormArray>this.data.get('working_records')).insert(null,this.EmployeeSV.working_records_form(this.work_records));
  }
  save(){
    (<FormArray>this.data.get('working_records')).insert(null,this.EmployeeSV.working_records_form(this.work_records));
  }

  close(){
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }

}
