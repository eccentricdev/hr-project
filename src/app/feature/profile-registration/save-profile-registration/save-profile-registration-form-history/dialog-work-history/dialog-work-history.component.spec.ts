import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWorkHistoryComponent } from './dialog-work-history.component';

describe('DialogWorkHistoryComponent', () => {
  let component: DialogWorkHistoryComponent;
  let fixture: ComponentFixture<DialogWorkHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogWorkHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWorkHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
