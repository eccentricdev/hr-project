import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AcademicWorkTypesService } from 'src/app/core/service/register/academic-work-types.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-academic-work',
  templateUrl: './dialog-academic-work.component.html',
  styleUrls: ['./dialog-academic-work.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogAcademicWorkComponent implements OnInit {

  personnel_academic_worksForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_academic_work_uid: null,
    personnel_uid: null,
    academic_work_type_id: null,
    academic_work_title_name: '',
    journal_name: '',
    reference_url: null,
    academic_work_description: null,
  }
  academicWorkTypes$ = new Observable()
  constructor(public dialogRef: MatDialogRef<DialogAcademicWorkComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public EmployeeSV: EmployeeService,
    public AcademicWorkTypesSV: AcademicWorkTypesService,
  ) { 
    this.academicWorkTypes$ = this.AcademicWorkTypesSV.getAll()
  }

  ngOnInit(): void {
    // if(this.data.formEdu != null){
    //   this.personnel_academic_worksForm = {
    //     status_id: this.data.formArray.status_id,
    //     created_by: this.data.formArray.created_by,
    //     created_datetime: this.data.formArray.created_datetime,
    //     updated_by: this.data.formArray.updated_by,
    //     updated_datetime: this.data.formArray.updated_datetime,
    //     personnel_academic_work_uid: this.data.formArray.personnel_academic_work_uid,
    //     personnel_uid: this.data.formArray.personnel_uid,
    //     academic_work_type_id: this.data.formArray.academic_work_type_id,
    //     academic_work_title_name: this.data.formArray.academic_work_title_name,
    //     journal_name: this.data.formArray.journal_name,
    //     reference_url: this.data.formArray.reference_url,
    //     academic_work_description: this.data.formArray.academic_work_description,
    // }
    // }
  }
  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_academic_works')).insert(null, this.EmployeeSV.personnel_academic_works_form(this.personnel_academic_worksForm));
  }
  save() {
    (<FormArray>this.data.get('personnel_academic_works')).insert(null, this.EmployeeSV.personnel_academic_works_form(this.personnel_academic_worksForm));
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }

}
