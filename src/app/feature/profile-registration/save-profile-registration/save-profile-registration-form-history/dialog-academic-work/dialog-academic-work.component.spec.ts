import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAcademicWorkComponent } from './dialog-academic-work.component';

describe('DialogAcademicWorkComponent', () => {
  let component: DialogAcademicWorkComponent;
  let fixture: ComponentFixture<DialogAcademicWorkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogAcademicWorkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAcademicWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
