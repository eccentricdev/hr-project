import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWorkBeforeStartComponent } from './dialog-work-before-start.component';

describe('DialogWorkBeforeStartComponent', () => {
  let component: DialogWorkBeforeStartComponent;
  let fixture: ComponentFixture<DialogWorkBeforeStartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogWorkBeforeStartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWorkBeforeStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
