import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-work-before-start',
  templateUrl: './dialog-work-before-start.component.html',
  styleUrls: ['./dialog-work-before-start.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogWorkBeforeStartComponent implements OnInit {

  work_history = {
    is_deleted: true,
    create_by: null,
    create_datetime: null,
    update_by: null,
    update_datetime: null,
    delete_by: null,
    delete_datetime: null,
    working_history_id: null,
    personnel_uid: null,
    work_place_name: '',
    job_position_name: '',
    job_description: '',
    final_salary_amount: 0,
    start_date: null,
    end_date: null,
    experience_income_amount: 0,
    profession_income_amount: 0,
    other_income_amount: 0,
    remark: '',
    row_order: null,
  }

  constructor(    public dialogRef: MatDialogRef<DialogWorkBeforeStartComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public EmployeeSV:EmployeeService,
    
    ) { }

  ngOnInit(): void {
  }
  saveClose(){
    this.close();
    (<FormArray>this.data.get('working_histories')).insert(null,this.EmployeeSV.working_histories_form(this.work_history));
  }
  save(){
    (<FormArray>this.data.get('working_histories')).insert(null,this.EmployeeSV.working_histories_form(this.work_history));
  }

  close(){
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }


}
