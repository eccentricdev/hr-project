import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Country, CountryService } from 'src/app/core/service/common/country.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { QualificationsService } from 'src/app/core/service/common/qualifications.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-history-education',
  templateUrl: './history-education.component.html',
  styleUrls: ['./history-education.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryEducationComponent implements OnInit,OnDestroy {
  educationsForm = {
      status_id: null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      personnel_education_uid: null,
      personnel_uid: null,
      education_level_id: null,
      quali_id: null,
      field_name: '',
      other_institute_name: '',
      country_id: null,
      country_name: '',
      start_year: null,
      finish_year: null,
      grade: '',
      edu_highest: true,
      edu_employ: true,
      start_date: null,
      finish_date: null,
      row_order: 0
  }
  country = []
  unSubAll$ = new Subject<void>();
  educationLevels$ = new Observable();
  qualifications$ = new Observable();
  constructor(    public dialogRef: MatDialogRef<HistoryEducationComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public EmployeeSV:EmployeeService,
    public CountrySV:CountryService,
    public EducationLevelsSV:EducationLevelsService,
    public QualificationsSV:QualificationsService,
    private cdRef: ChangeDetectorRef,
    
    ) {
      this.educationLevels$ = this.EducationLevelsSV.getAll()
      this.qualifications$ = this.QualificationsSV.getAll()
     }

  ngOnInit(): void {
    console.log(this.data)
    
    if(this.data.formEdu != null){
      this.educationsForm = {
        status_id:this.data.formEdu.status_id,
        created_by:this.data.formEdu.created_by,
        created_datetime:this.data.formEdu.created_datetime,
        updated_by:this.data.formEdu.updated_by,
        updated_datetime:this.data.formEdu.updated_datetime,
        personnel_education_uid:this.data.formEdu.personnel_education_uid,
        personnel_uid:this.data.formEdu.personnel_uid,
        education_level_id:this.data.formEdu.education_level_id,
        quali_id:this.data.formEdu.quali_id,
        field_name:this.data.formEdu.field_name,
        other_institute_name:this.data.formEdu.other_institute_name,
        country_id:this.data.formEdu.country_id,
        country_name:this.data.formEdu.country_name,
        start_year:this.data.formEdu.start_year,
        finish_year:this.data.formEdu.finish_year,
        grade:this.data.formEdu.grade,
        edu_highest:this.data.formEdu.edu_highest,
        edu_employ:this.data.formEdu.edu_employ,
        start_date:this.data.formEdu.start_date,
        finish_date:this.data.formEdu.finish_date,
        row_order:this.data.formEdu.row_order
    }
    }
    console.log(this.educationsForm)
  }
  select_nationality(coutnry: Country) {
    console.log(coutnry)
    this.educationsForm.country_id = coutnry.country_id
  }
  inputCountry(){

    setTimeout(() => {
      this.CountrySV.query(`?search=${this.educationsForm.country_name}`).pipe(
        tap((x:any)=> this.country = [...x])
      ).subscribe(x=>this.cdRef.detectChanges())
    }, 500);

  }
  save(){
    if(this.data.formEdu == null){
    (<FormArray>this.data.form.get('personnel_educations')).insert(null,this.EmployeeSV.educations_form(this.educationsForm));
    }else{
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('status_id').setValue(this.educationsForm.status_id); ;
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('created_by').setValue(this.educationsForm.created_by);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('created_datetime').setValue(this.educationsForm.created_datetime);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('updated_by').setValue(this.educationsForm.updated_by);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('updated_datetime').setValue(this.educationsForm.updated_datetime);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('personnel_education_uid').setValue(this.educationsForm.personnel_education_uid);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('personnel_uid').setValue(this.educationsForm.personnel_uid);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('education_level_id').setValue(this.educationsForm.education_level_id);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('quali_id').setValue(this.educationsForm.quali_id);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('field_name').setValue(this.educationsForm.field_name);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('other_institute_name').setValue(this.educationsForm.other_institute_name);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('country_id').setValue(this.educationsForm.country_id);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('country_name').setValue(this.educationsForm.country_name);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('start_year').setValue(this.educationsForm.start_year);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('finish_year').setValue(this.educationsForm.finish_year);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('grade').setValue(this.educationsForm.grade);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('edu_highest').setValue(this.educationsForm.edu_highest);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('edu_employ').setValue(this.educationsForm.edu_employ);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('start_date').setValue(this.educationsForm.start_date);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('finish_date').setValue(this.educationsForm.finish_date);
      (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('row_order').setValue(this.educationsForm.row_order);
    }
  }
  saveClose(){
    
    if(this.data.formEdu == null){
      (<FormArray>this.data.form.get('personnel_educations')).insert(null,this.EmployeeSV.educations_form(this.educationsForm));
      this.close();
      }else{
        console.log((<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index])  ;
        console.log(this.data.index)  ;
        // console.log((<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('status_id').value)
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('status_id').setValue(this.educationsForm.status_id);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('created_by').setValue(this.educationsForm.created_by);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('created_datetime').setValue(this.educationsForm.created_datetime);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('updated_by').setValue(this.educationsForm.updated_by);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('updated_datetime').setValue(this.educationsForm.updated_datetime);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('personnel_education_uid').setValue(this.educationsForm.personnel_education_uid);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('personnel_uid').setValue(this.educationsForm.personnel_uid);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('education_level_id').setValue(this.educationsForm.education_level_id);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('quali_id').setValue(this.educationsForm.quali_id);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('field_name').setValue(this.educationsForm.field_name);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('other_institute_name').setValue(this.educationsForm.other_institute_name);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('country_id').setValue(this.educationsForm.country_id);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('country_name').setValue(this.educationsForm.country_name);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('start_year').setValue(this.educationsForm.start_year);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('finish_year').setValue(this.educationsForm.finish_year);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('grade').setValue(this.educationsForm.grade);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('edu_highest').setValue(this.educationsForm.edu_highest);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('edu_employ').setValue(this.educationsForm.edu_employ);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('start_date').setValue(this.educationsForm.start_date);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('finish_date').setValue(this.educationsForm.finish_date);
        (<FormArray>this.data.form.get('personnel_educations')).controls[this.data.index].get('row_order').setValue(this.educationsForm.row_order);
        this.close();
      }
  }

  close(){
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    ngOnDestroy() {
      this.unSubAll$.next();
      this.unSubAll$.complete();
    }
}
