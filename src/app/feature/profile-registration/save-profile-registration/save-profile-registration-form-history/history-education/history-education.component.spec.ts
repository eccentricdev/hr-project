import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryEducationComponent } from './history-education.component';

describe('HistoryEducationComponent', () => {
  let component: HistoryEducationComponent;
  let fixture: ComponentFixture<HistoryEducationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryEducationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
