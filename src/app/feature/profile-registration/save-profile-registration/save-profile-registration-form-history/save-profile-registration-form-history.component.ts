import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DialogAcademicWorkComponent } from './dialog-academic-work/dialog-academic-work.component';
import { DialogHistoryOutstandingWorksComponent } from './dialog-history-outstanding-works/dialog-history-outstanding-works.component';
import { DialogPropertyHistoryComponent } from './dialog-property-history/dialog-property-history.component';
import { DialogTalentComponent } from './dialog-talent/dialog-talent.component';
import { DialogTrainingHistoryComponent } from './dialog-training-history/dialog-training-history.component';
import { DialogWorkBeforeStartComponent } from './dialog-work-before-start/dialog-work-before-start.component';
import { DialogWorkHistoryComponent } from './dialog-work-history/dialog-work-history.component';
import { HistoryEducationComponent } from './history-education/history-education.component';
import swal from 'sweetalert2'
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelEducationsService } from 'src/app/core/service/register/personnel-educations.service';
import { EducationLevelsService } from 'src/app/core/service/common/education-levels.service';
import { WorkingHistoriesService } from 'src/app/core/service/register/working-histories.service';
import { PersonnelSkillsService } from 'src/app/core/service/register/personnel-skills.service';
import { WorkingRecordsService } from 'src/app/core/service/register/working-records.service';
import { PersonnelAwardsService } from 'src/app/core/service/register/personnel-awards.service';
import { AssetHoldingsService } from 'src/app/core/service/register/asset-holdings.service';
import { PersonnelTrainingsService } from 'src/app/core/service/register/personnel-trainings.service';
import { PersonnelAcademicWorksService } from 'src/app/core/service/register/personnel-academic-works.service';
import { AcademicWorkTypesService } from 'src/app/core/service/register/academic-work-types.service';
import { AwardTypesService } from 'src/app/core/service/register/award-types.service';
import { WorkTypesService } from 'src/app/core/service/register/work-types.service';

@Component({
  selector: 'app-save-profile-registration-form-history',
  templateUrl: './save-profile-registration-form-history.component.html',
  styleUrls: ['./save-profile-registration-form-history.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormHistoryComponent implements OnInit {
  rows : any = []
  @Input() form : FormGroup
  educationLevels$ = new Observable();
  academicWorkTypes$ = new Observable()
  awardTypes$ = new Observable()
  workTypes$ = new Observable()
  constructor(public dialog :MatDialog,
              public appSV: AppService,
              public PersonnelEducationsSV: PersonnelEducationsService,
              public PersonnelSkillsSV: PersonnelSkillsService,
              public PersonnelAwardsSV: PersonnelAwardsService,
              public PersonnelTrainingsSV: PersonnelTrainingsService,
              public PersonnelAcademicWorksSV: PersonnelAcademicWorksService,
              public AssetHoldingsSV: AssetHoldingsService,
              public EducationLevelsSV: EducationLevelsService,
              public WorkingHistoriesSV: WorkingHistoriesService,
              public WorkingRecordsSV: WorkingRecordsService,
              public AcademicWorkTypesSV: AcademicWorkTypesService,
              public AwardTypesSV: AwardTypesService,
              public WorkTypesSV: WorkTypesService,
              ) { 
                this.educationLevels$ = this.EducationLevelsSV.getAll()
                this.academicWorkTypes$ = this.AcademicWorkTypesSV.getAll()
                this.awardTypes$ = this.AwardTypesSV.getAll()
                this.workTypes$ = this.WorkTypesSV.getAll()
              }

  ngOnInit(): void {
  }

  openHistoryOutstandingWord() {
    const dialogRef = this.dialog.open(
      DialogHistoryOutstandingWorksComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  openProperyHistory() {
    const dialogRef = this.dialog.open(
      DialogTrainingHistoryComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_talent() {
    const dialogRef = this.dialog.open(
      DialogTalentComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_training_history() {
    const dialogRef = this.dialog.open(
      DialogTrainingHistoryComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_work_before_start() {
    const dialogRef = this.dialog.open(
      DialogWorkBeforeStartComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_work_history() {
    const dialogRef = this.dialog.open(
      DialogWorkHistoryComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_train() {
    const dialogRef = this.dialog.open(
      DialogPropertyHistoryComponent, {
      width: '80%',
      disableClose: true,
      data: this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  del_education_form(i,id){
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_educations')).removeAt(i)
            this.PersonnelEducationsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_educations')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }

    
  }
  del_work_history_form(i,id){

    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('working_histories')).removeAt(i)
            this.WorkingHistoriesSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('working_histories')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
    
  }
  del_working_records_form(i,id){
    
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('working_records')).removeAt(i)
            this.WorkingRecordsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('working_records')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  del_skills_form(i,id){
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_skills')).removeAt(i)
            this.WorkingHistoriesSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_skills')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
    
  }
  del_personnel_awards_form(i,id){
    
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_awards')).removeAt(i)
            this.PersonnelAwardsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_awards')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  del_asset_holdings_form(i,id){
    
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('asset_holdings')).removeAt(i)
            this.AssetHoldingsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('asset_holdings')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  del_personnel_trainings_form(i,id){
    
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_trainings')).removeAt(i)
            this.PersonnelTrainingsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_trainings')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  del_personnel_academic_works_form(i,id){
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_academic_works')).removeAt(i)
            this.PersonnelAcademicWorksSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_academic_works')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  open_history_education() {
    const dialogRef = this.dialog.open(
      HistoryEducationComponent, {
      width: '80%',
      disableClose: true,
      data: { form:this.form,formEdu:null }
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_edit_history_education(form,index) {
    const dialogRef = this.dialog.open(
      HistoryEducationComponent, {
      width: '80%',
      disableClose: true,
      data: { form:this.form,formEdu:form , index : index }
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }

  open_academic_works() {
    const dialogRef = this.dialog.open(
      DialogAcademicWorkComponent, {
      width: '80%',
      disableClose: true,
      data:  this.form
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  open_edit_academic_works(form,index) {
    const dialogRef = this.dialog.open(
      DialogAcademicWorkComponent, {
      width: '80%',
      disableClose: true,
      data: { form:this.form,formArray:form , index : index }
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
}
