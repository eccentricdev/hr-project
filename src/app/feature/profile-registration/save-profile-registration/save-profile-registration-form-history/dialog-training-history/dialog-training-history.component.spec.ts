import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTrainingHistoryComponent } from './dialog-training-history.component';

describe('DialogTrainingHistoryComponent', () => {
  let component: DialogTrainingHistoryComponent;
  let fixture: ComponentFixture<DialogTrainingHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogTrainingHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTrainingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
