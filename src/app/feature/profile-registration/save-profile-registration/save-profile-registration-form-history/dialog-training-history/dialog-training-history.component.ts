import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-training-history',
  templateUrl: './dialog-training-history.component.html',
  styleUrls: ['./dialog-training-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogTrainingHistoryComponent implements OnInit {
  assetForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    asset_holding_id: null,
    personnel_uid: null,
    asset_id: null,
    asset_code: '',
    asset_name: '',
    receive_date: null,
    return_date: null,
    remark: ''
  }
  constructor(    public dialogRef: MatDialogRef<DialogTrainingHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public EmployeeSV:EmployeeService,
    ) { }

  ngOnInit(): void {
  }
  saveClose(){
    this.close();
    (<FormArray>this.data.get('asset_holdings')).insert(null,this.EmployeeSV.asset_holdings_form(this.assetForm));
  }
  save(){
    (<FormArray>this.data.get('asset_holdings')).insert(null,this.EmployeeSV.asset_holdings_form(this.assetForm));
  }

  close(){
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }

}
