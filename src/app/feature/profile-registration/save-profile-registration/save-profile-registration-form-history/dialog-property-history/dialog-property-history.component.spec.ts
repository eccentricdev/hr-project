import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPropertyHistoryComponent } from './dialog-property-history.component';

describe('DialogPropertyHistoryComponent', () => {
  let component: DialogPropertyHistoryComponent;
  let fixture: ComponentFixture<DialogPropertyHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogPropertyHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPropertyHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
