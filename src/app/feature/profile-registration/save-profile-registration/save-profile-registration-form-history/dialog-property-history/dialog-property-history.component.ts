import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AwardTypesService } from 'src/app/core/service/register/award-types.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-property-history',
  templateUrl: './dialog-property-history.component.html',
  styleUrls: ['./dialog-property-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogPropertyHistoryComponent implements OnInit {
  trainForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    training_id: null,
    personnel_uid: null,
    from_date: null,
    to_date: null,
    course_name: '',
    training_location: '',
    training_type_id: null,
  }
  awardTypes$ = new Observable()
  constructor(
    public dialogRef: MatDialogRef<DialogPropertyHistoryComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public EmployeeSV: EmployeeService,
    public AwardTypesSV: AwardTypesService,
    
  ) { 
    this.awardTypes$ = this.AwardTypesSV.getAll()
  }

  ngOnInit(): void {
  }
  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_trainings')).insert(null, this.EmployeeSV.personnel_trainings_form(this.trainForm));
  }
  save() {
    (<FormArray>this.data.get('personnel_trainings')).insert(null, this.EmployeeSV.personnel_trainings_form(this.trainForm));
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }

}
