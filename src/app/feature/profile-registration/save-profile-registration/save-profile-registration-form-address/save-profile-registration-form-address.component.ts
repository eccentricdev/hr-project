import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap, takeUntil, tap } from 'rxjs/operators';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { EmployeeAddressTypeService } from 'src/app/core/service/register/employee-address-type.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-save-profile-registration-form-address',
  templateUrl: './save-profile-registration-form-address.component.html',
  styleUrls: ['./save-profile-registration-form-address.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormAddressComponent implements OnInit,OnDestroy {
  rows: any = []
  EmployeeAddressTypeData: any = []
  subDistrict$ = new Observable()
  subDistricts = []
  unSubAll$ = new Subject<void>();
  @Input() form: FormGroup
  @Input() formAddress: FormGroup
  constructor(public dialog: MatDialog,
    public EmployeeAddressTypeSV: EmployeeAddressTypeService,
    public EmployeeSV: EmployeeService,
    public SubDistrictSV:SubDistrictService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.EmployeeAddressTypeSV.getAll().subscribe(x => {
      console.log(x)
      this.EmployeeAddressTypeData = x
    })
    // this.subDistrict$ = this.SubDistrictSV.getAllCommon()
  }

  ngOnInit(): void {

    this.formAddress.get('address.other_sub_district_name_th').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.SubDistrictSV.query(`?search=${text}`)),
      tap((subDistricts: any) => this.subDistricts = [...subDistricts]),
      tap((subDistricts: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.formAddress.get('address.other_district_name_th').disable()
    this.formAddress.get('address.other_province_name_th').disable()
  }

  selectSubDistrict(data){
    console.log(data)
    this.formAddress.get('address.sub_district_id').setValue(data.sub_district_id)
    this.formAddress.get('address.other_district_name_th').setValue(data.district_name_th)
    this.formAddress.get('address.other_province_name_th').setValue(data.province_name_th)
  }
  chengeAddressRegistered(value) {
    console.log(value)
    if (value.checked) {
      //    let formAddress = this.form.get('personnel_addresses') as FormArray
      //  formAddress.removeAt(1)
      this.form.get('personnel_addresses').value.forEach((element, i) => {
        if (i == 0) {
         // (<FormArray>this.form.get('personnel_addresses')).insert(1, this.EmployeeSV.personnel_addresses_form(element));
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address_type_id').setValue(2);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.address_name').setValue(element.address.address_name);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.house_no').setValue(element.address.house_no);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.village_no').setValue(element.address.village_no);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.building_name').setValue(element.address.building_name);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.room_no').setValue(element.address.room_no);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.floor_no').setValue(element.address.floor_no);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.village_name').setValue(element.address.village_name);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.alley_name').setValue(element.address.alley_name);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.street_name').setValue(element.address.street_name);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.sub_district_id').setValue(element.address.sub_district_id);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_sub_district_name_th').setValue(element.address.other_sub_district_name_th);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_sub_district_name_en').setValue(element.address.other_sub_district_name_en);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_district_name_th').setValue(element.address.other_district_name_th);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_district_name_en').setValue(element.address.other_district_name_en);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_province_name_th').setValue(element.address.other_province_name_th);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_province_name_en').setValue(element.address.other_province_name_en);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.postal_code').setValue(element.address.postal_code);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.country_id').setValue(element.address.country_id);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.telephone_no').setValue(element.address.telephone_no);
        }
      });
    } else {
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.address_name').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.house_no').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.village_no').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.building_name').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.room_no').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.floor_no').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.village_name').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.alley_name').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.street_name').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.sub_district_id').setValue(null);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_sub_district_name_th').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_sub_district_name_en').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_district_name_th').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_district_name_en').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_province_name_th').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.other_province_name_en').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.postal_code').setValue('');
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.country_id').setValue(null);
          (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address.telephone_no').setValue('');
    }
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }

  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
