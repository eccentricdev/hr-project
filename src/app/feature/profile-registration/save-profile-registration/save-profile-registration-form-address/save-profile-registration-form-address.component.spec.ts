import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormAddressComponent } from './save-profile-registration-form-address.component';

describe('SaveProfileRegistrationFormAddressComponent', () => {
  let component: SaveProfileRegistrationFormAddressComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormAddressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
