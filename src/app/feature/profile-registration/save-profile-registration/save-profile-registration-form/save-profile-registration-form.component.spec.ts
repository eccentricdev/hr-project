import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormComponent } from './save-profile-registration-form.component';

describe('SaveProfileRegistrationFormComponent', () => {
  let component: SaveProfileRegistrationFormComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
