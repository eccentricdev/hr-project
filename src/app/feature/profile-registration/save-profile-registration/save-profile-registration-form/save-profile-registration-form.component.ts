import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-save-profile-registration-form',
  templateUrl: './save-profile-registration-form.component.html',
  styleUrls: ['./save-profile-registration-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormComponent extends BaseForm implements OnInit {

  constructor(public router:Router,
    public fb: FormBuilder,  
    public activeRoute: ActivatedRoute,
    public EmployeeSV: EmployeeService,
    public appSV: AppService,
    ) { 
    super(fb,activeRoute)
  }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.EmployeeSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(res => console.log(res)),
          tap((res:any) => {
            if(res.personnel_contacts.length == 0){
              (<FormArray>this.form.get('personnel_contacts')).insert(0,this.EmployeeSV.personnel_contacts_form());
              (<FormArray>this.form.get('personnel_contacts')).controls[0].get('contact_type_id').setValue(2);
              (<FormArray>this.form.get('personnel_contacts')).insert(1,this.EmployeeSV.personnel_contacts_form());
              (<FormArray>this.form.get('personnel_contacts')).controls[1].get('contact_type_id').setValue(3);
            }
            if(res.personnel_hires.length == 0){
              (<FormArray>this.form.get('personnel_hires')).insert(0,this.EmployeeSV.personnel_hires_form());
            }
          }),
        ).subscribe()
      break;

      case 'add':
        
        (<FormArray>this.form.get('personnel_addresses')).insert(0,this.EmployeeSV.personnel_addresses_form());
        (<FormArray>this.form.get('personnel_addresses')).controls[0].get('address_type_id').setValue(1);
        (<FormArray>this.form.get('personnel_addresses')).insert(1,this.EmployeeSV.personnel_addresses_form());
        (<FormArray>this.form.get('personnel_addresses')).controls[1].get('address_type_id').setValue(2);

        (<FormArray>this.form.get('personnel_contacts')).insert(0,this.EmployeeSV.personnel_contacts_form());
        (<FormArray>this.form.get('personnel_contacts')).controls[0].get('contact_type_id').setValue(2);
        (<FormArray>this.form.get('personnel_contacts')).insert(1,this.EmployeeSV.personnel_contacts_form());
        (<FormArray>this.form.get('personnel_contacts')).controls[1].get('contact_type_id').setValue(3);

        (<FormArray>this.form.get('personnel_hires')).insert(0,this.EmployeeSV.personnel_hires_form());


      break;
    
      default:
        break;
    }
  }
  clickAddFormAddress(){
    console.log('1')
  }

  setUpform(res){
    console.log(res)
    console.log(res.personnel_work_chart_header)
    // patch value 
    this.form.patchValue(
      res
    //   {
    //   is_deleted:res.is_deleted,
    //   create_by:res.create_by,
    //   create_datetime:res.create_datetime,
    //   update_by:res.update_by,
    //   update_datetime:res.update_datetime,
    //   delete_by:res.delete_by,
    //   delete_datetime:res.delete_datetime,
    //   personnel_uid:res.personnel_uid,
    //   personnel_no:res.personnel_no,
    //   prefix_id:res.prefix_id,
    //   other_prefix_name:res.other_prefix_name,
    //   first_name_th:res.first_name_th,
    //   middle_name_th:res.middle_name_th,
    //   last_name_th:res.last_name_th,
    //   first_name_en:res.first_name_en,
    //   middle_name_en:res.middle_name_en,
    //   last_name_en:res.last_name_en,
    //   birth_date:res.birth_date,
    //   gender_id:res.gender_id,
    //   country_of_birth_id:res.country_of_birth_id,
    //   province_of_birth_id:res.province_of_birth_id,
    //   nationality_id:res.nationality_id,
    //   nationality_name:res.nationality_name,
    //   ethnicity_id:res.ethnicity_id,
    //   ethnicity_name:res.ethnicity_name,
    //   religion_id:res.religion_id,
    //   blood_group_id:res.blood_group_id,
    //   retire_year:res.retire_year,
    //   current_decoration_id:res.current_decoration_id,
    //   deformation_id:res.deformation_id,
    //   citizen_id:res.citizen_id,
    //   citizen_card_issue_by:res.citizen_card_issue_by,
    //   citizen_card_issue_date:res.citizen_card_issue_date,
    //   citizen_card_expiry_date:res.citizen_card_expiry_date,
    //   tax_id:res.tax_id,
    //   social_security_card_id:res.social_security_card_id,
    //   passport_no:res.passport_no,
    //   password_name:res.password_name,
    //   passport_issue_date:res.passport_issue_date,
    //   passport_expiry_date:res.passport_expiry_date,
    //   visa_no:res.visa_no,
    //   visa_name:res.visa_name,
    //   visa_issue_date:res.visa_issue_date,
    //   visa_expiry_date:res.visa_expiry_date,
    //   work_permit_no:res.work_permit_no,
    //   work_permit_name:res.work_permit_name,
    //   work_permit_issue_date:res.work_permit_issue_date,
    //   work_permit_expiry_date:res.work_permit_expiry_date,
    //   uid_no:res.uid_no,
    //   personnel_status_id:res.personnel_status_id,
    //   current_agency_uid:res.current_agency_uid,
    //   current_manpower_uid:res.current_manpower_uid,
    //   primary_position_uid:res.primary_position_uid,
    //   marital_status_id:res.marital_status_id,
    //   military_status_id:res.military_status_id,
    //   bank_type_id:res.bank_type_id,
    //   bank_account_no:res.bank_account_no,
    // }
    )

    if(res.personnel_work_chart_header){
      this.form.get('personnel_work_chart_header').patchValue({
        status_id:res.personnel_work_chart_header?.status_id,
        created_by:res.personnel_work_chart_header?.created_by,
        created_datetime:res.personnel_work_chart_header?.created_datetime,
        updated_by:res.personnel_work_chart_header?.updated_by,
        updated_datetime:res.personnel_work_chart_header?.updated_datetime,
        personnel_work_chart_header_uid:res.personnel_work_chart_header?.personnel_work_chart_header_uid,
        personnel_uid:res.personnel_uid,
        work_chart_header_uid:res.personnel_work_chart_header?.work_chart_header_uid,
      })
    }
    if (res.personnel_contacts) {
      res.personnel_contacts.forEach((personnel_contacts) => {
        let itemsArray = this.form.get('personnel_contacts') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_contacts_form(personnel_contacts))
      })
    }
    if (res.personnel_addresses) {
      res.personnel_addresses.forEach((personnel_addresses) => {
        let itemsArray = this.form.get('personnel_addresses') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_addresses_form(personnel_addresses))
      })
    }
    if (res.personnel_families) {
      res.personnel_families.forEach((personnel_families) => {
        let itemsArray = this.form.get('personnel_families') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_families_form(personnel_families))
      })
    }
    if (res.personnel_educations) {
      res.personnel_educations.forEach((personnel_educations) => {
        let itemsArray = this.form.get('personnel_educations') as FormArray
        itemsArray.push(this.EmployeeSV.educations_form(personnel_educations))
      })
    }
    if (res.working_histories) {
      res.working_histories.forEach((working_histories) => {
        let itemsArray = this.form.get('working_histories') as FormArray
        itemsArray.push(this.EmployeeSV.working_histories_form(working_histories))
      })
    }
    if (res.working_records) {
      res.working_records.forEach((working_records) => {
        let itemsArray = this.form.get('working_records') as FormArray
        itemsArray.push(this.EmployeeSV.working_records_form(working_records))
      })
    }
    if (res.name_change_histories) {
      res.name_change_histories.forEach((name_change_histories) => {
        let itemsArray = this.form.get('name_change_histories') as FormArray
        itemsArray.push(this.EmployeeSV.name_change_histories_form(name_change_histories))
      })
    }
    if (res.asset_holdings) {
      res.asset_holdings.forEach((asset_holdings) => {
        let itemsArray = this.form.get('asset_holdings') as FormArray
        itemsArray.push(this.EmployeeSV.asset_holdings_form(asset_holdings))
      })
    }
    if (res.personnel_awards) {
      res.personnel_awards.forEach((personnel_awards) => {
        let itemsArray = this.form.get('personnel_awards') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_awards_form(personnel_awards))
      })
    }
    if (res.personnel_skills) {
      res.personnel_skills.forEach((personnel_skills) => {
        let itemsArray = this.form.get('personnel_skills') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_skills_form(personnel_skills))
      })
    }
    if (res.personnel_trainings) {
      res.personnel_trainings.forEach((personnel_trainings) => {
        let itemsArray = this.form.get('personnel_trainings') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_trainings_form(personnel_trainings))
      })
    }
    if (res.personnel_licenses) {
      res.personnel_licenses.forEach((personnel_licenses) => {
        let itemsArray = this.form.get('personnel_licenses') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_licenses_form(personnel_licenses))
      })
    }
    if (res.personnel_hires) {
      console.log(res.personnel_hires)
      res.personnel_hires.forEach((personnel_hires) => {
        let itemsArray = this.form.get('personnel_hires') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_hires_form(personnel_hires))
      })
    }
    if (res.personnel_academic_works) {
      res.personnel_academic_works.forEach((personnel_academic_works) => {
        let itemsArray = this.form.get('personnel_academic_works') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_academic_works_form(personnel_academic_works))
      })
    }
    if (res.personnel_position_history) {
      res.personnel_position_history.forEach((personnel_position_history) => {
        let itemsArray = this.form.get('personnel_position_history') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_position_history_form(personnel_position_history))
      })
    }
    if (res.personnel_executive_position) {
      res.personnel_executive_position.forEach((personnel_executive_position) => {
        let itemsArray = this.form.get('personnel_executive_position') as FormArray
        itemsArray.push(this.EmployeeSV.personnel_executive_position_form(personnel_executive_position))
      })
    }
  }
  close(){
    this.router.navigate(['app/pr_save_profile_registration'])
  }
  save() {


    switch (this.state) {
      case 'edit':
        this.EmployeeSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
              this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            // this.SubjectComponent.ngOnInit()
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_save_profile_registration'])
          })
      break;

      case 'add':
        this.EmployeeSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
              this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            // this.SubjectComponent.ngOnInit()
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_save_profile_registration'])
          })
      break;
      default:
        break;
    }

   



    

  }


  createForm() {
    return this.baseFormBuilder.group({
      is_deleted: [true],
      create_by: [null],
      create_datetime: [null],
      update_by: [null],
      update_datetime: [null],
      delete_by: [null],
      delete_datetime: [null],
      personnel_uid: [null],
      personnel_no: [''],
      prefix_id: [null],
      other_prefix_name: [''],
      first_name_th: [''],
      middle_name_th: [''],
      last_name_th: [''],
      first_name_en: [''],
      middle_name_en: [''],
      last_name_en: [''],
      birth_date: [null],
      gender_id: [null],
      country_of_birth_id: [null],
      country_of_birth_name: [''],
      province_of_birth_id: [null],
      province_of_birth_name: [null],
      nationality_id: [null],
      nationality_name: [''],
      ethnicity_id: [null],
      ethnicity_name: [null],
      religion_id: [null],
      blood_group_id: [null],
      retire_year: [null],
      current_decoration_id: [null],
      deformation_id: [null],
      citizen_id: [null],
      citizen_card_issue_by: [null],
      citizen_card_issue_date: [null],
      citizen_card_expiry_date: [null],
      tax_id: [null],
      social_security_card_id: [null],
      passport_no: [''],
      passport_name: [''],
      passport_issue_date: [null],
      passport_expiry_date: [null],
      visa_no: [null],
      visa_name: [null],
      visa_issue_date: [null],
      visa_expiry_date: [null],
      work_permit_no: [''],
      work_permit_name: [''],
      work_permit_issue_date: [null],
      work_permit_expiry_date: [null],
      uid_no: [''],
      personnel_status_id: [null],
      current_agency_uid: [null],
      current_manpower_uid: [null],
      primary_position_uid: [null],
      marital_status_id: [null],
      military_status_id: [null],
      bank_type_id: [null],
      bank_account_no: [''],
      photo_url: [''],
      weight: [0],
      height: [0],
      email: [''],
      mobile_no: [''],
      has_passport_book: [false],
      is_foreign: [false],
      personnel_contacts: this.baseFormBuilder.array([]),
      personnel_addresses: this.baseFormBuilder.array([]),
      personnel_families: this.baseFormBuilder.array([]),
      personnel_educations: this.baseFormBuilder.array([]),
      working_histories: this.baseFormBuilder.array([]),
      working_records: this.baseFormBuilder.array([]),
      name_change_histories: this.baseFormBuilder.array([]),
      asset_holdings: this.baseFormBuilder.array([]),
      personnel_awards: this.baseFormBuilder.array([]),
      personnel_skills: this.baseFormBuilder.array([]),
      personnel_trainings: this.baseFormBuilder.array([]),
      personnel_licenses: this.baseFormBuilder.array([]),
      personnel_hires: this.baseFormBuilder.array([]),
      personnel_academic_works: this.baseFormBuilder.array([]),
      personnel_position_history: this.baseFormBuilder.array([]),
      personnel_executive_position: this.baseFormBuilder.array([]),
      personnel_work_chart_header : this.baseFormBuilder.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        personnel_work_chart_header_uid: [null],
        personnel_uid: [null],
        work_chart_header_uid: [null],
        work_chart_header_name: [null],
      }),
    })
  }
}
