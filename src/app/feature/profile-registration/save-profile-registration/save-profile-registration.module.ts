import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveProfileRegistrationListComponent } from './save-profile-registration-list/save-profile-registration-list.component';
import { SaveProfileRegistrationFormGeneralComponent } from './save-profile-registration-form-general/save-profile-registration-form-general.component';
import { SaveProfileRegistrationFormPersonalStatusComponent } from './save-profile-registration-form-personal-status/save-profile-registration-form-personal-status.component';
import { SaveProfileRegistrationFormAddressComponent } from './save-profile-registration-form-address/save-profile-registration-form-address.component';
import { SaveProfileRegistrationFormHistoryComponent } from './save-profile-registration-form-history/save-profile-registration-form-history.component';
import { SaveProfileRegistrationFormHireComponent } from './save-profile-registration-form-hire/save-profile-registration-form-hire.component';
import { SaveProfileRegistrationFormCurrentPositionComponent } from './save-profile-registration-form-current-position/save-profile-registration-form-current-position.component';
import { SaveProfileRegistrationFormProfessionalLicenseComponent } from './save-profile-registration-form-professional-license/save-profile-registration-form-professional-license.component';
import { SaveProfileRegistrationFormComponent } from './save-profile-registration-form/save-profile-registration-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { DialogFamilyComponent } from './save-profile-registration-form-personal-status/dialog-family/dialog-family.component';
import { HistoryEducationComponent } from './save-profile-registration-form-history/history-education/history-education.component';
import { DialogWorkBeforeStartComponent } from './save-profile-registration-form-history/dialog-work-before-start/dialog-work-before-start.component';
import { DialogTalentComponent } from './save-profile-registration-form-history/dialog-talent/dialog-talent.component';
import { DialogWorkHistoryComponent } from './save-profile-registration-form-history/dialog-work-history/dialog-work-history.component';
import { DialogHistoryOutstandingWorksComponent } from './save-profile-registration-form-history/dialog-history-outstanding-works/dialog-history-outstanding-works.component';
import { DialogPropertyHistoryComponent } from './save-profile-registration-form-history/dialog-property-history/dialog-property-history.component';
import { DialogTrainingHistoryComponent } from './save-profile-registration-form-history/dialog-training-history/dialog-training-history.component';
import { ProfessionalLicenseDialogComponent } from './save-profile-registration-form-professional-license/professional-license-dialog/professional-license-dialog.component';
import { DialogNameChangrHistoryComponent } from './save-profile-registration-form-personal-status/dialog-name-changr-history/dialog-name-changr-history.component';
import { ContactComponent } from './save-profile-registration-form-general/contact/contact.component';
import { DialogAcademicWorkComponent } from './save-profile-registration-form-history/dialog-academic-work/dialog-academic-work.component';
import { DialogExecutivePositionComponent } from './dialog-executive-position/dialog-executive-position.component';
import { WorkChartComponent } from './work-chart/work-chart.component';

const routes:Routes = [
  {
    path:'',
    component:SaveProfileRegistrationListComponent
  },
  {
    path:'add',
    component:SaveProfileRegistrationFormComponent
  },
  {
    path:'edit/:id',
    component:SaveProfileRegistrationFormComponent
  },
  
]

@NgModule({
  declarations: [
    SaveProfileRegistrationListComponent, 
    SaveProfileRegistrationFormGeneralComponent, 
    SaveProfileRegistrationFormPersonalStatusComponent, 
    SaveProfileRegistrationFormAddressComponent, 
    SaveProfileRegistrationFormHistoryComponent, 
    SaveProfileRegistrationFormHireComponent, 
    SaveProfileRegistrationFormCurrentPositionComponent, 
    SaveProfileRegistrationFormProfessionalLicenseComponent,
    SaveProfileRegistrationFormComponent,
    DialogFamilyComponent,
    HistoryEducationComponent,
    DialogWorkBeforeStartComponent,
    DialogTalentComponent,
    DialogWorkHistoryComponent,
    DialogHistoryOutstandingWorksComponent,
    DialogPropertyHistoryComponent,
    DialogTrainingHistoryComponent,
    ProfessionalLicenseDialogComponent,
    DialogNameChangrHistoryComponent,
    ContactComponent,
    DialogAcademicWorkComponent,
    DialogExecutivePositionComponent,
    WorkChartComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  entryComponents: [DialogFamilyComponent,
    HistoryEducationComponent,
    DialogWorkBeforeStartComponent,
    DialogTalentComponent,
    DialogWorkHistoryComponent,
    DialogHistoryOutstandingWorksComponent,
    DialogPropertyHistoryComponent,
    DialogTrainingHistoryComponent,
    ProfessionalLicenseDialogComponent],
})
export class SaveProfileRegistrationModule { }
