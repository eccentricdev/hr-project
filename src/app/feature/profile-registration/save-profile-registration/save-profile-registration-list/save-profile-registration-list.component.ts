import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { WorkShiftsService } from 'src/app/core/service/common/work-shifts.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-profile-registration-list',
  templateUrl: './save-profile-registration-list.component.html',
  styleUrls: ['./save-profile-registration-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationListComponent extends BaseList implements OnInit {
  rows:any = []
  personnel$ = new Observable()
  agencys$ = new Observable()
  req = {
    personnel_no : '',
    display_name_th : '',
    citizen_id : ''
  }
  constructor(
    public router : Router,
    public EmployeeSV : EmployeeService,
    public AgencysSV : AgencysService,
    public WorkShiftsService : WorkShiftsService,
    public appSV : AppService,
  ) { super()
   this.personnel$ = this.EmployeeSV.getAll().pipe(
    map(x=> this.updateMatTable(x))
  )
   this.agencys$ = this.AgencysSV.getAll()
    this.WorkShiftsService.getAll().subscribe(x=>console.log(x))
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.personnel$  = this.EmployeeSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      personnel_no : '',
      display_name_th : '',
      citizen_id : '',
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_save_profile_registration/add'])
  }
  editPage(id){
    console.log(id)
    this.router.navigate(['app/pr_save_profile_registration/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.EmployeeSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.personnel$ = this.EmployeeSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
