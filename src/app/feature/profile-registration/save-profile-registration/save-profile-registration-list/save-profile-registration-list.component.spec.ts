import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationListComponent } from './save-profile-registration-list.component';

describe('SaveProfileRegistrationListComponent', () => {
  let component: SaveProfileRegistrationListComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
