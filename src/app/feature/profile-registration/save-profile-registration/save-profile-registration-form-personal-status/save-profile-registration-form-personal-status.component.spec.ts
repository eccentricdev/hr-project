import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormPersonalStatusComponent } from './save-profile-registration-form-personal-status.component';

describe('SaveProfileRegistrationFormPersonalStatusComponent', () => {
  let component: SaveProfileRegistrationFormPersonalStatusComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormPersonalStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormPersonalStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormPersonalStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
