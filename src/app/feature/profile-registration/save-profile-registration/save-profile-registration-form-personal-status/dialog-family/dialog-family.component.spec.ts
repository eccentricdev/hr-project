import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogFamilyComponent } from './dialog-family.component';

describe('DialogFamilyComponent', () => {
  let component: DialogFamilyComponent;
  let fixture: ComponentFixture<DialogFamilyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogFamilyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
