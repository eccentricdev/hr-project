import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Country, CountryService } from 'src/app/core/service/common/country.service';
import { LivingStatussService } from 'src/app/core/service/common/living-statuss.service';
import { MaritalStatusService } from 'src/app/core/service/common/marital-status.service';
import { MilitaryStatusService } from 'src/app/core/service/common/military-status.service';
import { prefix, PrefixService } from 'src/app/core/service/common/prefix.service';
import { RelationshipsService } from 'src/app/core/service/common/relationships.service';
import { ReligionService } from 'src/app/core/service/common/religion.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-dialog-family',
  templateUrl: './dialog-family.component.html',
  styleUrls: ['./dialog-family.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogFamilyComponent implements OnInit,OnDestroy {
  rows : any = []
  unSubAll$ = new Subject<void>();
  country : any = []
  formFamily = {
    personnel_uid: null,
    family_id: null,
    relationship_id: null,
      family : {
        is_deleted:true,
        create_by:null,
        create_datetime:null,
        update_by:null,
        update_datetime:null,
        delete_by:null,
        delete_datetime:null,
        family_id:null,
        personnel_uid:null,
        card_no:'',
        prefix_id:null,
        other_prefix_name:'',
        first_name_th:'',
        middle_name_th:'',
        last_name_th:'',
        first_name_en:'',
        nationality_name:'',
        middle_name_en:'',
        last_name_en:'',
        nationality_id:null,
        religion_id:null,
        occupation_id:null,
        living_status_id:null,
        marital_status_id:null,
        remark:'',
        maiden_name:'',
        row_order:null,
        no_of_childs:0,
        is_child_study:true,
       
      }
  }
  prefixData$ = new Observable<prefix[]>()
  relationships$ = new Observable()
  livingStatuss$ = new Observable()
  maritalStatus$ = new Observable()
  religion$ = new Observable()
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  constructor(
    public dialogRef: MatDialogRef<DialogFamilyComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public dialog : MatDialog,
    public PrefixSV : PrefixService,
    public EmployeeSV : EmployeeService,
    public RelationshipsSV : RelationshipsService,
    public LivingStatussSV : LivingStatussService,
    public MaritalStatusSV : MaritalStatusService,
    public CountrySV : CountryService,
    public ReligionSV : ReligionService,
    private cdRef: ChangeDetectorRef,
  ) { 
    this.prefixData$ =  this.PrefixSV.getAll<prefix>()
    this.relationships$ = this.RelationshipsSV.getAll()
    this.livingStatuss$ = this.LivingStatussSV.getAll()
    this.maritalStatus$ = this.MaritalStatusSV.getAll()
    this.religion$ = this.ReligionSV.getAll()
  }

  ngOnInit(): void {
    console.log(this.data)
  }
  select_nationality(coutnry: Country) {
    console.log(coutnry)
    this.formFamily.family.nationality_id = coutnry.country_id
  }
  inputCountry(){

    setTimeout(() => {
      this.CountrySV.query(`?search=${this.formFamily.family.nationality_name}`).pipe(
        tap((x:any)=> this.country = [...x])
      ).subscribe(x=>this.cdRef.detectChanges())
    }, 500);


      // debounceTime(500),
      // switchMap(text => this.CountrySV.query(`?search=${text}`)),
      // tap((Province: any) => this.Province = [...Province]),
      // tap((Province: any) => this.cdRef.detectChanges()),

  }
  saveClose(){
    this.close();
    (<FormArray>this.data.get('personnel_families')).insert(0,this.EmployeeSV.personnel_families_form(this.formFamily));
  }
  save(){
    (<FormArray>this.data.get('personnel_families')).insert(0,this.EmployeeSV.personnel_families_form(this.formFamily));
    console.log(this.data)
  }

  close(){
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    dateChangeBirth(date: moment.Moment) {
      // this.form.get('date_of_birth').setValue(moment(date).add(7, 'hours').toDate())
      let todayDate = moment()
      let calculateAge = todayDate.diff(date, 'years')
      this.age.nativeElement.value = calculateAge.toString()
      console.log(calculateAge)
    }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}

