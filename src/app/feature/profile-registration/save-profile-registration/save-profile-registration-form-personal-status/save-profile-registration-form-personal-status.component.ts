import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, throwError } from 'rxjs';
import { MaritalStatusService } from 'src/app/core/service/common/marital-status.service';
import { MilitaryStatusService } from 'src/app/core/service/common/military-status.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { DialogFamilyComponent } from './dialog-family/dialog-family.component';
import swal from 'sweetalert2'
import { DialogNameChangrHistoryComponent } from './dialog-name-changr-history/dialog-name-changr-history.component';
import { NameChangeHistoriesService } from 'src/app/core/service/register/name-change-histories.service';
import { catchError } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelFamiliesService } from 'src/app/core/service/register/personnel-families.service';
import { BankTypesService } from 'src/app/core/service/register/bank-types.service';
import { RelationshipsService } from 'src/app/core/service/common/relationships.service';
import { LivingStatussService } from 'src/app/core/service/common/living-statuss.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';

@Component({
  selector: 'app-save-profile-registration-form-personal-status',
  templateUrl: './save-profile-registration-form-personal-status.component.html',
  styleUrls: ['./save-profile-registration-form-personal-status.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormPersonalStatusComponent implements OnInit {
  rows : any = []
  rowsName : any = []
  militaryStatus$ = new Observable()
  maritalStatus$ = new Observable()
  bankTypes$ = new Observable()
  relationships$ = new Observable()
  livingStatuss$ = new Observable()
  prefix$ = new Observable()
  @Input() form : FormGroup
  constructor(public dialog : MatDialog,
              public EmployeeSV:EmployeeService,        
              public MilitaryStatusSV:MilitaryStatusService,        
              public MaritalStatusSV:MaritalStatusService,        
              public NameChangeHistoriesSV:NameChangeHistoriesService,        
              public PersonnelFamiliesSV:PersonnelFamiliesService,        
              public BankTypesSV:BankTypesService,        
              public RelationshipsSV:RelationshipsService,        
              public LivingStatussSV:LivingStatussService,        
              public PrefixSV:PrefixService,        
              public appSV:AppService,        
    ) { 
      this.militaryStatus$ = this.MilitaryStatusSV.getAll()
      this.maritalStatus$ = this.MaritalStatusSV.getAll()
      this.bankTypes$ = this.BankTypesSV.getAll()
      this.relationships$ = this.RelationshipsSV.getAll()
      this.livingStatuss$ = this.LivingStatussSV.getAll()
      this.prefix$ = this.PrefixSV.getAll()
    }

  ngOnInit(): void {
  }

  delFamily(i,id){
    (<FormArray>this.form.get('personnel_families')).removeAt(i);
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_families')).removeAt(i)
            this.PersonnelFamiliesSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_families')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  delNameChange(i,id){
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('name_change_histories')).removeAt(i)
            this.NameChangeHistoriesSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('name_change_histories')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }

    openFamilyDialog() {
      const dialogRef = this.dialog.open( 
        DialogFamilyComponent, {
        width: '80%',
        disableClose: true,
        data: this.form  
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }
    openNameChangeDialog() {
      const dialogRef = this.dialog.open( 
        DialogNameChangrHistoryComponent, {
        width: '80%',
        disableClose: true,
        data: this.form  
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
        console.log(this.form.value.name_change_histories)
      })
  
    }
}
