import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNameChangrHistoryComponent } from './dialog-name-changr-history.component';

describe('DialogNameChangrHistoryComponent', () => {
  let component: DialogNameChangrHistoryComponent;
  let fixture: ComponentFixture<DialogNameChangrHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogNameChangrHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNameChangrHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
