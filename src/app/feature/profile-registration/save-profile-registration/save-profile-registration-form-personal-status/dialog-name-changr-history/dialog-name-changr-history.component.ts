import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { prefix, PrefixService } from 'src/app/core/service/common/prefix.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-name-changr-history',
  templateUrl: './dialog-name-changr-history.component.html',
  styleUrls: ['./dialog-name-changr-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogNameChangrHistoryComponent implements OnInit {
  prefixData$ = new Observable<prefix[]>()
  formNameChange = {
      status_id: null,
      created_by: '',
      created_datetime: null,
      updated_by: '',
      updated_datetime: null,
      name_change_history_id: null,
      personnel_uid: null,
      prefix_id: null,
      other_prefix_name: '',
      first_name_th: '',
      middle_name_th: '',
      last_name_th: '',
      first_name_en: '',
      middle_name_en: '',
      last_name_en: '',
      change_date: null
  }
  constructor(
    public dialogRef: MatDialogRef<DialogNameChangrHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public dialog : MatDialog,
    public PrefixSV : PrefixService,
    public EmployeeSV : EmployeeService,
  ) { 
    this.prefixData$ = this.PrefixSV.getAll<prefix>()
  }

  ngOnInit(): void {
  }

  saveClose(){
    this.close();
    (<FormArray>this.data.get('name_change_histories')).insert(null,this.EmployeeSV.name_change_histories_form(this.formNameChange));
  }
  save(){
    (<FormArray>this.data.get('name_change_histories')).insert(null,this.EmployeeSV.name_change_histories_form(this.formNameChange));
    console.log(this.data)
  }

  close(){
    this.dialogRef.close('close')
  }
}
