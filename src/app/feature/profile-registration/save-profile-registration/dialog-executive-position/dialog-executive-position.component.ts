import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';

@Component({
  selector: 'app-dialog-executive-position',
  templateUrl: './dialog-executive-position.component.html',
  styleUrls: ['./dialog-executive-position.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogExecutivePositionComponent implements OnInit {

  personnel_executive_position = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    personnel_executive_position_uid: null,
    personnel_uid: null,
    position_uid: null,
    is_deputy: true,
    salary_amount: 0,
    start_date: null,
    end_date: null,
    remark: '',
  }
  position$ = new Observable()
  constructor(public dialogRef: MatDialogRef<DialogExecutivePositionComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public EmployeeSV: EmployeeService,
    public PositionSV: PositionService,
  ) { 
    this.position$ = this.PositionSV.getAll()
  }

  ngOnInit(): void {
    this.personnel_executive_position.personnel_uid = this.data.value.personnel_uid
  }
  saveClose() {
    this.close();
    (<FormArray>this.data.get('personnel_executive_position')).insert(null, this.EmployeeSV.personnel_executive_position_form(this.personnel_executive_position));
  }
  save() {
    (<FormArray>this.data.get('personnel_executive_position')).insert(null, this.EmployeeSV.personnel_executive_position_form(this.personnel_executive_position));
  }

  close() {
    this.dialogRef.close('close')
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }


}
