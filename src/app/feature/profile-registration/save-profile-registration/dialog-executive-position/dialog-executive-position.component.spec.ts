import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogExecutivePositionComponent } from './dialog-executive-position.component';

describe('DialogExecutivePositionComponent', () => {
  let component: DialogExecutivePositionComponent;
  let fixture: ComponentFixture<DialogExecutivePositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogExecutivePositionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogExecutivePositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
