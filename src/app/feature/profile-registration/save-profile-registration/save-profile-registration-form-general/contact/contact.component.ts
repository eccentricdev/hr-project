import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap, takeUntil, tap } from 'rxjs/operators';
import { RelationshipsService } from 'src/app/core/service/common/relationships.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { EmployeeContactTypeService } from 'src/app/core/service/register/employee-contact-type.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactComponent implements OnInit,OnDestroy,OnChanges {
  EmployeeContactTypeData = []
  subDistricts = []
  unSubAll$ = new Subject<void>();
  relationships$ = new Observable()
  @Input() formContact : FormGroup
  @Input() form : FormGroup
  constructor(
    public EmployeeContactTypeSV:EmployeeContactTypeService,
    public SubDistrictSV:SubDistrictService,
    public RelationshipsSV:RelationshipsService,
    private cdRef: ChangeDetectorRef,
  ) { 
    this.EmployeeContactTypeSV.getAll().subscribe(x=>this.EmployeeContactTypeData = x)
    this.relationships$ = this.RelationshipsSV.getAll()
  }
  ngOnChanges(changes: SimpleChanges): void {
   console.log(changes)
  }

  ngOnInit(): void {

    this.formContact.get('contact.address.other_sub_district_name_th').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.SubDistrictSV.query(`?sub_district_name_th=${text}`)),
      tap((subDistricts: any) => this.subDistricts = [...subDistricts]),
      tap((subDistricts: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.formContact.get('contact.address.other_district_name_th').disable()
    this.formContact.get('contact.address.other_province_name_th').disable()
  }
  chengeAddressRegistered(value,form){
    console.log(value)
    if(value.checked){
    this.form.get('personnel_addresses').value.forEach((element,i) => {
      console.log(element)
      if(i == 0){
        // (<FormArray>this.form.get('personnel_contacts')).insert(0,this.EmployeeSV.personnel_contacts_form(element));
        form.get('contact_type_id').setValue(2);
        form.get('contact.address.address_name').setValue(element.address.address_name);
        form.get('contact.address.house_no').setValue(element.address.house_no);
        form.get('contact.address.village_no').setValue(element.address.village_no);
        form.get('contact.address.building_name').setValue(element.address.building_name);
        form.get('contact.address.room_no').setValue(element.address.room_no);
        form.get('contact.address.floor_no').setValue(element.address.floor_no);
        form.get('contact.address.village_name').setValue(element.address.village_name);
        form.get('contact.address.alley_name').setValue(element.address.alley_name);
        form.get('contact.address.street_name').setValue(element.address.street_name);
        form.get('contact.address.sub_district_id').setValue(element.address.sub_district_id);
        form.get('contact.address.other_sub_district_name_th').setValue(element.address.other_sub_district_name_th);
        form.get('contact.address.other_sub_district_name_en').setValue(element.address.other_sub_district_name_en);
        form.get('contact.address.other_district_name_th').setValue(element.address.other_district_name_th);
        form.get('contact.address.other_district_name_en').setValue(element.address.other_district_name_en);
        form.get('contact.address.other_province_name_th').setValue(element.address.other_province_name_th);
        form.get('contact.address.other_province_name_en').setValue(element.address.other_province_name_en);
        form.get('contact.address.postal_code').setValue(element.address.postal_code);
        form.get('contact.address.country_id').setValue(element.address.country_id);
        form.get('contact.address.telephone_no').setValue(element.address.telephone_no);
      }
    });
    }else{
      form.get('contact.address.address_name').setValue('');
      form.get('contact.address.house_no').setValue('');
      form.get('contact.address.village_no').setValue('');
      form.get('contact.address.building_name').setValue('');
      form.get('contact.address.room_no').setValue('');
      form.get('contact.address.floor_no').setValue('');
      form.get('contact.address.village_name').setValue('');
      form.get('contact.address.alley_name').setValue('');
      form.get('contact.address.street_name').setValue('');
      form.get('contact.address.sub_district_id').setValue(null);
      form.get('contact.address.other_sub_district_name_th').setValue('');
      form.get('contact.address.other_sub_district_name_en').setValue('');
      form.get('contact.address.other_district_name_th').setValue('');
      form.get('contact.address.other_district_name_en').setValue('');
      form.get('contact.address.other_province_name_th').setValue('');
      form.get('contact.address.other_province_name_en').setValue('');
      form.get('contact.address.postal_code').setValue('');
      form.get('contact.address.country_id').setValue(null);
      form.get('contact.address.telephone_no').setValue('');
    }
  }
  chengeAddressContact(value,form){
    console.log(value)
    if(value.checked){
    this.form.get('personnel_addresses').value.forEach((element,i) => {
      if(i == 1){
        form.get('contact_type_id').setValue(3);
        form.get('contact.address.address_name').setValue(element.address.address_name);
        form.get('contact.address.house_no').setValue(element.address.house_no);
        form.get('contact.address.village_no').setValue(element.address.village_no);
        form.get('contact.address.building_name').setValue(element.address.building_name);
        form.get('contact.address.room_no').setValue(element.address.room_no);
        form.get('contact.address.floor_no').setValue(element.address.floor_no);
        form.get('contact.address.village_name').setValue(element.address.village_name);
        form.get('contact.address.alley_name').setValue(element.address.alley_name);
        form.get('contact.address.street_name').setValue(element.address.street_name);
        form.get('contact.address.sub_district_id').setValue(element.address.sub_district_id);
        form.get('contact.address.other_sub_district_name_th').setValue(element.address.other_sub_district_name_th);
        form.get('contact.address.other_sub_district_name_en').setValue(element.address.other_sub_district_name_en);
        form.get('contact.address.other_district_name_th').setValue(element.address.other_district_name_th);
        form.get('contact.address.other_district_name_en').setValue(element.address.other_district_name_en);
        form.get('contact.address.other_province_name_th').setValue(element.address.other_province_name_th);
        form.get('contact.address.other_province_name_en').setValue(element.address.other_province_name_en);
        form.get('contact.address.postal_code').setValue(element.address.postal_code);
        form.get('contact.address.country_id').setValue(element.address.country_id);
        form.get('contact.address.telephone_no').setValue(element.address.telephone_no);
      }
    });
    }else{
      form.get('contact.address.address_name').setValue('');
      form.get('contact.address.house_no').setValue('');
      form.get('contact.address.village_no').setValue('');
      form.get('contact.address.building_name').setValue('');
      form.get('contact.address.room_no').setValue('');
      form.get('contact.address.floor_no').setValue('');
      form.get('contact.address.village_name').setValue('');
      form.get('contact.address.alley_name').setValue('');
      form.get('contact.address.street_name').setValue('');
      form.get('contact.address.sub_district_id').setValue(null);
      form.get('contact.address.other_sub_district_name_th').setValue('');
      form.get('contact.address.other_sub_district_name_en').setValue('');
      form.get('contact.address.other_district_name_th').setValue('');
      form.get('contact.address.other_district_name_en').setValue('');
      form.get('contact.address.other_province_name_th').setValue('');
      form.get('contact.address.other_province_name_en').setValue('');
      form.get('contact.address.postal_code').setValue('');
      form.get('contact.address.country_id').setValue(null);
      form.get('contact.address.telephone_no').setValue('');
    }
  }
  selectSubDistrict(data){
    console.log(data)
    this.formContact.get('contact.address.sub_district_id').setValue(data.sub_district_id)
    this.formContact.get('contact.address.other_district_name_th').setValue(data.district_name_th)
    this.formContact.get('contact.address.other_province_name_th').setValue(data.province_name_th)
  }


  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
