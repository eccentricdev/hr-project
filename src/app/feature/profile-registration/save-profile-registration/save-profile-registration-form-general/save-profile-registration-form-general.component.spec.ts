import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveProfileRegistrationFormGeneralComponent } from './save-profile-registration-form-general.component';

describe('SaveProfileRegistrationFormGeneralComponent', () => {
  let component: SaveProfileRegistrationFormGeneralComponent;
  let fixture: ComponentFixture<SaveProfileRegistrationFormGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveProfileRegistrationFormGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveProfileRegistrationFormGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
