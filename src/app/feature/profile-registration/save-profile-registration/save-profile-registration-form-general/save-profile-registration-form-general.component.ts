import { Component, OnInit, ChangeDetectionStrategy, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BloodGroupService } from 'src/app/core/service/common/blood-group.service';
import { Country, CountryService } from 'src/app/core/service/common/country.service';
import { DeformsService } from 'src/app/core/service/common/deforms.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { ProvinceService } from 'src/app/core/service/common/province.service';
import { ReligionService } from 'src/app/core/service/common/religion.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { EmployeeContactTypeService } from 'src/app/core/service/register/employee-contact-type.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-save-profile-registration-form-general',
  templateUrl: './save-profile-registration-form-general.component.html',
  styleUrls: ['./save-profile-registration-form-general.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveProfileRegistrationFormGeneralComponent implements OnInit {
  EmployeeContactTypeData = []
  country = []
  Province = []
  // Province$ = new Observable()
  host:string = `${environment.rsuArApiName}`
  religion$ = new Observable()
  bloodGroup$ = new Observable()
  deforms$ = new Observable()
  subDistrict$ = new Observable()
  prefix$ = new Observable()
  unSubAll$ = new Subject<void>();
  genders$ = new Observable();
  insigniaDetail$ = new Observable();
  countryName : any
  @Input() form : FormGroup
  @ViewChild('age') age: ElementRef<HTMLInputElement>

  constructor(
    public EmployeeContactTypeSV:EmployeeContactTypeService,
    public EmployeeSV:EmployeeService,
    public CountrySV:CountryService,
    public ProvinceSV:ProvinceService,
    public ReligionSV:ReligionService,
    public BloodGroupSV:BloodGroupService,
    public SubDistrictSV:SubDistrictService,
    public PrefixSV:PrefixService,
    public DeformsSV:DeformsService,
    private cdRef: ChangeDetectorRef,
    private GendersSV: GendersService,
    private InsigniaDetailSV: InsigniaDetailService,
  ) { 
    this.EmployeeContactTypeSV.getAll().subscribe(x=>this.EmployeeContactTypeData = x)
    // this.Country$ = this.CountrySV.getAllCommon()
    // this.Province$ = this.ProvinceSV.getAllCommon() 
    this.religion$ = this.ReligionSV.getAll()
    this.insigniaDetail$ = this.InsigniaDetailSV.query('/all')
    this.bloodGroup$ = this.BloodGroupSV.getAll()
    this.deforms$ = this.DeformsSV.getAll()
    // this.subDistrict$ = this.SubDistrictSV.getAllCommon()
    this.prefix$ = this.PrefixSV.getAll()
    this.genders$ = this.GendersSV.getAll()
  }

  ngOnInit(): void {
    this.form.get('photo_url').disable()
    this.form.get('country_of_birth_name').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.CountrySV.query(`?search=${text}`)),
      tap((country: Country[]) => console.log(country)),
      tap((country: Country[]) => this.country = [...country]),
      tap((country: Country[]) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('province_of_birth_name').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.ProvinceSV.query(`?search=${text}`)),
      tap((Province: Country[]) => console.log(Province)),
      tap((Province: any) => this.Province = [...Province]),
      tap((Province: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('nationality_name').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.CountrySV.query(`?search=${text}`)),
      tap((Province: any) => this.Province = [...Province]),
      tap((Province: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('ethnicity_name').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.CountrySV.query(`?search=${text}`)),
      tap((Province: any) => this.Province = [...Province]),
      tap((Province: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('birth_date').valueChanges.pipe(
      debounceTime(500),
     
      tap((x: any) =>{
        this.dateChangeBirth(x)
      }),
      tap((x: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()

  }
  // dateChange() {

  // dateChange(form, date: moment.Moment, text) {
  //   console.log(date)
  //   form.get(`${text}`).setValue(moment(date).add(7,'hours').toDate())
  // }
  
  selectCountry(coutnry: Country) {
    this.form.get('country_of_birth_id').setValue(coutnry.country_id)
  }
  select_nationality(coutnry: Country) {
    this.form.get('nationality_id').setValue(coutnry.country_id)
  }
  select_ethnicity(coutnry: Country) {
    this.form.get('ethnicity_id').setValue(coutnry.country_id)
  }
  selectProvince(province) {
    this.form.get('province_of_birth_id').setValue(province.province_id)
  }
  
  dateChangeBirth(date: moment.Moment) {
    // this.form.get('date_of_birth').setValue(moment(date).add(7, 'hours').toDate())
    let todayDate = moment()
    let calculateAge = todayDate.diff(date, 'years')
    this.age.nativeElement.value = calculateAge.toString()
    console.log(calculateAge)
  }
  getFile(file) {
    console.log(file);
    this.form.get('photo_url').setValue(file.file_url)
  }
  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
