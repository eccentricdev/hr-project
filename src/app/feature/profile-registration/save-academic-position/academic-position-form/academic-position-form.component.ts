import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { Subject, throwError, Observable } from 'rxjs';
import { catchError, delay, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { AcademicPositionAdjustmentsService } from 'src/app/core/service/register/academic-position-adjustments.service';
import { AcademicPositionsService } from 'src/app/core/service/common/academic-positions.service';
import * as moment from 'moment';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';

@Component({
  selector: 'app-academic-position-form',
  templateUrl: './academic-position-form.component.html',
  styleUrls: ['./academic-position-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcademicPositionFormComponent extends BaseForm implements OnInit, OnDestroy {

  employee$ = new Observable()
  academicPosition$ = new Observable()
  unSubAll$ = new Subject<boolean>()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  rows: any = []
  employeeData: any = []
  agency_uid: any
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  constructor(
    public dialog: MatDialog,
    public router: Router,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public AcademicPositionAdjustmentSV: AcademicPositionAdjustmentsService,
    public employeeSV: EmployeeService,
    public AgencysSV: AgencysService,
    public academicPositionSV: AcademicPositionsService,
    public ApproveStatussSV: ApproveStatussService
  ) {
    super(fb, activeRoute)
    this.agencys$ = this.AgencysSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
  }

  ngOnInit(): void {
    this.academicPosition$ = this.academicPositionSV.getAll()
    // this.employee$ = this.employeeSV.getAll().pipe(
    //   tap(x => console.log(x)),
    //   map((employee) => {
    //      let employeeData =  employee.map((x:any) => {
    //         return { ...x, displayName: x.first_name_th + ' ' + x.last_name_th };
    //       })
    //       return employeeData
    //   })
    // )
   

    switch (this.state) {
      case 'edit':
        this.AcademicPositionAdjustmentSV.get(this.id).pipe(
          takeUntil(this.unSubAll$),
          tap(x => console.log(x)),
          tap(res => this.setUpform(res))
        ).subscribe()
        break;
      case 'add':

        break;
    }

    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.form.get('retirement_age').enable()
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.employeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.form.get('retirement_age').enable()
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.employeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.form.get('retirement_age').enable()
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.employeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
    
  }
  

  selectEmployeeCitizen_id(data) {
    console.log(data)
  }
  dateChange() {

  }
  select_personnels(data) {
    console.log(data)
    this.employeeSV.get(data).pipe(
      tap(x => console.log(x)),
      tap((x: any) => {
        this.position.nativeElement.value = x.personnel_hires[0]?.position?.position_name_th == undefined ? 'ไม่มีข้อมูล' : x.personnel_hires[0]?.position?.position_name_th
        this.agency_uid = x.personnel_hires[0].agency_uid
        this.form.get('personnel_uid').setValue(x.personnel_uid)
        this.form.get('personnel_no').setValue(x.personnel_no)
        this.form.get('citizen_id').setValue(x.citizen_id)
        this.form.get('display_name_th').setValue(x.display_name_th)
      }),
      takeUntil(this.unSubAll$)
    ).subscribe()

  }

  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [new Date()],
      updated_by: [''],
      updated_datetime: [new Date()],
      academic_position_adjustment_uid: [null],
      personnel_uid: [null],
      order_document_no: [''],
      document_no: [''],
      order_date: [new Date()],
      document_date: [new Date()],
      academic_position_id: [null],
      effective_date: [new Date()],
      remark: [''],
      approve_status: [0],
      personnel_no: [null],
      citizen_id: [''],
      display_name_th: [''],
    })
  }

  setUpform(res) {
    this.form.patchValue(res)
    this.select_personnels(res.personnel_uid)
  }

  close() {
    this.router.navigate(['app/pr_academic_position'])
  }

  save() {
    switch (this.state) {
      case 'edit':
        this.AcademicPositionAdjustmentSV.update(this.form.getRawValue()).pipe(
          takeUntil(this.unSubAll$),
          catchError(err => {
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_academic_position'])
          })
        break;

      case 'add':
        this.AcademicPositionAdjustmentSV.add(this.form.getRawValue()).pipe(
          takeUntil(this.unSubAll$),
          catchError(err => {
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_academic_position'])
          })
        break;
      default:
        break;
    }
  }

  ngOnDestroy(): void {
    this.unSubAll$.next(true)
    this.unSubAll$.complete()
  }
}