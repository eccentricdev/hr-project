import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicPositionFormComponent } from './academic-position-form.component';

describe('AcademicPositionFormComponent', () => {
  let component: AcademicPositionFormComponent;
  let fixture: ComponentFixture<AcademicPositionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcademicPositionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicPositionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
