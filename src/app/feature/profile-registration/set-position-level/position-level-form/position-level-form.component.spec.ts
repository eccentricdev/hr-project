import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionLevelFormComponent } from './position-level-form.component';

describe('PositionLevelFormComponent', () => {
  let component: PositionLevelFormComponent;
  let fixture: ComponentFixture<PositionLevelFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionLevelFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionLevelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
