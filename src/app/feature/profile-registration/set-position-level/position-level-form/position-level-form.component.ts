import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';

@Component({
  selector: 'app-position-level-form',
  templateUrl: './position-level-form.component.html',
  styleUrls: ['./position-level-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionLevelFormComponent extends BaseForm implements OnInit {
  baseFormBuilder: any;
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PositionLevelsSV: PositionLevelsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PositionLevelsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      position_level_id:res.position_level_id,
      position_level_code:res.position_level_code,
      position_level_name_th:res.position_level_name_th,
      position_level_name_en:res.position_level_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_position_level'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.PositionLevelsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_position_level'])
          })
        break;
      case 'add':
      
        this.PositionLevelsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_position_level'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      position_level_id: [null],
      position_level_code: [''],
      position_level_name_th: [''],
      position_level_name_en: [''],
    })
  }
}
