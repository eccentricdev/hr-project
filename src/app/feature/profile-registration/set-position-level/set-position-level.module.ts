import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionLevelListComponent } from './position-level-list/position-level-list.component';
import { PositionLevelFormComponent } from './position-level-form/position-level-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PositionLevelListComponent
  },
  {
    path:'add',
    component:PositionLevelFormComponent
  },
  {
    path:'edit/:id',
    component:PositionLevelFormComponent
  },
  
]

@NgModule({
  declarations: [PositionLevelListComponent, PositionLevelFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetPositionLevelModule { }
