import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-position-level-list',
  templateUrl: './position-level-list.component.html',
  styleUrls: ['./position-level-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionLevelListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  positionLevels$ = new Observable()
  req = {
    position_level_code : '',
    position_level_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public PositionLevelsSV : PositionLevelsService,
  ) { super()
    this.positionLevels$ = this.PositionLevelsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.positionLevels$  = this.PositionLevelsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      position_level_code : '',
      position_level_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_position_level/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_position_level/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.PositionLevelsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.positionLevels$ = this.PositionLevelsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
