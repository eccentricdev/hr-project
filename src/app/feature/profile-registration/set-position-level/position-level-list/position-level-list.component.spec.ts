import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionLevelListComponent } from './position-level-list.component';

describe('PositionLevelListComponent', () => {
  let component: PositionLevelListComponent;
  let fixture: ComponentFixture<PositionLevelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionLevelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionLevelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
