import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionSalaryListComponent } from './position-salary-list/position-salary-list.component';
import { PositionSalaryFormComponent } from './position-salary-form/position-salary-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PositionSalaryListComponent
  },
  {
    path:'add',
    component:PositionSalaryFormComponent
  },
  {
    path:'edit/:id',
    component:PositionSalaryFormComponent
  },
  
]

@NgModule({
  declarations: [PositionSalaryListComponent, PositionSalaryFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SavePositionSalaryModule { }
