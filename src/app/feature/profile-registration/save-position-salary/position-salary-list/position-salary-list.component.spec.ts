import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionSalaryListComponent } from './position-salary-list.component';

describe('PositionSalaryListComponent', () => {
  let component: PositionSalaryListComponent;
  let fixture: ComponentFixture<PositionSalaryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionSalaryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionSalaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
