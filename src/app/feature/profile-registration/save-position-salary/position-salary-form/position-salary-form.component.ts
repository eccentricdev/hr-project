import { Component, OnInit, ChangeDetectionStrategy, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, delay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelSalaryAdjustmentsService } from 'src/app/core/service/register/personnel-salary-adjustments.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-position-salary-form',
  templateUrl: './position-salary-form.component.html',
  styleUrls: ['./position-salary-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionSalaryFormComponent extends BaseForm implements OnInit {
  rows: any = []
  employeeData: any = []
  personnels$ = new Observable()
  position$ = new Observable()
  positionLevel$ = new Observable()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  agency_uid : any
  unSubAll$ = new Subject<void>();
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  @ViewChild('service_life') service_life: ElementRef<HTMLInputElement>
  
  constructor(public dialog: MatDialog,
    public FormBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public EmployeeSV: EmployeeService,
    public PositionSV: PositionService,
    public AgencysSV: AgencysService,
    private cdRef: ChangeDetectorRef,
    public PositionLevelsSV: PositionLevelsService,
    public ApproveStatussSV: ApproveStatussService,
    public PersonnelSalaryAdjustmentsSV: PersonnelSalaryAdjustmentsService,
    public router: Router) {
    super(FormBuilder, activeRoute)
    this.personnels$ = this.EmployeeSV.getAll()
    this.position$ = this.PositionSV.getAll()
    this.positionLevel$ = this.PositionLevelsSV.getAll()
    this.agencys$ = this.AgencysSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
  }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.PersonnelSalaryAdjustmentsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
    this.form.get('current_salary_amount').disable()

    this.form.get('new_salary_amount').valueChanges.pipe(
      delay(1000),
      tap(x=>{
        if(x == '' || x == null){
          this.form.get('increase_percent').setValue(0,{ emitEvent: false })
          this.form.get('increase_amount').setValue(0,{ emitEvent: false })
        }else{
          let a = Number(x)-this.form.get('current_salary_amount').value
          this.form.get('increase_percent').setValue(a/100,{ emitEvent: false })
          this.form.get('increase_amount').setValue(a,{ emitEvent: false })
        }

      }),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('increase_amount').valueChanges.pipe(
      delay(1000),
      tap(x=>{
        if(x == '' || x == null){
          this.form.get('new_salary_amount').setValue(0,{ emitEvent: false }) 
          this.form.get('increase_percent').setValue(0,{ emitEvent: false })
        }else{
          let a = 0
          a = this.form.get('current_salary_amount').value+Number(x)
          this.form.get('new_salary_amount').setValue(a,{ emitEvent: false })
  
          let b = (Number(x))/100
          this.form.get('increase_percent').setValue(b,{ emitEvent: false })
        }
      }),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('increase_percent').valueChanges.pipe(
      delay(1000),
      tap(x=>{
        if(x == '' || x == null || x == 0){
          this.form.get('new_salary_amount').setValue(0,{ emitEvent: false })
          this.form.get('increase_amount').setValue(0,{ emitEvent: false })
        }else{
          let a = 0
          a = (this.form.get('current_salary_amount').value*x)/100
          this.form.get('new_salary_amount').setValue(this.form.get('current_salary_amount').value+Number(a),{ emitEvent: false })
  
          this.form.get('increase_amount').setValue(a,{ emitEvent: false })
        }
      }),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
    
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      position_salary_adjustment_uid:res.position_salary_adjustment_uid,
      personnel_uid:res.personnel_uid,
      order_document_no:res.order_document_no,
      document_no:res.document_no,
      document_date:res.document_date,
      order_date:res.order_date,
      is_salary_adjustment:res.is_salary_adjustment,
      current_salary_amount:res.current_salary_amount,
      new_salary_amount:res.new_salary_amount,
      increase_amount:res.increase_amount,
      increase_percent:res.increase_percent,
      salary_remark:res.salary_remark,
      is_position_adjustment:res.is_position_adjustment,
      is_position_change:res.is_position_change,
      is_management_position:res.is_management_position,
      is_act_for:res.is_act_for,
      manpower_uid:res.manpower_uid,
      position_uid:res.position_uid,
      position_adjustment_remark:res.position_adjustment_remark,
      is_position_level_adjustment:res.is_position_level_adjustment,
      position_level_id:res.position_level_id,
      position_level_adjustment_remark:res.position_level_adjustment_remark,
      approve_status:res.approve_status,
    })
    this.select_personnels(res.personnel_uid)
  }
  select_personnels(data){
    console.log(data)
    this.EmployeeSV.get(data).pipe(
      tap(x => console.log(x)),
      tap((x:any) => {
        let todayDate = moment()
        if(x.birth_date == null){
          this.age.nativeElement.value = 'ไม่พบข้อมูล'
          
        }else{
          let calculateAge = todayDate.diff(x.birth_date, 'years')
          this.age.nativeElement.value = calculateAge.toString()
        }
        this.position.nativeElement.value = x.personnel_hires[0]?.position?.position_name_th == undefined ? 'ไม่มีข้อมูล' : x.personnel_hires[0]?.position?.position_name_th
        
        let startDate = moment()
        let calculateStartday = startDate.diff(x.personnel_hires[0].start_date, 'day')
        let calculateStartM = startDate.diff(x.personnel_hires[0].start_date, 'months')
        let calculateStartY = startDate.diff(x.personnel_hires[0].start_date, 'years')
        
        console.log(calculateStartday)
        console.log(calculateStartM)
        console.log(calculateStartY)
        if(x.personnel_hires[0].start_date == null){
          this.service_life.nativeElement.value = 'ไม่พบข้อมูล'
        }else{
          this.service_life.nativeElement.value = `${calculateStartday+`วัน`+calculateStartM+`เดือน`+calculateStartY+`ปี`}`
          this.form.get('start_date').setValue(x.personnel_hires[0].start_date)
        }
        this.form.get('position_level_id_start').setValue(x.personnel_hires[0]?.position_level_id)
        this.form.get('current_salary_amount').setValue(x.personnel_hires[0]?.salary_amount)

        this.form.get('personnel_uid').setValue(x.personnel_uid)
        this.form.get('personnel_no').setValue(x.personnel_no)
        this.form.get('citizen_id').setValue(x.citizen_id)
        this.form.get('display_name_th').setValue(x.display_name_th)

        this.agency_uid = x.personnel_hires[0].agency_uid

      }),
      takeUntil(this.unSubAll$)
    ).subscribe()

  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }
  close() {
    this.router.navigate(['app/pr_edit_position_salary'])
  }
  save() {
    switch (this.state) {
      case 'edit':

        this.PersonnelSalaryAdjustmentsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_edit_position_salary'])
          })
        break;
      case 'add':

        this.PersonnelSalaryAdjustmentsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_edit_position_salary'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      position_salary_adjustment_uid: [null],
      personnel_uid: [null],
      order_document_no: [''],
      document_no: [''],
      order_date: [null],
      document_date: [null],
      is_salary_adjustment: [true],
      current_salary_amount: [null],
      new_salary_amount: [null],
      increase_amount: [null],
      increase_percent: [null],
      salary_remark: [''],
      is_position_adjustment: [true],
      is_position_change: [true],
      is_management_position: [true],
      is_act_for: [true],
      manpower_uid: [null],
      position_uid: [null],
      position_adjustment_remark: [''],
      is_position_level_adjustment: [true],
      position_level_id: [null],
      start_date: [null],
      position_level_id_start: [null],
      position_level_adjustment_remark: [''],
      approve_status: [null],
      personnel_no: [null],
      citizen_id: [''],
      display_name_th: [''],
    })
  }
  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
