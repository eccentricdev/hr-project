import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionSalaryFormComponent } from './position-salary-form.component';

describe('PositionSalaryFormComponent', () => {
  let component: PositionSalaryFormComponent;
  let fixture: ComponentFixture<PositionSalaryFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionSalaryFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionSalaryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
