import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelTypeFormComponent } from './personnel-type-form.component';

describe('PersonnelTypeFormComponent', () => {
  let component: PersonnelTypeFormComponent;
  let fixture: ComponentFixture<PersonnelTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnelTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
