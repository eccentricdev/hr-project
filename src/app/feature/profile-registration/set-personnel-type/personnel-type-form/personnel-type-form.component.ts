import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';

@Component({
  selector: 'app-personnel-type-form',
  templateUrl: './personnel-type-form.component.html',
  styleUrls: ['./personnel-type-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonnelTypeFormComponent extends BaseForm implements OnInit {
  baseFormBuilder: any;
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PersonnelTypesSV: PersonnelTypesService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PersonnelTypesSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      personnel_type_id:res.personnel_type_id,
      personnel_type_code:res.personnel_type_code,
      personnel_type_name_th:res.personnel_type_name_th,
      personnel_type_name_en:res.personnel_type_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_personnel_type'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.PersonnelTypesSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_personnel_type'])
          })
        break;
      case 'add':
      
        this.PersonnelTypesSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_personnel_type'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      personnel_type_id: [null],
      personnel_type_code: [''],
      personnel_type_name_th: [''],
      personnel_type_name_en: [''],
    })
  }
}
