import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonnelTypeListComponent } from './personnel-type-list/personnel-type-list.component';
import { PersonnelTypeFormComponent } from './personnel-type-form/personnel-type-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PersonnelTypeListComponent
  },
  {
    path:'add',
    component:PersonnelTypeFormComponent
  },
  {
    path:'edit/:id',
    component:PersonnelTypeFormComponent
  },
  
]

@NgModule({
  declarations: [PersonnelTypeListComponent, PersonnelTypeFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetPersonnelTypeModule { }
