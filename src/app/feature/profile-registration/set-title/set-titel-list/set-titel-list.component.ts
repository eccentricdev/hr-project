import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-titel-list',
  templateUrl: './set-titel-list.component.html',
  styleUrls: ['./set-titel-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTitelListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  prefix$ = new Observable()
  req = {
    prefix_code : '',
    prefix_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public PrefixSV : PrefixService,
  ) { super()
    this.prefix$ = this.PrefixSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
   }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.prefix$  = this.PrefixSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      prefix_code : '',
      prefix_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_title/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_title/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.PrefixSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.prefix$ = this.PrefixSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
