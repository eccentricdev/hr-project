import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SetTitelFormComponent } from './set-titel-form/set-titel-form.component';
import { SetTitelListComponent } from './set-titel-list/set-titel-list.component';

const routes:Routes = [
  {
    path:'',
    component:SetTitelListComponent
  },
  {
    path:'add',
    component:SetTitelFormComponent
  },
  {
    path:'edit/:id',
    component:SetTitelFormComponent
  },
  
]

@NgModule({
  declarations: [SetTitelFormComponent,SetTitelListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetTitleModule { }
