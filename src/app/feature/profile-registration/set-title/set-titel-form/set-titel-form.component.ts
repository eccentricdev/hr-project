import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';

@Component({
  selector: 'app-set-titel-form',
  templateUrl: './set-titel-form.component.html',
  styleUrls: ['./set-titel-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTitelFormComponent extends BaseForm implements OnInit {

  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PrefixSV: PrefixService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.PrefixSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      prefix_id:res.prefix_id,
      prefix_code:res.prefix_code,
      prefix_name_th:res.prefix_name_th,
      prefix_name_en:res.prefix_name_en,
      prefix_short_name_th:res.prefix_short_name_th,
      prefix_short_name_en:res.prefix_short_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_title'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.PrefixSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_title'])
          })
        break;
      case 'add':
      
        this.PrefixSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_title'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      prefix_id: [null],
      prefix_code: [''],
      prefix_name_th: [''],
      prefix_short_name_th: [''],
      prefix_short_name_en: [''],
      prefix_name_en: [''],
    })
  }
}
