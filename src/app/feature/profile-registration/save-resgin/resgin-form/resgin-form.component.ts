import { Component, OnInit, ChangeDetectionStrategy, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, delay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { ExitTypeService } from 'src/app/core/service/common/exit-type.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelExitsService } from 'src/app/core/service/register/personnel-exits.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-resgin-form',
  templateUrl: './resgin-form.component.html',
  styleUrls: ['./resgin-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResginFormComponent extends BaseForm implements OnInit,OnDestroy{
  rows :any = []
  exitType$ = new Observable()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  birth
  agency_uid : any
  employeeData: any = []
  unSubAll$ = new Subject<boolean>()
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  @ViewChild('service_life') service_life: ElementRef<HTMLInputElement>
  @ViewChild('retired') retired: ElementRef<HTMLInputElement>
  personnels$ = new Observable()
  constructor(public dialog :MatDialog,
    public FormBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public PersonnelExitsSV: PersonnelExitsService,
    public ExitTypeSV: ExitTypeService,
    public EmployeeSV: EmployeeService,
    public AgencysSV: AgencysService,
    public ApproveStatussSV: ApproveStatussService,
    public appSV: AppService,
              public router:Router) {
                super(FormBuilder, activeRoute) 
                this.exitType$ = this.ExitTypeSV.getAll()
                this.personnels$ = this.EmployeeSV.getAll()
                this.agencys$ = this.AgencysSV.getAll()
                this.approveStatuss$ = this.ApproveStatussSV.getAll()
               }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.PersonnelExitsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }

    this.form.get('retirement_age').valueChanges.pipe(
      delay(1000),
      tap(x=>{
        if(x == ''){
          this.retired.nativeElement.value = ''
        }
      }),
      tap(x=>{
        let retiredYear = moment(this.birth)
        let a = this.form.get('retirement_age').value - Number(this.age.nativeElement.value) 
        let calculateretiredYear = retiredYear.add(a, 'years')
        this.retired.nativeElement.value = String(Number(calculateretiredYear.year().toString())+543)
      })
    ).subscribe()

    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          this.retired.nativeElement.value = ''
          this.form.get('retirement_age').enable()
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          this.retired.nativeElement.value = ''
          this.form.get('retirement_age').enable()
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          this.retired.nativeElement.value = ''
          this.form.get('retirement_age').enable()
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
   
  }
  setUpform(res) {
    // patch value 
    console.log(res)
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      personnel_exit_uid:res.personnel_exit_uid,
      personnel_uid:res.personnel_uid,
      exit_type_id:res.exit_type_id,
      description:res.description,
      remark:res.remark,
      document_no:res.document_no,
      document_date:res.document_date,
      approve_status:res.approve_status,
      retirement_age:res.retirement_age,
    })
    this.select_personnels(res.personnel_uid)
  }
  select_personnels(data){
    console.log(data)
    this.EmployeeSV.get(data).pipe(
      tap(x => console.log(x)),
      tap((x:any) => {
        let todayDate = moment()
        this.birth = x.birth_date
        if(x.birth_date == null){
          this.age.nativeElement.value = 'ไม่พบข้อมูล'
          this.retired.nativeElement.value = 'ไม่พบข้อมูล'
          this.form.get('retirement_age').disable({ emitEvent: false })
        }else{
          this.form.get('retirement_age').enable()
          let calculateAge = todayDate.diff(x.birth_date, 'years')
          this.age.nativeElement.value = calculateAge.toString()
        }
        this.position.nativeElement.value = x.personnel_hires[0]?.position?.position_name_th == undefined ? 'ไม่มีข้อมูล' : x.personnel_hires[0]?.position?.position_name_th
        
        let startDate = moment()
        let calculateStartday = startDate.diff(x.personnel_hires[0].start_date, 'day')
        let calculateStartM = startDate.diff(x.personnel_hires[0].start_date, 'months')
        let calculateStartY = startDate.diff(x.personnel_hires[0].start_date, 'years')
        

        console.log(calculateStartday)
        console.log(calculateStartM)
        console.log(calculateStartY)

        if(x.personnel_hires[0].start_date == null){
          this.service_life.nativeElement.value = 'ไม่พบข้อมูล'
        }else{
          this.service_life.nativeElement.value = `${calculateStartday+`วัน`+calculateStartM+`เดือน`+calculateStartY+`ปี`}`
          this.form.get('start_date').setValue(x.personnel_hires[0].start_date)
        }
        this.form.get('personnel_uid').setValue(x.personnel_uid)
        this.form.get('personnel_no').setValue(x.personnel_no)
        this.form.get('citizen_id').setValue(x.citizen_id)
        this.form.get('display_name_th').setValue(x.display_name_th)

        this.agency_uid = x.personnel_hires[0].agency_uid 

      }),
    ).subscribe()

  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    openUpload() {
      const dialogRef = this.dialog.open(
        UploadFileDialogComponent, {
        width: '40%',
        disableClose: true,
        data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }

  close(){
    this.router.navigate(['app/pr_resgin'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.PersonnelExitsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_resgin'])
          })
        break;
      case 'add':
      
        this.PersonnelExitsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_resgin'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      personnel_exit_uid: [null],
      personnel_uid: [null],
      last_working_date: [null],
      exit_type_id: [null],
      description: [''],
      remark: [''],
      document_no: [''],
      document_date: [null],
      start_date: [null],
      retirement_age: [null],
      approve_status: [null],
      personnel_no: [null],
      citizen_id: [''],
      display_name_th: [''],
    })
  }
  ngOnDestroy(): void {
    this.unSubAll$.next(true)
    this.unSubAll$.complete()
  }
}
