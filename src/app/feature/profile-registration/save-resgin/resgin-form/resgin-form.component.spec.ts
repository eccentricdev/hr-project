import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResginFormComponent } from './resgin-form.component';

describe('ResginFormComponent', () => {
  let component: ResginFormComponent;
  let fixture: ComponentFixture<ResginFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResginFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
