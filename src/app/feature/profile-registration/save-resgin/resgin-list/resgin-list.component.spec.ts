import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResginListComponent } from './resgin-list.component';

describe('ResginListComponent', () => {
  let component: ResginListComponent;
  let fixture: ComponentFixture<ResginListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResginListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResginListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
