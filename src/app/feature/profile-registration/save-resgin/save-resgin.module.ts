import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResginFormComponent } from './resgin-form/resgin-form.component';
import { ResginListComponent } from './resgin-list/resgin-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ResginListComponent
  },
  {
    path:'add',
    component:ResginFormComponent
  },
  {
    path:'edit/:id',
    component:ResginFormComponent
  },
  
]


@NgModule({
  declarations: [ResginListComponent,ResginFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveResginModule { }
