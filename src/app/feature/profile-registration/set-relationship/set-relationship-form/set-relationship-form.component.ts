import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { RelationshipsService } from 'src/app/core/service/common/relationships.service';

@Component({
  selector: 'app-set-relationship-form',
  templateUrl: './set-relationship-form.component.html',
  styleUrls: ['./set-relationship-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRelationshipFormComponent extends BaseForm implements OnInit {

  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public RelationshipsSV: RelationshipsService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.RelationshipsSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      relationship_id:res.relationship_id,
      relationship_code:res.relationship_code,
      relationship_name_th:res.relationship_name_th,
      relationship_name_en:res.relationship_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_relationship'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.RelationshipsSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_relationship'])
          })
        break;
      case 'add':
      
        this.RelationshipsSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_relationship'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      relationship_id: [null],
      relationship_code: [''],
      relationship_name_th: [''],
      relationship_name_en: [''],
    })
  }
}
