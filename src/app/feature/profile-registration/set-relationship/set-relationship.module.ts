import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRelationshipListComponent } from './set-relationship-list/set-relationship-list.component';
import { SetRelationshipFormComponent } from './set-relationship-form/set-relationship-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetRelationshipListComponent
  },
  {
    path:'add',
    component:SetRelationshipFormComponent
  },
  {
    path:'edit/:id',
    component:SetRelationshipFormComponent
  },
  
]

@NgModule({
  declarations: [SetRelationshipListComponent, SetRelationshipFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRelationshipModule { }
