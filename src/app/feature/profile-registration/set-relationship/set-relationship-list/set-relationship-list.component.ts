import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { RelationshipsService } from 'src/app/core/service/common/relationships.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-relationship-list',
  templateUrl: './set-relationship-list.component.html',
  styleUrls: ['./set-relationship-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRelationshipListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  relationship$ = new Observable()
  req = {
    relationship_code : '',
    relationship_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public RelationshipsSV : RelationshipsService,
  ) { super()
    this.relationship$ = this.RelationshipsSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.relationship$  = this.RelationshipsSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      relationship_code : '',
      relationship_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_relationship/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_relationship/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.RelationshipsSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.relationship$ = this.RelationshipsSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })


        }
      })
    }
  }
}
