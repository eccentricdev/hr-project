import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { PersonnelProfileCategoriesService } from 'src/app/core/service/register/personnel-profile-categories.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-save-request-to-edit-history-dialog',
  templateUrl: './save-request-to-edit-history-dialog.component.html',
  styleUrls: ['./save-request-to-edit-history-dialog.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveRequestToEditHistoryDialogComponent implements OnInit {
  personnel_update_detail = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    personnel_update_detail_uid: null,
    personnel_update_history_uid: null,
    personnel_profile_category_id: null,
    remark: '',
    first_name_th: '',
    middle_name_th: '',
    last_name_th: '',
    first_name_en: '',
    middle_name_en: '',
    last_name_en: '',
    file_name: '',
    document_name: '',
    document_url: '',
    mime_type_name: ''
  }
  personnelProfileCategories$ = new Observable()
  constructor(    public dialogRef: MatDialogRef<SaveRequestToEditHistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,
    public PersonnelProfileCategoriesSV:PersonnelProfileCategoriesService,
    public FormBuilder:FormBuilder,
    public dialog :MatDialog) { 
      this.personnelProfileCategories$ = this.PersonnelProfileCategoriesSV.getAll()
    }

  ngOnInit(): void {
    this.personnel_update_detail.personnel_update_history_uid = this.data.value.personnel_update_history_uid
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  personnel_update_detail_form(element?) {
    if (element == undefined) {
      return this.FormBuilder.group({
        created_by: [null],
        created_datetime: [null],
        update_by: [null],
        update_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        personnel_update_detail_uid: [null],
        personnel_profile_category_id: [null],
        personnel_update_history_uid: [null],
        remark: [''],
        first_name_th: [''],
        middle_name_th: [''],
        last_name_th: [''],
        first_name_en: [''],
        middle_name_en: [''],
        last_name_en: [''],
        file_name: [''],
        document_name: [''],
        document_url: [''],
        mime_type_name: ['']
      })
    } else {
      return this.FormBuilder.group({
        created_by: element.created_by,
        created_datetime: element.created_datetime,
        update_by: element.update_by,
        updated_by: element.updated_by,
        updated_datetime: element.updated_datetime,
        personnel_update_detail_uid: element.personnel_update_detail_uid,
        personnel_profile_category_id: element.personnel_profile_category_id,
        personnel_update_history_uid: element.personnel_update_history_uid,
        remark: element.remark,
        first_name_th: element.first_name_th,
        middle_name_th: element.middle_name_th,
        last_name_th: element.last_name_th,
        first_name_en: element.first_name_en,
        middle_name_en: element.middle_name_en,
        last_name_en: element.last_name_en,
        file_name: element.file_name,
        document_name: element.document_name,
        document_url: element.document_url,
        mime_type_name: element.mime_type_name,
      })
    }
  }
  save(){
    (<FormArray>this.data.get('personnel_update_detail')).insert(null, this.personnel_update_detail_form(this.personnel_update_detail));
  }
  saveClose(){
    this.close();
    (<FormArray>this.data.get('personnel_update_detail')).insert(null, this.personnel_update_detail_form(this.personnel_update_detail));
  }

  close(){
    this.dialogRef.close('close')
  }
}
