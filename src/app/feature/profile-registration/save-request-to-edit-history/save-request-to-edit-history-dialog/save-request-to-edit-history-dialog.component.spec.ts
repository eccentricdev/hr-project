import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveRequestToEditHistoryDialogComponent } from './save-request-to-edit-history-dialog.component';

describe('SaveRequestToEditHistoryDialogComponent', () => {
  let component: SaveRequestToEditHistoryDialogComponent;
  let fixture: ComponentFixture<SaveRequestToEditHistoryDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveRequestToEditHistoryDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveRequestToEditHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
