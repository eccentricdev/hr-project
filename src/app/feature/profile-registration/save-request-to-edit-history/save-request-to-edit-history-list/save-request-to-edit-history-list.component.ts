import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, takeUntil, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelHiresService } from 'src/app/core/service/register/personnel-hires.service';
import { PersonnelUpdateHistoriesService } from 'src/app/core/service/register/personnel-update-histories.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-request-to-edit-history-list',
  templateUrl: './save-request-to-edit-history-list.component.html',
  styleUrls: ['./save-request-to-edit-history-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveRequestToEditHistoryListComponent extends BaseList implements OnInit,OnDestroy {
  rows:any = [
    {m:'1'}
  ]
  unSubAll$ = new Subject<void>();
  employeeData : any  = []
  req = {
    transfer_document_no : '',
    name : '',
    transfer_order_date : null,
    personnel_uid : null,
    approve_status : null,
  }
  personnelUpdateHistories$ = new Observable()
  employee$ = new Observable()
  position$ = new Observable()
  personnelHires$ = new Observable()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  constructor(
    public router : Router,
    public appSV : AppService,
    public EmployeeSV : EmployeeService,
    public PersonnelHiresSV : PersonnelHiresService,
    public PositionSV : PositionService,
    public AgencysSV : AgencysService,
    public PersonnelUpdateHistoriesSV: PersonnelUpdateHistoriesService,
    public ApproveStatussSV: ApproveStatussService,
  ) { super()
    this.personnelUpdateHistories$ = this.PersonnelUpdateHistoriesSV.getAll().pipe(
      tap(x=>console.log(x)),
      map(x=> this.updateMatTable(x))
    )
    this.employee$ = this.EmployeeSV.getAll()
    this.personnelHires$ = this.PersonnelHiresSV.getAll()
    this.position$ = this.PositionSV.getAll()
    this.agencys$ = this.AgencysSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.personnelUpdateHistories$  = this.PersonnelUpdateHistoriesSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      transfer_document_no : '',
      name : '',
      transfer_order_date : null,
      personnel_uid : null,
      approve_status : null,
    }
    this.search()
  }
  inputSearch(){
    console.log()
    this.EmployeeSV.query(`?display_name_th=${this.req.name}`).pipe(
      tap((employee: any) => console.log(employee)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
  }
  select_personnels(value){
    console.log(value)
    this.req.personnel_uid = value
  }
  addPage(){
    this.router.navigate(['app/pr_edit_profile_registration/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_edit_profile_registration/edit',id])
  }
  dateChange(){

  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.PersonnelUpdateHistoriesSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.personnelUpdateHistories$ = this.PersonnelUpdateHistoriesSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
  ngOnDestroy(): void {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
