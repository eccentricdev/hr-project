import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveRequestToEditHistoryListComponent } from './save-request-to-edit-history-list.component';

describe('SaveRequestToEditHistoryListComponent', () => {
  let component: SaveRequestToEditHistoryListComponent;
  let fixture: ComponentFixture<SaveRequestToEditHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveRequestToEditHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveRequestToEditHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
