import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveRequestToEditHistoryListComponent } from './save-request-to-edit-history-list/save-request-to-edit-history-list.component';
import { SaveRequestToEditHistoryFormComponent } from './save-request-to-edit-history-form/save-request-to-edit-history-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SaveRequestToEditHistoryDialogComponent } from './save-request-to-edit-history-dialog/save-request-to-edit-history-dialog.component';

const routes:Routes = [
  {
    path:'',
    component:SaveRequestToEditHistoryListComponent
  },
  {
    path:'add',
    component:SaveRequestToEditHistoryFormComponent
  },
  {
    path:'edit/:id',
    component:SaveRequestToEditHistoryFormComponent
  },
  
]

@NgModule({
  declarations: [SaveRequestToEditHistoryListComponent, SaveRequestToEditHistoryFormComponent, SaveRequestToEditHistoryDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  entryComponents: [SaveRequestToEditHistoryDialogComponent,]
})
export class SaveRequestToEditHistoryModule { }
