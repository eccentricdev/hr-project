import { Component, OnInit, ChangeDetectionStrategy, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, delay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelProfileCategoriesService } from 'src/app/core/service/register/personnel-profile-categories.service';
import { PersonnelUpdateHistoriesService } from 'src/app/core/service/register/personnel-update-histories.service';
import { SaveRequestToEditHistoryDialogComponent } from '../save-request-to-edit-history-dialog/save-request-to-edit-history-dialog.component';
import swal from 'sweetalert2'
import { PersonnelUpdateDetailsService } from 'src/app/core/service/register/personnel-update-details.service';

@Component({
  selector: 'app-save-request-to-edit-history-form',
  templateUrl: './save-request-to-edit-history-form.component.html',
  styleUrls: ['./save-request-to-edit-history-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveRequestToEditHistoryFormComponent extends BaseForm implements OnInit,OnDestroy {
  rows: any = []
  personnels$ = new Observable()
  position$ = new Observable()
  agencys$ = new Observable()
  approveStatuss$ = new Observable()
  agency_uid: any
  personnelProfileCategories$ = new Observable()
  employeeData: any = []
  unSubAll$ = new Subject<boolean>()
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  @ViewChild('service_life') service_life: ElementRef<HTMLInputElement>

  constructor(public dialog: MatDialog,
    public FormBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PersonnelUpdateHistoriesSV: PersonnelUpdateHistoriesService,
    public PersonnelUpdateDetailsSV: PersonnelUpdateDetailsService,
    public PersonnelProfileCategoriesSV: PersonnelProfileCategoriesService,
    public ApproveStatussSV: ApproveStatussService,
    public EmployeeSV: EmployeeService,
    public PositionSV: PositionService,
    public AgencysSV: AgencysService,
    public router: Router) {
    super(FormBuilder, activeRoute)
    this.personnels$ = this.EmployeeSV.getAll()
    this.position$ = this.PositionSV.getAll()
    this.agencys$ = this.AgencysSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
    this.personnelProfileCategories$ = this.PersonnelProfileCategoriesSV.getAll()
  }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.PersonnelUpdateHistoriesSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if(x == ''){
          this.form.get('personnel_uid').setValue(null,{ emitEvent: false })
          this.form.get('personnel_no').setValue('',{ emitEvent: false })
          this.form.get('citizen_id').setValue(null,{ emitEvent: false })
          this.form.get('display_name_th').setValue('',{ emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null,{ emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      personnel_update_history_uid: res.personnel_update_history_uid,
      personnel_uid: res.personnel_uid,
      document_no: res.document_no,
      document_date: res.document_date,
      approve_status: res.approve_status,
      request_date: res.request_date,

    })
    if (res.personnel_update_detail) {
      res.personnel_update_detail.forEach((personnel_update_detail) => {
        let itemsArray = this.form.get('personnel_update_detail') as FormArray
        itemsArray.push(this.personnel_update_detail_form(personnel_update_detail))
      })
    }
    this.select_personnels(res.personnel_uid)
  }
  select_personnels(data) {
    console.log(data)
    this.EmployeeSV.get(data).pipe(
      tap(x => console.log(x)),
      tap((x: any) => {
        let todayDate = moment()
        if(x.birth_date == null){
          this.age.nativeElement.value = 'ไม่พบข้อมูล'
          
        }else{
          let calculateAge = todayDate.diff(x.birth_date, 'years')
          this.age.nativeElement.value = calculateAge.toString()
        }
        this.position.nativeElement.value = x.personnel_hires[0]?.position?.position_name_th == undefined ? 'ไม่มีข้อมูล' : x.personnel_hires[0]?.position?.position_name_th

        let startDate = moment()
        let calculateStartday = startDate.diff(x.personnel_hires[0].start_date, 'day')
        let calculateStartM = startDate.diff(x.personnel_hires[0].start_date, 'months')
        let calculateStartY = startDate.diff(x.personnel_hires[0].start_date, 'years')

        console.log(calculateStartday)
        console.log(calculateStartM)
        console.log(calculateStartY)
        if(x.personnel_hires[0].start_date == null){
          this.service_life.nativeElement.value = 'ไม่พบข้อมูล'
        }else{
          this.service_life.nativeElement.value = `${calculateStartday+`วัน`+calculateStartM+`เดือน`+calculateStartY+`ปี`}`
          this.form.get('start_date').setValue(x.personnel_hires[0].start_date)
        }

        this.form.get('personnel_uid').setValue(x.personnel_uid)
        this.form.get('personnel_no').setValue(x.personnel_no)
        this.form.get('citizen_id').setValue(x.citizen_id)
        this.form.get('display_name_th').setValue(x.display_name_th)

        this.agency_uid = x.personnel_hires[0].agency_uid

      }),
    ).subscribe()

  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }
  openDialog() {
    const dialogRef = this.dialog.open(
      SaveRequestToEditHistoryDialogComponent, {
      width: '80%',
      disableClose: true,
      data: this.form  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }
  close() {
    this.router.navigate(['app/pr_edit_profile_registration'])
  }
  save() {
    switch (this.state) {
      case 'edit':

        this.PersonnelUpdateHistoriesSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_edit_profile_registration'])
          })
        break;
      case 'add':

        this.PersonnelUpdateHistoriesSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_edit_profile_registration'])
          })
        break;
    }

  }

  del_personnel_update_detail_form(i,id){
    console.log(i);
    if(id != null){
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_update_detail')).removeAt(i)
            this.PersonnelUpdateDetailsSV.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
              
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
  
          }
        })
      }
    }else{
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
  
          if (result.value) {
            (<FormArray>this.form.get('personnel_update_detail')).removeAt(i)
            this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
          }
        })
      }
    }
    
    
  }
  personnel_update_detail_form(element?) {
    if (element == undefined) {
      return this.FormBuilder.group({
        created_by: [null],
        created_datetime: [null],
        update_by: [null],
        update_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        personnel_update_detail_uid: [null],
        personnel_profile_category_id: [null],
        personnel_update_history_uid: [null],
        remark: [''],
        first_name_th: [''],
        middle_name_th: [''],
        last_name_th: [''],
        first_name_en: [''],
        middle_name_en: [''],
        last_name_en: [''],
        file_name: [''],
        document_name: [''],
        document_url: [''],
        mime_type_name: ['']
      })
    } else {
      return this.FormBuilder.group({
        created_by: element.created_by,
        created_datetime: element.created_datetime,
        update_by: element.update_by,
        updated_by: element.updated_by,
        updated_datetime: element.updated_datetime,
        personnel_update_detail_uid: element.personnel_update_detail_uid,
        personnel_profile_category_id: element.personnel_profile_category_id,
        personnel_update_history_uid: element.personnel_update_history_uid,
        remark: element.remark,
        first_name_th: element.first_name_th,
        middle_name_th: element.middle_name_th,
        last_name_th: element.last_name_th,
        first_name_en: element.first_name_en,
        middle_name_en: element.middle_name_en,
        last_name_en: element.last_name_en,
        file_name: element.file_name,
        document_name: element.document_name,
        document_url: element.document_url,
        mime_type_name: element.mime_type_name,
      })
    }
  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      personnel_update_history_uid: [null],
      personnel_uid: [null],
      document_no: [''],
      document_date: [null],
      approve_status: [true],
      request_date: [null],
      start_date: [null],
      personnel_no: [null],
      citizen_id: [''],
      display_name_th: [''],
      personnel_update_detail: this.baseFormBuilder.array([]),
    })
  }
  ngOnDestroy(): void {
    this.unSubAll$.next(true)
    this.unSubAll$.complete()
  }
}
