import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveRequestToEditHistoryFormComponent } from './save-request-to-edit-history-form.component';

describe('SaveRequestToEditHistoryFormComponent', () => {
  let component: SaveRequestToEditHistoryFormComponent;
  let fixture: ComponentFixture<SaveRequestToEditHistoryFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveRequestToEditHistoryFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveRequestToEditHistoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
