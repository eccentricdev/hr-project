import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ExitTypeService } from 'src/app/core/service/common/exit-type.service';

@Component({
  selector: 'app-set-type-termination-form',
  templateUrl: './set-type-termination-form.component.html',
  styleUrls: ['./set-type-termination-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypeTerminationFormComponent extends BaseForm implements OnInit {

  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public ExitTypeSV: ExitTypeService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.ExitTypeSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      exit_type_id:res.exit_type_id,
      exit_type_code:res.exit_type_code,
      exit_type_name_th:res.exit_type_name_th,
      exit_type_name_en:res.exit_type_name_en,
    })
  }
  close(){
    this.router.navigate(['app/pr_set_type_termination'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.ExitTypeSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_type_termination'])
          })
        break;
      case 'add':
      
        this.ExitTypeSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_set_type_termination'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      exit_type_id: [null],
      exit_type_code: [''],
      exit_type_name_th: [''],
      exit_type_name_en: [''],
    })
  }
}
