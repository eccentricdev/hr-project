import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTypeTerminationFormComponent } from './set-type-termination-form.component';

describe('SetTypeTerminationFormComponent', () => {
  let component: SetTypeTerminationFormComponent;
  let fixture: ComponentFixture<SetTypeTerminationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetTypeTerminationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTypeTerminationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
