import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { ExitTypeService } from 'src/app/core/service/common/exit-type.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-type-termination-list',
  templateUrl: './set-type-termination-list.component.html',
  styleUrls: ['./set-type-termination-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetTypeTerminationListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  exitType$ = new Observable()
  req = {
    exit_type_code : '',
    exit_type_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public ExitTypeSV : ExitTypeService,
  ) { super()
    this.exitType$ = this.ExitTypeSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.exitType$  = this.ExitTypeSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      exit_type_code : '',
      exit_type_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/pr_set_type_termination/add'])
  }
  editPage(id){
    this.router.navigate(['app/pr_set_type_termination/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        
        if (result.value) {
        
          this.ExitTypeSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.exitType$ = this.ExitTypeSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
