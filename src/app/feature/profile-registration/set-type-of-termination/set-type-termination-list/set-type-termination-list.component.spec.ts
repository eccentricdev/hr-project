import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTypeTerminationListComponent } from './set-type-termination-list.component';

describe('SetTypeTerminationListComponent', () => {
  let component: SetTypeTerminationListComponent;
  let fixture: ComponentFixture<SetTypeTerminationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetTypeTerminationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTypeTerminationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
