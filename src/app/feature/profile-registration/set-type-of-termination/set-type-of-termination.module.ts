import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetTypeTerminationListComponent } from './set-type-termination-list/set-type-termination-list.component';
import { SetTypeTerminationFormComponent } from './set-type-termination-form/set-type-termination-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetTypeTerminationListComponent
  },
  {
    path:'add',
    component:SetTypeTerminationFormComponent
  },
  {
    path:'edit/:id',
    component:SetTypeTerminationFormComponent
  },
  
]

@NgModule({
  declarations: [SetTypeTerminationListComponent, SetTypeTerminationFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetTypeOfTerminationModule { }
