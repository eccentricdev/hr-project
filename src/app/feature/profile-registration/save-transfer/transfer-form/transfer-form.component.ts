import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, debounceTime, delay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ApproveStatussService } from 'src/app/core/service/common/approve-statuss.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { EmployeeService } from 'src/app/core/service/register/employee.service';
import { PersonnelTransferService } from 'src/app/core/service/register/personnel-transfer.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransferFormComponent extends BaseForm implements OnInit, OnDestroy {
  rows: any = []
  // personnel :any = []
  position$ = new Observable()
  agencys$ = new Observable()
  personnels$ = new Observable()
  approveStatuss$ = new Observable()
  unSubAll$ = new Subject<void>();
  employeeData: any = []
  agency_uid: any
  @ViewChild('age') age: ElementRef<HTMLInputElement>
  @ViewChild('position') position: ElementRef<HTMLInputElement>
  @ViewChild('service_life') service_life: ElementRef<HTMLInputElement>
  
  constructor(public dialog: MatDialog,
    public FormBuilder: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PersonnelTransferSV: PersonnelTransferService,
    public PositionSV: PositionService,
    public AgencysSV: AgencysService,
    public EmployeeSV: EmployeeService,
    public ApproveStatussSV: ApproveStatussService,
    private cdRef: ChangeDetectorRef,
    public router: Router) {
      super(FormBuilder, activeRoute)
    this.position$ = this.PositionSV.getAll()
    this.agencys$ = this.AgencysSV.getAll()
    this.personnels$ = this.EmployeeSV.getAll()
    this.approveStatuss$ = this.ApproveStatussSV.getAll()
  }

  ngOnInit(): void {



    switch (this.state) {
      case 'edit':
        this.PersonnelTransferSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
    this.form.get('citizen_id').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if (x == '') {
          this.form.get('personnel_uid').setValue(null, { emitEvent: false })
          this.form.get('personnel_no').setValue('', { emitEvent: false })
          this.form.get('citizen_id').setValue(null, { emitEvent: false })
          this.form.get('display_name_th').setValue('', { emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null, { emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?citizen_id=${text}`)),
      tap((employee: any) => {
        console.log(employee)
        this.employeeData = []
      }),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('personnel_no').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if (x == '') {
          this.form.get('personnel_uid').setValue(null, { emitEvent: false })
          this.form.get('personnel_no').setValue('', { emitEvent: false })
          this.form.get('citizen_id').setValue(null, { emitEvent: false })
          this.form.get('display_name_th').setValue('', { emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null, { emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?personnel_no=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    this.form.get('display_name_th').valueChanges.pipe(
      delay(1000),
      tap((x: any) => {
        if (x == '') {
          this.form.get('personnel_uid').setValue(null, { emitEvent: false })
          this.form.get('personnel_no').setValue('', { emitEvent: false })
          this.form.get('citizen_id').setValue(null, { emitEvent: false })
          this.form.get('display_name_th').setValue('', { emitEvent: false })
          this.age.nativeElement.value = ''
          
          this.service_life.nativeElement.value = ''
          this.form.get('start_date').setValue(null, { emitEvent: false })
          this.agency_uid = null
        }
      }),
      switchMap(text => this.EmployeeSV.query(`?display_name_th=${text}`)),
      tap((employee: any) => this.employeeData = []),
      tap((employee: any) => this.employeeData = [...employee]),
      takeUntil(this.unSubAll$)
    ).subscribe()

    // this.form.get('personnel_name').valueChanges.pipe(
    //   debounceTime(500),
    //   switchMap(text => this.EmployeeSV.query(`?search=${text}`)),
    //   tap((country: any) => this.personnel = [...country]),
    //   tap((country: any) => this.cdRef.detectChanges()),
    //   takeUntil(this.unSubAll$)
    // ).subscribe()


  }
  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id: res.status_id,
      created_by: res.created_by,
      created_datetime: res.created_datetime,
      updated_by: res.updated_by,
      updated_datetime: res.updated_datetime,
      personnel_transfer_uid: res.personnel_transfer_uid,
      personnel_uid: res.personnel_uid,
      transfer_date: res.transfer_date,
      agency_uid: res.agency_uid,
      position_uid: res.position_uid,
      manpower_uid: res.manpower_uid,
      transfer_document_no: res.transfer_document_no,
      transfer_order_date: res.transfer_order_date,
      remark: res.remark,
      order_document_no: res.order_document_no,
      approve_status: res.approve_status,
    })
    this.select_personnels(res.personnel_uid)
  }

  select_personnels(data) {
    console.log(data)
    this.EmployeeSV.get(data).pipe(
      tap(x => console.log(x)),
      tap((x: any) => {
        let todayDate = moment()

        if (x.birth_date == null) {
          this.age.nativeElement.value = 'ไม่พบข้อมูล'
          
        } else {
          let calculateAge = todayDate.diff(x.birth_date, 'years')
          this.age.nativeElement.value = calculateAge.toString()
        }

        let startDate = moment()
        let calculateStartday = startDate.diff(x.personnel_hires[0].start_date, 'day')
        let calculateStartM = startDate.diff(x.personnel_hires[0].start_date, 'months')
        let calculateStartY = startDate.diff(x.personnel_hires[0].start_date, 'years')

        console.log(calculateStartday)
        console.log(calculateStartM)
        console.log(calculateStartY)
        if (x.personnel_hires[0].start_date == null) {
          this.service_life.nativeElement.value = 'ไม่พบข้อมูล'
        } else {
          this.service_life.nativeElement.value = `${calculateStartday + `วัน` + calculateStartM + `เดือน` + calculateStartY + `ปี`}`
          this.form.get('start_date').setValue(x.personnel_hires[0].start_date)
        }

        this.form.get('personnel_uid').setValue(x.personnel_uid)
        this.form.get('personnel_no').setValue(x.personnel_no)
        this.form.get('citizen_id').setValue(x.citizen_id)
        this.form.get('display_name_th').setValue(x.display_name_th)

        this.agency_uid = x.personnel_hires[0].agency_uid

      }),
    ).subscribe()

  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }
  close() {
    this.router.navigate(['app/pr_transfer'])
  }
  save() {
    switch (this.state) {
      case 'edit':

        this.PersonnelTransferSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_transfer'])
          })
        break;
      case 'add':

        this.PersonnelTransferSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/pr_transfer'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      personnel_transfer_uid: [null],
      personnel_uid: [null],
      personnel_name: [''],
      transfer_date: [null],
      agency_uid: [null],
      position_uid: [null],
      manpower_uid: [null],
      transfer_document_no: [''],
      transfer_order_date: [null],
      remark: [''],
      start_date: [null],
      approve_status: [null],
      order_document_no: [''],
      personnel_no: [null],
      citizen_id: [''],
      display_name_th: [''],
    })
  }
  ngOnDestroy() {
    this.unSubAll$.next();
    this.unSubAll$.complete();
  }
}
