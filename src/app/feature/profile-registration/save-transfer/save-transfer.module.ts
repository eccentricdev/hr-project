import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferListComponent } from './transfer-list/transfer-list.component';
import { TransferFormComponent } from './transfer-form/transfer-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:TransferListComponent
  },
  {
    path:'add',
    component:TransferFormComponent
  },
  {
    path:'edit/:id',
    component:TransferFormComponent
  },
  
]

@NgModule({
  declarations: [TransferListComponent, TransferFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveTransferModule { }
