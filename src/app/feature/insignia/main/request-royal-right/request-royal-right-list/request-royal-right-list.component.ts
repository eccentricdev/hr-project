import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaEligibleProcessingService } from 'src/app/core/service/insignia/insignia-eligible-processing.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-request-royal-right-list',
  templateUrl: './request-royal-right-list.component.html',
  styleUrls: ['./request-royal-right-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestRoyalRightListComponent extends BaseList implements OnInit {

  organization$ = new Observable()
  agencys$ = new Observable()
  rows:any = []
  dataSearch:any = ''

  constructor(public router : Router,
    public appService : AppService,
    public organizationDataService : OrganizationDataService,
    public agencysService : AgencysService,
    public insigniaEligibleProcessingService : InsigniaEligibleProcessingService) {
    super();
    this.organization$ = this.organizationDataService.getAll()
    this.agencys$ = this.agencysService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.insigniaEligibleProcessingService.getAll().pipe(
      tap(x => console.log(x)),
      map((x:any) => x.filter(x => x.insignia_category_id == 1)),
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
  }

  search(){
    // console.log(this.dataSearch);
      setTimeout(() => {
        this.insigniaEligibleProcessingService.query(`?search=${this.dataSearch}`).pipe(
          tap((x:any) => x.filter(x => x.insignia_category_id == 2)),
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/request_royal_right/add']) 
  }
  editPage(id){
    this.router.navigate(['app/request_royal_right/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.insigniaEligibleProcessingService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              // console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
