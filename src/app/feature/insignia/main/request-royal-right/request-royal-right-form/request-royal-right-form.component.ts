import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaEligiblePersonnelService } from 'src/app/core/service/insignia/insignia-eligible-personnel.service';
import { InsigniaEligibleProcessingService } from 'src/app/core/service/insignia/insignia-eligible-processing.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';


@Component({
  selector: 'app-request-royal-right-form',
  templateUrl: './request-royal-right-form.component.html',
  styleUrls: ['./request-royal-right-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestRoyalRightFormComponent extends BaseForm implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  agencys$ = new Observable()
  organization$ = new Observable()
  processYear$ = new Observable()
  insignias$ = new Observable()

  dataEdit: any
  requestDatas:any = []
  levels = []


  displayedColumns: string[] = ['select','1','2','3','4','5','6','7','8','9','10','11'];
  selection = new SelectionModel<any>(true, []);
  rows: any = []
  


  constructor(public formBuilder: FormBuilder,
    public agencysService: AgencysService,
    public organizationDataService: OrganizationDataService,
    public insigniaEligiblePersonnelService: InsigniaEligiblePersonnelService,
    public insigniaEligibleProcessingService: InsigniaEligibleProcessingService,
    public insigniaDetailService : InsigniaDetailService,
    public positionLevelsService : PositionLevelsService,
    public router: Router,
    public cdRef: ChangeDetectorRef,
    public activeRoute: ActivatedRoute,
    public appSV: AppService) {
    super(formBuilder, activeRoute);
    this.agencys$ = this.agencysService.getAll()
    this.organization$ = this.organizationDataService.getAll()
    this.processYear$ = this.insigniaEligibleProcessingService.getProcess_date()
    this.insignias$ = this.insigniaDetailService.query('/all')
    this.positionLevelsService.getAll().pipe(
      tap(x => this.levels = x)
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.insigniaEligibleProcessingService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x[0]),
          tap(() => {
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              search: this.dataEdit.search,
              insignia_eligible_processing_uid: this.dataEdit.insignia_eligible_processing_uid,
              insignia_category_id: this.dataEdit.insignia_category_id,
              document_no: this.dataEdit.document_no,
              processing_date: this.dataEdit.processing_date,
              organization_uid: this.dataEdit.organization_uid,
              agency_uid: this.dataEdit.agency_uid,
            })
            this.getResultArray(this.dataEdit.insignia_eligible_personnel)
          }),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getResultArray(array) {
    // console.log(array);
    this.rows = array
    this.rows = new MatTableDataSource(this.rows);
    this.rows.sort = this.sort;
    this.rows.paginator = this.paginator;
    this.cdRef.detectChanges()
  }

  getProcess() {
    console.log(this.form.getRawValue());
    let item = {
      processing_date: this.form.value.processing_date,
      organization_uid: this.form.value.organization_uid,
      agency_uid: this.form.value.agency_uid
    }
    // console.log(item);
    this.insigniaEligibleProcessingService.add_insignia(item).pipe(
      catchError(err => {
        if (err.error.err == 400) {
          this.appSV.swaltAlertError(err.error.msg)
        } else {
          this.appSV.swaltAlertError('Error')
        }
        return throwError(err)
      })).subscribe((x: any) => {
        console.log()
        this.router.navigate(['app/request_royal_right/edit',x.insignia_eligible_processing_uid])
        this.cdRef.detectChanges()
      })

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.rows.data.length;
    return numSelected === numRows;
  }


  masterToggle(check?) {
    console.log(check);
    this.isAllSelected() ?  this.selection.clear() :  this.rows.data.forEach(row => this.selection.select(row));
    if(check){
      this.requestDatas = []
      // console.log(this.rows.data);
      this.rows.data.forEach(element => {
        // console.log(element);
        this.requestDatas.push(element.insignia_request_data)
      });
      console.log(this.requestDatas);
    } else{
      this.requestDatas = []
      console.log(this.requestDatas);
    }
        
  }

  checkboxLabel(row?: any): string {
    console.log(row);
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  }

  selectList(check, item) {
    // console.log(check);
    // console.log(item);
    if (check) {
      this.requestDatas.push(item.insignia_request_data)
      // console.log(this.requestDatas);
    } else {
      let index = this.requestDatas.findIndex(x => x.insignia_eligible_processing_uid == item.insignia_eligible_processing_uid)
      this.requestDatas.splice(index, 1)
      // console.log(this.requestDatas);
    }
  }


  //สร้างคำขอรับเครื่องราช
  getRequest(el){
    let item = {
      insignia_eligible_processing: null,
      insignia_eligible_personnel_uid: el.insignia_eligible_personnel_uid,
      insignia_eligible_processing_uid: el.insignia_eligible_processing_uid,
      processing_date: el.processing_date,
      personnel_uid: el.personnel_uid,
      corresponding_insignia_uid: el.corresponding_insignia_uid,
      insignia_request_condition_uid: el.insignia_request_condition_uid,
      current_position_uid: el.current_position_uid,
      current_position_level_id: el.elcurrent_position_level_id,
      current_insignia_uid: null,
      current_insignia_received_date: null,
      request_status: null,
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null
    }
    // console.log(item);
    
  }


  createRequest(){
    // console.log(this.requestDatas);
    this.insigniaEligibleProcessingService.add_request(this.requestDatas).pipe(
      catchError(err => {
        this.appSV.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appSV.swaltAlert()
        this.router.navigate(['app/present_royal'])
      })
  }


  // save() {
  //   console.log('form', this.form.getRawValue());
  //   switch (this.state) {
  //     case 'edit':
  //       this.insigniaEligibleProcessingService.update(this.form.getRawValue()).pipe(
  //         catchError(err => {
  //           this.appSV.swaltAlertError('Error',err.error.msg)
  //           return throwError(err)
  //         })).subscribe((x: any) => {
  //           console.log(x)
  //           this.appSV.swaltAlert()
  //           this.router.navigate(['app/request_royal_right'])
  //         })
  //       break;
  //     case 'add':
  //       // this.insigniaEligibleProcessingService.add(this.form.getRawValue()).pipe(
  //       //   catchError(err => {
  //       //     this.appSV.swaltAlertError('Error')
  //       //     return throwError(err)
  //       //   })).subscribe((x: any) => {
  //       //     console.log(x)
  //       //     this.appSV.swaltAlert()
  //       //     this.router.navigate(['app/request_royal_right'])
  //       //   })
  //         this.insigniaEligibleProcessingService.add(this.form.getRawValue()).pipe(
  //           catchError(err => {
  //             // alert ตรงนี่
  //             if (err.error.err == 400) {
  //               this.appSV.swaltAlertError('', err.error.msg)
  //             } else {
  //               this.appSV.swaltAlertError('', 'Error')
  //             }
  //             return throwError(err)
  //           })).subscribe((x: any) => {
  //             console.log(x)
  //             // this.SubjectComponent.ngOnInit()
  //             this.appSV.swaltAlert()
  //             this.router.navigate(['app/request_royal_right'])
  //           })
  //       break;
  //   }

  // }

  close() {
    this.router.navigate(['app/request_royal_right'])
  }



  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      search: [''],
      insignia_eligible_processing_uid: [null],
      insignia_category_id: [1],
      document_no: [''],
      processing_date: [null],
      organization_uid: [null],
      agency_uid: [null],
      insignia_eligible_personnel: this.baseFormBuilder.array([])
    })
  }
}
