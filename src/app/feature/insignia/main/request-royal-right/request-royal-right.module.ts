import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestRoyalRightListComponent } from './request-royal-right-list/request-royal-right-list.component';
import { RequestRoyalRightFormComponent } from './request-royal-right-form/request-royal-right-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:RequestRoyalRightListComponent
  },
  {
    path:'add',
    component:RequestRoyalRightFormComponent
  },
  {
    path:'edit/:id',
    component:RequestRoyalRightFormComponent
  },
  
]

@NgModule({
  declarations: [
    RequestRoyalRightListComponent, 
    RequestRoyalRightFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class RequestRoyalRightModule { }
