import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { timeStamp } from 'console';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaEligibleProcessingService } from 'src/app/core/service/insignia/insignia-eligible-processing.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { SaveRequestToEditHistoryDialogComponent } from 'src/app/feature/profile-registration/save-request-to-edit-history/save-request-to-edit-history-dialog/save-request-to-edit-history-dialog.component';

@Component({
  selector: 'app-save-recive-royal-group-form',
  templateUrl: './save-recive-royal-group-form.component.html',
  styleUrls: ['./save-recive-royal-group-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveReciveRoyalGroupFormComponent extends BaseForm implements OnInit {

  dateProcess$ = new Observable()
  insignias$ = new Observable()

  detail = {
    government_gazette_version_no: '',
    government_gazette_parts_no: '',
    government_gazette_page_no: '',
    announcement_date: null
  }

  dataType: any = null
  dataEdit: any
  dataPerson: any
  personType: any = []
  insigniaDetails: any = []
  year:any
  dataRequest:any = []
  
  
  

  constructor(public formBuilder: FormBuilder,
    public insigniaEligibleProcessingService : InsigniaEligibleProcessingService,
    public insigniaAssignService: InsigniaAssignService,
    public insigniaDetailService: InsigniaDetailService,
    public insigniaRequestService : InsigniaRequestService,
    public dialog: MatDialog,
    public router: Router,
    public cdRef: ChangeDetectorRef,
    public activeRoute: ActivatedRoute,
    public personnelService: PersonnelService,
    public appSV: AppService) {
    super(formBuilder, activeRoute);
    this.insignias$ = this.insigniaRequestService.query('/list').pipe(
      tap(x => console.log(x)),
    )
    this.dateProcess$ = this.insigniaEligibleProcessingService.getProcess_date()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.insigniaAssignService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          tap(() => {
            this.form.patchValue({
              bestow_date: this.dataEdit.bestow_date,
              version_no: this.dataEdit.version_no,
              parts_no: this.dataEdit.parts_no,
              page_no: this.dataEdit.page_no,
            })
            // if (this.dataEdit.value.insignia_assign_bestow) {
            //   this.dataEdit.value.insignia_assign_bestow.forEach(element => {
            //     let formarray = this.form.get('insignia_assign_bestow') as FormArray
            //     formarray.push(this.getDetailArray(element))
            //   });
            // }
          })
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  getPerson(){
    this.insigniaRequestService.get_date(this.year).pipe(
      tap(x => console.log(x)),
      tap((x:any) => {
        this.dataPerson = x
        let data = x.filter(x => x.sign_status &&  !x.received_status)
        data.forEach(element => {
          console.log(element);
          const control = <FormArray>this.form.get('insignia_request_uid');
          control.push(this.formBuilder.control(element.insignia_request_uid))
        });
      //  this.form.value.insignia_request_uid.forEach(el => {
      //       let rq = this.dataPerson.find(x => x.insignia_request_uid == el)
      //       console.log(rq);
      //       this.dataRequest.push(rq)
      //   })
      // console.log(this.dataRequest);
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  

  setValue(){

  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        break;
      case 'add':
        this.insigniaRequestService.add_group(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/save_recive_royal'])
          })
        break;
    }
  }

  close() {
    this.router.navigate(['app/save_recive_royal'])
  }

  // addArray(){
  //   let formarray = this.form.get('insignia_assign_bestow') as FormArray
  //   formarray.push(this.createArrayDetail())
  // }


  
  

  // getDetailArray(el) {
  //   return this.baseFormBuilder.group({
  //     insignia_detail_uid: el.insignia_detail_uid,
  //     bestow_date: el.bestow_date,
  //     current_position_uid: el.current_position_uid,
  //     current_position_level_uid: el.current_position_level_uid,
  //     government_gazette_version_no: el.government_gazette_version_no,
  //     government_gazette_parts_no: el.government_gazette_parts_no,
  //     government_gazette_page_no: el.government_gazette_page_no,
  //     announcement_date: el.announcement_date,
  //     license_receive_status: el.license_receive_status,
  //     license_receive_date: el.license_receive_date,
  //     insignia_receive_status: el.insignia_receive_status,
  //     insignia_receive_date: el.insignia_receive_date,
  //   })
  // }

  createForm() {
    return this.baseFormBuilder.group({
      bestow_date: null,
      version_no: '',
      parts_no: '',
      page_no: '',
      insignia_request_uid:this.baseFormBuilder.array([])
    })
  }



}
