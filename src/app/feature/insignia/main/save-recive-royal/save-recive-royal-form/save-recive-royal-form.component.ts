import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaPersonnelsService } from 'src/app/core/service/insignia/insignia-personnels.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupInsigniaListComponent } from '../popup-insignia-list/popup-insignia-list.component';

@Component({
  selector: 'app-save-recive-royal-form',
  templateUrl: './save-recive-royal-form.component.html',
  styleUrls: ['./save-recive-royal-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveReciveRoyalFormComponent extends BaseForm implements OnInit {

  personnel$ = new Observable()
  insigniaRequest$ = new Observable()

  positions: any = []
  personType: any = []
  insigniaDetails: any = []
  assignData: any = []
  personIn: any = []
  personOut: any = []

  dataType: any = null
  dataEdit: any
  dataPerson: any = null
  dataCitizen: any = null
  personalId: any = null
  citizenId: any = null
  manNo: any = null
  datapersonId: any = null
  resultAssign: any
  personnel_in_house: any = null



  typePerson = [{ check: true, name: 'บุคคลภายใน' }, { check: false, name: 'บุคคลภายนอก' }]

  constructor(public formBuilder: FormBuilder,
    public insigniaAssignService: InsigniaAssignService,
    public insigniaDetailService: InsigniaDetailService,
    public insigniaPersonnelsService: InsigniaPersonnelsService,
    public positionService: PositionService,
    public dialog: MatDialog,
    public router: Router,
    public cdRef: ChangeDetectorRef,
    public personnelTypesService: PersonnelTypesService,
    public insigniaRequestService: InsigniaRequestService,
    public activeRoute: ActivatedRoute,
    public personnelService: PersonnelService,
    public appSV: AppService) {
    super(formBuilder, activeRoute);

    this.insigniaRequestService.query('/list').pipe(
      // tap(x => console.log(x)),
      map((x: any) => x.filter(x => x.sign_status == true && !x.received_status)),
      // tap(x => console.log(x)),
      tap(x => {
        this.personIn = x.filter(x => x.personnel_in_house == true)
        this.personOut = x.filter(x => x.personnel_in_house == false)
        // console.log(this.personIn);
        // console.log(this.personOut);
      })
    ).subscribe()

    this.insigniaAssignService.getAll().pipe(
      tap(x => this.assignData = x)
    ).subscribe()

    this.positionService.getAll().pipe(
      tap(x => this.positions = x)
    ).subscribe()

    this.personnelTypesService.getAll().pipe(
      tap(x => this.personType = x)
    ).subscribe()

    // this.insigniaPersonnelsService.query('/list').pipe(
    //   tap(x => console.log(x)),
    //   // tap(x => this.insigniaDetails = x)
    // ).subscribe()

    this.insigniaDetailService.query('/all').pipe(
      tap(x => this.insigniaDetails = x)
    ).subscribe()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        // this.form.get('personnel_in_house').disable()
        // this.form.get('personnel_in_house').disable()
        this.insigniaPersonnelsService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          concatMap(() => {
            return this.insigniaRequestService.query('/list').pipe(
              // tap(x => console.log(x)),
              map((x: any) => x.filter(x => x.sign_status == true)),
              // tap(x => console.log(x)),
              tap(x => {
                this.personIn = x.filter(x => x.personnel_in_house == true)
                this.personOut = x.filter(x => x.personnel_in_house == false)
                // console.log(this.personIn);
                // console.log(this.personOut);
              })
            )
          }),
          // tap(() => {
          //   if (this.dataEdit.personnel_in_house) {
          //     this.getPersonnelIn(this.dataEdit.personnel_uid)
          //   } else {
          //     return this.insigniaPersonnelsService.get_Citizen(this.dataEdit.outsider_citizen_id).pipe(
          //       tap(x => console.log(x)),
          //       tap(x => {
          //         let data = x[0]
          //         this.citizenId = data.insignia_holder[0]?.insignia_assign?.insignia_request_uid
          //         this.getPersonOut(this.citizenId)
          //       })
          //     )
          //   }
          // }),
          concatMap(() => {
            console.log(this.dataEdit.personnel_in_house);
            if (this.dataEdit.personnel_in_house) {
              return this.insigniaPersonnelsService.get_Person(this.dataEdit.personnel_uid).pipe(
                tap(x => console.log(x)),
                tap((x: any) => {
                  if (x != 0) {
                    this.form.get('insignia_request_guid').setValue(x[0]?.insignia_holder[0]?.insignia_assign?.insignia_request_uid)
                    // this.personalId = x[0]?.insignia_holder[0]?.insignia_assign?.insignia_request_uid
                    this.resultAssign = x[0]?.insignia_holder
                    this.manNo = x[0]?.personnel?.manpower_no
                    this.personnelService.get(this.dataEdit.personnel_uid).pipe(
                      tap(x => console.log(x)),
                      tap(x => this.dataPerson = x),
                      tap(() => this.cdRef.detectChanges())
                    ).subscribe()
                  }
                })
              )
              // this.insigniaPersonnelsService.get_Person(this.dataEdit.personnel_uid).pipe(
              //   tap(x => console.log(x)),
              //   tap(x => {
              //     let data = x[0]
              //     this.personalId = data.insignia_holder[0]?.insignia_assign?.insignia_request_uid
              //     this.getPersonIn(this.personalId)
              //   })
              // )
            } else {
              return this.insigniaPersonnelsService.get_Citizen(this.dataEdit.outsider_citizen_id).pipe(
                tap(x => console.log(x)),
                tap(x => {
                  let data = x[0]
                  this.form.get('insignia_request_guid').setValue(data.insignia_holder[0]?.insignia_assign?.insignia_request_uid)
                  // this.citizenId = data.insignia_holder[0]?.insignia_assign?.insignia_request_uid
                  this.dataCitizen = data?.insignia_personnel
                  this.resultAssign = data?.insignia_holder
                  console.log(this.resultAssign);

                  // this.getPersonOut(this.citizenId)
                })
              )
            }
          }),
          tap(() => {
            this.personnel_in_house = this.dataEdit.personnel_in_house

            // this.form.patchValue({
            //     insignia_personnel_uid: this.dataEdit.insignia_personnel_uid,
            //     personnel_in_house: this.dataEdit.personnel_in_house,
            //     personnel_uid: this.dataEdit.personnel_uid,
            //     outsider_prefix_id: this.dataEdit.outsider_prefix_id,
            //     outsider_gender_id: this.dataEdit.outsider_gender_id,
            //     outsider_name: this.dataEdit.outsider_name,
            //     outsider_citizen_id: this.dataEdit.outsider_citizen_id,
            //     outsider_position_uid: this.dataEdit.outsider_position_uid,
            //     insignia_uid: this.dataEdit.insignia_uid,
            //     insignia_last_receive_date: this.dataEdit.insignia_last_receive_date
            // })
          }),
          tap(() => this.cdRef.detectChanges())
        ).subscribe()
        break;
      case 'add':
        break;
    }
  }

  // pathValuePerson(el) {
  //   this.formPersson.patchValue({
  //     insignia_personnel_uid: el.insignia_personnel_uid,
  //     personnel_in_house: el.personnel_in_house,
  //     personnel_uid: el.personnel_uid,
  //     outsider_prefix_id: el.outsider_prefix_id,
  //     outsider_gender_id: el.outsider_gender_id,
  //     outsider_name: el.outsider_name,
  //     outsider_citizen_id: el.outsider_citizen_id,
  //     outsider_position_uid: el.outsider_position_uid,
  //     insignia_uid: el.insignia_uid,
  //     insignia_last_receive_date: el.insignia_last_receive_date
  //   })
  // }

  // setTypePersonnel(value) {
  //   if (value) {
  //     this.form.patchValue({
  //       outsider_name: '',
  //       outsider_citizen_id: '',
  //       outsider_position_name: '',
  //     })
  //   } else {
  //     this.form.get('insignia_personnel_uid').setValue(null)
  //     this.form.get('personnel_uid').setValue(null)
  //   }
  // }

  // getPersonnelIn(id){
  //   this.insigniaPersonnelsService.get_Person(id).pipe(
  //     tap(x => console.log(x)),
  //     tap((x: any) => {
  //       if (x != 0) {
  //         this.resultAssign = x[0]?.insignia_holder
  //         this.manNo = x[0]?.personnel?.manpower_no
  //         // this.form.get('insignia_personnel_uid').setValue(x[0].insignia_personnel?.insignia_personnel_uid)
  //         // this.form.get('personnel_uid').setValue(x[0].insignia_personnel?.personnel_uid)
  //         this.personnelService.get(this.form.value.personnel_uid).pipe(
  //           tap(x => console.log(x)),
  //           tap(x => this.dataPerson = x),
  //           tap(() => this.cdRef.detectChanges())
  //         ).subscribe()
  //       } else {
  //         this.personnelService.get(this.datapersonId.personnel_uid).pipe(
  //           tap(x => console.log(x)),
  //           tap(x => this.dataPerson = x),
  //           tap(() => this.cdRef.detectChanges())
  //         ).subscribe()
  //       }
  //     }),
  //     tap(() => this.cdRef.detectChanges())
  //   )
  // }

  getPersonIn(id) {
    console.log(id);
    this.insigniaRequestService.get(id).pipe(
      // tap(x => console.log(x)),
      tap(x => this.datapersonId = x),
      concatMap(() => {
        return this.personnelService.get(this.datapersonId.personnel_uid).pipe(
          // tap(x => console.log(x)),
          tap(x => this.dataPerson = x),
          tap(() => this.cdRef.detectChanges())
        )
      }),
      // concatMap((x: any) => {
      //   return this.insigniaPersonnelsService.get_Person(x.personnel_uid).pipe(
      //     tap(x => console.log(x)),
      //     tap((x: any) => {
      //       if (x != 0) {
      //         this.resultAssign = x[0]?.insignia_holder
      //         this.manNo = x[0]?.personnel?.manpower_no
      //         // this.form.get('insignia_personnel_uid').setValue(x[0].insignia_personnel?.insignia_personnel_uid)
      //         // this.form.get('personnel_uid').setValue(x[0].insignia_personnel?.personnel_uid)
      //         this.personnelService.get(this.form.value.personnel_uid).pipe(
      //           tap(x => console.log(x)),
      //           tap(x => this.dataPerson = x),
      //           tap(() => this.cdRef.detectChanges())
      //         ).subscribe()
      //       } else {
      //         this.personnelService.get(this.datapersonId.personnel_uid).pipe(
      //           tap(x => console.log(x)),
      //           tap(x => this.dataPerson = x),
      //           tap(() => this.cdRef.detectChanges())
      //         ).subscribe()
      //       }
      //     })
      //   )
      // }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  getPersonOut(id) {
    this.insigniaRequestService.get(id).pipe(
      tap(x => console.log(x)),
      tap(x => this.dataCitizen = x),
      // concatMap((x: any) => {
      //   return this.insigniaPersonnelsService.get_Citizen(x.personnel_uid).pipe(
      //     tap(x => console.log(x)),
      //     tap((x: any) => {
      //       if (x != 0) {
      //         this.resultAssign = x[0]?.insignia_holder
      //         console.log(this.resultAssign);
      //       }
      //     })
      //   )
      // }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  popupAdd() {
    const dialogRef = this.dialog.open(
      PopupInsigniaListComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: null }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ  
    })

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {

      }
    })
  }

  popupEdit(array, index) {
    console.log(array);
    if (this.state == 'edit') {
      const dialogRef = this.dialog.open(
        PopupInsigniaListComponent, {
        width: '90%',
        // disableClose: true,
        data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ     
      })
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
        this.insigniaAssignService.getAll().pipe(
          tap(x => this.assignData = x)
        ).subscribe()
        this.router.navigate(['app/save_recive_royal/edit', this.id])
      })
    } else if (this.state == 'add') {
      this.appSV.swaltAlertError('กรุณาสร้าง การรับเครื่องราชฯ ก่อน!!')
    }

  }

  // addfile() {
  //   const dialogRef = this.dialog.open(
  //     UploadFileDialogComponent, {
  //     width: '70%',
  //     // disableClose: true,
  //     data: { editForm: null, state: 'add' }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
  //   }
  //   )

  //   dialogRef.afterClosed().subscribe(callback => {
  //     console.log(callback)
  //     if (callback) {
  //       let formarray = this.form.get('insignia_assign_document') as FormArray
  //       formarray.push(this.createArrayDoc(callback))
  //     }
  //   })
  // }

  // editfile(array, index) {
  //   const dialogRef = this.dialog.open(
  //     UploadFileDialogComponent, {
  //     width: '70%',
  //     // disableClose: true,
  //     data: { editForm: array, state: 'edit', remark: array.remark } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
  //   }
  //   )

  //   dialogRef.afterClosed().subscribe(callback => {
  //     console.log(callback)
  //     if (callback) {
  //       let formarray = this.form.get('insignia_assign_document') as FormArray
  //       formarray.controls[index].patchValue({
  //         file_name: callback.file_name,
  //         remark: callback.remark,
  //       })
  //     }
  //   })
  // }

  // downloadDoc(url) {
  //   this.insigniaAssignService.download_file(url)
  // }

  // deleteItem(item, i) {
  //   // if (item.insignia_assign_document_uid) {
  //   //   this.manpowerAssignmentDocumentsService.deleteData(item.insignia_assign_document_uid).pipe(
  //   //     catchError(err => {
  //   //       this.appSV.swaltAlertError('Error')
  //   //       return throwError(err)
  //   //     })).subscribe((x: any) => {
  //   //       console.log(x)
  //   //       this.ngOnInit()
  //   //       this.appSV.swaltAlert()
  //   //     })
  //   // } else {
  //   //   let formarray = this.form.get('insignia_assign_document') as FormArray
  //   //   formarray.removeAt(i)
  //   // }
  // }

  saveRequestAssign(id) {
    console.log(id);
    this.insigniaRequestService.get_assing({ insignia_requests_uid: id }).pipe(
      catchError(err => {
        this.appSV.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appSV.swaltAlert()
        this.router.navigate(['app/save_recive_royal'])
      })
  }



  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        break;
      case 'add':
        this.insigniaRequestService.get_assing(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            // this.router.navigate(['app/save_recive_royal/edit', x.personnel_uid])
            this.router.navigate(['app/save_recive_royal'])
          })
        break;
    }

  }

  close() {
    this.router.navigate(['app/save_recive_royal'])
  }

  // getDetailArray(el) {
  //   this.resultAssign = {
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     search: el.search,
  //     insignia_assign_uid: el.insignia_assign_uid,
  //     insignia_request_uid: el.insignia_request_uid,
  //     insignia_uid: el.insignia_uid,
  //     insignia_personnel_uid: el.insignia_personnel_uid,
  //     bestow_date: el.bestow_date,
  //     version_no: el.version_no,
  //     parts_no: el.parts_no,
  //     page_no: el.page_no,
  //     announcement_date: el.announcement_date,
  //     license_receive_status: el.license_receive_status,
  //     license_receive_date: el.license_receive_date,
  //     insignia_receive_status: el.insignia_receive_status,
  //     insignia_receive_date: el.insignia_receive_date,
  //     return_status: el.return_status,
  //     pay_amount: el.pay_amount,
  //     return_date: el.pay_amount,
  //   }
  //   // return this.baseFormBuilder.group({
  //   //   status_id: el.status_id,
  //   //   created_by: el.created_by,
  //   //   created_datetime: el.created_datetime,
  //   //   updated_by: el.updated_by,
  //   //   updated_datetime: el.updated_datetime,
  //   //   search: el.search,
  //   //   insignia_assign_bestow_uid: el.insignia_assign_bestow_uid,
  //   //   insignia_assign_uid: el.insignia_assign_uid,
  //   //   bestow_date: el.bestow_date,
  //   //   current_position_uid: el.current_position_uid,
  //   //   current_position_level_uid: el.current_position_level_uid,
  //   //   government_gazette_version_no: el.government_gazette_version_no,
  //   //   government_gazette_parts_no: el.government_gazette_parts_no,
  //   //   government_gazette_page_no: el.government_gazette_page_no,
  //   //   announcement_date: el.announcement_date,
  //   //   license_receive_status: el.license_receive_status,
  //   //   license_receive_date: el.license_receive_date,
  //   //   insignia_receive_status: el.insignia_receive_status,
  //   //   insignia_receive_date: el.insignia_receive_date,
  //   //   return_status: el.return_status,
  //   //   pay_amount: el.pay_amount,
  //   //   return_date: el.return_date
  //   // })
  // }

  // createArrayDoc(el) {
  //   return this.baseFormBuilder.group({
  //     status_id: null,
  //     created_by: null,
  //     created_datetime: null,
  //     updated_by: null,
  //     updated_datetime: null,
  //     search: null,
  //     insignia_assign_document_uid: null,
  //     insignia_assign_uid: this.id ? this.id : null,
  //     file_name: el.file_name,
  //     document_name: el.file.original_fileName,
  //     document_url: el.file.file_url,
  //     mime_type_name: el.file.mime_type_name,
  //     row_order: null,
  //     remark: el.remark
  //   })
  // }

  // getArrayDoc(el) {
  //   return this.baseFormBuilder.group({
  //     status_id: el.status_id,
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     search: el.search,
  //     insignia_assign_document_uid: el.insignia_assign_document_uid,
  //     insignia_assign_uid: el.insignia_assign_uid,
  //     file_name: el.file_name,
  //     document_name: el.document_name,
  //     document_url: el.document_url,
  //     mime_type_name: el.mime_type_name,
  //     row_order: el.row_order,
  //     remark: el.remark
  //   })
  // }



  createForm() {
    return this.baseFormBuilder.group({
      insignia_request_guid: null
      // insignia_personnel_uid: null,
      // personnel_in_house: null,
      // personnel_uid: null,
      // outsider_prefix_id: null,
      // outsider_gender_id: null,
      // outsider_name: '',
      // outsider_citizen_id: '',
      // outsider_position_uid: null,
    })
  }

}
