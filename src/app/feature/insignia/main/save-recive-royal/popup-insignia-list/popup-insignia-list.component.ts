import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject, ViewChild } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { from, Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { InsigniaAssignDocumentService } from 'src/app/core/service/insignia/insignia-assign-document.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-popup-insignia-list',
  templateUrl: './popup-insignia-list.component.html',
  styleUrls: ['./popup-insignia-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupInsigniaListComponent extends BaseForm implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  insigniaDetails$ = new Observable()
  levelPosition:any = []
  rows: any = []

  dataRequest:any = null
  dataEdit: any 
  request: any = {
    status_id: null,
    created_by: "",
    created_datetime: null,
    updated_by: "",
    updated_datetime: null,
    search: '',
    insignia_assign_bestow_uid: null,
    insignia_assign_uid: null,
    bestow_date: null,
    insignia_detail_uid: null,
    current_position_uid: null,
    current_position_level_uid: null,
    government_gazette_version_no: '',
    government_gazette_parts_no: '',
    government_gazette_page_no: '',
    announcement_date: null,
    license_receive_status: null,
    license_receive_date: null,
    insignia_receive_status: null,
    insignia_receive_date: null,
    return_status: null,
    pay_amount: 0,
    return_date: null
  }

  constructor(
    public dialogRef: MatDialogRef<PopupInsigniaListComponent>,
    public dialog: MatDialog,
    public insigniaDetailService: InsigniaDetailService,
    public insigniaAssignDocumentService : InsigniaAssignDocumentService,
    public formBuilder: FormBuilder,
    public positionLevelsService : PositionLevelsService,
    public insigniaRequestService : InsigniaRequestService,
    public insigniaAssignService : InsigniaAssignService,
    public activeRoute: ActivatedRoute,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    super(formBuilder, activeRoute);
    this.insigniaDetails$ = this.insigniaDetailService.query('/all').pipe(
      tap(x => console.log(x)),
    )
    this.positionLevelsService.getAll().pipe(
      tap(x => this.levelPosition = x)
    ).subscribe()
  }

  ngOnInit(): void {

    console.log(this.data);

    this.form.get('insignia_uid').disable()
    this.form.get('license_receive_date').disable()
    this.form.get('insignia_receive_date').disable()
    this.form.get('return_status').disable()
    this.form.get('pay_amount').disable()
    this.form.get('return_date').disable()

    // if (this.data.editForm != null) {
      let assign = this.data.editForm.insignia_assign?.insignia_assign_uid
      this.insigniaAssignService.get(assign).pipe(
        tap(x => console.log(x)),
        tap(x => this.dataEdit = x ),
        concatMap(() => {
          let assign = this.dataEdit.insignia_request_uid
          return this.insigniaRequestService.get(assign).pipe(
            tap((x:any) => console.log(x)),
            tap(x => this.dataRequest = x.current_position_level_id)
          )
        }),
        tap(() => {
          this.form.patchValue({
            status_id: this.dataEdit.status_id,
            created_by: this.dataEdit.created_by,
            created_datetime: this.dataEdit.created_datetime,
            updated_by: this.dataEdit.updated_by,
            updated_datetime: this.dataEdit.updated_datetime,
            search: this.dataEdit.search,
            insignia_assign_uid: this.dataEdit.insignia_assign_uid,
            insignia_request_uid: this.dataEdit.insignia_request_uid,
            insignia_uid: this.dataEdit.insignia_uid,
            insignia_personnel_uid: this.dataEdit.insignia_personnel_uid,
            bestow_date: this.dataEdit.bestow_date,
            version_no: this.dataEdit.version_no,
            parts_no: this.dataEdit.parts_no,
            page_no: this.dataEdit.page_no,
            announcement_date: this.dataEdit.announcement_date,
            license_receive_status: this.dataEdit.license_receive_status,
            license_receive_date: this.dataEdit.license_receive_date,
            insignia_receive_status: this.dataEdit.insignia_receive_status,
            insignia_receive_date: this.dataEdit.insignia_receive_date,
            return_status: this.dataEdit.return_status,
            pay_amount: this.dataEdit.pay_amount,
            return_date: this.dataEdit.pay_amount,
          }) 
          let formDocs = this.form.get('insignia_assign_document') as FormArray
          while (formDocs.length != 0) {
            formDocs.removeAt(0)
          }
          if(this.dataEdit.insignia_assign_document.length != 0){
            this.getResultArray(this.dataEdit.insignia_assign_document)
            this.dataEdit.insignia_assign_document.forEach(element => {
              formDocs.push(this.getArrayDoc(element))
            });
          }
        })
        
      ).subscribe()
      // this.getArrayDetails(this.data.editForm)
    // }
  }

  setStatus1(){
    if(this.form.value.license_receive_status){
      this.form.get('license_receive_date').enable()
    }else{
      this.form.get('license_receive_date').disable()
    }
  }

  setStatus2(){
    if(this.form.value.insignia_receive_status){
      this.form.get('insignia_receive_date').enable()
    }else{
      this.form.get('insignia_receive_date').disable()
    }
  }
  setStatus3(){
    if(this.form.value.license_receive_status){
      this.form.get('license_receive_date').enable()
    }else{
      this.form.get('license_receive_date').disable()
    }
  }

  getResultArray(array) {
    console.log(array);
    this.rows = array
    this.rows = new MatTableDataSource(this.rows);
    this.rows.sort = this.sort;
    this.rows.paginator = this.paginator;
    this.cdRef.detectChanges()
  }

  // getAssign(el) {
  //   console.log(el);
    
  // }


  // createArrayDetails(el?) {
  //   console.log('array', el)
  //   if (el == undefined) {
  //     return this.formBuilder.group({
  //       status_id: null,
  //       created_by: "",
  //       created_datetime: null,
  //       updated_by: "",
  //       updated_datetime: null,
  //       search: '',
  //       insignia_assign_bestow_uid: null,
  //       insignia_assign_uid: null,
  //       bestow_date: null,
  //       insignia_detail_uid: null,
  //       current_position_uid: null,
  //       current_position_level_uid: null,
  //       government_gazette_version_no: '',
  //       government_gazette_parts_no: '',
  //       government_gazette_page_no: '',
  //       announcement_date: null,
  //       license_receive_status: null,
  //       license_receive_date: null,
  //       insignia_receive_status: null,
  //       insignia_receive_date: null,
  //       return_status: null,
  //       pay_amount: 0,
  //       return_date: null
  //     })
  //   } else {
  //     return this.formBuilder.group({
  //       status_id: el.status_id,
  //       created_by: el.created_by,
  //       created_datetime: el.created_datetime,
  //       updated_by: el.updated_by,
  //       updated_datetime: el.updated_datetime,
  //       search: el.search,
  //       insignia_assign_bestow_uid: el.insignia_assign_bestow_uid,
  //       insignia_assign_uid: el.insignia_assign_uid,
  //       bestow_date: el.bestow_date,
  //       insignia_detail_uid: el.insignia_detail_uid,
  //       current_position_uid: el.current_position_uid,
  //       current_position_level_uid: el.current_position_level_uid,
  //       government_gazette_version_no: el.government_gazette_version_no,
  //       government_gazette_parts_no: el.government_gazette_parts_no,
  //       government_gazette_page_no: el.government_gazette_page_no,
  //       announcement_date: el.announcement_date,
  //       license_receive_status: el.license_receive_status,
  //       license_receive_date: el.license_receive_date,
  //       insignia_receive_status: el.insignia_receive_status,
  //       insignia_receive_date: el.insignia_receive_date,
  //       return_status: el.return_status,
  //       pay_amount: el.pay_amount,
  //       return_date: el.return_date
  //     })
  //   }
  // }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('insignia_assign_document') as FormArray
        formarray.push(this.createArrayDoc(callback))
        this.getResultArray(this.form.value.insignia_assign_document)
      }
    })
  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        let formarray = this.form.get('insignia_assign_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })
  }

  downloadDoc(url) {
    this.insigniaAssignService.download_file(url)
  }

  deleteItemDoc(id, i) {
  if (id) {
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.insigniaAssignDocumentService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              let formarray = this.form.get('insignia_assign_document') as FormArray
              formarray.removeAt(i)
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })
        }
      })
    }
  } else {
    let formarray = this.form.get('insignia_assign_document') as FormArray
    formarray.removeAt(i)
  }
}

  createArrayDoc(el) {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      insignia_assign_document_uid: null,
      insignia_assign_uid: this.data.editForm.insignia_assign.insignia_assign_uid? this.data.editForm.insignia_assign.insignia_assign_uid :  null,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null,
      remark: el.remark
    })
  }

  getArrayDoc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      search: el.search,
      insignia_assign_document_uid: el.insignia_assign_document_uid,
      insignia_assign_uid: el.insignia_assign_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark: el.remark
    })
  }



  save() {
    this.insigniaAssignService.update(this.form.getRawValue()).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
      })
  }

  saveClose() {
    this.insigniaAssignService.update(this.form.getRawValue()).pipe(
      catchError(err => {
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.close()
      })
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      insignia_assign_uid: null,
      insignia_request_uid: null,
      insignia_uid: null,
      insignia_personnel_uid: null,
      bestow_date: null,
      version_no: 0,
      parts_no: 0,
      page_no: 0,
      announcement_date: null,
      license_receive_status: null,
      license_receive_date: null,
      insignia_receive_status: null,
      insignia_receive_date: null,
      return_status: null,
      pay_amount: null,
      return_date: null,
      insignia_assign_document: this.baseFormBuilder.array([])
    })
  }


  close() {
    this.dialogRef.close('close')
  }

}
