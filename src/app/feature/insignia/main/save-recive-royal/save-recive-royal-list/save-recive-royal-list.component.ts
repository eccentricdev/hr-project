import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { prefix, PrefixService } from 'src/app/core/service/common/prefix.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaPersonnelsService } from 'src/app/core/service/insignia/insignia-personnels.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-save-recive-royal-list',
  templateUrl: './save-recive-royal-list.component.html',
  styleUrls: ['./save-recive-royal-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveReciveRoyalListComponent extends BaseList implements OnInit {

  rows:any = []
  personnel$ = new Observable()
  prefix$ = new Observable()
  dataSearch:any = ''

  constructor(public router : Router,
    public personnelService :PersonnelService,
    public prefixService : PrefixService,
    public insigniaPersonnelsService : InsigniaPersonnelsService,
    public insigniaAssignService: InsigniaAssignService,
    public appService : AppService,) {
    super();
    this.personnel$ = this.personnelService.getAll()
    this.prefix$ = this.prefixService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.insigniaPersonnelsService.query('/list').pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
    // this.insigniaAssignService.getAll().pipe(
    //   tap(x => console.log(x)),
    //   tap(x => this.rows = this.updateMatTable(x))
    // ).subscribe()
  }

  search(){
    // console.log(this.dataSearch);
      setTimeout(() => {
        this.insigniaPersonnelsService.query(`?search=${this.dataSearch}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }


  addPage1(){
    this.router.navigate(['app/save_recive_royal/add']) 
  }
  addPage2(){
    this.router.navigate(['app/save_recive_royal/add_group']) 
  }

  editPage(id){
    this.router.navigate(['app/save_recive_royal/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.insigniaPersonnelsService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
