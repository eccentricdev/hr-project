import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveReciveRoyalListComponent } from './save-recive-royal-list/save-recive-royal-list.component';
import { SaveReciveRoyalFormComponent } from './save-recive-royal-form/save-recive-royal-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupInsigniaListComponent } from './popup-insignia-list/popup-insignia-list.component';
import { SaveReciveRoyalGroupFormComponent } from './save-recive-royal-group-form/save-recive-royal-group-form.component';

const routes:Routes = [
  {
    path:'',
    component:SaveReciveRoyalListComponent
  },
  {
    path:'add',
    component:SaveReciveRoyalFormComponent
  },
  {
    path:'edit/:id',
    component:SaveReciveRoyalFormComponent
  },
  {
    path:'add_group',
    component:SaveReciveRoyalGroupFormComponent
  },
  {
    path:'edit_group/:id',
    component:SaveReciveRoyalGroupFormComponent
  },
  
]

@NgModule({
  declarations: [
    SaveReciveRoyalListComponent, 
    SaveReciveRoyalFormComponent, 
    PopupInsigniaListComponent, 
    SaveReciveRoyalGroupFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SaveReciveRoyalModule { }
