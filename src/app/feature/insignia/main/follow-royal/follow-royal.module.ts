import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FollowRoyalListComponent } from './follow-royal-list/follow-royal-list.component';
import { FollowRoyalFormComponent } from './follow-royal-form/follow-royal-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';


const routes:Routes = [
  {
    path:'',
    component:FollowRoyalListComponent
  },
  {
    path:'add',
    component:FollowRoyalFormComponent
  },
  {
    path:'edit/:id',
    component:FollowRoyalFormComponent
  },
  
]


@NgModule({
  declarations: [
    FollowRoyalListComponent, 
    FollowRoyalFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class FollowRoyalModule { }
