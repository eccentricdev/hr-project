import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { InsigniaAssignsTrackingService } from 'src/app/core/service/insignia/insignia-assigns-tracking.service';
import { InsigniaEligibleProcessingService } from 'src/app/core/service/insignia/insignia-eligible-processing.service';
import { InsigniaReturnService } from 'src/app/core/service/insignia/insignia-return.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-follow-royal-list',
  templateUrl: './follow-royal-list.component.html',
  styleUrls: ['./follow-royal-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FollowRoyalListComponent extends BaseList implements OnInit {

  years$ = new Observable()
  rows:any = []
  searchData:any = null
  
  constructor(public router : Router,
    public insigniaEligibleProcessingService : InsigniaEligibleProcessingService,
    public insigniaAssignsTrackingService :InsigniaAssignsTrackingService,
    public insigniaReturnService : InsigniaReturnService,) {
    super();
    this.years$ = this.insigniaEligibleProcessingService.getProcess_date()
  }

  ngOnInit(): void {
    // this.getService()
  }

  search(data){
    // console.log(this.searchData);
    
    // console.log(data);
      setTimeout(() => {
        this.insigniaAssignsTrackingService.query(`?year=${data}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  // getService(){
  //   this.insigniaReturnService.getList().pipe(
  //     tap(x => console.log(x)),
  //     tap(x => this.rows = this.updateMatTable(x))
  //   ).subscribe()
  // }

  editPage(id){
    this.router.navigate(['app/follow_royal/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        


        }
      })
    }
  }

 

}
