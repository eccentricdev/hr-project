import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-follow-royal-form',
  templateUrl: './follow-royal-form.component.html',
  styleUrls: ['./follow-royal-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FollowRoyalFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
