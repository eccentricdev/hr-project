import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestRoyalMedalListComponent } from './request-royal-medal-list/request-royal-medal-list.component';
import { RequestRoyalMedalFormComponent } from './request-royal-medal-form/request-royal-medal-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:RequestRoyalMedalListComponent
  },
  {
    path:'add',
    component:RequestRoyalMedalFormComponent
  },
  {
    path:'edit/:id',
    component:RequestRoyalMedalFormComponent
  },
  
]

@NgModule({
  declarations: [
    RequestRoyalMedalListComponent, 
    RequestRoyalMedalFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class RequestRoyalMedalModule { }
