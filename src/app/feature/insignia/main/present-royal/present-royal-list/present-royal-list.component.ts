import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';
import { catchError } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-present-royal-list',
  templateUrl: './present-royal-list.component.html',
  styleUrls: ['./present-royal-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresentRoyalListComponent extends BaseList implements OnInit {

  rows:any = []
  dataSearch:any = ''

  personnel$ = new Observable()
  genders$ = new Observable()
  insigniaDetail$ = new Observable()
  positions$ = new Observable()
  constructor(public router : Router,
    public appService : AppService,
    public personnelService : PersonnelService,
    public gendersService : GendersService,
    public cdRef: ChangeDetectorRef,
    public positionService : PositionService,
    public insigniaDetailService : InsigniaDetailService,
    public insigniaRequestService : InsigniaRequestService) {
    super();
    this.personnel$ = this.personnelService.getAll()
    this.genders$ = this.gendersService.getAll()
    this.positions$ = this.positionService.getAll()
    // this.insigniaDetail$ = this.insigniaDetailService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    this.insigniaRequestService.getListRequest().pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x)),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  search(){
    console.log(this.dataSearch);
      setTimeout(() => {
        this.insigniaRequestService.query(`?search=${this.dataSearch}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  getAssign(id){
    console.log(id);
    this.insigniaRequestService.get_assing({insignia_request_guid:id}).pipe(
      catchError(err => {
        // alert ตรงนี่
        this.appService.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appService.swaltAlert()
        this.router.navigate(['app/save_recive_royal'])
      })
  }

  addPage(){
    this.router.navigate(['app/present_royal/add']) 
  }
  editPage(id){
    this.router.navigate(['app/present_royal/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.insigniaRequestService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
