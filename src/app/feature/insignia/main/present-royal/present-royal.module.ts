import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresentRoyalListComponent } from './present-royal-list/present-royal-list.component';
import { PresentRoyalFormComponent } from './present-royal-form/present-royal-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes:Routes = [
  {
    path:'',
    component:PresentRoyalListComponent
  },
  {
    path:'add',
    component:PresentRoyalFormComponent
  },
  {
    path:'edit/:id',
    component:PresentRoyalFormComponent
  },
  
]


@NgModule({
  declarations: [
    PresentRoyalListComponent, 
    PresentRoyalFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PresentRoyalModule { }
