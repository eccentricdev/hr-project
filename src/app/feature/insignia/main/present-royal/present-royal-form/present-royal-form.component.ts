import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, switchMap, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { GendersService } from 'src/app/core/service/common/genders.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { WorkGroupsService } from 'src/app/core/service/common/work-groups.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaPersonnelsService } from 'src/app/core/service/insignia/insignia-personnels.service';
import { InsigniaRequestService } from 'src/app/core/service/insignia/insignia-request.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';

@Component({
  selector: 'app-present-royal-form',
  templateUrl: './present-royal-form.component.html',
  styleUrls: ['./present-royal-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresentRoyalFormComponent extends BaseForm implements OnInit {



  organizationData$ = new Observable()
  personnels$ = new Observable()
  positions$ = new Observable()
  agencys$ = new Observable()
  insigniaDetail$ = new Observable()

  dataEdit: any
  dataPersonnel: any
  dateToday = new Date()
  dataType: any
  prefixs: any = []
  genders: any = []
  levels: any = []
  workGroups: any = []
  positions: any = []
  dataAssign:any 
  insigniaDetail:any = []
  typePerson = [{ check: true, name: 'บุคคลภายใน' }, { check: false, name: 'บุคคลภายนอก' }]


  constructor(public formBuilder: FormBuilder,
    public positionService: PositionService,
    public agencysService: AgencysService,
    public gendersService: GendersService,
    public prefixService: PrefixService,
    public cdRef: ChangeDetectorRef,
    public workGroupsService: WorkGroupsService,
    public positionLevelsService: PositionLevelsService,
    public insigniaDetailService: InsigniaDetailService,
    public insigniaRequestService: InsigniaRequestService,
    public insigniaPersonnelsService : InsigniaPersonnelsService,
    public organizationDataService: OrganizationDataService,
    public personnelService: PersonnelService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService) {
    super(formBuilder, activeRoute);
    this.organizationData$ = this.organizationDataService.getAll()
    this.positionService.getAll().pipe(
      tap(x => this.positions = x)
    ).subscribe()

    this.personnels$ = this.personnelService.getAll()
    this.agencys$ = this.agencysService.getAll()

    this.insigniaDetail$ = this.insigniaDetailService.query('/all').pipe(
      // tap(x => console.log(x)),
      tap(x => this.insigniaDetail = x)
    )
    this.prefixService.getAll().pipe(
      tap(x => this.prefixs = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

    this.gendersService.getAll().pipe(
      tap(x => this.genders = x)
    ).subscribe()

    this.positionLevelsService.getAll().pipe(
      tap(x => this.levels = x)
    ).subscribe()

    this.workGroupsService.getAll().pipe(
      tap(x => this.workGroups = x)
    ).subscribe()

  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('request_date').setValue(this.dateToday)
    this.form.get('request_date').disable()
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.insigniaRequestService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          concatMap(() => {
            if(this.dataEdit.personnel_in_house){
              return this.insigniaPersonnelsService.get_Person(this.dataEdit.personnel_uid).pipe(
                // tap(x => console.log(x)),
                tap(x => this.dataAssign = x[0].insignia_personnel)
              )
            }else{
              return this.insigniaPersonnelsService.get_Citizen(this.dataEdit.outsider_citizen_id).pipe(
                // tap(x => console.log(x)),
                tap(x => this.dataAssign = x[0].insignia_personnel)
              )
            }
          }),
          tap(() => {
            this.getPersonnel(this.dataEdit.personnel_uid)
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              search: this.dataEdit.search,
              insignia_request_uid: this.dataEdit.insignia_request_uid,
              document_no: this.dataEdit.document_no,
              request_date: this.dataEdit.request_date,
              processing_date: this.dataEdit.processing_date,
              personnel_uid: this.dataEdit.personnel_uid,
              insignia_uid: this.dataEdit.insignia_uid,
              come_from_detail: this.dataEdit.come_from_detail,
              request_status: this.dataEdit.request_status,
              signatory_name: this.dataEdit.signatory_name,
              duplicate_last_year: this.dataEdit.duplicate_last_year,
              sign_status: this.dataEdit.sign_status,
              sign_name: this.dataEdit.sign_name,
              received_status: this.dataEdit.received_status,
              personnel_in_house: this.dataEdit.personnel_in_house,
              outsider_prefix_id: this.dataEdit.outsider_prefix_id,
              outsider_gender_id: this.dataEdit.outsider_gender_id,
              outsider_name: this.dataEdit.outsider_name,
              outsider_citizen_id: this.dataEdit.outsider_citizen_id,
              outsider_position_uid: this.dataEdit.outsider_position_uid,
              current_position_uid: this.dataEdit.current_position_uid,
              current_position_level_id: this.dataEdit.current_position_level_id,
              current_insignia_uid: this.dataEdit.current_insignia_uid,
              insignia_last_receive_date: this.dataEdit.insignia_last_receive_date
            })
          })
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setPerson() {
    if (this.form.get('personnel_in_house').value) {
      this.form.patchValue({
        outsider_prefix_id: null,//รหัสคำนำหน้าบุคคลภายนอก
        outsider_gender_id: null,//เพศบุคคลภายนอก
        outsider_name: "",//ชื่อบุคคลภายนอก
        outsider_citizen_id: "",//รหัสบัตรประชาชนบุคคลภายนอก
        outsider_position_uid: null,//รหัสตำแหน่งบุคคลภายนอก
        current_position_uid: null,//ตำแหน่ง ณ ปีที่ขอ
        current_position_level_id: null,//ระดับตำแหน่ง ณ ปีที่ขอ
        current_insignia_uid: null,//รหัสเครื่องราชล่าสุดที่ได้รับ
      })
    }else{
      this.form.get('personnel_uid').setValue(null)
    }
  }

  getPersonnel(id) {
    console.log(id);
    this.personnelService.get(id).pipe(
      // tap(x => console.log(x)),
      tap(x => this.dataPersonnel = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()

  }

  getSignature() {
    let items = {
      ...this.form.getRawValue(),
      sign_status: true
    }
    // console.log(items);
    this.insigniaRequestService.update(items).pipe(
      catchError(err => {
        this.appSV.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appSV.swaltAlert()
        this.router.navigate(['app/present_royal'])
      })
  }



  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.insigniaRequestService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            // console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/present_royal'])
          })
        break;
      case 'add':
        this.insigniaRequestService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            // console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/present_royal'])
          })
        break;
    }

  }

  close() {
    this.router.navigate(['app/present_royal'])
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      search: [''],
      insignia_request_uid: [null],
      document_no: [''],
      request_date: [null],
      processing_date: [null],
      personnel_uid: [null],
      insignia_uid: [null],
      come_from_detail: [''],
      request_status: false,
      signatory_name: [''],
      duplicate_last_year: null,
      sign_status: false,
      sign_name: '',
      received_status: false,
      personnel_in_house: true,
      outsider_prefix_id: null,
      outsider_gender_id: null,
      outsider_name: '',
      outsider_citizen_id: '',
      outsider_position_uid: null,
      current_position_uid: null,
      current_position_level_id: null,
      current_insignia_uid: null,
      insignia_last_receive_date: null
    })
  }

}
