import { ChangeDetectorRef, ViewChild } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PrefixService } from 'src/app/core/service/common/prefix.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaPersonnelsService } from 'src/app/core/service/insignia/insignia-personnels.service';
import { InsigniaReturnDetailService } from 'src/app/core/service/insignia/insignia-return-detail.service';
import { InsigniaReturnDocumentService } from 'src/app/core/service/insignia/insignia-return-document.service';
import { InsigniaReturnService } from 'src/app/core/service/insignia/insignia-return.service';
import { ReturnReasonService } from 'src/app/core/service/insignia/return-reason.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { PopupReturnDetailComponent } from '../popup-return-detail/popup-return-detail.component';
import swal from 'sweetalert2'
@Component({
  selector: 'app-return-royal-form',
  templateUrl: './return-royal-form.component.html',
  styleUrls: ['./return-royal-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReturnRoyalFormComponent extends BaseForm implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // rows:any = [{1:1}]
  returnReason$ = new Observable()
  personnel$ = new Observable()
  insigniaDetail$ = new Observable()
  insigniaAssigns: any = []
  dataEdit: any
  dataPerson: any
  dateToday = new Date()
  dataArrayDetail: any = []
  personData: any = []
  prefixs: any = []
  rowsAssign: any = []

  constructor(public formBuilder: FormBuilder,
    public personnelService: PersonnelService,
    public insigniaDetailService: InsigniaDetailService,
    public insigniaReturnService: InsigniaReturnService,
    public returnReasonService: ReturnReasonService,
    public insigniaAssignService: InsigniaAssignService,
    public insigniaReturnDetailService: InsigniaReturnDetailService,
    public insigniaPersonnelsService: InsigniaPersonnelsService,
    public insigniaReturnDocumentService: InsigniaReturnDocumentService,
    public prefixService: PrefixService,
    public cdRef: ChangeDetectorRef,
    public router: Router,
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public appSV: AppService) {
    super(formBuilder, activeRoute);

    this.returnReason$ = this.returnReasonService.getAll()
    this.personnel$ = this.insigniaPersonnelsService.query('/list').pipe(
      // tap(x => console.log(x)),
      map((x: any) => x.filter(x => x.personnel_in_house == true)),
      // tap(x => console.log(x)),
    )

    this.personnelService.getAll().pipe(
      tap(x => this.personData = x)
    ).subscribe()

    this.insigniaAssignService.getAll().pipe(
      tap(x => this.insigniaAssigns = x)
    ).subscribe()

    this.insigniaDetail$ = this.insigniaDetailService.query('/all')

  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    this.form.get('document_date').setValue(this.dateToday)
    this.form.get('document_date').disable()
    this.form.get('document_no').disable()
    switch (this.state) {
      case 'edit':
        this.form.get('personnel_uid').disable()
        this.insigniaReturnService.get(this.id).pipe(
          tap(x => console.log(x)),
          tap(x => this.dataEdit = x),
          // concatMap(() => {
          //   return  this.insigniaReturnDetailService.getAll().pipe(
          //     tap((x:any) => this.dataArrayDetail = x.filter(x => x.insignia_return_uid == this.dataEdit.insignia_return_uid)),
          //     tap(() => console.log(this.dataArrayDetail)),
          //     tap(x => this.getAssginArray(this.dataArrayDetail))
          //   )
          // }),
          tap(() => {
            this.getPerson(this.dataEdit.personnel_uid)
            this.form.patchValue({
              status_id: this.dataEdit.status_id,
              created_by: this.dataEdit.created_by,
              created_datetime: this.dataEdit.created_datetime,
              updated_by: this.dataEdit.updated_by,
              updated_datetime: this.dataEdit.updated_datetime,
              search: this.dataEdit.search,
              insignia_return_uid: this.dataEdit.insignia_return_uid,
              document_no: this.dataEdit.document_no,
              document_date: this.dataEdit.document_date,
              personnel_uid: this.dataEdit.personnel_uid,
              return_reason_id: this.dataEdit.return_reason_id,
              reason_other_detail: this.dataEdit.reason_other_detail,
              signatory_name: this.dataEdit.signatory_name,
              signatory_date: this.dataEdit.signatory_date,
              return_status: this.dataEdit.return_status,
              sign_name: this.dataEdit.sign_name,
              sign_date: this.dataEdit.sign_date,
            })
            //   let formDocs = this.form.get('insignia_assign_document') as FormArray
            // while (formDocs.length != 0) {
            //   formDocs.removeAt(0)
            // }
            // if(this.dataEdit.insignia_return_document.length != 0){
            //   this.getResultArray(this.dataEdit.insignia_return_document)
            //   this.dataEdit.insignia_return_document.forEach(element => {
            //     formDocs.push(this.get_Doc(element))
            //   });
            // }
            let formarray1 = this.form.get('insignia_return_detail') as FormArray
            let formarray2 = this.form.get('insignia_return_document') as FormArray
            while (formarray1.length != 0) {
              formarray1.removeAt(0)
            }
            while (formarray2.length != 0) {
              formarray2.removeAt(0)
            }
            this.dataEdit.insignia_return_detail.forEach(element => {
              formarray1.push(this.getDetail(element))
            });

            this.dataEdit.insignia_return_document.forEach(element => {
              formarray2.push(this.get_Doc(element))
            });

          })
        ).subscribe()
        break;
      case 'add':
        break;
    }
  }

  getAssginArray(array) {
    // console.log(array);
    this.rowsAssign = array
    this.rowsAssign = new MatTableDataSource(this.rowsAssign);
    this.rowsAssign.sort = this.sort;
    this.rowsAssign.paginator = this.paginator;
    this.cdRef.detectChanges()
  }

  getPerson(id) {
    this.personnelService.get(id).pipe(
      // tap(x => console.log(x)),
      tap(x => this.dataPerson = x),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
  }

  getSignature() {
    let items = {
      ...this.form.getRawValue(),
      sign_status: true,
      sign_date: new Date()
    }
    // console.log(items);
    this.insigniaReturnService.update(items).pipe(
      catchError(err => {
        this.appSV.swaltAlertError('Error')
        return throwError(err)
      })).subscribe((x: any) => {
        console.log(x)
        this.appSV.swaltAlert()
        this.router.navigate(['app/return_royal'])
      })
  }

  popDetailAdd() {
    if (this.form.get('insignia_return_uid').value != null) {
      const dialogRef = this.dialog.open(
        PopupReturnDetailComponent, {
        width: '70%',
        // disableClose: true,
        data: { form: this.form, editForm: null, idRetrun: this.id, personnel_uid: this.form.get('personnel_uid').value }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      })
      dialogRef.afterClosed().subscribe(callback => {
        // console.log(callback)

        this.getAssginArray(this.form.value.insignia_return_detail)
        // this.insigniaReturnDetailService.getAll().pipe(
        //   tap((x:any) => this.dataArrayDetail = x.filter(x => x.insignia_return_uid == this.id)),
        //   tap(() => console.log(this.dataArrayDetail)),
        //   tap(() => this.getAssginArray(this.dataArrayDetail))
        // ).subscribe()
      })
    } else {
      this.appSV.swaltAlertError('กรุณาบันทึกข้อมูล หลักฐานการส่งคืน/ชดใช้ราคาเครื่องราชอิสริยาภรณ์')
    }


  }

  popDetailEdit(array, index) {
    // console.log(array);
    // console.log(status);
    const dialogRef = this.dialog.open(
      PopupReturnDetailComponent, {
      width: '70%',
      // disableClose: true,
      data: { form: this.form, editForm: array, ind: index }  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })
  }

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: null, state: 'add' }
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      // console.log(callback)
      if (callback) {
        let formarray = this.form.get('insignia_return_document') as FormArray
        formarray.push(this.create_Doc(callback))
      }
    })

  }

  downloadDoc(url) {
    this.insigniaReturnService.download_file(url)
  }

  editfile(array, index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array, state: 'edit', remark: array.remark }
    }
    )
    dialogRef.afterClosed().subscribe(callback => {
      // console.log(callback)
      if (callback) {
        let formarray = this.form.get('insignia_return_document') as FormArray
        formarray.controls[index].patchValue({
          file_name: callback.file_name,
          remark: callback.remark,
        })
      }
    })

  }

  deleteItemDoc(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.insigniaReturnDocumentService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('insignia_return_document') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
    } else {
      let formarray = this.form.get('insignia_return_document') as FormArray
      formarray.removeAt(i)
    }
  }

  deleteDetail(id, i) {
    if (id) {
      {
        swal.getTitle()
        swal.fire({
          text: 'ยืนยันการลบรายการ',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          console.log(result)
          if (result.value) {
            this.insigniaReturnDetailService.deleteData(id).pipe(
              catchError(err => {
                // alert ตรงนี่
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                let formarray = this.form.get('insignia_return_detail') as FormArray
                formarray.removeAt(i)
                this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              })
          }
        })
      }
      // this.insigniaReturnDetailService.deleteData(item.insignia_return_detail_uid).pipe(
      //   catchError(err => {
      //     this.appSV.swaltAlertError('Error')
      //     return throwError(err)
      //   })).subscribe((x: any) => {
      //     console.log(x)
      //     this.ngOnInit()
      //     this.appSV.swaltAlert()
      //   })
    } else {
      let formarray = this.form.get('insignia_return_detail') as FormArray
      formarray.removeAt(i)
    }
  }

  create_Doc(el) {
    return this.baseFormBuilder.group({
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      insignia_return_document_uid: null,
      insignia_return_guid: this.id ? this.id : null,
      remark: el.remark,
      file_name: el.file_name,
      document_name: el.file.original_fileName,
      document_url: el.file.file_url,
      mime_type_name: el.file.mime_type_name,
      row_order: null
    })
  }


  get_Doc(el) {
    return this.baseFormBuilder.group({
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      search: el.search,
      insignia_return_document_uid: el.insignia_return_document_uid,
      insignia_return_guid: el.insignia_return_guid,
      remark: el.remark,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
    })
  }

  getDetail(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      search: el.search,
      insignia_return_detail_uid: el.insignia_return_detail_uid,
      insignia_return_uid: el.insignia_return_uid,
      insignia_assign_uid: el.insignia_assign_uid,
      is_return: el.is_return,
      is_refund: el.is_refund,
      refund_amount: el.refund_amount,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url
    })
  }

  close() {
    this.router.navigate(['app/return_royal'])
  }

  save() {
    console.log('form', this.form.getRawValue());
    switch (this.state) {
      case 'edit':
        this.insigniaReturnService.update(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/return_royal'])
          })
        break;

      case 'add':
        this.insigniaReturnService.add(this.form.getRawValue()).pipe(
          catchError(err => {
            // alert ตรงนี่
            console.log(err)
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/return_royal/edit', x.insignia_return_uid])
          })
        break;
      default:
        break;
    }

  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      search: [''],
      insignia_return_uid: [null],
      document_no: [''],
      document_date: [null],
      personnel_uid: [null],
      return_reason_id: [null],
      reason_other_detail: [''],
      signatory_name: [''],
      signatory_date: [null],
      return_status: false,
      sign_status: false,
      sign_name: [''],
      sign_date: [null],
      insignia_return_detail: this.baseFormBuilder.array([]),
      insignia_return_document: this.baseFormBuilder.array([])
    })
  }

}
