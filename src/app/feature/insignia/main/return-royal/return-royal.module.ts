import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnRoyalListComponent } from './return-royal-list/return-royal-list.component';
import { ReturnRoyalFormComponent } from './return-royal-form/return-royal-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupReturnDetailComponent } from './popup-return-detail/popup-return-detail.component';


const routes:Routes = [
  {
    path:'',
    component:ReturnRoyalListComponent
  },
  {
    path:'add',
    component:ReturnRoyalFormComponent
  },
  {
    path:'edit/:id',
    component:ReturnRoyalFormComponent
  },
  
]

@NgModule({
  declarations: [
    ReturnRoyalListComponent, 
    ReturnRoyalFormComponent, 
    PopupReturnDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ReturnRoyalModule { }
