import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaReturnService } from 'src/app/core/service/insignia/insignia-return.service';
import { PersonnelService } from 'src/app/core/service/register/personnel.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-return-royal-list',
  templateUrl: './return-royal-list.component.html',
  styleUrls: ['./return-royal-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReturnRoyalListComponent extends BaseList implements OnInit {

  personnel$ = new Observable()
  rows:any = []
  dataSearch:any = ''

  constructor(public router : Router,
    public personnelService : PersonnelService,
    public insigniaReturnService : InsigniaReturnService,
    public appService : AppService
    ) {
    super();
    this.personnel$ = this.personnelService.getAll()
  }

  ngOnInit(): void {
    this.getService()
  }

  getService(){
    // this.insigniaReturnService.getAll().pipe(
    //   tap(x => console.log(x)),
    //   tap(x => this.rows = this.updateMatTable(x))
    // ).subscribe()
    this.insigniaReturnService.query('/list').pipe(
      tap(x => console.log(x)),
      tap(x => this.rows = this.updateMatTable(x))
    ).subscribe()
    
  }

  search(){
    // console.log(this.dataSearch);
      setTimeout(() => {
        this.insigniaReturnService.query(`?search=${this.dataSearch}`).pipe(
          tap(x => this.rows = this.updateMatTable(x))
        ).subscribe()
      }, 500);
  }

  addPage(){
    this.router.navigate(['app/return_royal/add']) 
  }
  editPage(id){
    this.router.navigate(['app/return_royal/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)
        if (result.value) {
          this.insigniaReturnService.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appService.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.getService()
              this.appService.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }

}
