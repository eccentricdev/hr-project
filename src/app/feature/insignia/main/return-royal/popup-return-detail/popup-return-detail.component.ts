import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaAssignService } from 'src/app/core/service/insignia/insignia-assign.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaReturnDetailService } from 'src/app/core/service/insignia/insignia-return-detail.service';
import { ViewUploadDialogComponent } from 'src/app/shared/components/view-upload-dialog/view-upload-dialog.component';


@Component({
  selector: 'app-popup-return-detail',
  templateUrl: './popup-return-detail.component.html',
  styleUrls: ['./popup-return-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupReturnDetailComponent extends BaseForm implements OnInit {

  insigniaDetas$ = new Observable()
  check: any = null
  insignia:any = ''
  insigniasData:any = []

  request = {
    status_id: null,
    created_by: "",
    created_datetime: null,
    updated_by: "",
    search: "",
    insignia_return_detail_uid: null,
    insignia_return_uid: null,
    insignia_assign_uid: null,
    is_return: false,
    is_refund: false,
    refund_amount: 0,
    file_name: '',
    document_name: '',
    document_url: ''

  }

  constructor(public dialogRef: MatDialogRef<PopupReturnDetailComponent>,
    public formBuilder: FormBuilder,
    public insigniaAssignService: InsigniaAssignService,
    public insigniaReturnDetailService: InsigniaReturnDetailService,
    public insigniaDetailService: InsigniaDetailService,
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public appService: AppService,
    private cdRef: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    super(formBuilder, activeRoute);
   
  }

  ngOnInit(): void {
    console.log(this.data);
    // this.form.get('file_name').disable()
    if (this.data.editForm != null) {
      this.getArrayDetails(this.data.editForm)
    } else {
      this.insigniaDetas$ = this.insigniaAssignService.get_person(this.data.personnel_uid).pipe(
        tap(x => console.log(x))
      )
      // this.form.get('insignia_return_uid').setValue(this.data.idRetrun)
      this.request.insignia_return_uid = this.data.idRetrun
    }
  }

  // setRadio(check) {
  //   console.log(check);
  //   if (check == 'ส่งคืน') {
  //     this.form.get('is_return').setValue(true)
  //     this.form.get('is_pay').setValue(false)
  //   } else if (check == 'ชดใช้') {
  //     this.form.get('is_pay').setValue(true)
  //     this.form.get('is_return').setValue(false)
  //   }
  // }


  setRadio(check) {
    console.log(check);
    if (check == 'ส่งคืน') {
      this.request.is_return = true
      this.request.is_refund = false
    } else if (check == 'ชดใช้') {
      this.request.is_return = false
      this.request.is_refund = true
    }
  }

  getArrayDetails(el) {
    console.log(el);
    
    this.insigniaAssignService.get(el.insignia_assign_uid).pipe(
      tap(x => console.log(x)),
      tap((x:any) => this.insignia = x.insignia_uid),
      concatMap(() => {
        return  this.insigniaDetailService.query('/all').pipe(
          tap(x => console.log(x)),
          tap(x => this.insigniasData =x)
        )
      }),
      tap(() => this.cdRef.detectChanges())
    ).subscribe()
    if(el.is_return){
      this.check = 'ส่งคืน'
    }else if(el.is_refund){
      this.check = 'ชดใช้'
    }

    this.request = {
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      search: el.search,
      insignia_return_detail_uid: el.insignia_return_detail_uid,
      insignia_return_uid: el.insignia_return_uid,
      insignia_assign_uid: el.insignia_assign_uid,
      is_return: el.is_return,
      is_refund: el.is_refund,
      refund_amount: el.refund_amount,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url
    }
    
  }
  

  // getArrayDetails(el) {
  //   console.log(el);
  //   this.form.patchValue({
  //     created_by: el.created_by,
  //     created_datetime: el.created_datetime,
  //     updated_by: el.updated_by,
  //     updated_datetime: el.updated_datetime,
  //     search: el.search,
  //     insignia_return_detail_uid: el.insignia_return_detail_uid,
  //     insignia_return_uid: el.insignia_return_uid,
  //     insignia_assign_uid: el.insignia_assign_uid,
  //     is_return: el.is_return,
  //     is_pay: el.is_pay,
  //     refund_amount: el.refund_amount,
  //     file_name: el.file_name,
  //     document_name: el.document_name,
  //     document_url: el.document_url,
  //   })
  //   this.cdRef.detectChanges()
  //   // this.request = {
  //   //   status_id: el.status_id,
  //   //   created_by: el.created_by,
  //   //   created_datetime: el.created_datetime,
  //   //   updated_by: el.updated_by,
  //   //   search: el.search,
  //   //   insignia_return_detail_uid: el.insignia_return_detail_uid,
  //   //   insignia_return_guid: el.insignia_return_guid,
  //   //   insignia_detail_guid: el.insignia_detail_guid,
  //   //   is_return: el.is_return,
  //   //   is_pay: el.is_pay,
  //   //   refund_amount: el.refund_amount,
  //   //   document_name: el.document_name,
  //   //   document_url: el.document_url
  //   // }
  // }

  createArrayDetails(el?) {
    console.log('array', el)
    if (el == undefined) {
      return this.formBuilder.group({
        status_id: null,
        created_by: "",
        created_datetime: null,
        updated_by: "",
        search: "",
        insignia_return_detail_uid: null,
        insignia_return_uid: null,
        insignia_assign_uid: null,
        is_return: false,
        is_refund: false,
        refund_amount: 0,
        file_name: '',
        document_name: '',
        document_url: ''
      })
    } else {
      return this.formBuilder.group({
        status_id: el.status_id,
        created_by: el.created_by,
        created_datetime: el.created_datetime,
        updated_by: el.updated_by,
        search: el.search,
        insignia_return_detail_uid: el.insignia_return_detail_uid,
        insignia_return_uid: el.insignia_return_uid,
        insignia_assign_uid: el.insignia_assign_uid,
        is_return: el.is_return,
        is_refund: el.is_refund,
        refund_amount: el.refund_amount,
        file_name: el.file_name,
        document_name: el.document_name,
        document_url: el.document_url
      })
    }
  }

  getResultFile(file) {
    console.log(file);
    this.form.patchValue({
      document_name: file.original_fileName,
      document_url: file.file_url,
    })
  }

  viewfile(url) {
    const dialogRef = this.dialog.open(
      ViewUploadDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: url  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
      }
    })
  }

  save() {
    console.log(this.form.getRawValue());
    // this.insigniaReturnDetailService.add(this.form.getRawValue()).pipe(
    //   catchError(err => {
    //     this.appService.swaltAlertError('Error')
    //     return throwError(err)
    //   })).subscribe((x: any) => {
    //     console.log(x)
    //     this.appService.swaltAlert()
    //   })

    if (this.data.editForm == null) {
      (<FormArray>this.data.form.get('insignia_return_detail')).push(this.createArrayDetails(this.request))
    } else {
      (<FormArray>this.data.form.get('insignia_return_detail')).controls[this.data.ind].patchValue({
        insignia_return_detail_uid: this.request.insignia_return_detail_uid,
        insignia_return_uid: this.request.insignia_return_uid,
        insignia_assign_uid: this.request.insignia_assign_uid,
        is_return: this.request.is_return,
        is_refund: this.request.is_refund,
        refund_amount: this.request.refund_amount,
        file_name: this.request.file_name,
        document_name: this.request.document_name,
        document_url: this.request.document_url
      })
    }
  }

  saveClose() {
    // this.insigniaReturnDetailService.add(this.form.getRawValue()).pipe(
    //   catchError(err => {
    //     this.appService.swaltAlertError('Error')
    //     return throwError(err)
    //   })).subscribe((x: any) => {
    //     console.log(x)
    //     this.appService.swaltAlert()
    //     this.close()
    //   })
    if (this.data.editForm == null) {
      (<FormArray>this.data.form.get('insignia_return_detail')).push(this.createArrayDetails(this.request))
      this.close()
    } else {
      (<FormArray>this.data.form.get('insignia_return_detail')).controls[this.data.ind].patchValue({
        insignia_return_detail_uid: this.request.insignia_return_detail_uid,
        insignia_return_uid: this.request.insignia_return_uid,
        insignia_assign_uid: this.request.insignia_assign_uid,
        is_return: this.request.is_return,
        is_refund: this.request.is_refund,
        refund_amount: this.request.refund_amount,
        file_name: this.request.file_name,
        document_name: this.request.document_name,
        document_url: this.request.document_url
      })
      this.close()
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      insignia_return_detail_uid: null,
      insignia_return_uid: null,
      insignia_assign_uid: null,
      is_return: false,
      is_pay: false,
      refund_amount: 0,
      file_name: '',
      document_name: '',
      document_url: '',
    })
  }

  close() {
    this.dialogRef.close('close')
  }

}
