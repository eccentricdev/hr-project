import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRoyalRightListComponent } from './set-royal-right-list/set-royal-right-list.component';
import { SetRoyalRightFormComponent } from './set-royal-right-form/set-royal-right-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { HoldingRoyaComponent } from './set-royal-right-form/holding-roya/holding-roya.component';

const routes:Routes = [
  {
    path:'',
    component:SetRoyalRightListComponent
  },
  {
    path:'add',
    component:SetRoyalRightFormComponent
  },
  {
    path:'edit/:id',
    component:SetRoyalRightFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetRoyalRightListComponent, 
    SetRoyalRightFormComponent, HoldingRoyaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRoyalRightModule { }
