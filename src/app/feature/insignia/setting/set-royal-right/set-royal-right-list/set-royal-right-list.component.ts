import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { PositionTypesService } from 'src/app/core/service/common/position-types.service';
import { InsigniaRequestConditionService } from 'src/app/core/service/insignia/insignia-request-condition.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-royal-right-list',
  templateUrl: './set-royal-right-list.component.html',
  styleUrls: ['./set-royal-right-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalRightListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  insigniaRequestCondition$ = new Observable()
  PersonnelTypes$ = new Observable()
  req = {
    search : '',
    medalRequestCondition_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public InsigniaRequestConditionSV : InsigniaRequestConditionService,
    public PersonnelTypesSV : PersonnelTypesService,
  ) { super()
    this.insigniaRequestCondition$ = this.InsigniaRequestConditionSV.getAll().pipe(
      tap(x=>console.log(x)),
      map(x=> this.updateMatTable(x))
    )
    this.PersonnelTypes$ = this.PersonnelTypesSV.getAll()
   }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.insigniaRequestCondition$  = this.InsigniaRequestConditionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  } 

  clear(){
    this.req = {
      search : '',
      medalRequestCondition_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/set_royal_right/add'])
  }
  editPage(id){
    this.router.navigate(['app/set_royal_right/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.InsigniaRequestConditionSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.insigniaRequestCondition$ = this.InsigniaRequestConditionSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}