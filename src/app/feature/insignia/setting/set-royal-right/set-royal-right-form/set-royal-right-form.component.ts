import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PersonnelTypesService } from 'src/app/core/service/common/personnel-types.service';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { PositionTypesService } from 'src/app/core/service/common/position-types.service';
import { WorkFieldsService } from 'src/app/core/service/common/work-fields.service';
import { WorkGroupsService } from 'src/app/core/service/common/work-groups.service';
import { InsigniaConditionDetailService } from 'src/app/core/service/insignia/insignia-condition-detail.service';
import { InsigniaConditionHoldingRoyalService } from 'src/app/core/service/insignia/insignia-condition-holding-royal.service';
import { InsigniaConditionPositionLevelService } from 'src/app/core/service/insignia/insignia-condition-position-level.service';
import { InsigniaConditionPositionTypeService } from 'src/app/core/service/insignia/insignia-condition-position-type.service';
import { InsigniaConditionPositionService } from 'src/app/core/service/insignia/insignia-condition-position.service';
import { InsigniaConditionWorkGroupService } from 'src/app/core/service/insignia/insignia-condition-work-group.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaRequestConditionService } from 'src/app/core/service/insignia/insignia-request-condition.service';
import { InsigniaRequestDocumentService } from 'src/app/core/service/insignia/insignia-request-document.service';
import { OperatorCompareService } from 'src/app/core/service/insignia/operator-compare.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { WorkChartHeaderService } from 'src/app/core/service/organization/work-chart-header.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { HoldingRoyaComponent } from './holding-roya/holding-roya.component';

@Component({
  selector: 'app-set-royal-right-form',
  templateUrl: './set-royal-right-form.component.html',
  styleUrls: ['./set-royal-right-form.component.scss'],
 // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalRightFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]
  insigniaConditionPositionType$ = new Observable()
  Position$ = new Observable()
  positionLevels$ = new Observable()
  workGroups$ = new Observable()
  positionTypes$ = new Observable()
  operatorCompare$ = new Observable()
  insigniaDetail$ = new Observable()
  personnelTypes$ = new Observable()

  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public InsigniaRequestConditionSV : InsigniaRequestConditionService,
    public InsigniaRequestDocumentSV : InsigniaRequestDocumentService,
    public InsigniaConditionDetailSV : InsigniaConditionDetailService,
    public PersonnelTypesSV : PersonnelTypesService,
    public PositionSV : PositionService,
    public PositionLevelsSV : PositionLevelsService,
    public WorkGroupsSV : WorkGroupsService,
    public PositionTypesSV : PositionTypesService,
    public InsigniaDetailSV : InsigniaDetailService,
    public OperatorCompareSV : OperatorCompareService,
    
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.Position$ = this.PositionSV.getAll()
    this.positionLevels$ = this.PositionLevelsSV.getAll()
    this.workGroups$ = this.WorkGroupsSV.getAll()
    this.positionTypes$ = this.PositionTypesSV.getAll()
    this.insigniaDetail$ = this.InsigniaDetailSV.query(`/insignia`)
    this.operatorCompare$ = this.OperatorCompareSV.getAll()
    this.personnelTypes$ = this.PersonnelTypesSV.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.InsigniaRequestConditionSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue(res)
    let insignia_condition_detail = this.form.get('insignia_condition_detail') as FormArray
    let formdoc = this.form.get('insignia_request_document') as FormArray
    if(res.insignia_condition_detail){
      res.insignia_condition_detail.forEach(element => {
        insignia_condition_detail.push(this.get_insignia_condition_detail(element))
      });
    }
   
 
   
    if(res.insignia_request_document){
      res.insignia_request_document.forEach(element => {
        formdoc.push(this.getAR_doc(element))
      });
    }
  }
  add_iinsignia_condition_detail(){
    let insignia_condition_detail = this.form.get('insignia_condition_detail') as FormArray
    insignia_condition_detail.insert(0,this.create_insignia_condition_detail())
  }
  
  del_insignia_condition_detail(item, i) {
    if (item.insignia_condition_detail_uid) {
      this.InsigniaConditionDetailSV.deleteData(item.insignia_condition_detail_uid).pipe(
        catchError(err => {
          this.appSV.swaltAlertError('Error')
          return throwError(err)
        })).subscribe((x: any) => {
          console.log(x)
          this.ngOnInit()
          this.appSV.swaltAlert()
        })
    } else {
      let formarray = this.form.get('insignia_condition_detail') as FormArray
      formarray.removeAt(i)
    }
  }
  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: {editForm: null, state:'add'}         
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('insignia_request_document') as FormArray
          formarray.push(this.createAR_doc(callback))
        }
      }
    })

  }
  add_holding_royal() {
    const dialogRef = this.dialog.open(
      HoldingRoyaComponent, {
      width: '70%',
      // disableClose: true,
      data: this.form        
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }
  edit_holding_royal(form,index) {
    const dialogRef = this.dialog.open(
      HoldingRoyaComponent, {
      width: '70%',
      // disableClose: true,
      data: { form:this.form,formEdit:form , index : index }
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
     
    })

  }

  editfile(array,index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array , state:'edit' ,remark: array.remark}  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ                     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('insignia_request_document') as FormArray
          formarray.controls[index].patchValue({
            file_name: callback.file_name,
            remark:callback.remark
          })
        }

      }
    })

  }
  downloadDoc(url){
    this.InsigniaRequestConditionSV.download_file(url)
  }
  deleteItemDoc(item, i) {
    if (item.insignia_request_document_uid) {
      this.InsigniaRequestDocumentSV.deleteData(item.insignia_request_document_uid).pipe(
        catchError(err => {
          this.appSV.swaltAlertError('Error')
          return throwError(err)
        })).subscribe((x: any) => {
          console.log(x)
          this.ngOnInit()
          this.appSV.swaltAlert()
        })
    } else {
      let formarray = this.form.get('insignia_request_document') as FormArray
      formarray.removeAt(i)
    }
  }
  getAR_doc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      insignia_request_document_uid: el.insignia_request_document_uid,
      insignia_request_condition_uid: el.insignia_request_condition_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }
  createAR_doc(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      insignia_request_document_uid: null,
      insignia_request_condition_uid: this.id? this.id : null,
      file_name: el.file_name,
      document_name: el.file?.original_fileName,
      document_url: el.file?.file_url,
      mime_type_name: el.file?.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }



  close(){
    this.router.navigate(['app/set_royal_right'])
  }

  save() {
    switch (this.state) {
      case 'edit':
    
        this.InsigniaRequestConditionSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_right'])
          })
        break;
      case 'add':
      
        this.InsigniaRequestConditionSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_right'])
          })
        break;
    }
  }
 
  get_insignia_condition_detail(el) {
    return this.formBuilder.group({
      status_id:el.status_id,
      created_by:el.created_by,
      created_datetime:el.created_datetime,
      updated_by:el.updated_by,
      updated_datetime:el.updated_datetime,
      search:el.search,
      insignia_condition_detail_uid:el.insignia_condition_detail_uid,
      insignia_request_condition_uid:el.insignia_request_condition_uid,
      work_group_id:el.work_group_id,
      position_type_id:el.position_type_id,
      position_level_id:el.position_level_id,
      position_uid:el.position_uid,
      work_at_position_year:el.work_at_position_year,
      operator_compare_work_id:el.operator_compare_work_id,
      holding_insignia_uid:el.holding_insignia_uid,
      can_request_insignia_uid:el.can_request_insignia_uid,
      operator_compare_holder_id:el.operator_compare_holder_id,
      holding_year:el.holding_year,
      salary_min_amount:el.salary_min_amount,
      salary_max_amount:el.salary_max_amount,
      request_at_retire_year:el.request_at_retire_year,
    })
  }
  create_insignia_condition_detail() {
    return this.formBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      search: [''],
      insignia_condition_detail_uid: [null],
      insignia_request_condition_uid: [null],
      work_group_id: [null],
      position_type_id: [null],
      position_level_id: [null],
      position_uid: [null],
      work_at_position_year: [null],
      operator_compare_work_id: [null],
      holding_insignia_uid: [null],
      can_request_insignia_uid: [null],
      operator_compare_holder_id: [null],
      holding_year: [null],
      salary_min_amount: [null],
      salary_max_amount: [null],
      request_at_retire_year: true,
    
    })
  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      insignia_request_condition_uid: [null],
      personnel_type_id: [null],
      insignia_request_condition_code: [''],
      insignia_request_condition_name: [''],
      affiliated_under_ministerial: [true],
      duration_operator_compare: [null],
      duration_year: [0],
      before_request_days: [0],
      insignia_condition_detail: this.baseFormBuilder.array([]),
      insignia_request_document: this.baseFormBuilder.array([]),
    })
  }

}
