import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PositionLevelsService } from 'src/app/core/service/common/position-levels.service';
import { PositionTypesService } from 'src/app/core/service/common/position-types.service';
import { WorkGroupsService } from 'src/app/core/service/common/work-groups.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaRequestConditionService } from 'src/app/core/service/insignia/insignia-request-condition.service';
import { OperatorCompareService } from 'src/app/core/service/insignia/operator-compare.service';
import { PositionService } from 'src/app/core/service/organization/position.service';

@Component({
  selector: 'app-holding-roya',
  templateUrl: './holding-roya.component.html',
  styleUrls: ['./holding-roya.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class HoldingRoyaComponent implements OnInit {
  trainForm = {
    status_id: null,
    created_by: '',
    created_datetime: null,
    updated_by: '',
    updated_datetime: null,
    insignia_condition_detail_uid: null,
    insignia_request_condition_uid: null,
    work_group_id: null,
    position_type_id: null,
    position_level_id: null,
    position_uid: null,
    work_at_position_year: 0,
    operator_compare_work_id: null,
    holding_insignia_uid: null,
    can_request_insignia_uid: null,
    operator_compare_holder_id: null,
    holding_year: 0,
    salary_min_amount: 0,
    salary_max_amount: 0,
    request_at_retire_year: true,
  }
  insigniaDetail$ = new Observable()
  operatorCompare$ = new Observable()
  workGroups$ = new Observable()
  positionTypes$ = new Observable()
  positionLevels$ = new Observable()
  constructor(
    public dialogRef: MatDialogRef<HoldingRoyaComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data,
    public InsigniaRequestConditionSV : InsigniaRequestConditionService,
    public WorkGroupsSV : WorkGroupsService,
    public PositionTypesSV : PositionTypesService,
    public PositionLevelsSV : PositionLevelsService,
    public PositionSV : PositionService,
    public InsigniaDetailSV : InsigniaDetailService,
    public OperatorCompareSV : OperatorCompareService,
  ) { 
    // this.insigniaDetail$ = this.InsigniaDetailSV.query(`/insignia`)
    this.insigniaDetail$ = this.InsigniaDetailSV.query('/insignia').pipe(
      map((x:any) => {
             let data =  x.map((x:any) => {
                return { ...x, displayName: x.insignia_short_name + ' ' + x.insignia_name };
              })
              return data
          })
    )
    this.operatorCompare$ = this.OperatorCompareSV.getAll()
    this.workGroups$ = this.WorkGroupsSV.getAll()
    this.positionTypes$ = this.PositionTypesSV.getAll()
    this.positionLevels$ = this.PositionLevelsSV.getAll()
  }

  ngOnInit(): void {
    if(this.data.formEdit != null){
      this.trainForm = {
        status_id:this.data.formEdit.status_id,
        created_by:this.data.formEdit.created_by,
        created_datetime:this.data.formEdit.created_datetime,
        updated_by:this.data.formEdit.updated_by,
        updated_datetime:this.data.formEdit.updated_datetime,
        insignia_condition_detail_uid:this.data.formEdit.insignia_condition_detail_uid,
        insignia_request_condition_uid:this.data.formEdit.insignia_request_condition_uid,
        work_group_id:this.data.formEdit.work_group_id,
        position_type_id:this.data.formEdit.position_type_id,
        position_level_id:this.data.formEdit.position_level_id,
        position_uid:this.data.formEdit.position_uid,
        work_at_position_year:this.data.formEdit.work_at_position_year,
        operator_compare_work_id:this.data.formEdit.operator_compare_work_id,
        holding_insignia_uid:this.data.formEdit.holding_insignia_uid,
        can_request_insignia_uid:this.data.formEdit.can_request_insignia_uid,
        operator_compare_holder_id:this.data.formEdit.operator_compare_holder_id,
        holding_year:this.data.formEdit.holding_year,
        salary_min_amount:this.data.formEdit.salary_min_amount,
        salary_max_amount:this.data.formEdit.salary_max_amount,
        request_at_retire_year:this.data.formEdit.request_at_retire_year,
      }
    }
  }
  
  saveClose() {
    
    if(this.data.formEdit == null){
      (<FormArray>this.data.get('insignia_condition_detail')).insert(null, this.InsigniaRequestConditionSV.get_insignia_condition_detail(this.trainForm));
      this.close();
      }else{
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('status_id').setValue(this.trainForm.status_id); ;
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('created_by').setValue(this.trainForm.created_by);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('created_datetime').setValue(this.trainForm.created_datetime);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('updated_by').setValue(this.trainForm.updated_by);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('updated_datetime').setValue(this.trainForm.updated_datetime);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('insignia_condition_detail_uid').setValue(this.trainForm.insignia_condition_detail_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('insignia_request_condition_uid').setValue(this.trainForm.insignia_request_condition_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('work_group_id').setValue(this.trainForm.work_group_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_type_id').setValue(this.trainForm.position_type_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_level_id').setValue(this.trainForm.position_level_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_uid').setValue(this.trainForm.position_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('work_at_position_year').setValue(this.trainForm.work_at_position_year);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('operator_compare_work_id').setValue(this.trainForm.operator_compare_work_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('holding_insignia_uid').setValue(this.trainForm.holding_insignia_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('can_request_insignia_uid').setValue(this.trainForm.can_request_insignia_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('operator_compare_holder_id').setValue(this.trainForm.operator_compare_holder_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('holding_year').setValue(this.trainForm.holding_year);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('salary_min_amount').setValue(this.trainForm.salary_min_amount);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('salary_max_amount').setValue(this.trainForm.salary_max_amount);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('request_at_retire_year').setValue(this.trainForm.request_at_retire_year);
        this.close();
      }
  }
  save() {
    if(this.data.formEdit == null){
      (<FormArray>this.data.get('insignia_condition_detail')).insert(null, this.InsigniaRequestConditionSV.get_insignia_condition_detail(this.trainForm));
      }else{
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('status_id').setValue(this.trainForm.status_id); ;
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('created_by').setValue(this.trainForm.created_by);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('created_datetime').setValue(this.trainForm.created_datetime);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('updated_by').setValue(this.trainForm.updated_by);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('updated_datetime').setValue(this.trainForm.updated_datetime);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('insignia_condition_detail_uid').setValue(this.trainForm.insignia_condition_detail_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('insignia_request_condition_uid').setValue(this.trainForm.insignia_request_condition_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('work_group_id').setValue(this.trainForm.work_group_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_type_id').setValue(this.trainForm.position_type_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_level_id').setValue(this.trainForm.position_level_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('position_uid').setValue(this.trainForm.position_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('work_at_position_year').setValue(this.trainForm.work_at_position_year);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('operator_compare_work_id').setValue(this.trainForm.operator_compare_work_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('holding_insignia_uid').setValue(this.trainForm.holding_insignia_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('can_request_insignia_uid').setValue(this.trainForm.can_request_insignia_uid);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('operator_compare_holder_id').setValue(this.trainForm.operator_compare_holder_id);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('holding_year').setValue(this.trainForm.holding_year);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('salary_min_amount').setValue(this.trainForm.salary_min_amount);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('salary_max_amount').setValue(this.trainForm.salary_max_amount);
        (<FormArray>this.data.form.get('insignia_condition_detail')).controls[this.data.index].get('request_at_retire_year').setValue(this.trainForm.request_at_retire_year);
      }
    
    
  }

  close() {
    this.dialogRef.close('close')
  }
}
