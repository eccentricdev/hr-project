import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRoyalDateListComponent } from './set-royal-date-list/set-royal-date-list.component';
import { SetRoyalDateFormComponent } from './set-royal-date-form/set-royal-date-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SetCourtFormComponent } from 'src/app/feature/personnel-discipline/setting/set-court/set-court-form/set-court-form.component';
import { SetCourtListComponent } from 'src/app/feature/personnel-discipline/setting/set-court/set-court-list/set-court-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetRoyalDateListComponent
  },
  {
    path:'add',
    component:SetRoyalDateFormComponent
  },
  {
    path:'edit/:id',
    component:SetRoyalDateFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetRoyalDateListComponent, 
    SetRoyalDateFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRoyalDateModule { }
