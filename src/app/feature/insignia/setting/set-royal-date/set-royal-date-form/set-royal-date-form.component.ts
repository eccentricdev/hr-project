import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { BestowScheduleService } from 'src/app/core/service/insignia/bestow-schedule.service';

@Component({
  selector: 'app-set-royal-date-form',
  templateUrl: './set-royal-date-form.component.html',
  styleUrls: ['./set-royal-date-form.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalDateFormComponent extends BaseForm implements OnInit {
  mount = [
    {
      id : 1,
      name : 'มกราคม'
    },
    {
      id : 2,
      name : 'กุมภาพันธ์'
    },
    {
      id : 3,
      name : 'มีนาคม'
    },
    {
      id : 4,
      name : 'เมษายน'
    },
    {
      id : 5,
      name : 'พฤษภาคม'
    },
    {
      id : 6,
      name : 'มิถุนายน'
    },
    {
      id : 7,
      name : 'กรกฎาคม'
    },
    {
      id : 8,
      name : 'สิงหาคม'
    },
    {
      id : 9,
      name : 'กันยายน'
    },
    {
      id : 10,
      name : 'ตุลาคม'
    },
    {
      id : 11,
      name : 'พฤศจิกายน'
    },
    {
      id : 12,
      name : 'ธันวาคม'
    },
  ]
  status$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public BestowScheduleSV : BestowScheduleService,
    public StatusService : StatusService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.status$ = this.StatusService.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.BestowScheduleSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue(res)
  }
  close(){
    this.router.navigate(['app/set_royal_date'])
  }

  save() {
    switch (this.state) {
      case 'edit':
    
        this.BestowScheduleSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_date'])
          })
        break;
      case 'add':
      
        this.BestowScheduleSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_date'])
          })
        break;
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      bestow_schedule_id: [null],
      bestow_day: [null],
      bestow_month: [null],
      bestow_description: [''],
    })
  }

}
