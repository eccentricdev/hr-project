import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { BestowScheduleService } from 'src/app/core/service/insignia/bestow-schedule.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-royal-date-list',
  templateUrl: './set-royal-date-list.component.html',
  styleUrls: ['./set-royal-date-list.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalDateListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  bestowSchedule$ = new Observable()
  insigniaCategory$ = new Observable()
  req = {
    search : '',
    insignia_name : ''
  }
  mount = [
    {
      id : 1,
      name : 'มกราคม'
    },
    {
      id : 2,
      name : 'กุมภาพันธ์'
    },
    {
      id : 3,
      name : 'มีนาคม'
    },
    {
      id : 4,
      name : 'เมษายน'
    },
    {
      id : 5,
      name : 'พฤษภาคม'
    },
    {
      id : 6,
      name : 'มิถุนายน'
    },
    {
      id : 7,
      name : 'กรกฎาคม'
    },
    {
      id : 8,
      name : 'สิงหาคม'
    },
    {
      id : 9,
      name : 'กันยายน'
    },
    {
      id : 10,
      name : 'ตุลาคม'
    },
    {
      id : 11,
      name : 'พฤศจิกายน'
    },
    {
      id : 12,
      name : 'ธันวาคม'
    },
  ]
  constructor(
    public router : Router,
    public appSV : AppService,
    public BestowScheduleSV : BestowScheduleService,
  ) { super()
    this.bestowSchedule$ = this.BestowScheduleSV.getAll().pipe(
      tap(x=>console.log(x)),
      map(x=> this.updateMatTable(x))
    )
    // this.insigniaCategory$ = this.InsigniaCategorySV.getAll()
   }

  ngOnInit(): void {
  }
  
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.bestowSchedule$  = this.BestowScheduleSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      search : '',
      insignia_name : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/set_royal_date/add'])
  }
  editPage(id){
    this.router.navigate(['app/set_royal_date/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.BestowScheduleSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.bestowSchedule$ = this.BestowScheduleSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
