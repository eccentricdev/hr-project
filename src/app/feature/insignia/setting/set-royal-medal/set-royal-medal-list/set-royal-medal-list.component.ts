import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { MedalRequestConditionService } from 'src/app/core/service/insignia/medal-request-condition.service';
import { OperatorCompareService } from 'src/app/core/service/insignia/operator-compare.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-royal-medal-list',
  templateUrl: './set-royal-medal-list.component.html',
  styleUrls: ['./set-royal-medal-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalMedalListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  medalRequestCondition$ = new Observable()
  insigniaDetail$ = new Observable()
  operatorCompare$ = new Observable()
  req = {
    search : '',
    medalRequestCondition_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public MedalRequestConditionSV : MedalRequestConditionService,
    public InsigniaDetailSV : InsigniaDetailService,
    public OperatorCompareSV : OperatorCompareService,
  ) { super()
    this.medalRequestCondition$ = this.MedalRequestConditionSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
    this.insigniaDetail$ = this.InsigniaDetailSV.query('/medal')
    this.operatorCompare$ = this.OperatorCompareSV.getAll()
   }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.medalRequestCondition$  = this.MedalRequestConditionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  } 

  clear(){
    this.req = {
      search : '',
      medalRequestCondition_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/set_royal_medal/add'])
  }
  editPage(id){
    this.router.navigate(['app/set_royal_medal/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.MedalRequestConditionSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.medalRequestCondition$ = this.MedalRequestConditionSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
