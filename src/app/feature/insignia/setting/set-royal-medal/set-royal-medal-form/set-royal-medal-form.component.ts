import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { MedalRequestConditionService } from 'src/app/core/service/insignia/medal-request-condition.service';
import { MedalRequestDocumentService } from 'src/app/core/service/insignia/medal-request-document.service';
import { OperatorCompareService } from 'src/app/core/service/insignia/operator-compare.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-set-royal-medal-form',
  templateUrl: './set-royal-medal-form.component.html',
  styleUrls: ['./set-royal-medal-form.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalMedalFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]
  operatorCompare$ = new Observable()
  insigniaDetail$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public MedalRequestConditionSV: MedalRequestConditionService,
    public MedalRequestDocumentSV: MedalRequestDocumentService,
    public OperatorCompareSV: OperatorCompareService,
    public InsigniaDetailSV: InsigniaDetailService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.operatorCompare$ = this.OperatorCompareSV.getAll()
    this.insigniaDetail$ = this.InsigniaDetailSV.query('/medal')
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.MedalRequestConditionSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue(res)
    let formdoc = this.form.get('medal_request_document') as FormArray
    if(res.medal_request_document){
      res.medal_request_document.forEach(element => {
        formdoc.push(this.getAR_doc(element))
      });
    }
  }

  

  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: {editForm: null, state:'add'}         
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('medal_request_document') as FormArray
          formarray.push(this.createAR_doc(callback))
        }
      }
    })

  }

  editfile(array,index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array , state:'edit' ,remark: array.remark}  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ                     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('medal_request_document') as FormArray
          formarray.controls[index].patchValue({
            file_name: callback.file_name,
            remark:callback.remark
          })
        }

      }
    })

  }
  downloadDoc(url){
    this.MedalRequestConditionSV.download_file(url)
  }
  deleteItemDoc(item, i) {
    if (item.medal_request_document_uid) {
      this.MedalRequestDocumentSV.deleteData(item.medal_request_document_uid).pipe(
        catchError(err => {
          this.appSV.swaltAlertError('Error')
          return throwError(err)
        })).subscribe((x: any) => {
          console.log(x)
          this.ngOnInit()
          this.appSV.swaltAlert()
        })
    } else {
      let formarray = this.form.get('medal_request_document') as FormArray
      formarray.removeAt(i)
    }
  }
  getAR_doc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      medal_request_document_uid: el.medal_request_document_uid,
      medal_request_condition_uid: el.medal_request_condition_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }
  createAR_doc(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      medal_request_document_uid: null,
      medal_request_condition_uid: this.id? this.id : null,
      file_name: el.file_name,
      document_name: el.file?.original_fileName,
      document_url: el.file?.file_url,
      mime_type_name: el.file?.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }

  close(){
    this.router.navigate(['app/set_royal_medal'])
  }

  save() {
    switch (this.state) {
      case 'edit':
    
        this.MedalRequestConditionSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_medal'])
          })
        break;
      case 'add':
      
        this.MedalRequestConditionSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_medal'])
          })
        break;
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      medal_request_condition_uid: [null],
      personnel_type_id: [null],
      medal_request_condition_code: [''],
      insignia_uid: [null],
      affiliated_under_ministerial: [true],
      duration_operator_compare: [null],
      duration_year: [0],
      before_request_days: [0],
      medal_request_document: this.baseFormBuilder.array([]),
    })
  }

}
