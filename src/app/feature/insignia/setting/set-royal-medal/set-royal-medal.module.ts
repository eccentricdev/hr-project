import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRoyalMedalListComponent } from './set-royal-medal-list/set-royal-medal-list.component';
import { SetRoyalMedalFormComponent } from './set-royal-medal-form/set-royal-medal-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetRoyalMedalListComponent
  },
  {
    path:'add',
    component:SetRoyalMedalFormComponent
  },
  {
    path:'edit/:id',
    component:SetRoyalMedalFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetRoyalMedalListComponent, 
    SetRoyalMedalFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRoyalMedalModule { }
