import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaTypeService } from 'src/app/core/service/insignia/insignia-type.service';

import { RefundDocumentService } from 'src/app/core/service/insignia/refund-document.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-set-royal-details-form',
  templateUrl: './set-royal-details-form.component.html',
  styleUrls: ['./set-royal-details-form.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalDetailsFormComponent extends BaseForm implements OnInit {

  academicYear = []
  semesterData = []
  rows:any = [{1:1}]
  host:string = `${environment.rsuArApiName}`
  insignia$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public dialog: MatDialog,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public InsigniaDetailSV : InsigniaDetailService,
    public InsigniaTypeSV : InsigniaTypeService,
    public RefundDocumentSV : RefundDocumentService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.insignia$ = this.InsigniaTypeSV.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        switch (this.state) {
          case 'edit':
            this.InsigniaDetailSV.get(this.id).pipe(
              tap(res => this.setUpform(res)),
              tap(x => console.log(x)),
            ).subscribe()
        }
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue(res)
    let formdoc = this.form.get('refund_document') as FormArray
    if(res.refund_document){
      res.refund_document.forEach(element => {
        formdoc.push(this.getAR_doc(element))
      });
    }
  }
  addfile() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: {editForm: null, state:'add'}         
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('refund_document') as FormArray
          formarray.push(this.createAR_doc(callback))
        }
      }
    })

  }

  editfile(array,index) {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '70%',
      // disableClose: true,
      data: { editForm: array , state:'edit' ,remark: array.remark}  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ                     
    })
    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
      if (callback) {
        if(callback.file != null){
          let formarray = this.form.get('refund_document') as FormArray
          formarray.controls[index].patchValue({
            file_name: callback.file_name,
            remark:callback.remark
          })
        }

      }
    })

  }
  downloadDoc(url){
    this.InsigniaDetailSV.download_file(url)
  }
  deleteItemDoc(item, i) {
    if (item.refund_document_uid) {
      this.RefundDocumentSV.deleteData(item.refund_document_uid).pipe(
        catchError(err => {
          this.appSV.swaltAlertError('Error')
          return throwError(err)
        })).subscribe((x: any) => {
          console.log(x)
          this.ngOnInit()
          this.appSV.swaltAlert()
        })
    } else {
      let formarray = this.form.get('refund_document') as FormArray
      formarray.removeAt(i)
    }
  }
  getAR_doc(el) {
    return this.baseFormBuilder.group({
      status_id: el.status_id,
      created_by: el.created_by,
      created_datetime: el.created_datetime,
      updated_by: el.updated_by,
      updated_datetime: el.updated_datetime,
      refund_document_uid: el.refund_document_uid,
      insignia_uid: el.insignia_uid,
      file_name: el.file_name,
      document_name: el.document_name,
      document_url: el.document_url,
      mime_type_name: el.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }
  createAR_doc(el) {
    console.log(el);
    return this.baseFormBuilder.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      refund_document_uid: null,
      insignia_uid: this.id? this.id : null,
      file_name: el.file_name,
      document_name: el.file?.original_fileName,
      document_url: el.file?.file_url,
      mime_type_name: el.file?.mime_type_name,
      row_order: el.row_order,
      remark:el.remark
    })
  }

  getFile_insignia_man_photo_url(file) {
    console.log(file);
    this.form.get('insignia_man_photo_url').setValue(file.file_url)
  }
  getFile_insignia_woman_photo_url(file) {
    console.log(file);
    this.form.get('insignia_woman_photo_url').setValue(file.file_url)
  }
  getFile_decorate_man_photo_url(file) {
    console.log(file);
    this.form.get('decorate_man_photo_url').setValue(file.file_url)
  }
  getFile_decorate_woman_photo_url(file) {
    console.log(file);
    this.form.get('decorate_woman_photo_url').setValue(file.file_url)
  }

  
  close(){
    this.router.navigate(['app/set_royal_details'])
  }

  save() {
    switch (this.state) {
      case 'edit':
    
        this.InsigniaDetailSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_details'])
          })
        break;
      case 'add':
      
        this.InsigniaDetailSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_details'])
          })
        break;
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      insignia_uid: [null],
      insignia_type_id: [null],
      insignia_code: [''],
      insignia_name: [''],
      insignia_short_name: [''],
      insignia_level_name: [''],
      insignia_level: [0],
      insignia_line_level: [0],
      is_return: [true],
      refund_man_amount: [0],
      refund_woman_amount: [0],
      remark: [''],
      insignia_man_photo_url: [''],
      insignia_woman_photo_url: [''],
      decorate_man_photo_url: [''],
      decorate_woman_photo_url: [''],
      refund_document: this.baseFormBuilder.array([]),
    })
  }

}
