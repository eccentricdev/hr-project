import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRoyalDetailsFormComponent } from './set-royal-details-form/set-royal-details-form.component';
import { SetRoyalDetailsListComponent } from './set-royal-details-list/set-royal-details-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';



const routes:Routes = [
  {
    path:'',
    component:SetRoyalDetailsListComponent
  },
  {
    path:'add',
    component:SetRoyalDetailsFormComponent
  },
  {
    path:'edit/:id',
    component:SetRoyalDetailsFormComponent
  },
  
]


@NgModule({
  declarations: [
    SetRoyalDetailsFormComponent, 
    SetRoyalDetailsListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRoyalDetailsModule { }
