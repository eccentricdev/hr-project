import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaDetailService } from 'src/app/core/service/insignia/insignia-detail.service';
import { InsigniaTypeService } from 'src/app/core/service/insignia/insignia-type.service';
import { MedalRequestConditionService } from 'src/app/core/service/insignia/medal-request-condition.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-royal-details-list',
  templateUrl: './set-royal-details-list.component.html',
  styleUrls: ['./set-royal-details-list.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalDetailsListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  insigniaDetail$ = new Observable()
  insignia$ = new Observable()
  req = {
    search : '',
    medalRequestCondition_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public InsigniaDetailSV : InsigniaDetailService,
    public InsigniaTypeSV : InsigniaTypeService
  ) { super()
    this.insigniaDetail$ = this.InsigniaDetailSV.query('/all').pipe(
      tap(x=>console.log(x)),
      map(x=> this.updateMatTable(x))
    )
    this.insignia$ = this.InsigniaTypeSV.getAll().pipe(
      tap(x=>console.log(x))
    )
   }

  ngOnInit(): void {
  }
  
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.insigniaDetail$  = this.InsigniaDetailSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  } 

  clear(){
    this.req = {
      search : '',
      medalRequestCondition_name_th : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/set_royal_details/add'])
  }
  editPage(id){
    this.router.navigate(['app/set_royal_details/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.InsigniaDetailSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.insigniaDetail$ = this.InsigniaDetailSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
