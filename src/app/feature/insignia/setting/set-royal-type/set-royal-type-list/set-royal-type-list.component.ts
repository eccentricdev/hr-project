import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaCategoryService } from 'src/app/core/service/insignia/insignia-category.service';
import { InsigniaTypeService } from 'src/app/core/service/insignia/insignia-type.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-royal-type-list',
  templateUrl: './set-royal-type-list.component.html',
  styleUrls: ['./set-royal-type-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalTypeListComponent extends BaseList implements OnInit {
  rows:any = [{1:1}]
  insignia$ = new Observable()
  insigniaCategory$ = new Observable()
  req = {
    search : '',
    insignia_name : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public InsigniaTypeSV : InsigniaTypeService,
    public InsigniaCategorySV : InsigniaCategoryService,
  ) { super()
    this.insignia$ = this.InsigniaTypeSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
    this.insigniaCategory$ = this.InsigniaCategorySV.getAll()
   }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.insignia$  = this.InsigniaTypeSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      search : '',
      insignia_name : ''
    }
    this.search()
  }
  addPage(){
    this.router.navigate(['app/set_royal_type/add'])
  }
  editPage(id){
    this.router.navigate(['app/set_royal_type/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.InsigniaTypeSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.insignia$ = this.InsigniaTypeSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
