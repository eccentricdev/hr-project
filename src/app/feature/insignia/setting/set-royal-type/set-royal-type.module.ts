import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetRoyalTypeListComponent } from './set-royal-type-list/set-royal-type-list.component';
import { SetRoyalTypeFormComponent } from './set-royal-type-form/set-royal-type-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetRoyalTypeListComponent
  },
  {
    path:'add',
    component:SetRoyalTypeFormComponent
  },
  {
    path:'edit/:id',
    component:SetRoyalTypeFormComponent
  },
  
]

@NgModule({
  declarations: [
    SetRoyalTypeListComponent, 
    SetRoyalTypeFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetRoyalTypeModule { }
