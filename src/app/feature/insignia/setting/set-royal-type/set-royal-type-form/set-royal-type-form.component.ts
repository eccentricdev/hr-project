import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { InsigniaCategoryService } from 'src/app/core/service/insignia/insignia-category.service';
import { InsigniaTypeService } from 'src/app/core/service/insignia/insignia-type.service';

@Component({
  selector: 'app-set-royal-type-form',
  templateUrl: './set-royal-type-form.component.html',
  styleUrls: ['./set-royal-type-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetRoyalTypeFormComponent extends BaseForm implements OnInit {
  insigniaCategory$ = new Observable()
  constructor(public formBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public InsigniaCategorySV: InsigniaCategoryService,
    public InsigniaTypeSV: InsigniaTypeService,
    public appSV: AppService,) {
    super(formBuilder, activeRoute);
    this.insigniaCategory$ = this.InsigniaCategorySV.getAll()
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.InsigniaTypeSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }
  setUpform(res) {
    // patch value 
    this.form.patchValue(res)
  }
  close(){
    this.router.navigate(['app/set_royal_type'])
  }

  save() {
    switch (this.state) {
      case 'edit':
    
        this.InsigniaTypeSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_type'])
          })
        break;
      case 'add':
      
        this.InsigniaTypeSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/set_royal_type'])
          })
        break;
    }
  }

  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      insignia_type_id: [null],
      insignia_category_id: [null],
      insignia_type_code: [''],
      insignia_type_name_th: [''],
      insignia_type_description: [''],
    })
  }

}
