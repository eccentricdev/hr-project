import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { AssignOutsiderReportService } from 'src/app/core/service/insignia/insignia-report/assign-outsider-report.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-insignia-out-print-report',
  templateUrl: './insignia-out-print-report.component.html',
  styleUrls: ['./insignia-out-print-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InsigniaOutPrintReportComponent implements OnInit {

  url: any
  searchSearch = {
    start_date: null,
    end_date: null,
  }
  fileData = [
    {
      id: 'xls',
      name: 'Excel'
    },
    {
      id: 'mht',
      name: 'HTML'
    },
    {
      id: 'pdf',
      name: 'PDF'
    },
    {
      id: 'csv',
      name: 'CSV'
    },
  ]
  fileDownload: any

  constructor(private sanitizer: DomSanitizer,
    public assignOutsiderReportService: AssignOutsiderReportService,
    public AppSV: AppService,
    public datepipe: DatePipe,
    public router: Router) { }

  ngOnInit(): void {
    this.assignOutsiderReportService.getSearchReport().pipe(
      tap(x => console.log(x)),
      tap(x => {
        this.searchSearch = {
          start_date : x.start_date,
          end_date : x.end_date,
        }
      })
    ).subscribe(x => {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.rsuArApiName}/api/insignia/report/AssignOutsider/Print/pdf?start_date=${x.start_date}&end_date=${x.end_date}`)
    })
  }

  back() {
    this.router.navigate(['app/insignia_out_report'])
  }


  dowload() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.rsuArApiName}/api/insignia/report/AssignOutsider/Print/${this.fileDownload}?start_date=${this.searchSearch.start_date}&end_date=${this.searchSearch.end_date}`)
  }

}
