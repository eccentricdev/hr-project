import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsigniaOutReportComponent } from './insignia-out-report/insignia-out-report.component';
import { InsigniaOutPrintReportComponent } from './insignia-out-print-report/insignia-out-print-report.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:InsigniaOutReportComponent,
  } ,
  {
    path:'list',
    component:InsigniaOutPrintReportComponent,
  } 
]

@NgModule({
  declarations: [
    InsigniaOutReportComponent,
    InsigniaOutPrintReportComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class InsigniaOutReportModule { }
