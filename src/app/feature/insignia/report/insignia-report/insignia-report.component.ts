
import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { configs } from 'src/app/core/configs/config';
import { AppService } from 'src/app/core/service/app.service';
import { AssignPersonnelReportService } from 'src/app/core/service/insignia/insignia-report/assign-personnel-report.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';

@Component({
  selector: 'app-insignia-report',
  templateUrl: './insignia-report.component.html',
  styleUrls: ['./insignia-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InsigniaReportComponent implements OnInit {

  assignPersonnel$ =  new Observable()
  searchReport$ = Subject
  displayedColumns: string[] = ['1', '2', '3', '4','5','6','7','8','9','10','11'];
  dataSource = [{}];
  show:boolean = false

  dataSearch = {
    start_date:null,
    end_date:null
  }

  startDate:any = null
  endDate:any = null

  constructor(  private sanitizer: DomSanitizer,
    public assignPersonnelReportService : AssignPersonnelReportService,
    public appSV:AppService,
    public datePipe:DatePipe,
    public router : Router) { }

  ngOnInit(): void {
    this.getservice()
  }

  getservice(){
    this.assignPersonnel$ = this.assignPersonnelReportService.getAll().pipe(
      tap(x => console.log(x))
    )
  }

  search(){
    this.show = true
    const start = this.datePipe.transform(this.startDate, configs.formatDate)
    const end = this.datePipe.transform(this.endDate, configs.formatDate)
    this.dataSearch.start_date = start
    this.dataSearch.end_date = end
    console.log(this.dataSearch);
    let queryStr = createQueryStringFromObject(this.dataSearch)
    if (queryStr) {
      this.assignPersonnel$ = this.assignPersonnelReportService.query(`?${queryStr}`).pipe(
        tap(x => console.log(x))
      )
    }
  }

  clear(){
    this.dataSearch = {
      start_date:null,
      end_date:null
    }
    this.startDate = null
    this.endDate = null 
  }



  onClickprint(){
    // this.appSV.searchReport$.next()
    // this.appSV.searchReport$.next(this.dataSearch)
    this.assignPersonnelReportService.nextState(this.dataSearch)
    this.router.navigate(['app/insignia_report/list'])
  }



 

 

  

}
