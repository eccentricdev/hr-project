import { DatePipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { AssignPersonnelReportService } from 'src/app/core/service/insignia/insignia-report/assign-personnel-report.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-insignia-print-report',
  templateUrl: './insignia-print-report.component.html',
  styleUrls: ['./insignia-print-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InsigniaPrintReportComponent implements OnInit {

  url: any
  searchSearch = {
    start_date: null,
    end_date: null,
  }
  fileData = [
    {
      id: 'xls',
      name: 'Excel'
    },
    {
      id: 'mht',
      name: 'HTML'
    },
    {
      id: 'pdf',
      name: 'PDF'
    },
    {
      id: 'csv',
      name: 'CSV'
    },
  ]
  fileDownload: any

  constructor(private sanitizer: DomSanitizer,
    public assignPersonnelReportService: AssignPersonnelReportService,
    public AppSV: AppService,
    public datepipe: DatePipe,
    public router: Router) { }

  ngOnInit(): void {
  // this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.rsuArApiName}/api/insignia/report/AssignPersonnel/Print/pdf`)
    this.assignPersonnelReportService.getSearchReport().pipe(
      tap(x => console.log(x)),
      tap(x => {
        this.searchSearch = {
          start_date : x.start_date,
          end_date : x.end_date,
        }
      })
    ).subscribe(x => {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.rsuArApiName}/api/insignia/report/AssignPersonnel/Print/pdf?start_date=${x.start_date}&end_date=${x.end_date}`)
    })
  }


  back() {
    this.router.navigate(['app/insignia_report'])
  }


  dowload() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.rsuArApiName}/api/insignia/report/AssignPersonnel/Print/${this.fileDownload}?start_date=${this.searchSearch.start_date}&end_date=${this.searchSearch.end_date}`)
  }

}
