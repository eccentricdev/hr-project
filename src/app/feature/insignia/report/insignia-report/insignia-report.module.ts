import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InsigniaReportComponent } from './insignia-report.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { InsigniaPrintReportComponent } from './insignia-print-report/insignia-print-report.component';


const routes:Routes = [
  {
    path:'',
    component:InsigniaReportComponent,
  } ,
  {
    path:'list',
    component:InsigniaPrintReportComponent,
  } 
]


@NgModule({
  declarations: [InsigniaReportComponent, InsigniaPrintReportComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    
  ]
})
export class InsigniaReportModule { }
