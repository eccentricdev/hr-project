import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetDetailJobDescriptionListComponent } from './set-detail-job-description-list/set-detail-job-description-list.component';
import { SetDetailJobDescriptionFormComponent } from './set-detail-job-description-form/set-detail-job-description-form.component';
import { SetDetailJobDescriptionSummariesComponent } from './set-detail-job-description-summaries/set-detail-job-description-summaries.component';
import { SetDetailJobDescriptionSpeciflcationsComponent } from './set-detail-job-description-speciflcations/set-detail-job-description-speciflcations.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetDetailJobDescriptionListComponent
  },
  {
    path:'add',
    component:SetDetailJobDescriptionFormComponent
  },
  {
    path:'edit/:id',
    component:SetDetailJobDescriptionFormComponent
  },
  
]

@NgModule({
  declarations: [SetDetailJobDescriptionListComponent, SetDetailJobDescriptionFormComponent, SetDetailJobDescriptionSummariesComponent, SetDetailJobDescriptionSpeciflcationsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  entryComponents: [SetDetailJobDescriptionSummariesComponent,SetDetailJobDescriptionSpeciflcationsComponent],
})
export class SetDetailJobDescriptionModule { }
