import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDetailJobDescriptionFormComponent } from './set-detail-job-description-form.component';

describe('SetDetailJobDescriptionFormComponent', () => {
  let component: SetDetailJobDescriptionFormComponent;
  let fixture: ComponentFixture<SetDetailJobDescriptionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDetailJobDescriptionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDetailJobDescriptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
