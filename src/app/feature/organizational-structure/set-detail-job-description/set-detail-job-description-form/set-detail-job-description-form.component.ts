import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { JobDescriptionService } from 'src/app/core/service/organization/job-description.service';
import { SetDetailJobDescriptionSpeciflcationsComponent } from '../set-detail-job-description-speciflcations/set-detail-job-description-speciflcations.component';
import { SetDetailJobDescriptionSummariesComponent } from '../set-detail-job-description-summaries/set-detail-job-description-summaries.component';

@Component({
  selector: 'app-set-detail-job-description-form',
  templateUrl: './set-detail-job-description-form.component.html',
  styleUrls: ['./set-detail-job-description-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDetailJobDescriptionFormComponent extends BaseForm implements OnInit {
  rows :any = []
  constructor(public dialog :MatDialog,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public JobDescriptionSV: JobDescriptionService,
    public router:Router) { 
      super(fb, activeRoute)
    }

    ngOnInit(): void {
      switch (this.state) {
        case 'edit':
          this.JobDescriptionSV.get(this.id).pipe(
            tap(res => this.setUpform(res)),
            tap(x => console.log(x)),
          ).subscribe()
          break;
        case 'add':
  
          break;
      }
    }
  
    setUpform(res) {
      // patch value 
      this.form.patchValue({
        status_id:res.status_id,
        created_by:res.created_by,
        created_datetime:res.created_datetime,
        updated_by:res.updated_by,
        updated_datetime:res.updated_datetime,
        job_description_uid:res.job_description_uid,
        job_description_code:res.job_description_code,
        job_description_name_en:res.job_description_name_en,
        job_description_name_th:res.job_description_name_th,
        remark:res.remark,
      })
    }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    openSummaries() {
      const dialogRef = this.dialog.open(
        SetDetailJobDescriptionSummariesComponent, {
        width: '40%',
        disableClose: true,
        data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }
    openSpecifications() {
      const dialogRef = this.dialog.open(
        SetDetailJobDescriptionSpeciflcationsComponent, {
        width: '40%',
        disableClose: true,
        data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }
    close(){
      this.router.navigate(['app/detail_job_description'])
    }
    save() {
      switch (this.state) {
        case 'edit':
      
          this.JobDescriptionSV.update(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/detail_job_description'])
            })
          break;
        case 'add':
        
          this.JobDescriptionSV.add(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/detail_job_description'])
            })
          break;
      }
  
    }
    createForm() {
      return this.baseFormBuilder.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        job_description_uid: [null],
        job_description_code: [''],
        job_description_name_en: [],
        job_description_name_th: [''],
        remark: [''],
      })
    }
}
