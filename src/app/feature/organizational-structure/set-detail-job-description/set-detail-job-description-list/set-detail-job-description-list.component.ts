import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { JobDescriptionService } from 'src/app/core/service/organization/job-description.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-detail-job-description-list',
  templateUrl: './set-detail-job-description-list.component.html',
  styleUrls: ['./set-detail-job-description-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDetailJobDescriptionListComponent extends BaseList implements OnInit {
  rows:any = []
  jobDescription$ = new Observable()
  req = {
    job_description_code : '',
    job_description_name_th : ''
  }
  constructor(
    public router : Router,
    public JobDescriptionSV : JobDescriptionService,
    public appSV : AppService,
  ) { super()
    this.jobDescription$ = this.JobDescriptionSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  } 

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.jobDescription$  = this.JobDescriptionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      job_description_code : '',
      job_description_name_th : ''
    }
    let qyr = createQueryStringFromObject(this.req)
    this.jobDescription$  = this.JobDescriptionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }
  addPage(){
    this.router.navigate(['app/detail_job_description/add'])
  }
  editPage(id){
    this.router.navigate(['app/detail_job_description/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.JobDescriptionSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.jobDescription$ = this.JobDescriptionSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
