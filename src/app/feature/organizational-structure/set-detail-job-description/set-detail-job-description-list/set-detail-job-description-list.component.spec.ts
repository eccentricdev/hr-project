import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDetailJobDescriptionListComponent } from './set-detail-job-description-list.component';

describe('SetDetailJobDescriptionListComponent', () => {
  let component: SetDetailJobDescriptionListComponent;
  let fixture: ComponentFixture<SetDetailJobDescriptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDetailJobDescriptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDetailJobDescriptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
