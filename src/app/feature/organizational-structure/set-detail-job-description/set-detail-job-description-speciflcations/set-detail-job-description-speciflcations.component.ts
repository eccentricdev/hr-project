import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-set-detail-job-description-speciflcations',
  templateUrl: './set-detail-job-description-speciflcations.component.html',
  styleUrls: ['./set-detail-job-description-speciflcations.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDetailJobDescriptionSpeciflcationsComponent implements OnInit {


  constructor(    public dialogRef: MatDialogRef<SetDetailJobDescriptionSpeciflcationsComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,) { }

  ngOnInit(): void {
  }
  save(){
    this.close()
  }

  close(){
    this.dialogRef.close('close')
  }


}
