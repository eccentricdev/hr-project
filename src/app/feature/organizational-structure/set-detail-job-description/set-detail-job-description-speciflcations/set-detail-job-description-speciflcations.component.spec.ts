import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDetailJobDescriptionSpeciflcationsComponent } from './set-detail-job-description-speciflcations.component';

describe('SetDetailJobDescriptionSpeciflcationsComponent', () => {
  let component: SetDetailJobDescriptionSpeciflcationsComponent;
  let fixture: ComponentFixture<SetDetailJobDescriptionSpeciflcationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDetailJobDescriptionSpeciflcationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDetailJobDescriptionSpeciflcationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
