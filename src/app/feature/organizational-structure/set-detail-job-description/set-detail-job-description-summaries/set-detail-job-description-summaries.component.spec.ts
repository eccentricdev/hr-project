import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDetailJobDescriptionSummariesComponent } from './set-detail-job-description-summaries.component';

describe('SetDetailJobDescriptionSummariesComponent', () => {
  let component: SetDetailJobDescriptionSummariesComponent;
  let fixture: ComponentFixture<SetDetailJobDescriptionSummariesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDetailJobDescriptionSummariesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDetailJobDescriptionSummariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
