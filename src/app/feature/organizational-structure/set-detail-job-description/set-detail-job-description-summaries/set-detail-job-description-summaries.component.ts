import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-set-detail-job-description-summaries',
  templateUrl: './set-detail-job-description-summaries.component.html',
  styleUrls: ['./set-detail-job-description-summaries.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDetailJobDescriptionSummariesComponent implements OnInit {

  constructor(    public dialogRef: MatDialogRef<SetDetailJobDescriptionSummariesComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data,) { }

  ngOnInit(): void {
  }
  save(){
    this.close()
  }

  close(){
    this.dialogRef.close('close')
  }


}
