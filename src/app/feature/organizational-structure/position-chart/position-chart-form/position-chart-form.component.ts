import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable, throwError } from 'rxjs';
import { tap, filter, map, catchError, takeUntil, debounceTime, switchMap, delay } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';
import { Organization } from 'src/app/core/service/organization/organization.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { WorkChartHeaderService } from 'src/app/core/service/organization/work-chart-header.service';
import { AddNodeModalComponent } from '../../../../shared/components/add-node-modal/add-node-modal.component';

@Component({
  selector: 'app-position-chart-form',
  templateUrl: './position-chart-form.component.html',
  styleUrls: ['./position-chart-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionChartFormComponent extends BaseForm implements OnInit {
  rows :any = []
  nodes = []
  selectNode
  filterNode = []
  private unsubAll$ = new Subject<boolean>()
  agency$ = new Observable()
  agencys = []
  selectPostion 
  searchPosition$ = new Observable()
  position = []
  
  public get getAgencyForm(): AbstractControl {
    return this.form.get('root_position')
  }
  

  constructor(
    public dialog :MatDialog,
    public router: Router,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public organizationSV: WorkChartHeaderService,
    private agencySV: AgencysService,
    private WorkChartHeaderSV :WorkChartHeaderService,
    private positionSV: PositionService,
    private cdRef: ChangeDetectorRef
  ) {
    super(fb,activeRoute)
    this.positionSV.getAll().pipe(
      tap(res => this.position = [...res])
    ).subscribe()
    
    this.agencySV.getAll().pipe(
      tap((res: any) => this.agencys = [...res])
    ).subscribe()

    this.WorkChartHeaderSV.getAll().subscribe()
   }

  form: FormGroup;
  baseFormBuilder: FormBuilder;
  public state: string;
  protected id: number;

  debug: number = 0

    ngOnInit(): void {
      this.form.get('agency_uid').valueChanges.pipe(
        tap(id => {
          let agen = this.agencys.find(x => x.agency_uid == id)
          this.form.get('no_of_manpowers').setValue(agen?.no_of_manpowers)
        }),
        takeUntil(this.unsubAll$)        
      ).subscribe()



      
      this.getAgencyForm.valueChanges.pipe(
        tap(id => console.log(id)),
        tap(id => this.selectPostion = this.position.find(x => x.position_uid == id)),
        takeUntil(this.unsubAll$)
      ).subscribe()


      this.searchPosition$ = this.getAgencyForm.valueChanges.pipe(
        debounceTime(500),
        switchMap(text => this.positionSV.query(`?position_name_th=${text}`)),
        tap((res: any) => this.position = [...res]),        
      )
      
  
      switch (this.state) {
        case 'edit':
          this.organizationSV.get<any>(this.id).pipe(
            tap(res => this.setUpform(res)),
            tap((res: any) =>  this.searchPosition$ = this.positionSV.query(`?root_position=${res.root_position}`).pipe(
              tap((res: any) => this.position = [...res]),
            )),
            delay(100),
            map(res => {
              let node = {
                ...res.work_chart,
                code: res.position_code,
                name: res.work_chart.position_name_th,
                childs: [  ...res.work_chart.work_charts?.map(lv2 => {
                  return {
                    ...lv2,
                    code: lv2.position_code,
                    name: lv2.position_name_th || lv2.agency_name_th,
                    childs: this.setupNode(lv2)
                  }
                })]
              }
              return node
            }),
            tap(res => this.nodes = [ ...[res]]),
            tap(res => console.log(res))
          ).subscribe()
        break;
        case 'add':  
        break;
      }
    }
  
  
  
    setupNode(source: any){
      if(!source.work_charts) return []
      let node = source.work_charts.map(res => {
        return {
          ...res,
          code: res.position_code || res.agency_code,
          name: res.position_name_th || res.agency_name_th,
          childs: this.setupNode(res)
        }
      })
      return node
    }
  
    item(item){
      console.log(item)
      this.selectNode = item
      // console.log('select node event')
      // console.log(this.selectNode)
    }
  
  
    addNode(node){
  
      this.open().pipe(
        tap(({ name, agenId, agenName, close, positionId, positionName, noManofPowers, agencyCode, positionCode}) => {
          if(close) return 
            this.insertNodeIntoTree(
              this.nodes[0],
              node.name,
              {
                agency_code: agencyCode,
                no_of_manpowers: noManofPowers,
                work_chart_uid: null,
                parent_work_chart_uid: null,
                work_chart_type_id: null,
                level: this.selectNode.level + 1,
                work_chart_code: '',
                work_chart_name: '',
                name: name || agenName || positionName,
                code: positionCode,
                agency_uid: agenId,
                position_uid: positionId,
                work_chart_header_uid: null,
                work_charts: [],
                childs: []
              }
            )
          // }
        })
      ).subscribe()
    }
  
  
    deleteNode(node){
      // if(!this.selectNode) {
      //   return this.appSV.swaltAlertError('โปรดเลือกแผนผังที่ต้องการเพิ่ม')
      // }
    
      this.deleteNodeFromTree(this.nodes[0],node.name)
    }
  
  
    drop(event){
      let _event = {...event}
      console.log(event)
      let  {dropNode,targetNode}  = _event
      this.deleteNodeFromTree(this.nodes[0],dropNode.name)
      this.insertNodeIntoTree(this.nodes[0],targetNode.name,dropNode)
    }
  
    targetNode(event){
      // console.log('target node event')
      // console.log(event)
    } 
  
    
  
    insertNodeIntoTree(node, nodeId, newNode) {
      if (node.name == nodeId) {
          if (newNode) {
              // newNode.childs = [];
              if(!node.childs) {
                node.childs = []
                node.work_charts = []
              }
              
              node.childs.push(newNode);
              node.work_charts = [...node.childs]
          }
  
      } else if (node.childs != null) {
          for (let i = 0; i < node.childs.length; i++) {
              this.insertNodeIntoTree(node.childs[i], nodeId, newNode);
          }
  
      }
  
  }
  
    deleteNodeFromTree(node, name) {
      if (node.childs != null) {
          for (let i = 0; i < node.childs.length; i++) {
              let filtered = node.childs.filter(f => f.name == name);
              if (filtered && filtered.length > 0) {
                  node.childs = node.childs.filter(f => f.name != name);
                  node.work_charts = [ ...node.childs]
                  return;
              }
              this.deleteNodeFromTree(node.childs[i], name,);
          }
      }
  }
  
  createNode(){
    console.log(this.selectPostion)
    if(!this.getAgencyForm.value) return this.appSV.swaltAlertError('ไม่สามารถสร้างโครงสร้างองค์กรได้ โปรดเลือกหน่วยงานสูงสุด')
    this.nodes = [
      {
        no_of_manpowers: 0,
        agency_code: this.selectPostion.position_code,
        work_chart_uid: null,
        parent_work_chart_uid: null,
        work_chart_type_id: null,
        level: 1,
        work_chart_code: '',
        work_chart_name: '',
        name: this.selectPostion.position_name_th,
        code: this.selectPostion.position_code,
        agency_uid: null,
        position_uid: this.selectPostion.position_uid,
        work_chart_header_uid: null,
        work_charts: [],
        childs: []
      }
    ]
  }
  
    setUpform(res){
      // patch value 
      this.form.patchValue(res)
    }
  
  
    open(){
      const dialogRef = this.dialog.open(AddNodeModalComponent, {
        width: '500px',
        height: '300px',
        data: {agency: this.agencys, position: this.position}
      });
  
      return dialogRef.afterClosed()
    }
      close(){
        this.router.navigate(['app/position_chart'])
      }
      save() {
        
        switch (this.state) {
          case 'edit':
            let reqEdit = this.form.getRawValue()
            reqEdit.work_chart = {
              ...this.nodes[0],
              work_charts: [...this.nodes[0].childs]
            }
            console.log(reqEdit)
            this.organizationSV.update(reqEdit).pipe(
              catchError(err => {
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                this.appSV.swaltAlert()
                this.router.navigate(['app/position_chart'])
              })
            break;
          case 'add':
            let req = this.form.getRawValue()
            req.work_chart = {
              ...this.nodes[0],
              work_charts: [...this.nodes[0].childs]
            }
            console.log(req)
  
            this.organizationSV.add(req).pipe(
              catchError(err => {
                this.appSV.swaltAlertError('Error')
                return throwError(err)
              })).subscribe((x: any) => {
                console.log(x)
                this.appSV.swaltAlert()
                this.router.navigate(['app/position_chart'])
              })
            break;
        }
      }
  
      createForm() {
        return this.baseFormBuilder.group({
          status_id: [null],
          created_by: [null],
          created_datetime: [null],
          updated_by: [null],
          updated_datetime: [null],
          work_chart_header_uid: [null],
          work_chart_header_code: [''],
          work_chart_header_name_th: [''],
          work_chart_header_name_en: [''],
          root_position: [null],
          effective_date: [null],
          version_name: [''],
          agency_uid:[null],
          no_of_manpowers: [{value:0, disabled: true}],
          work_chart: this.baseFormBuilder.group({}) 
        })
      }


      displayRootposiion(positionUid){
        if(positionUid) return this.position.find(x => x.position_uid == positionUid)?.position_name_th
      }
  
  
      ngOnDestroy(): void {
        this.unsubAll$.next(true)
        this.unsubAll$.complete()
      }

}
