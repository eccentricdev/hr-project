import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionChartFormComponent } from './position-chart-form.component';

describe('PositionChartFormComponent', () => {
  let component: PositionChartFormComponent;
  let fixture: ComponentFixture<PositionChartFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionChartFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionChartFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
