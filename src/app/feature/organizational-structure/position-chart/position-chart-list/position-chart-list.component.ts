import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { concatMap, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';
import { Organization } from 'src/app/core/service/organization/organization.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { WorkChartHeaderService } from 'src/app/core/service/organization/work-chart-header.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-position-chart-list',
  templateUrl: './position-chart-list.component.html',
  styleUrls: ['./position-chart-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionChartListComponent extends BaseList implements OnInit {
  orgranization$ = new Observable()
  position$ = new Observable()
  rows:any = []

  req = {
    work_chart_header_code: null,
    work_chart_header_name_th: null
  }
  constructor(
    public router : Router,
    private WorkChartHeaderSV: WorkChartHeaderService,
    private poitionSV: PositionService,
    private appSW: AppService    
  ) {
    super()
    this.orgranization$ = this.WorkChartHeaderSV.getAll<Organization>().pipe(
      map(res => {
        return this.updateMatTable(res)
      })
    )
    this.position$ = this.poitionSV.getAll()
   } 

  ngOnInit(): void {
  }
  addPage(){
    this.router.navigate(['app/position_chart/add'])
  }
  editPage(id){
    this.router.navigate(['app/position_chart/edit',id])
  }

  search(){
    let queryString = createQueryStringFromObject(this.req)
    console.log(queryString)
    if(queryString) return this.orgranization$ = this.WorkChartHeaderSV.query(`?${queryString}`).pipe(
      tap(x=>console.log(x)),
      map(res => {
        return this.updateMatTable(res)
      })
    )
    if(!queryString) return this.orgranization$ = this.WorkChartHeaderSV.getAll().pipe(
      map(res => {
        return this.updateMatTable(res)
      })
    )
  }

  clear(){
    this.req.work_chart_header_code = null
    this.req.work_chart_header_name_th = null
    this.search()
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
          this.orgranization$  = this.WorkChartHeaderSV.deleteData(id).pipe(
            concatMap(() => this.orgranization$ = this.WorkChartHeaderSV.getAll<Organization>().pipe(
              map(res => {
                return this.updateMatTable(res)
              })
            )),
            tap(() => this.appSW.swaltAlert('ลบสำเร็จ'))
          )
        }
      })
    }
  }
}
