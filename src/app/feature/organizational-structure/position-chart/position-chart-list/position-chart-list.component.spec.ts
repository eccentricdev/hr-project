import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionChartListComponent } from './position-chart-list.component';

describe('PositionChartListComponent', () => {
  let component: PositionChartListComponent;
  let fixture: ComponentFixture<PositionChartListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionChartListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionChartListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
