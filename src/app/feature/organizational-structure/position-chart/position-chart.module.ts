import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionChartListComponent } from './position-chart-list/position-chart-list.component';
import { PositionChartFormComponent } from './position-chart-form/position-chart-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PositionChartListComponent
  },
  {
    path:'add',
    component:PositionChartFormComponent
  },
  {
    path:'edit/:id',
    component:PositionChartFormComponent
  },
  
]

@NgModule({
  declarations: [PositionChartListComponent, PositionChartFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PositionChartModule { }
