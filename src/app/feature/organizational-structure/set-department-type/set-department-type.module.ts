import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetDepartmentTypeListComponent } from './set-department-type-list/set-department-type-list.component';
import { SetDepartmentTypeFormComponent } from './set-department-type-form/set-department-type-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetDepartmentTypeListComponent
  },
  {
    path:'add',
    component:SetDepartmentTypeFormComponent
  },
  {
    path:'edit/:id',
    component:SetDepartmentTypeFormComponent
  },
  
]



@NgModule({
  declarations: [SetDepartmentTypeListComponent, SetDepartmentTypeFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetDepartmentTypeModule { }
