import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { AgencysTypeService } from 'src/app/core/service/organization/agencys-type.service';

@Component({
  selector: 'app-set-department-type-form',
  templateUrl: './set-department-type-form.component.html',
  styleUrls: ['./set-department-type-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDepartmentTypeFormComponent extends BaseForm implements OnInit {
  status$ = new Observable()
  constructor(public router : Router,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public AgencysTypeSV: AgencysTypeService,
    public StatusSV: StatusService,
    ) {  super(fb, activeRoute)
      this.status$ = this.StatusSV.getAll()
    }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.AgencysTypeSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      agency_type_id:res.agency_type_id,
      agency_type_code:res.agency_type_code,
      agency_type_name_th:res.agency_type_name_th,
      agency_type_name_en:res.agency_type_name_en,
      agency_type_color_code:res.agency_type_color_code,
    })
  }
  close(){
    this.router.navigate(['app/department_type'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.AgencysTypeSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/department_type'])
          })
        break;
      case 'add':
      
        this.AgencysTypeSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/department_type'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [1],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      agency_type_id: [null],
      agency_type_code: [''],
      agency_type_name_th: [''],
      agency_type_name_en: [''],
      agency_type_color_code: [''],
    })
  }
}
