import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDepartmentTypeFormComponent } from './set-department-type-form.component';

describe('SetDepartmentTypeFormComponent', () => {
  let component: SetDepartmentTypeFormComponent;
  let fixture: ComponentFixture<SetDepartmentTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDepartmentTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDepartmentTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
