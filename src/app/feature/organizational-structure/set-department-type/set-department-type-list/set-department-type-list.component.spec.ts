import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDepartmentTypeListComponent } from './set-department-type-list.component';

describe('SetDepartmentTypeListComponent', () => {
  let component: SetDepartmentTypeListComponent;
  let fixture: ComponentFixture<SetDepartmentTypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDepartmentTypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDepartmentTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
