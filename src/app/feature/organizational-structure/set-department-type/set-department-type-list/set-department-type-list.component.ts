import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { StatusService } from 'src/app/core/service/common/status.service';
import { AgencysTypeService } from 'src/app/core/service/organization/agencys-type.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-department-type-list',
  templateUrl: './set-department-type-list.component.html',
  styleUrls: ['./set-department-type-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDepartmentTypeListComponent extends BaseList implements OnInit {
  rows:any = []
  agencysType$ = new Observable()
  status$ = new Observable()
  req = {
    agency_type_code : '',
    agency_type_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public AgencysTypeSV : AgencysTypeService,
    public StatusSV : StatusService,
  ) { super()
    this.agencysType$ = this.AgencysTypeSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
    this.status$ = this.StatusSV.getAll()
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.agencysType$  = this.AgencysTypeSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      agency_type_code : '',
      agency_type_name_th : ''
    }
    let qyr = createQueryStringFromObject(this.req)
    this.agencysType$  = this.AgencysTypeSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }
  addPage(){
    this.router.navigate(['app/department_type/add'])
  }
  editPage(id){
    this.router.navigate(['app/department_type/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.AgencysTypeSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.agencysType$ = this.AgencysTypeSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
