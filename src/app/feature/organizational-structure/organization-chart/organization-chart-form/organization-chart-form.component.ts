import { Organization, OrganizationService } from 'src/app/core/service/organization/organization.service';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { AbstractControl, Form, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { catchError, debounce, debounceTime, delay, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { Observable, of, Subject, throwError } from 'rxjs';
import { AddNodeModalComponent } from '../../../../shared/components/add-node-modal/add-node-modal.component';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';

@Component({
  selector: 'app-organization-chart-form',
  templateUrl: './organization-chart-form.component.html',
  styleUrls: ['./organization-chart-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationChartFormComponent extends BaseForm implements OnInit,OnDestroy {
  rows :any = []
  nodes = []
  selectNode
  filterNode = []
  private unsubAll$ = new Subject<boolean>()
  agency$ = new Observable()
  agencys = []
  agency

  displayView: boolean = false

  searchAgency$ = new Observable()
  
  public get getAgencyForm(): AbstractControl {
    return this.form.get('root_agency')
  }
  

  constructor(
    public dialog :MatDialog,
    public router: Router,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public organizationSV: OrganizationHeaderService,
    private agencySV: AgencysService,
    private cdRef: ChangeDetectorRef
  ) {
    super(fb,activeRoute)
    // this.agencySV.getAll().subscribe(x => this.agencys = [...x])

    // this.agency$ = this.agencySV.getAll().pipe(
    //   tap((res: any) => this.agencys = [...res]),
    // )
   }

  form: FormGroup;
  baseFormBuilder: FormBuilder;
  public state: string;
  protected id: number;

  debug: number = 0

  ngOnInit(): void {
    this.getAgencyForm.valueChanges.pipe(
      tap(id => this.agency = this.agencys.find(x => x.agency_uid == id)),
      takeUntil(this.unsubAll$),
    ).subscribe()

    this.searchAgency$ = this.getAgencyForm.valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.agencySV.query(`?agency_name_th=${text}`)),
      tap((res: any) => this.agencys = [...res]),
      takeUntil(this.unsubAll$),
      catchError(err => {
        console.error(err.error.message)
        return of([])
      })
    )
    

    switch (this.state) {
      case 'edit':
        this.organizationSV.get<any>(this.id).pipe(
          tap(res => this.searchAgency$ = this.agencySV.query(`?agency_uid=${res.root_agency}`).pipe(
            tap((res: any) => this.agencys = [...res]),
            tap((res: any) => this.cdRef.detectChanges())
          )),
          delay(200),
          tap(res => this.setUpform(res)),          
          map(res => {
            let node = {
              ...res.organization_chart,
              code: res.agency_code,
              name: res.organization_chart.agency_name_th,
              childs: [  ...res.organization_chart.organization_charts?.map(lv2 => {
                return {
                  ...lv2,
                  code: lv2.agency_code,
                  name: lv2.agency_name_th || lv2.organization_chart_name,
                  childs: this.setupNode(lv2)
                }
              })]
            }
            return node
          }),
          tap(res => this.nodes = [ ...[res]]),
          tap(() => this.displayView = true)
        ).subscribe()
      break;
      case 'add':
        this.displayView = true

      break;
    }
  }



  setupNode(source: any){
    if(!source.organization_charts) return []
    let node = source.organization_charts.map(res => {
      return {
        ...res,
        code: res.agency_code,
        name: res.agency_name_th || res.organization_chart_name,
        childs: this.setupNode(res)
      }
    })
    return node
  }

  item(item){
    // console.log(item)
    this.selectNode = item
  }


  addNode(node){

    this.open().pipe(
      tap(({ name, agenId, agenName, close, noManofPowers , agencyCode }) => {
        if(close) return 
          this.insertNodeIntoTree(
            this.nodes[0],
            node.name,
            {
              no_of_manpowers: noManofPowers,
              agency_code: agencyCode,
              organization_chart_uid: null,
              parent_organization_chart_uid: null,
              organization_chart_type_id: null,
              level: this.selectNode.level + 1,
              organization_chart_code: '',
              organization_chart_name: !agenId  ? name : '',
              agency_uid: agenId,
              organization_chart_header_uid: null,
              name: agenId ? agenName : name,
              code: agencyCode,
              organization_charts: [],
              childs: []
            }
          )
          console.log(this.nodes)
          
        // }
      })
    ).subscribe()
  }


  deleteNode(node){
    
    this.deleteNodeFromTree(this.nodes[0],node.name)
  }


  drop(event){
    let _event = {...event}
    let  {dropNode,targetNode}  = _event
    this.deleteNodeFromTree(this.nodes[0],dropNode.name)
    this.insertNodeIntoTree(this.nodes[0],targetNode.name,dropNode)
  }

  targetNode(event){
    // console.log('target node event')
    // console.log(event)
  } 

  

  insertNodeIntoTree(node, nodeId, newNode) {
    if (node.name == nodeId) {
        if (newNode) {
            // newNode.childs = [];
            if(!node.childs) {
              node.childs = []
              
            }

            if(!node.organization_charts) {
              node.organization_charts = []
            }
            
            node.childs.push(newNode);
            node.organization_charts.push(newNode)
            
        }

    } else if (node.childs != null) {
        for (let i = 0; i < node.childs.length; i++) {
            this.insertNodeIntoTree(node.childs[i], nodeId, newNode);
        }

    }

}

  deleteNodeFromTree(node, name) {
    if (node.childs != null) {
        for (let i = 0; i < node.childs.length; i++) {
            let filtered = node.childs.filter(f => f.name == name);
            if (filtered && filtered.length > 0) {
                node.childs = node.childs.filter(f => f.name != name);
                // node.organization_charts = [ ...node.childs]
                node.organization_charts = node.childs
                return;
            }
            this.deleteNodeFromTree(node.childs[i], name);
        }
    }
}

creatNode(){
  if(!this.getAgencyForm.value) return this.appSV.swaltAlertError('ไม่สามารถสร้างโครงสร้างองค์กรได้ โปรดเลือกหน่วยงานสูงสุด')
  this.nodes = [
    {
      no_of_manpowers: this.agency.no_of_manpowers,
      agency_code: this.agency.agency_code,
      organization_chart_uid: null,
      parent_organization_chart_uid: null,
      organization_chart_type_id: null,
      level: 1,
      organization_chart_code: null,
      organization_chart_name: null,
      agency_uid: this.agency.agency_uid,
      organization_chart_header_uid: null,
      organization_charts: [],
      name: this.agency.agency_name_th,
      code: this.agency.agency_code,
      childs: []
    }
  ]
}

  setUpform(res){
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      organization_chart_header_uid:res.organization_chart_header_uid,
      organization_chart_header_code:res.organization_chart_header_code,
      organization_chart_header_name_th:res.organization_chart_header_name_th,
      organization_chart_header_name_en:res.organization_chart_header_name_en,
      root_agency:res.root_agency,
      effective_date:res.effective_date,
      version_name:res.version_name,
    })
    // if (res.organization_chart) {
    //   res.organization_chart.organization_charts.forEach((employee_contacts) => {
    //     let itemsArray = this.form.get('employee_contacts') as FormArray
    //     itemsArray.push(this.EmployeeSV.employee_contacts_form(employee_contacts))
    //   })
    // }
  }


  open(){
    const dialogRef = this.dialog.open(AddNodeModalComponent, {
      width: '500px',
      height: '300px',
      data: {agency: this.agencys}
    });

    return dialogRef.afterClosed()
  }
    close(){
      this.router.navigate(['app/organization_chart'])
    }
    save() {
      
      switch (this.state) {
        case 'edit':
          let reqEdit = this.form.getRawValue()
          console.log(this.nodes)
          
          reqEdit.organization_chart = {
            ...this.nodes[0],
            organization_charts: [...this.nodes[0].childs]
          }
          console.log(reqEdit)
          
          this.organizationSV.update(reqEdit).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/organization_chart'])
            })
          break;
        case 'add':
          let req = this.form.getRawValue()
          req.organization_chart = {
            ...this.nodes[0],
            organization_charts: [...this.nodes[0].childs]
          }
          console.log(req)

          this.organizationSV.add(req).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/organization_chart'])
            })
          break;
      }
    }

    createForm() {
      return this.baseFormBuilder.group({
        status_id: [null],
        created_by: [null],
        created_datetime: [null],
        updated_by: [null],
        updated_datetime: [null],
        organization_chart_header_uid: [null],
        root_agency: [null],
        effective_date: [null],
        version_name: [null],
        organization_chart_header_code: [null],
        organization_chart_header_name_th: [null],
        organization_chart_header_name_en: [null],
        organization_chart: this.baseFormBuilder.group({})

        // root_agency:[null],
        // effective_date: [null],
        // version_name:[''],
        // organization_chart_uid: [null],
        // parent_organization_chart_uid: [null],
        // organization_chart_type_id: [null],
        // level: [1],
        // organization_chart_code: [''],
        // organization_chart_name: [''],
        // version_uid: [null],
        // organization_charts: this.baseFormBuilder.array([]),
      })
    }


    displayAgency(agencyId){
      if(agencyId) return this.agencys.find(agency => agency.agency_uid == agencyId)?.agency_name_th
    }


    ngOnDestroy(): void {
      this.unsubAll$.next(true)
      this.unsubAll$.complete()
    }
}
