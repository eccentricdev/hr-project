import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationChartFormComponent } from './organization-chart-form.component';

describe('OrganizationChartFormComponent', () => {
  let component: OrganizationChartFormComponent;
  let fixture: ComponentFixture<OrganizationChartFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationChartFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationChartFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
