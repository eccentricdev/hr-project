import { BaseList } from './../../../../core/base/base-list';
import { Observable, throwError } from 'rxjs';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Organization, OrganizationService } from 'src/app/core/service/organization/organization.service';
import swal from 'sweetalert2'
import { catchError, map } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';
import { OrganizationHeaderService } from 'src/app/core/service/organization/organization-header.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-organization-chart-list',
  templateUrl: './organization-chart-list.component.html',
  styleUrls: ['./organization-chart-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrganizationChartListComponent extends BaseList implements OnInit {
  rows:any = []
  orgranization$ = new Observable<Organization[]>()
  // @ViewChild(MatSort,{ static: true }) sort: MatSort;
  // @ViewChild(MatPaginator,{ static: true }) paginator: MatPaginator;

  req = {
    organization_chart_header_code: null,
    organization_chart_header_name_th: null,
  }
  constructor(
    
    public router : Router,
    public appSV : AppService,
    private orgnizationSV: OrganizationHeaderService
  ) { 
    super()
    this.orgranization$ = this.orgnizationSV.getAll<Organization>().pipe(
      map(x=> {
        return this.updateMatTable(x)
      })
    )
  } 

  ngOnInit(): void {

  }
  addPage(){
    this.router.navigate(['app/organization_chart/add'])
  }
  editPage(id){
    this.router.navigate(['app/organization_chart/edit',id])
  }

  search(){
    let qyr = createQueryStringFromObject(this.req)
    if(qyr) return this.orgranization$ = this.orgnizationSV.query(`?${qyr}`).pipe(
      map((res: any) => this.updateMatTable(res)),
    )
    if(!qyr) return this.orgranization$ = this.orgnizationSV.getAll<Organization>().pipe(
      map((res: any) => this.updateMatTable(res)),
    )
    
  }

  clear(){
    this.req.organization_chart_header_name_th = null,
    this.req.organization_chart_header_code = null,
    this.search()
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.orgnizationSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.orgranization$ = this.orgnizationSV.getAll<Organization>().pipe(
                map(x=> {
                  return this.updateMatTable(x)
                })
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
