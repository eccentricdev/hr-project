import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationChartListComponent } from './organization-chart-list.component';

describe('OrganizationChartListComponent', () => {
  let component: OrganizationChartListComponent;
  let fixture: ComponentFixture<OrganizationChartListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationChartListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationChartListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
