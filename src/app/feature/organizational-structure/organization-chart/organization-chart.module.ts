import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationChartListComponent } from './organization-chart-list/organization-chart-list.component';
import { OrganizationChartFormComponent } from './organization-chart-form/organization-chart-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { NodeComponent } from '../../../shared/components/node/node.component';
import { AddNodeModalComponent } from '../../../shared/components/add-node-modal/add-node-modal.component';

const routes:Routes = [
  {
    path:'',
    component:OrganizationChartListComponent
  },
  {
    path:'add',
    component:OrganizationChartFormComponent
  },
  {
    path:'edit/:id',
    component:OrganizationChartFormComponent
  },
  
]

@NgModule({
  declarations: [
    OrganizationChartListComponent, 
    OrganizationChartFormComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class OrganizationChartModule { }
