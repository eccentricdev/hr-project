import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { AgencysTypeService } from 'src/app/core/service/organization/agencys-type.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-department-data-list',
  templateUrl: './set-department-data-list.component.html',
  styleUrls: ['./set-department-data-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDepartmentDataListComponent extends BaseList implements OnInit {
  rows:any = []
  // agencysType:any = []
  agencys$ = new Observable()
  agencysType$ = new Observable()
  req = {
    agency_code : '',
    agency_name_th : '',
    agency_type_id : null
  }
  constructor(
    public router : Router,
    public AgencysSV : AgencysService,
    public AgencysTypeSV : AgencysTypeService,
    public appSV : AppService,
  ) {super()
    this.agencys$ = this.AgencysSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
    this.agencysType$ = this.AgencysTypeSV.getAll()
   } 

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.agencys$  = this.AgencysSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      agency_code : '',
      agency_name_th : '',
      agency_type_id : null
    }
    let qyr = createQueryStringFromObject(this.req)
    this.agencys$  = this.AgencysSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
    
  }
  addPage(){
    this.router.navigate(['app/department_data/add'])
  }
  editPage(id){
    this.router.navigate(['app/department_data/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.AgencysSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.agencys$ = this.AgencysSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })

        }
      })
    }
  }
}
