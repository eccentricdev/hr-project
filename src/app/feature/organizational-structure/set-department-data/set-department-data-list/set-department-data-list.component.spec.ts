import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDepartmentDataListComponent } from './set-department-data-list.component';

describe('SetDepartmentDataListComponent', () => {
  let component: SetDepartmentDataListComponent;
  let fixture: ComponentFixture<SetDepartmentDataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDepartmentDataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDepartmentDataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
