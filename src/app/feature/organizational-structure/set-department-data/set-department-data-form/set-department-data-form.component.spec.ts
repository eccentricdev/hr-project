import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDepartmentDataFormComponent } from './set-department-data-form.component';

describe('SetDepartmentDataFormComponent', () => {
  let component: SetDepartmentDataFormComponent;
  let fixture: ComponentFixture<SetDepartmentDataFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDepartmentDataFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDepartmentDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
