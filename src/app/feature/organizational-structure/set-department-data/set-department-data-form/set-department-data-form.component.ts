import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { AgencysTypeService } from 'src/app/core/service/organization/agencys-type.service';
import { AgencysService } from 'src/app/core/service/organization/agencys.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-set-department-data-form',
  templateUrl: './set-department-data-form.component.html',
  styleUrls: ['./set-department-data-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDepartmentDataFormComponent extends BaseForm implements OnInit {
  rows: any = []
  agencysType$ = new Observable()
  constructor(public dialog: MatDialog,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public AgencysSV: AgencysService,
    public AgencysTypeSV: AgencysTypeService,
    public router: Router) {
    super(fb, activeRoute)
    this.agencysType$ = this.AgencysTypeSV.getAll()
  }

  ngOnInit(): void {
    switch (this.state) {
      case 'edit':
        this.AgencysSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
        break;
      case 'add':

        break;
    }
  }

  setUpform(res) {
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      agency_uid:res.agency_uid,
      organization_uid:res.organization_uid,
      agency_type_id:res.agency_type_id,
      agency_group_id:res.agency_group_id,
      agency_code:res.agency_code,
      agency_name_en:res.agency_name_en,
      agency_name_th:res.agency_name_th,
      agency_short_name_en:res.agency_short_name_en,
      agency_short_name_th:res.agency_short_name_th,
      building_name:res.building_name,
      room_no:res.room_no,
      floor_no:res.floor_no,
      phone_no:res.phone_no,
      fax_no:res.fax_no,
      effective_date:res.effective_date,
      end_date:res.end_date,
      remark:res.remark
    })
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
    // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
  }
  openUpload() {
    const dialogRef = this.dialog.open(
      UploadFileDialogComponent, {
      width: '40%',
      disableClose: true,
      data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)

    })

  }
  close() {
    this.router.navigate(['app/department_data'])
  }
  save() {
    switch (this.state) {
      case 'edit':
    
        this.AgencysSV.update(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/department_data'])
          })
        break;
      case 'add':
      
        this.AgencysSV.add(this.form.getRawValue()).pipe(
          catchError(err => {
            this.appSV.swaltAlertError('Error')
            return throwError(err)
          })).subscribe((x: any) => {
            console.log(x)
            this.appSV.swaltAlert()
            this.router.navigate(['app/department_data'])
          })
        break;
    }

  }
  createForm() {
    return this.baseFormBuilder.group({
      status_id: [null],
      created_by: [''],
      created_datetime: [null],
      updated_by: [''],
      updated_datetime: [null],
      agency_uid: [null],
      organization_uid: [null],
      agency_type_id: [null],
      agency_group_id: [null],
      agency_code: [''],
      agency_name_en: [''],
      agency_name_th: [''],
      agency_short_name_en: [''],
      agency_short_name_th: [''],
      building_name: [''],
      room_no: [''],
      floor_no: [''],
      phone_no: [''],
      fax_no: [''],
      effective_date: [null],
      end_date: [null],
      remark: ['']
    })
  }
}
