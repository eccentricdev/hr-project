import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetDepartmentDataListComponent } from './set-department-data-list/set-department-data-list.component';
import { SetDepartmentDataFormComponent } from './set-department-data-form/set-department-data-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetDepartmentDataListComponent
  },
  {
    path:'add',
    component:SetDepartmentDataFormComponent
  },
  {
    path:'edit/:id',
    component:SetDepartmentDataFormComponent
  },
  
]

@NgModule({
  declarations: [SetDepartmentDataListComponent, SetDepartmentDataFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetDepartmentDataModule { }
