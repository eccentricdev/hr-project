import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import { Organization } from 'src/app/core/service/organization/organization.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-organization-list',
  templateUrl: './set-organization-list.component.html',
  styleUrls: ['./set-organization-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetOrganizationListComponent extends BaseList implements OnInit {
  rows:any = []
  orgranization$ = new Observable<Organization[]>()
  req = {
    organization_code : '',
    organization_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public OrganizationDataSV : OrganizationDataService,
  ) { super()
    this.orgranization$ = this.OrganizationDataSV.getAll<Organization>().pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.orgranization$  = this.OrganizationDataSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      organization_code : '',
      organization_name_th : ''
    }
    let qyr = createQueryStringFromObject(this.req)
    this.orgranization$  = this.OrganizationDataSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }
  addPage(){
    this.router.navigate(['app/organization_data/add'])
  }
  editPage(id){
    this.router.navigate(['app/organization_data/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.OrganizationDataSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
              this.orgranization$ = this.OrganizationDataSV.getAll<Organization>().pipe(
                map(x=> this.updateMatTable(x))
              )
            })

        }
      })
    }
  }
}
