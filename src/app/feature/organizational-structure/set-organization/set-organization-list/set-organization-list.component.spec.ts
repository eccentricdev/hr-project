import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetOrganizationListComponent } from './set-organization-list.component';

describe('SetOrganizationListComponent', () => {
  let component: SetOrganizationListComponent;
  let fixture: ComponentFixture<SetOrganizationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetOrganizationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetOrganizationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
