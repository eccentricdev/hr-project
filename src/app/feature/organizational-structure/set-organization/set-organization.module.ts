import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetOrganizationListComponent } from './set-organization-list/set-organization-list.component';
import { SetOrganizationFormComponent } from './set-organization-form/set-organization-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetOrganizationListComponent
  },
  {
    path:'add',
    component:SetOrganizationFormComponent
  },
  {
    path:'edit/:id',
    component:SetOrganizationFormComponent
  },
  
]

@NgModule({
  declarations: [SetOrganizationListComponent, SetOrganizationFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetOrganizationModule { }
