import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetOrganizationFormComponent } from './set-organization-form.component';

describe('SetOrganizationFormComponent', () => {
  let component: SetOrganizationFormComponent;
  let fixture: ComponentFixture<SetOrganizationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetOrganizationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetOrganizationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
