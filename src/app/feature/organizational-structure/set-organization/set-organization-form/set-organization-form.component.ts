import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, throwError } from 'rxjs';
import { catchError, debounceTime, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { SubDistrictService } from 'src/app/core/service/common/sub-district.service';
import { OrganizationDataService } from 'src/app/core/service/organization/organization-data.service';
import { UploadFileDialogComponent } from 'src/app/shared/components/upload-file-dialog/upload-file-dialog.component';

@Component({
  selector: 'app-set-organization-form',
  templateUrl: './set-organization-form.component.html',
  styleUrls: ['./set-organization-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetOrganizationFormComponent extends BaseForm implements OnInit {
  rows :any = []
  subDistricts = []
  unSubAll$ = new Subject<void>();
  constructor(public dialog :MatDialog,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public SubDistrictSV: SubDistrictService,
    public appSV: AppService,
    private cdRef: ChangeDetectorRef,
    public OrganizationDataSV: OrganizationDataService,
    public router:Router) {
      super(fb,activeRoute)
     }

  ngOnInit(): void {

    this.form.get('sub_district_name_th').valueChanges.pipe(
      debounceTime(500),
      switchMap(text => this.SubDistrictSV.query(`?search=${text}`)),
      tap((subDistricts: any) =>console.log(subDistricts)),
      tap((subDistricts: any) => this.subDistricts = [...subDistricts]),
      tap((subDistricts: any) => this.cdRef.detectChanges()),
      takeUntil(this.unSubAll$)
    ).subscribe()


    switch (this.state) {
      case 'edit':
        this.OrganizationDataSV.get(this.id).pipe(
          tap(res => this.setUpform(res)),
          tap(x => console.log(x)),
        ).subscribe()
      break;
      case 'add':
       
      break;
    }
  }

  setUpform(res){
    // patch value 
    this.form.patchValue({
      status_id:res.status_id,
      created_by:res.created_by,
      created_datetime:res.created_datetime,
      updated_by:res.updated_by,
      updated_datetime:res.updated_datetime,
      organization_uid:res.organization_uid,
      organization_code:res.organization_code,
      organization_name_en:res.organization_name_en,
      organization_name_th:res.organization_name_th,
      organization_short_name_en:res.organization_short_name_en,
      organization_short_name_th:res.organization_short_name_th,
      branch_name:res.branch_name,
      establish_date:res.establish_date,
      address:res.address,
      address_no:res.address_no,
      moo:res.moo,
      building_name:res.building_name,
      room_no:res.room_no,
      floor_no:res.floor_no,
      village_name:res.village_name,
      soi_name:res.soi_name,
      road_name:res.road_name,
      sub_district_id:res.sub_district_id,
      postal_code:res.postal_code,
      phone_no:res.phone_no,
      fax_no:res.fax_no
    })
  }

  selectSubDistrict(data){
    console.log(data)
    this.form.get('sub_district_id').setValue(data.sub_district_id)
    this.form.get('district_name_th').setValue(data.district_name_th)
    this.form.get('province_name_th').setValue(data.province_name_th)
  }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    openUpload() {
      const dialogRef = this.dialog.open(
        UploadFileDialogComponent, {
        width: '40%',
        disableClose: true,
        data: ''  // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
      }
      )
  
      dialogRef.afterClosed().subscribe(callback => {
        console.log(callback)
       
      })
  
    }
    close(){
      this.router.navigate(['app/organization_data'])
    }
    save() {
       
      switch (this.state) {
        case 'edit':
      
          this.OrganizationDataSV.update(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/organization_data'])
            })
          break;
        case 'add':
        
          this.OrganizationDataSV.add(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/organization_data'])
            })
          break;
      }
  
    }

    createForm() {
      return this.baseFormBuilder.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        organization_uid: [null],
        organization_code: [''],
        organization_name_en: [''],
        organization_name_th: [''],
        organization_short_name_en: [''],
        organization_short_name_th: [''],
        branch_name: [''],
        establish_date: [null],
        address: [''],
        address_no: [''],
        moo: [''],
        building_name: [''],
        room_no: [''],
        floor_no: [''],
        village_name: [''],
        soi_name: [''],
        road_name: [''],
        sub_district_id: [null],
        sub_district_name_th: [''],
        district_name_th: [''],
        province_name_th: [''],
        postal_code: [''],
        phone_no: [''],
        fax_no: [''] 
      })
    }

    ngOnDestroy() {
      this.unSubAll$.next();
      this.unSubAll$.complete();
    }
}
