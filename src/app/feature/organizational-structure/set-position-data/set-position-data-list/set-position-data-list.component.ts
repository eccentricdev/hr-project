import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseList } from 'src/app/core/base/base-list';
import { AppService } from 'src/app/core/service/app.service';
import { PositionService } from 'src/app/core/service/organization/position.service';
import { createQueryStringFromObject } from 'src/app/shared/util/func';
import swal from 'sweetalert2'

@Component({
  selector: 'app-set-position-data-list',
  templateUrl: './set-position-data-list.component.html',
  styleUrls: ['./set-position-data-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetPositionDataListComponent extends BaseList implements OnInit {
  rows:any = []
  position$ = new Observable()
  req = {
    position_code : '',
    position_name_th : ''
  }
  constructor(
    public router : Router,
    public appSV : AppService,
    public PositionSV : PositionService,
  ) { super()
    this.position$ = this.PositionSV.getAll().pipe(
      map(x=> this.updateMatTable(x))
    )
  } 

  ngOnInit(): void {
  }
  search(){
    let qyr = createQueryStringFromObject(this.req)
    this.position$  = this.PositionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }

  clear(){
    this.req = {
      position_code : '',
      position_name_th : ''
    }
    let qyr = createQueryStringFromObject(this.req)
    this.position$  = this.PositionSV.query(`?${qyr}`).pipe(
      map(x=> this.updateMatTable(x))
    )
  }
  addPage(){
    this.router.navigate(['app/position_data/add'])
  }
  editPage(id){
    this.router.navigate(['app/position_data/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        
          this.PositionSV.deleteData(id).pipe(
            catchError(err => {
              // alert ตรงนี่
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.position$ = this.PositionSV.getAll().pipe(
                map(x=> this.updateMatTable(x))
              )
              this.appSV.swaltAlert('ลบข้อมูลสำเสร็จ')
            })


        }
      })
    }
  }
}
