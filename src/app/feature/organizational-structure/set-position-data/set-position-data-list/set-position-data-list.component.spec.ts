import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPositionDataListComponent } from './set-position-data-list.component';

describe('SetPositionDataListComponent', () => {
  let component: SetPositionDataListComponent;
  let fixture: ComponentFixture<SetPositionDataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetPositionDataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPositionDataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
