import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetPositionDataListComponent } from './set-position-data-list/set-position-data-list.component';
import { SetPositionDataFormComponent } from './set-position-data-form/set-position-data-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetPositionDataListComponent
  },
  {
    path:'add',
    component:SetPositionDataFormComponent
  },
  {
    path:'edit/:id',
    component:SetPositionDataFormComponent
  },
  
]

@NgModule({
  declarations: [SetPositionDataListComponent, SetPositionDataFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetPositionDataModule { }
