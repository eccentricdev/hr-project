import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { JobDescriptionService } from 'src/app/core/service/organization/job-description.service';
import { PositionService } from 'src/app/core/service/organization/position.service';

@Component({
  selector: 'app-set-position-data-form',
  templateUrl: './set-position-data-form.component.html',
  styleUrls: ['./set-position-data-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetPositionDataFormComponent extends BaseForm implements OnInit {
  rows :any = []
  jobDescription$ = new Observable()
  constructor(
    public router:Router,
    public fb: FormBuilder,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public PositionSV: PositionService,
    public JobDescriptionSV: JobDescriptionService,
    ) { 
      super(fb, activeRoute)
      this.jobDescription$ = this.JobDescriptionSV.getAll()
    }

    ngOnInit(): void {
      switch (this.state) {
        case 'edit':
          this.PositionSV.get(this.id).pipe(
            tap(res => this.setUpform(res)),
            tap(x => console.log(x)),
          ).subscribe()
          break;
        case 'add':
  
          break;
      }
    }
  
    setUpform(res) {
      // patch value 
      this.form.patchValue({
        status_id:res.status_id,
        created_by:res.created_by,
        created_datetime:res.created_datetime,
        updated_by:res.updated_by,
        updated_datetime:res.updated_datetime,
        position_uid:res.position_uid,
        position_code:res.position_code,
        position_name_en:res.position_name_en,
        position_name_th:res.position_name_th,
        position_short_name_en:res.position_short_name_en,
        position_short_name_th:res.position_short_name_th,
        remark:res.remark,
        effective_date:res.effective_date,
        end_date:res.end_date,
        work_detail:res.work_detail,
      })
    }
  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
    close(){
      this.router.navigate(['app/position_data'])
    }
    save() {
      switch (this.state) {
        case 'edit':
      
          this.PositionSV.update(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/position_data'])
            })
          break;
        case 'add':
        
          this.PositionSV.add(this.form.getRawValue()).pipe(
            catchError(err => {
              this.appSV.swaltAlertError('Error')
              return throwError(err)
            })).subscribe((x: any) => {
              console.log(x)
              this.appSV.swaltAlert()
              this.router.navigate(['app/position_data'])
            })
          break;
      }
  
    }
    createForm() {
      return this.baseFormBuilder.group({
        status_id: [null],
        created_by: [''],
        created_datetime: [null],
        updated_by: [''],
        updated_datetime: [null],
        position_uid: [null],
        position_code: [''],
        position_name_en: [],
        position_name_th: [''],
        position_short_name_en: [''],
        position_short_name_th: [''],
        work_detail: [''],
        remark: [''],
        effective_date: [null],
        end_date: [null],
      })
    }
}
