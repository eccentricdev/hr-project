import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPositionDataFormComponent } from './set-position-data-form.component';

describe('SetPositionDataFormComponent', () => {
  let component: SetPositionDataFormComponent;
  let fixture: ComponentFixture<SetPositionDataFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetPositionDataFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPositionDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
