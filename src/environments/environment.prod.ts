export const environment = {
  production: true,
  hideForm: true,
  rsuArApiName:'https://hr-rmutr-demo.71dev.com/api',
  rsuCommonApiName:'https://rsu-common-api.71dev.com'
};
