export const environment = {
    production: true,
    hideForm: true,
    rsuArApiName:'https://uat-app.rsu.ac.th/hr-api',
    rsuCommonApiName:'https://rsu-common-api.71dev.com'
  };
  